from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.utils import timezone

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, ParseError
from rest_framework.relations import HyperlinkedIdentityField
from rest_framework.reverse import reverse
from rest_framework.fields import CurrentUserDefault

from engine.models import GameType, GameActionType, Game, Character, NonPlayableCharacterAttack, NonPlayableCharacter, Level, Tile, LevelTile, LevelType, LevelActionType, LevelNPCharacter, Background, RoomActionType, RoomType, Room, Weapon, Item, Stage, GameAction, LevelAction, RoomAction, PageType, PageActionType, Page, PageAction, Cutscene, CutsceneType, CutsceneAction, CutsceneActionType, Conversation, Dialogue, DialogueResponse
from players.models import PlayerLevel, PlayerRoom, PlayerCharacter, PlayerGame, PlayerStage, PlayerPage, PlayerCutscene, PlayerConversation
from profiles.models import Profile


class ParameterisedHyperlinkedIdentityField(HyperlinkedIdentityField):
    """
    Represents the instance, or a property on the instance, using hyperlinking.

    lookup_fields is a tuple of tuples of the form:
        ('model_field', 'url_parameter')
    """
    lookup_fields = (('pk', 'pk'),)

    def __init__(self, *args, **kwargs):
        self.lookup_fields = kwargs.pop('lookup_fields', self.lookup_fields)
        super(ParameterisedHyperlinkedIdentityField,
              self).__init__(*args, **kwargs)

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object.

        May raise a `NoReverseMatch` if the `view_name` and `lookup_field`
        attributes are not configured to correctly match the URL conf.
        """
        kwargs = {}
        for model_field, url_param in self.lookup_fields:
            attr = obj
            for field in model_field.split('.'):
                attr = getattr(attr, field)
            kwargs[url_param] = attr

        return reverse(view_name, kwargs=kwargs, request=request, format=format)


class MultipleFieldLookupMixin(object):
    """
    Apply this mixin to any view or viewset to get multiple field filtering
    based on a `lookup_fields` attribute, instead of the default single field filtering.
    """

    def get_object(self):
        queryset = self.get_queryset()             # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        filter = {}
        for field in self.lookup_fields:
            if self.kwargs[field]:  # Ignore empty fields.
                filter[field] = self.kwargs[field]
        obj = get_object_or_404(queryset, **filter)  # Lookup the object
        self.check_object_permissions(self.request, obj)
        return obj


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data + "==")
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            # 12 characters are more than enough.
            file_name = str(uuid.uuid4())[:12]
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension

# Serializers


class ProfileImageSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        model = Profile
        fields = ['thumbnail']


class UserPublicSerializer(serializers.ModelSerializer):
    username = serializers.CharField(
        required=False, allow_blank=True, read_only=True)
    thumbnail = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ['username', 'first_name',
                  'last_name', 'profile', 'thumbnail']

    def get_thumbnail(self, obj):
        profile = Profile.objects.filter(user__username=obj)[0]
        return ProfileImageSerializer(profile, context=self.context).data


class APIUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    user = UserPublicSerializer(read_only=True)
    created_by = UserPublicSerializer(read_only=True)
    created_at = serializers.DateTimeField(default=timezone.now())
    owner = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.user == request.user:
                return True
        return False


class GameTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = GameType
        fields = '__all__'


class GameActionTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game_types = GameTypeSerializer(many=True, read_only=True)

    class Meta:
        model = GameActionType
        fields = '__all__'


class GameSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    collaborators = APIUserSerializer(read_only=True, many=True)
    players = APIUserSerializer(read_only=True, many=True)
    game_type = GameTypeSerializer(read_only=True, required=False)

    class Meta:
        model = Game
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        created_by = self.context['request'].user
        image = None
        game_type = None
        if self.context['game_type'] != '':
            game_type = self.context['game_type']
        if self.context['image'] != '':
            image = self.context['image']
        return Game.objects.create(created_by=created_by, game_type=game_type, image=image, **validated_data)


class WeaponSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(allow_null=True, read_only=True)
    game = GameSerializer(read_only=True, required=False)

    class Meta:
        model = Weapon
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Weapon.objects.create(game=game, created_by=created_by, **validated_data)


class ItemSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(allow_null=True, read_only=True)
    game = GameSerializer(read_only=True, required=False)

    class Meta:
        model = Item
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Item.objects.create(game=game, created_by=created_by, **validated_data)


class CharacterSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    sprite_sheet = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    sprite_sheet_thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)
    default_weapon = WeaponSerializer(read_only=True, required=False)
    default_item = ItemSerializer(read_only=True, required=False)

    class Meta:
        model = Character
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        sprite_sheet = None
        if self.context['sprite_sheet'] != '':
            sprite_sheet = self.context['sprite_sheet']
        return Character.objects.create(game=game, created_by=created_by, image=image, sprite_sheet=sprite_sheet, **validated_data)


class NonPlayableCharacterAttackSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    sprite_sheet = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    sprite_sheet_thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)

    class Meta:
        model = NonPlayableCharacterAttack
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        sprite_sheet = None
        if self.context['sprite_sheet'] != '':
            sprite_sheet = self.context['sprite_sheet']
        return NonPlayableCharacterAttack.objects.create(game=game, created_by=created_by, image=image, sprite_sheet=sprite_sheet, **validated_data)


class NPCharacterSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    sprite_sheet = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    sprite_sheet_thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)
    attacks = NonPlayableCharacterAttackSerializer(
        read_only=True, required=False, many=True)

    class Meta:
        model = NonPlayableCharacter
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        sprite_sheet = None
        if self.context['sprite_sheet'] != '':
            sprite_sheet = self.context['sprite_sheet']
        return NonPlayableCharacter.objects.create(game=game, created_by=created_by, image=image, sprite_sheet=sprite_sheet, **validated_data)


class ConversationSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)

    class Meta:
        model = Conversation
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Conversation.objects.create(game=game, created_by=created_by, image=image, **validated_data)


class DialogueSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    conversation = ConversationSerializer(read_only=True, required=False)

    class Meta:
        model = Dialogue
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        conversation = self.context['conversation']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Dialogue.objects.create(conversation=conversation, created_by=created_by, image=image, **validated_data)


class DialogueResponseSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    dialgue = DialogueSerializer(read_only=True, required=False)
    connect_to = DialogueSerializer(read_only=True, required=False)

    class Meta:
        model = DialogueResponse
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        dialogue = self.context['dialogue']
        connect_to = self.context['connect_to']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return DialogueResponse.objects.create(dialogue=dialogue, connect_to=connect_to, created_by=created_by, image=image, **validated_data)


class LevelTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = LevelType
        fields = '__all__'


class LevelActionTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    level_types = LevelTypeSerializer(many=True, read_only=True)

    class Meta:
        model = LevelActionType
        fields = '__all__'


class TileSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)

    class Meta:
        model = Tile
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']

        tile = Tile.objects.create(game=game, created_by=created_by,
                                   image=image, **validated_data)

        return tile


class LevelTileSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)
    tile = TileSerializer(read_only=True, required=False)

    class Meta:
        model = LevelTile
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        tile = self.context['tile']
        level = self.context['level']

        level_tile = LevelTile.objects.create(game=game, created_by=created_by,
                                              tile=tile, **validated_data)
        level.level_tiles.add(level_tile)

        return level_tile


class LevelNPCharacterSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)
    npcharacter = NPCharacterSerializer(read_only=True, required=False)
    conversation = ConversationSerializer(read_only=True, required=False)

    class Meta:
        model = LevelNPCharacter
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        conversation = None
        created_by = self.context['request'].user
        npcharacter = self.context['npcharacter']
        level = self.context['level']
        conversation = self.context['conversation']

        level_npcharacter = LevelNPCharacter.objects.create(created_by=created_by,
                                                            npcharacter=npcharacter, conversation=conversation, **validated_data)
        level.level_npcharacters.add(level_npcharacter)

        return level_npcharacter


class LevelSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)
    level_type = LevelTypeSerializer(read_only=True, required=False)
    level_tiles = LevelTileSerializer(
        read_only=True, required=False, many=True)
    level_npcharacters = LevelNPCharacterSerializer(
        read_only=True, required=False, many=True)

    class Meta:
        model = Level
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        level_order = 1
        if Level.objects.filter(game=game).exists():
            last_level = Level.objects.filter(game=game).order_by('-order')[0]
            level_order = last_level.order + 1
        created_by = self.context['request'].user
        level_type = None
        if self.context['level_type'] != '':
            level_type = self.context['level_type']
        image = None
        if self.context['image'] != '':
            image = self.context['image']

        level = Level.objects.create(game=game, order=level_order, created_by=created_by,
                                     level_type=level_type, image=image, **validated_data)

        action_type = LevelActionType.objects.get(slug='complete-level')

        LevelAction.objects.create(
            action_type=action_type, complete_level=True, created_by=created_by, level=level)

        return level


class CutsceneTypeSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = CutsceneType
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return CutsceneType.objects.create(game=game, created_by=created_by, image=image, **validated_data)


class CutsceneActionTypeSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    cutscene_types = CutsceneTypeSerializer(
        required=False, read_only=True, many=True)

    class Meta:
        model = CutsceneActionType
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return CutsceneActionType.objects.create(game=game, created_by=created_by, image=image, **validated_data)


class CutsceneSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    cutscene_type = CutsceneTypeSerializer(required=False, read_only=True)
    conversation = ConversationSerializer(required=False, read_only=True)

    class Meta:
        model = Cutscene
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Cutscene.objects.create(game=game, created_by=created_by, image=image, **validated_data)


class BackgroundSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    game = GameSerializer(read_only=True, required=False)

    class Meta:
        model = Background
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Background.objects.create(game=game, created_by=created_by, image=image, **validated_data)


class RoomTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = RoomType
        fields = '__all__'


class RoomActionTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    room_types = RoomTypeSerializer(many=True, read_only=True)

    class Meta:
        model = RoomActionType
        fields = '__all__'


class RoomSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    room_type = RoomTypeSerializer(read_only=True)

    class Meta:
        model = Room
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        return Room.objects.create(**validated_data)


class PageTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = PageType
        fields = '__all__'


class PageActionTypeSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(read_only=True)
    slug = serializers.SlugField(
        max_length=255, required=False, read_only=True)
    created_by = APIUserSerializer(read_only=True)
    page_types = PageTypeSerializer(many=True, read_only=True)

    class Meta:
        model = PageActionType
        fields = '__all__'


class PageSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    game = GameSerializer(read_only=True)
    page_type = PageTypeSerializer(read_only=True)
    image = serializers.ImageField(
        read_only=True, allow_null=True, required=False)
    thumbnail = serializers.ImageField(
        read_only=True, allow_null=True, required=False)

    class Meta:
        model = Page
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        page_number = 1
        if Page.objects.filter(game=game).exists():
            last_page = Page.objects.filter(
                game=game).order_by('-page_number')[0]
            page_number = last_page.page_number + 1
        page_type = PageType.objects.get(slug='choose-your-own-adventure')
        created_by = self.context['request'].user
        image = None
        if self.context['image'] != '':
            image = self.context['image']
        return Page.objects.create(game=game, page_type=page_type, page_number=page_number, **validated_data)


class StageSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Room
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        return Stage.objects.create(**validated_data)


# ACTION SERIALIZERS
class GameActionSerializer(serializers.ModelSerializer):
    action_type = GameActionTypeSerializer(read_only=True)
    game = GameSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    owner = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = GameAction
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        action_type = self.context['action_type']
        complete_required = self.context['complete_required']
        created_by = self.context['request'].user
        return LevelAction.objects.create(game=game, action_type=action_type, created_by=created_by, **validated_data)


class CutsceneActionSerializer(serializers.ModelSerializer):
    action_type = CutsceneActionTypeSerializer(read_only=True, required=False)
    game = GameSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    owner = serializers.SerializerMethodField(read_only=True)
    change_cutscene = CutsceneSerializer(read_only=True)
    change_level = LevelSerializer(read_only=True)
    cutscene = CutsceneSerializer(read_only=True)

    class Meta:
        model = CutsceneAction
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = self.context['game']
        cutscene = self.context['cutscene']
        action_type = self.context['action_type']
        change_level = self.context['change_level']
        change_cutscene = self.context['change_cutscene']
        game_action_type = self.context['game_action_type']
        created_by = self.context['request'].user
        complete_required = self.context['complete_required']
        auto_select = self.context['auto_select']
        return CutsceneAction.objects.create(game=game, cutscene=cutscene, action_type=action_type, change_level=change_level, change_cutscene=change_cutscene, game_action_type=game_action_type, created_by=created_by, auto_select=auto_select, **validated_data)


class LevelActionSerializer(serializers.ModelSerializer):
    action_type = LevelActionTypeSerializer(read_only=True)
    game_action_type = GameActionTypeSerializer(read_only=True, required=False)
    cutscene_action_type = CutsceneActionTypeSerializer(
        read_only=True, required=False)
    change_cutscene = CutsceneSerializer(read_only=True)
    level = LevelSerializer(read_only=True)
    change_level = LevelSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    owner = serializers.SerializerMethodField(read_only=True)
    level_tile = LevelTileSerializer(read_only=True, required=False)
    game_actions = GameActionSerializer(
        read_only=True, many=True, required=False)

    class Meta:
        model = LevelAction
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        # print('Serializer Go')
        game = self.context['game']
        tile = self.context['tile']
        position_x = self.context['position_x']
        position_y = self.context['position_y']
        display = self.context['display']
        level_tile = None
        created_by = self.context['request'].user
        level = self.context['level']
        if tile != None:
            level_tile = LevelTile.objects.create(
                game=game, tile=tile, display=display, position_x=position_x, position_y=position_y, created_by=created_by)
            # level.level_tiles.add(level_tile)
        change_cutscene = self.context['change_cutscene']
        cutscene_action_type = self.context['cutscene_action_type']
        change_level = self.context['change_level']
        action_type = self.context['action_type']
        game_action_type = self.context['game_action_type']
        complete_required = self.context['complete_required']
        # print('Complete Required: ', complete_required)
        return LevelAction.objects.create(level=level, level_tile=level_tile, change_level=change_level, action_type=action_type, game_action_type=game_action_type, change_cutscene=change_cutscene, cutscene_action_type=cutscene_action_type, created_by=created_by, **validated_data)


class RoomActionSerializer(serializers.ModelSerializer):
    action_type = RoomActionTypeSerializer(read_only=True)
    room = RoomSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    owner = serializers.SerializerMethodField(read_only=True)
    game_actions = GameActionSerializer(read_only=True, many=True)
    level_actions = LevelActionSerializer(read_only=True, many=True)

    class Meta:
        model = RoomAction
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        room = None
        action_type = None
        complete_required = None
        created_by = None
        room = self.context['room']
        action_type = self.context['action_type']
        complete_required = self.context['complete_required']
        created_by = self.context['request'].user
        return RoomAction.objects.create(room=room, action_type=action_type, created_by=created_by, **validated_data)


class PageActionSerializer(serializers.ModelSerializer):
    action_type = PageActionTypeSerializer(read_only=True)
    page = PageSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)
    owner = serializers.SerializerMethodField(read_only=True)
    game_actions = GameActionSerializer(read_only=True, many=True)
    game_action_type = GameActionTypeSerializer(read_only=True)
    go_to_page = PageSerializer(read_only=True)

    class Meta:
        model = PageAction
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        page = None
        page_action_type = None
        go_to_page = None
        created_by = None
        game_action = None
        page = self.context['page']
        page_action_type = self.context['page_action_type']
        go_to_page = self.context['go_to_page']
        created_by = self.context['request'].user
        game_action_type = self.context['game_action_type']
        return PageAction.objects.create(game_action_type=game_action_type, page=page, action_type=page_action_type, go_to_page=go_to_page, created_by=created_by, **validated_data)


# PLAYER SERIALIZERS
class PlayerRoomSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PlayerRoom
        fields = '__all__'


class PlayerStageSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    stage = StageSerializer(read_only=True)
    room = PlayerRoomSerializer(read_only=True)

    class Meta:
        model = Stage
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        stage = self.context['stage']
        created_by = self.request.user
        return PlayerStage.objects.create(stage=stage, created_by=created_by, **validated_data)


class PlayerCharacterSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    character = CharacterSerializer(read_only=True)

    class Meta:
        model = PlayerCharacter
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False


class PlayerGameSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    game = GameSerializer(read_only=True, allow_null=True, required=False)
    character = PlayerCharacterSerializer(
        read_only=True, allow_null=True, required=False)
    room = PlayerRoomSerializer(
        read_only=True, allow_null=True, required=False)
    # level = PlayerLevelSerializer(
    #     read_only=True, allow_null=True, required=False)
    stage = PlayerStageSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = PlayerGame
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = None
        level = None
        created_by = None
        player_stage = None
        player_level = None
        player_character = None
        character = None
        # GAME SERIALIZER
        created_by = self.context['request'].user
        game = self.context['game']
        character = self.context['character']
        # print('Serializer Character: ', character)
        if self.context['level'] != '':
            level = self.context['level']
        if self.context['player_stage'] != '':
            player_stage = self.context['player_stage']
        if self.context['game'].game_type.slug == 'dungeon-crawler':
            player_character = PlayerCharacter.objects.create(
                character=character,
                created_by=created_by,
                name=character.name,
                strength=character.base_strength,
                skill=character.base_skill,
                speed=character.base_speed,
                health=character.base_health,
                wisdom=character.base_wisdom,
                luck=character.base_luck,
                hp=character.base_hp,
                keys=character.base_keys,
                bombs=character.base_bombs,
                damage=character.base_damage,
                active=True,

            )
        player_game = PlayerGame.objects.create(
            game=game, stage=player_stage, character=player_character, created_by=created_by, **validated_data)
        if game.game_type.slug == 'dungeon-crawler':
            player_level = PlayerLevel.objects.create(
                game=player_game, level=level, created_by=created_by)
        return player_game


class PlayerPageSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    page = PageSerializer(read_only=True)
    game = GameSerializer(read_only=True)
    player_game = PlayerGameSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = PlayerPage
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        created_by = self.request.user
        return PlayerPage.objects.create(created_by=created_by, **validated_data)


class PlayerLevelSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    level = LevelSerializer(read_only=True)
    game = PlayerGameSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = PlayerLevel
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = None
        player_game = None
        level = None
        created_by = None
        level = self.context['level']
        game = self.context['game']
        created_by = self.context['request'].user
        player_game = PlayerGame.objects.get(
            game=game, completed=False, created_by=created_by)
        return PlayerLevel.objects.create(
            game=player_game, level=level, created_by=created_by, **validated_data)


class PlayerCutsceneSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    cutscene = CutsceneSerializer(read_only=True)
    game = PlayerGameSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = PlayerCutscene
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = None
        player_game = None
        cutscene = None
        created_by = None
        cutscene = self.context['cutscene']
        game = self.context['game']
        created_by = self.context['request'].user
        player_game = PlayerGame.objects.get(
            game=game, completed=False, created_by=created_by)
        return PlayerCutscene.objects.create(
            game=player_game, cutscene=cutscene, created_by=created_by, **validated_data)


class PlayerConversationSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    conversation = ConversationSerializer(read_only=True)
    game = PlayerGameSerializer(read_only=True)
    created_by = APIUserSerializer(read_only=True)

    class Meta:
        model = PlayerConversation
        fields = '__all__'

    def get_owner(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            if obj.created_by == request.user:
                return True
        return False

    def create(self, validated_data):
        game = None
        player_game = None
        conversation = None
        created_by = None
        conversation = self.context['conversation']
        game = self.context['game']
        created_by = self.context['request'].user
        player_game = PlayerGame.objects.get(
            game=game, completed=False, created_by=created_by)
        return PlayerConversation.objects.create(
            game=player_game, conversation=conversation, created_by=created_by, **validated_data)
