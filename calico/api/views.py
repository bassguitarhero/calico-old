from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db.models import Q, Max, Count
# from django.db.models import Avg, Max, Min, Sum

from django.shortcuts import get_object_or_404, render, HttpResponseRedirect
from django.template.context_processors import csrf
from django.core.mail import EmailMessage
from django.utils import timezone

from django.template.loader import get_template

from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render
from rest_framework import viewsets, permissions

from rest_framework import generics, permissions, pagination, status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, ParseError, NotFound
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, AllowAny
from rest_framework.parsers import FileUploadParser, MultiPartParser, JSONParser, FormParser

from PIL import Image

from .serializers import (
    GameTypeSerializer,
    GameActionTypeSerializer,
    GameSerializer,
    CharacterSerializer,
    NPCharacterSerializer,
    NonPlayableCharacterAttackSerializer,
    ConversationSerializer,
    DialogueSerializer,
    DialogueResponseSerializer,
    LevelSerializer,
    TileSerializer,
    CutsceneTypeSerializer,
    CutsceneActionTypeSerializer,
    CutsceneSerializer,
    CutsceneActionSerializer,
    LevelTileSerializer,
    LevelTypeSerializer,
    LevelActionTypeSerializer,
    LevelActionSerializer,
    LevelNPCharacterSerializer,
    BackgroundSerializer,
    WeaponSerializer,
    ItemSerializer,
    PageTypeSerializer,
    PageActionTypeSerializer,
    PageSerializer,
    PageActionSerializer,
    PlayerGameSerializer,
    PlayerLevelSerializer,
    PlayerStageSerializer,
    PlayerRoomSerializer,
    PlayerPageSerializer,
    PlayerCharacterSerializer,
    PlayerCutsceneSerializer,
    PlayerConversationSerializer
)
from engine.models import (
    GameType,
    GameActionType,
    Game,
    Character,
    NonPlayableCharacter,
    NonPlayableCharacterAttack,
    CutsceneType,
    CutsceneActionType,
    Cutscene,
    LevelType,
    LevelActionType,
    Tile,
    LevelTile,
    Level,
    LevelNPCharacter,
    Conversation,
    Dialogue,
    DialogueResponse,
    Background,
    RoomType,
    RoomActionType,
    Room,
    Item,
    Weapon,
    PageType,
    PageActionType,
    Page,
    PageAction,
    Stage,
    GameAction,
    CutsceneAction,
    LevelAction,
    RoomAction
)
from players.models import (
    PlayerPage,
    PlayerLevel,
    PlayerRoom,
    PlayerCharacter,
    PlayerGame,
    PlayerStage,
    PlayerCutscene,
    PlayerConversation
)
from .permissions import IsOwnerOrReadOnly, IsOwner

# Create your views here.
# PAGINATION


class StandardPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'size'
    max_page_size = 200

    def get_paginated_response(self, data):
        author = False
        user = self.request.user
        if user.is_authenticated:
            author = True

        context = {
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'author': author,
            'results': data
        }
        return Response(context)


# READ DATA
class GamesListAPIView(generics.ListAPIView):
    serializer_class = GameSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    # pagination_class 	= StandardPagination

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Game.objects.filter(
                (Q(created_by=user) | Q(public=True, published=True))
            )
        else:
            return Game.objects.filter(public=True, published=True)


class GameDetailAPIView(generics.RetrieveAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    lookup_field = 'slug'
    permission_classes = [IsOwnerOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Game.objects.filter(slug=self.kwargs.get("slug")).exists():
                return Game.objects.get(slug=self.kwargs.get("slug"))
            else:
                raise NotFound()
        else:
            if Game.objects.filter(slug=self.kwargs.get("slug")).exists():
                return Game.objects.get(slug=self.kwargs.get("slug"))
            else:
                raise NotFound()

        def get_serializer_context(self):
            context = super().get_serializer_context()
            context['request'] = self.request

            return context


class GameTypesListAPIView(generics.ListAPIView):
    serializer_class = GameTypeSerializer
    permissions_classes = [AllowAny]

    def get_queryset(self, *args, **kwargs):
        user = None

        if self.request.user.is_authenticated:
            user = self.request.user
            if user.is_staff:
                return GameType.objects.all()
            else:
                return GameType.objects.filter(slug='choose-your-own-adventure')
        else:
            return GameType.objects.none()


class GameActionTypesListAPIView(generics.ListAPIView):
    serializer_class = GameActionTypeSerializer
    permissions_classes = [AllowAny]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return GameActionType.objects.all()
        else:
            return GameActionType.objects.none()


class CharacterListAPIView(generics.ListAPIView):
    serializer_class = CharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Character.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Character.objects.filter(game__slug=self.kwargs.get("slug"))


class CharacterDetailAPIView(generics.RetrieveAPIView):
    serializer_class = CharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Character.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("character_slug")).exists():
                return Character.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("character_slug"))
            else:
                raise NotFound()
        else:
            if Character.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("character_slug")).exists():
                return Character.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("character_slug"))
            else:
                raise NotFound()


class NPCharacterListAPIView(generics.ListAPIView):
    serializer_class = NPCharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return NonPlayableCharacter.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return NonPlayableCharacter.objects.filter(game__slug=self.kwargs.get("slug"))


class NPCharacterDetailAPIView(generics.RetrieveAPIView):
    serializer_class = NPCharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if NonPlayableCharacter.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_slug")).exists():
                return NonPlayableCharacter.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_slug"))
            else:
                raise NotFound()
        else:
            if NonPlayableCharacter.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_slug")).exists():
                return NonPlayableCharacter.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_slug"))
            else:
                raise NotFound()


class NPCharacterAttackListAPIView(generics.ListAPIView):
    serializer_class = NonPlayableCharacterAttackSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return NonPlayableCharacterAttack.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return NonPlayableCharacterAttack.objects.filter(game__slug=self.kwargs.get("slug"))


class NPCharacterAttackDetailAPIView(generics.RetrieveAPIView):
    serializer_class = NonPlayableCharacterAttackSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if NonPlayableCharacterAttack.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_attack_slug")).exists():
                return NonPlayableCharacterAttack.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_attack_slug"))
            else:
                raise NotFound()
        else:
            if NonPlayableCharacterAttack.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_attack_slug")).exists():
                return NonPlayableCharacterAttack.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("npcharacter_attack_slug"))
            else:
                raise NotFound()


class CutsceneListAPIView(generics.ListAPIView):
    serializer_class = CutsceneSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Cutscene.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Cutscene.objects.filter(game__slug=self.kwargs.get("slug"))


class CutsceneDetailAPIView(generics.RetrieveAPIView):
    serializer_class = CutsceneSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Cutscene.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("cutscene_slug")).exists():
                return Cutscene.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("cutscene_slug"))
            else:
                raise NotFound()
        else:
            if Cutscene.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("cutscene_slug")).exists():
                return Cutscene.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("cutscene_slug"))
            else:
                raise NotFound()


class TileListAPIView(generics.ListAPIView):
    serializer_class = TileSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Tile.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Tile.objects.filter(game__slug=self.kwargs.get("slug"))


class TileDetailAPIView(generics.RetrieveAPIView):
    serializer_class = TileSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Tile.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("tile_slug")).exists():
                return Tile.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("tile_slug"))
            else:
                raise NotFound()
        else:
            if Tile.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("tile_slug")).exists():
                return Tile.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("tile_slug"))
            else:
                raise NotFound()


class LevelTileListAPIView(generics.ListAPIView):
    serializer_class = LevelTileSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Level.objects.get(game__slug=self.kwargs.get('slug'), slug=self.kwargs.get('level_slug')).level_tiles.all()
        else:
            return Level.objects.get(game__slug=self.kwargs.get('slug'), slug=self.kwargs.get('level_slug')).level_tiles.all()


class LevelTileDetailAPIView(generics.RetrieveAPIView):
    serializer_class = LevelTileSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if LevelTile.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug")).exists():
                return LevelTile.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug"))
            else:
                raise NotFound()
        else:
            if LevelTile.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug")).exists():
                return LevelTile.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug"))
            else:
                raise NotFound()


class LevelNPCharacterListAPIView(generics.ListAPIView):
    serializer_class = LevelNPCharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Level.objects.get(game__slug=self.kwargs.get('slug'), slug=self.kwargs.get('level_slug')).level_npcharacters.all()
        else:
            return Level.objects.get(game__slug=self.kwargs.get('slug'), slug=self.kwargs.get('level_slug')).level_npcharacters.all()


class LevelNPCharacterDetailAPIView(generics.RetrieveAPIView):
    serializer_class = LevelNPCharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if LevelNPCharacter.objects.filter(npcharacter__game__slug=self.kwargs.get("slug"), id=self.kwargs.get("uuid")).exists():
                return LevelNPCharacter.objects.get(npcharacter__game__slug=self.kwargs.get("slug"), id=self.kwargs.get("uuid"))
            else:
                raise NotFound()
        else:
            if LevelNPCharacter.objects.filter(npcharacter__game__slug=self.kwargs.get("slug"), id=self.kwargs.get("uuid")).exists():
                return LevelNPCharacter.objects.get(npcharacter__game__slug=self.kwargs.get("slug"), id=self.kwargs.get("uuid"))
            else:
                raise NotFound()


class LevelListAPIView(generics.ListAPIView):
    serializer_class = LevelSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Level.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Level.objects.filter(game__slug=self.kwargs.get("slug"))


class LevelDetailAPIView(generics.RetrieveAPIView):
    serializer_class = LevelSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Level.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug")).exists():
                return Level.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug"))
            else:
                raise NotFound()
        else:
            if Level.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug")).exists():
                return Level.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("level_slug"))
            else:
                raise NotFound()


class LevelTypesListAPIView(generics.ListAPIView):
    serializer_class = LevelTypeSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return LevelType.objects.all()
        else:
            return LevelType.objects.none()


class ConversationListAPIView(generics.ListAPIView):
    serializer_class = ConversationSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Conversation.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Conversation.objects.filter(game__slug=self.kwargs.get("slug"))


class ConversationDetailAPIView(generics.RetrieveAPIView):
    serializer_class = ConversationSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Conversation.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("conversation_slug")).exists():
                return Conversation.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("conversation_slug"))
            else:
                raise NotFound()
        else:
            if Conversation.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("conversation_slug")).exists():
                return Conversation.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("conversation_slug"))
            else:
                raise NotFound()


class DialogueListAPIView(generics.ListAPIView):
    serializer_class = DialogueSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Dialogue.objects.filter(conversation__game__slug=self.kwargs.get("slug"), conversation__slug=self.kwargs.get('conversation_slug'))
        else:
            return Dialogue.objects.filter(conversation__game__slug=self.kwargs.get("slug"), conversation__slug=self.kwargs.get('conversation_slug'))


class DialogueDetailAPIView(generics.RetrieveAPIView):
    serializer_class = DialogueSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Dialogue.objects.filter(conversation__game__slug=self.kwargs.get("slug"), conversation__slug=self.kwargs.get('conversation_slug'), slug=self.kwargs.get('dialogue_slug')).exists():
                return Dialogue.objects.get(conversation__game__slug=self.kwargs.get("slug"), conversation__slug=self.kwargs.get('conversation_slug'), slug=self.kwargs.get('dialogue_slug'))
            else:
                raise NotFound()
        else:
            if Dialogue.objects.filter(conversation__game__slug=self.kwargs.get("slug"), conversation__slug=self.kwargs.get('conversation_slug'), slug=self.kwargs.get('dialogue_slug')).exists():
                return Dialogue.objects.get(conversation__game__slug=self.kwargs.get("slug"), conversation__slug=self.kwargs.get('conversation_slug'), slug=self.kwargs.get('dialogue_slug'))
            else:
                raise NotFound()


class DialogueResponseListAPIView(generics.ListAPIView):
    serializer_class = DialogueResponseSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        game = None
        conversation = None
        dialogue = None
        if self.request.user.is_authenticated:
            user = self.request.user
            game = Game.objects.get(slug=self.kwargs.get('slug'))
            conversation = Conversation.objects.get(
                game=game, slug=self.kwargs.get('conversation_slug'))
            dialogue = Dialogue.objects.get(
                conversation=conversation, slug=self.kwargs.get('dialogue_slug'))
            return DialogueResponse.objects.filter(dialogue=dialogue)
        else:
            return DialogueResponse.objects.filter(dialogue=dialogue)


class DialogueResponseDetailAPIView(generics.RetrieveAPIView):
    serializer_class = DialogueResponseSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if DialogueResponse.objects.filter(dialogue__conversation__game__slug=self.kwargs.get("slug"), dialogue__conversation__slug=self.kwargs.get('conversation_slug'), dialogue__slug=self.kwargs.get('dialogue_slug'), slug=self.kwargs.get('response_slug')).exists():
                return DialogueResponse.objects.get(dialogue__conversation__game__slug=self.kwargs.get("slug"), dialogue__conversation__slug=self.kwargs.get('conversation_slug'), dialogue__slug=self.kwargs.get('dialogue_slug'), slug=self.kwargs.get('response_slug'))
            else:
                raise NotFound()
        else:
            if DialogueResponse.objects.filter(dialogue__conversation__game__slug=self.kwargs.get("slug"), dialogue__conversation__slug=self.kwargs.get('conversation_slug'), dialogue__slug=self.kwargs.get('dialogue_slug'), slug=self.kwargs.get('response_slug')).exists():
                return DialogueResponse.objects.get(dialogue__conversation__game__slug=self.kwargs.get("slug"), dialogue__conversation__slug=self.kwargs.get('conversation_slug'), dialogue__slug=self.kwargs.get('dialogue_slug'), slug=self.kwargs.get('response_slug'))
            else:
                raise NotFound()


class BackgroundListAPIView(generics.ListAPIView):
    serializer_class = BackgroundSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Background.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Background.objects.filter(game__slug=self.kwargs.get("slug"))


class BackgroundDetailAPIView(generics.RetrieveAPIView):
    serializer_class = BackgroundSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Background.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("background_slug")).exists():
                return Background.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("background_slug"))
            else:
                raise NotFound()
        else:
            if Background.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("background_slug")).exists():
                return Background.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("background_slug"))
            else:
                raise NotFound()


class WeaponListAPIView(generics.ListAPIView):
    serializer_class = WeaponSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Weapon.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Weapon.objects.filter(game__slug=self.kwargs.get("slug"))


class WeaponDetailAPIView(generics.RetrieveAPIView):
    serializer_class = WeaponSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Weapon.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("weapon_slug")).exists():
                return Weapon.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("weapon_slug"))
            else:
                raise NotFound()
        else:
            if Weapon.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("weapon_slug")).exists():
                return Weapon.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("weapon_slug"))
            else:
                raise NotFound()


class ItemListAPIView(generics.ListAPIView):
    serializer_class = ItemSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return Item.objects.filter(game__slug=self.kwargs.get("slug"))
        else:
            return Item.objects.filter(game__slug=self.kwargs.get("slug"))


class ItemDetailAPIView(generics.RetrieveAPIView):
    serializer_class = ItemSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if Item.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("item_slug")).exists():
                return Item.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("item_slug"))
            else:
                raise NotFound()
        else:
            if Item.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("item_slug")).exists():
                return Item.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get("item_slug"))
            else:
                raise NotFound()


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all()
    permission_classes = [permissions.AllowAny]
    serializer_class = GameSerializer


def games_view(request, *args, **kwargs):
    pass


def game_view(request, slug, *args, **kwargs):
    pass


# GAME ACTIONS
class LevelActionTypesAPIView(generics.ListAPIView):
    serializer_class = LevelActionTypeSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return LevelActionType.objects.all().exclude(slug='complete-level')
        else:
            return LevelActionType.objects.none()


class LevelActionsAPIView(generics.ListCreateAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            level = Level.objects.filter(game__slug=self.kwargs.get(
                'slug'), slug=self.kwargs.get('level_slug'))[0]
            level_actions = LevelAction.objects.filter(level__game__slug=self.kwargs.get(
                'slug'), level__slug=self.kwargs.get('level_slug'))
            return level_actions
        else:
            return LevelAction.objects.none()

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data: ', data)

        user = None
        if self.request.user.is_authenticated:
            if Game.objects.filter(slug=self.kwargs.get('slug'), created_by=self.request.user).exists():
                game = None
                level = None
                action_type = None
                game_action_type = None
                change_level = None
                change_cutscene = None
                complete_required = False
                tile = None
                position_x = None
                position_y = None
                display = False

                # LEVEL TILE
                game = Game.objects.get(slug=self.kwargs.get('slug'))
                if data['tile_slug'] != '':
                    tile = Tile.objects.get(game=game, slug=data['tile_slug'])
                if data['position_x'] != '':
                    position_x = data['position_x']
                if data['position_y'] != '':
                    position_y = data['position_y']
                if data['display'] != '':
                    display = data['display']
                if data['display'] == "true":
                    display = True
                else:
                    display = False

                # LEVEL ACTION
                level = Level.objects.get(game__slug=self.kwargs.get(
                    'slug'), slug=self.kwargs.get('level_slug'))
                action_type = LevelActionType.objects.get(
                    slug=data['action_type'])
                if data['action_type'] == 'game-action':
                    game_action_type = GameActionType.objects.get(
                        slug=data['game_action_type'])
                if data['action_type'] == 'change-cutscene':
                    cutscene_action_type = CutsceneActionType.objects.get(
                        slug=data['action_type'])
                if data['change_level'] != '':
                    change_level = Level.objects.get(game__slug=self.kwargs.get(
                        'slug'), slug=data['change_level'])
                if data['change_cutscene'] != '':
                    change_cutscene = Cutscene.objects.get(
                        game__slug=self.kwargs.get('slug'), slug=data['change_cutscene'])
                if data['require_complete'] == 'true':
                    complete_required = True
                    data['complete_required'] = True
                else:
                    complete_required = False

                # SERIALIZER
                serializer = LevelActionSerializer(
                    data=data, context={'request': request, 'complete_required': complete_required, 'game': game, 'level': level, 'action_type': action_type, 'game_action_type': game_action_type, 'cutscene_action_type': cutscene_action_type, 'change_level': change_level, 'change_cutscene': change_cutscene, 'tile': tile, 'position_x': position_x, 'position_y': position_y, 'display': display})
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                # print('Errors: ', serializer.errors)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return NotFound()
        else:
            return NotFound()


class LevelActionAPIView(generics.RetrieveAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsOwnerOrReadOnly]
    lookup_field = 'level_action_slug'

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            if LevelAction.objects.filter(level__game__slug=self.kwargs.get("slug"), level__slug=self.kwargs.get("level_slug"), slug=self.kwargs.get('level_action_slug')).exists():
                return LevelAction.objects.get(level__game__slug=self.kwargs.get("slug"), level__slug=self.kwargs.get("level_slug"), slug=self.kwargs.get('level_action_slug'))
            else:
                raise LevelAction.objects.none()
        else:
            raise LevelAction.objects.none()


class LevelActionEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsOwnerOrReadOnly]
    lookup_field = 'level_action_slug'

    def get_object(self):
        return LevelAction.objects.filter(id=self.kwargs.get('uuid'), level__game__slug=self.kwargs.get('slug'), level__slug=self.kwargs.get('level_slug'), action_type__slug=self.kwargs.get('level_action_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data!!!: ', data)

        game = None
        level = None
        action_type = None
        game_action_type = None
        change_level = None
        cutscene_action_type = None
        change_cutscene = None
        complete_required = False
        tile = None
        position_x = None
        position_y = None
        display = False

        # LEVEL TILE
        game = Game.objects.get(slug=self.kwargs.get('slug'))
        if data['tile_slug'] != '':
            tile = Tile.objects.get(game=game, slug=data['tile_slug'])
        if data['position_x'] != '':
            position_x = data['position_x']
        if data['position_y'] != '':
            position_y = data['position_y']
        if data['display'] != '':
            display = data['display']
        if data['display'] == "true":
            display = True
        else:
            display = False

        # LEVEL ACTION
        level = Level.objects.get(game__slug=self.kwargs.get(
            'slug'), slug=self.kwargs.get('level_slug'))
        action_type = LevelActionType.objects.get(
            slug=data['action_type'])
        if data['action_type'] == 'game-action':
            game_action_type = GameActionType.objects.get(
                slug=data['game_action_type']
            )
        if data['action_type'] == 'change-cutscene':
            cutscene_action_type = CutsceneActionType.objects.get(
                slug=data['action_type'])
        if data['change_level'] != '':
            change_level = Level.objects.get(game__slug=self.kwargs.get(
                'slug'), slug=data['change_level'])
        if data['change_cutscene'] != '':
            change_cutscene = Cutscene.objects.get(
                game__slug=self.kwargs.get('slug'), slug=data['change_cutscene'])
        if data['require_complete'] == 'true':
            complete_required = True
        else:
            complete_required = False
        if data['defeat_all_enemies'] == 'true':
            defeat_all_enemies = True
        else:
            defeat_all_enemies = False
        if data['defeat_boss'] == 'true':
            defeat_boss = True
        else:
            defeat_boss = False

        instance = LevelAction.objects.get(id=self.kwargs.get('uuid'))

        if data['tile_slug'] != '':
            level_tile = instance.level_tile
            level_tile.position_x = data['position_x']
            level_tile.position_y = data['position_y']
            level_tile.tile = tile
            if data['display'] == "false":
                level_tile.display = False
            else:
                level_tile.display = True
            level_tile.save()

        instance.action_type = action_type
        instance.game_action_type = game_action_type
        instance.cutscene_action_type = cutscene_action_type
        instance.change_cutscene = change_cutscene
        instance.change_level = change_level
        instance.complete_required = complete_required
        instance.defeat_all_enemies = defeat_all_enemies
        instance.defeat_boss = defeat_boss

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = LevelActionSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game, 'tile': tile})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        instance = LevelAction.objects.get(id=self.kwargs.get('uuid'))
        if instance.level_tile != None:
            instance.level_tile.delete()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# GAME ADMIN
class GameCreateAPIView(generics.CreateAPIView):
    serializer_class = GameSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game_type = None
        if data['game_type_slug'] != '':
            game_type = GameType.objects.get(slug=data['game_type_slug'])

        serializer = GameSerializer(
            data=data, context={'request': request, 'image': image, 'game_type': game_type})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GameEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = GameSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'slug'

    def get_object(self):
        return Game.objects.filter(slug=self.kwargs.get("slug"), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        instance = Game.objects.get(slug=self.kwargs.get("slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')
        instance.map_width = self.request.data.get('map_width')
        instance.map_height = self.request.data.get('map_height')
        instance.tile_width = self.request.data.get('tile_width')
        instance.tile_height = self.request.data.get('tile_height')
        instance.sprite_width = self.request.data.get('sprite_width')
        instance.sprite_height = self.request.data.get('sprite_height')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.public = False
        if self.request.data.get('public') == 'true':
            instance.public = True
        instance.published = False
        if self.request.data.get('published') == 'true':
            instance.published = True

        game_type = None
        if data['game_type_slug'] != '':
            game_type = GameType.objects.get(slug=data['game_type_slug'])
            instance.game_type = game_type

        if 'remove-image' in data:
            if data['remove-image'] == 'true':
                instance.image = None

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = GameSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game_type': game_type})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CharacterCreateAPIView(generics.CreateAPIView):
    serializer_class = CharacterSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']
        sprite_sheet = None
        if data['sprite_sheet'] != '':
            sprite_sheet = data['sprite_sheet']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = CharacterSerializer(
            data=data, context={'request': request, 'image': image, 'sprite_sheet': sprite_sheet, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CharacterEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CharacterSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'character_slug'

    def get_object(self):
        return Character.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('character_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Character.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("character_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')
        instance.width = self.request.data.get('width')
        instance.height = self.request.data.get('height')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if 'sprite_sheet' in data:
            if data['sprite_sheet'] != '':
                # pass
                instance.sprite_sheet = data['sprite_sheet']
            else:
                data['sprite_sheet'] = instance.sprite_sheet
        elif instance.sprite_sheet:
            data['sprite_sheet'] = instance.sprite_sheet

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = CharacterSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NPCharacterCreateAPIView(generics.CreateAPIView):
    serializer_class = NPCharacterSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']
        sprite_sheet = None
        if data['sprite_sheet'] != '':
            sprite_sheet = data['sprite_sheet']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = NPCharacterSerializer(
            data=data, context={'request': request, 'image': image, 'game': game, 'sprite_sheet': sprite_sheet})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NPCharacterEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NPCharacterSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'npcharacter_slug'

    def get_object(self):
        return NonPlayableCharacter.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('npcharacter_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = NonPlayableCharacter.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("npcharacter_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if 'sprite_sheet' in data:
            if data['sprite_sheet'] != '':
                # pass
                instance.sprite_sheet = data['sprite_sheet']
            else:
                data['sprite_sheet'] = instance.sprite_sheet
        elif instance.sprite_sheet:
            data['sprite_sheet'] = instance.sprite_sheet

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = NPCharacterSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NPCharacterEditAttacksAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NPCharacterSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'npcharacter_slug'

    def get_object(self):
        return NonPlayableCharacter.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('npcharacter_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = NonPlayableCharacter.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("npcharacter_slug"))

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = NPCharacterSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NPCharacterAttackCreateAPIView(generics.CreateAPIView):
    serializer_class = NonPlayableCharacterAttackSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = NonPlayableCharacterAttackSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NPCharacterAttackEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = NonPlayableCharacterAttackSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'npcharacter_attack_slug'

    def get_object(self):
        return NonPlayableCharacterAttack.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('npcharacter_attack_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = NonPlayableCharacterAttack.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("npcharacter_attack_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if 'sprite_sheet' in data:
            if data['sprite_sheet'] != '':
                # pass
                instance.sprite_sheet = data['sprite_sheet']
            else:
                data['sprite_sheet'] = instance.sprite_sheet
        elif instance.sprite_sheet:
            data['sprite_sheet'] = instance.sprite_sheet

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = NonPlayableCharacterAttackSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CutsceneCreateAPIView(generics.CreateAPIView):
    serializer_class = CutsceneSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = CutsceneSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CutsceneEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CutsceneSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'cutscene_slug'

    def get_object(self):
        return Cutscene.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('cutscene_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Cutscene.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("cutscene_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')
        instance.width = self.request.data.get('width')
        instance.height = self.request.data.get('height')
        instance.url_youtube = self.request.data.get('url_youtube')
        instance.url_vimeo = self.request.data.get('url_vimeo')
        instance.url_archive = self.request.data.get('url_archive')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if data['autoplay'] == 'false':
            instance.autoplay = False
        else:
            instance.autoplay = True

        if data['loop'] == 'true':
            instance.loop = True
        else:
            instance.loop = False

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = CutsceneSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CutsceneActionTypeListAPIView(generics.ListAPIView):
    serializer_class = CutsceneActionTypeSerializer
    permissions_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        return CutsceneActionType.objects.all()


class CutsceneActionsListAPIView(generics.ListAPIView):
    serializer_class = CutsceneActionSerializer
    permissions_classes = [IsAuthenticated]

    def get_queryset(self):
        return CutsceneAction.objects.filter(game__slug=self.kwargs.get('slug'), cutscene__slug=self.kwargs.get('cutscene_slug'))


class CutsceneActionCreateAPIView(generics.CreateAPIView):
    serializer_class = CutsceneActionSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data: ', data)

        complete_required = False
        game = None
        cutscene = None
        cutscene_action_type = None
        level = None
        game_action_type = None
        change_cutscene = None

        if data['cutscene_slug'] != '':
            cutscene = Cutscene.objects.get(game__slug=self.kwargs.get(
                'slug'), slug=self.kwargs.get('cutscene_slug'))
        if data['action_type_slug'] != '':
            action_type = CutsceneActionType.objects.get(
                slug=data['action_type_slug'])
        if data['level_slug'] != '':
            change_level = Level.objects.get(
                game__slug=self.kwargs.get('slug'), slug=data['level_slug'])
        if data['change_cutscene_slug'] != '':
            change_cutscene = Cutscene.objects.get(game__slug=self.kwargs.get(
                'slug'), slug=data['change_cutscene_slug'])
        if data['game_action_type_slug'] != '':
            game_action_type = GameActionType.objects.get(
                slug=data['game_action_type_slug'])
        if data['require_complete'] == 'true':
            complete_required = True
        else:
            complete_required = False
        if data['auto_select'] == 'true':
            auto_select = True
            # GO THROUGH EVERY OTHER CUTSCENE ACTION AND REMOVE AUTO SELECT
            cutscene_actions = CutsceneAction.objects.filter(game__slug=self.kwargs.get(
                "slug"), cutscene__slug=self.kwargs.get('cutscene_slug'))
            for cutscene_action in cutscene_actions:
                cutscene_action.auto_select = False
                cutscene_action.save()
        else:
            auto_select = False

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = CutsceneActionSerializer(
            data=data, context={'request': request, 'game': game, 'cutscene': cutscene, 'action_type': action_type, 'change_level': change_level, 'change_cutscene': change_cutscene, 'game_action_type': game_action_type, 'complete_required': complete_required, 'auto_select': auto_select})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # print('Errors: ', serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CutsceneActionEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CutsceneActionSerializer
    permissions_classes = [IsAuthenticated]
    lookup_field = 'uuid'

    def get_object(self):
        return CutsceneAction.objects.get(game__slug=self.kwargs.get("slug"), cutscene__slug=self.kwargs.get('cutscene_slug'), slug=self.kwargs.get('cutscene_action_slug'))

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Edit Tile Data: ', data)

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = CutsceneAction.objects.get(game__slug=self.kwargs.get(
            "slug"), cutscene__slug=self.kwargs.get('cutscene_slug'), slug=self.kwargs.get('cutscene_action_slug'))

        complete_required = False
        game = None
        cutscene = None
        cutscene_action_type = None
        level = None
        game_action_type = None
        change_cutscene = None

        if data['cutscene_slug'] != '':
            instance.cutscene = Cutscene.objects.get(
                game__slug=self.kwargs.get('slug'), slug=self.kwargs.get('cutscene_slug'))
        if data['action_type_slug'] != '':
            instance.action_type = CutsceneActionType.objects.get(
                slug=data['action_type_slug'])
        if data['level_slug'] != '':
            instance.change_level = Level.objects.get(
                game__slug=self.kwargs.get('slug'), slug=data['level_slug'])
        if data['change_cutscene_slug'] != '':
            instance.change_cutscene = Cutscene.objects.get(
                game__slug=self.kwargs.get('slug'), slug=data['change_cutscene_slug'])
        if data['game_action_type_slug'] != '':
            instance.game_action_type = GameActionType.objects.get(
                slug=data['game_action_type_slug'])
        if data['require_complete'] == 'true':
            instance.complete_required = True
        else:
            instance.complete_required = False
        if data['auto_select'] == 'true':
            # GO THROUGH EVERY OTHER CUTSCENE ACTION AND REMOVE AUTO SELECT
            cutscene_actions = CutsceneAction.objects.filter(game__slug=self.kwargs.get(
                "slug"), cutscene__slug=self.kwargs.get('cutscene_slug'))
            for cutscene_action in cutscene_actions:
                cutscene_action.auto_select = False
                cutscene_action.save()
            instance.auto_select = True
        else:
            instance.auto_select = False

        instance.name = data['name']
        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = CutsceneActionSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TileCreateAPIView(generics.CreateAPIView):
    serializer_class = TileSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data: ', data)

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = TileSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # print('Errors: ', serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TileEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TileSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'tile_slug'

    def get_object(self):
        return Tile.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('tile_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Edit Tile Data: ', data)

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Tile.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("tile_slug"))
        instance.name = self.request.data.get('name')
        if self.request.data.get('walkable') == "false":
            instance.walkable = False
        else:
            instance.walkable = True
        if self.request.data.get('display') == "false":
            instance.display = False
        else:
            instance.display = True

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = TileSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LevelTileCreateAPIView(generics.CreateAPIView):
    serializer_class = LevelTileSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        tile = None
        if data['tile_slug'] != '':
            tile = Tile.objects.get(slug=data['tile_slug'])

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        level = Level.objects.get(
            slug=self.kwargs.get('level_slug'), game=game)

        serializer = LevelTileSerializer(
            data=data, context={'request': request, 'game': game, 'level': level, 'tile': tile})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LevelTileEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LevelTileSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'uuid'

    def get_object(self):
        return LevelTile.objects.filter(id=self.kwargs.get('uuid'), game__slug=self.kwargs.get("slug"), tile__slug=self.kwargs.get('level_tile_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data for level tile edit: ', data)

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        tile = Tile.objects.get(game=game, slug=data['tile_slug'])

        instance = LevelTile.objects.get(id=self.kwargs.get('uuid'))

        instance.position_x = data['position_x']
        instance.position_y = data['position_y']
        instance.tile = tile
        if data['display'] == "false":
            instance.display = False
        else:
            instance.display = True

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = LevelTileSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game, 'tile': tile})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LevelNPCharacterCreateAPIView(generics.CreateAPIView):
    serializer_class = LevelNPCharacterSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = None
        level = None
        npcharacter = None
        conversation = None
        if data['npcharacter_slug'] != '':
            npcharacter = NonPlayableCharacter.objects.get(
                slug=data['npcharacter_slug'])

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        level = Level.objects.get(
            slug=self.kwargs.get('level_slug'), game=game)
        if data['conversation_slug'] != '':
            conversation = Conversation.objects.get(
                game=game, slug=data['conversation_slug'])

        serializer = LevelNPCharacterSerializer(
            data=data, context={'request': request, 'game': game, 'level': level, 'npcharacter': npcharacter, 'conversation': conversation})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LevelNPCharacterEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LevelNPCharacterSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'uuid'

    def get_object(self):
        return LevelNPCharacter.objects.filter(id=self.kwargs.get('uuid'), game__slug=self.kwargs.get("slug"), npcharacter__slug=self.kwargs.get('level_npcharacter_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data for level npcharacter edit: ', data)

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        npcharacter = NonPlayableCharacter.objects.get(
            game=game, slug=data['npcharacter_slug'])

        instance = LevelNPCharacter.objects.get(id=self.kwargs.get('uuid'))

        instance.position_x = data['position_x']
        instance.position_y = data['position_y']
        instance.health = data['health']
        instance.npcharacter = npcharacter
        if data['display'] == "false":
            instance.display = False
        else:
            instance.display = True
        if data['randomize'] == "true":
            instance.randomize = True
        else:
            instance.randomize = False
        if data['hostile'] == "false":
            instance.hostile = False
        else:
            instance.hostile = True
        if data['conversation_slug'] != '':
            instance.conversation = Conversation.objects.get(
                game=game, slug=data['conversation_slug'])
        else:
            instance.conversation = None

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = LevelNPCharacterSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game, 'npcharacter': npcharacter})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LevelCreateAPIView(generics.CreateAPIView):
    serializer_class = LevelSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        level_type = None
        if data['level_type_slug'] != '':
            level_type = LevelType.objects.get(slug=data['level_type_slug'])

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = LevelSerializer(
            data=data, context={'request': request, 'image': image, 'game': game, 'level_type': level_type})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LevelEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LevelSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'level_slug'

    def get_object(self):
        return Level.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('level_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Level.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("level_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')
        instance.width = self.request.data.get('width')
        instance.height = self.request.data.get('height')
        instance.tile_width = self.request.data.get('tile_width')
        instance.tile_height = self.request.data.get('tile_height')
        instance.starting_x = self.request.data.get('starting_x')
        instance.starting_y = self.request.data.get('starting_y')

        if self.request.data.get('starting_level') == "true":
            instance.starting_level = True
        else:
            instance.starting_level = False

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        level_type = None
        if data['level_type_slug'] != '':
            level_type = LevelType.objects.get(slug=data['level_type_slug'])
            instance.level_type = level_type

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = LevelSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game, 'level_type': level_type})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ConversationCreateAPIView(generics.CreateAPIView):
    serializer_class = ConversationSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = ConversationSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ConversationEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ConversationSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'conversation_slug'

    def get_object(self):
        return Conversation.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('conversation_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Conversation.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("conversation_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = ConversationSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DialogueCreateAPIView(generics.CreateAPIView):
    serializer_class = DialogueSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))

        serializer = DialogueSerializer(
            data=data, context={'request': request, 'image': image, 'conversation': conversation})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DialogueEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DialogueSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'conversation_slug'

    def get_object(self):
        return Dialogue.objects.get(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('conversation_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        print('Data: ', data)

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))

        instance = Dialogue.objects.get(
            conversation=conversation, slug=self.kwargs.get("dialogue_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = DialogueSerializer(
            instance, data=data, partial=partial, context={'request': request, 'conversation': conversation})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DialogueResponseCreateAPIView(generics.CreateAPIView):
    serializer_class = DialogueResponseSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))
        dialogue = Dialogue.objects.get(
            conversation=conversation, slug=self.kwargs.get('dialogue_slug'))
        connect_to = None
        if data['connect_to_slug'] != '':
            connect_to = Dialogue.objects.get(
                conversation=conversation, slug=data['connect_to_slug'])

        serializer = DialogueResponseSerializer(
            data=data, context={'request': request, 'image': image, 'dialogue': dialogue, 'connect_to': connect_to})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # print('Errors: ', serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DialogueResponseEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DialogueResponseSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'response_slug'

    def get_object(self):
        return DialogueResponse.objects.filter(dialogue__conversation__game__slug=self.kwargs.get("slug"), dialogue__conversation__slug=self.kwargs.get('conversation_slug'), dialogue__slug=self.kwargs.get('dialogue_slug'), slug=self.kwargs.get('conversation_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        print('Dialogue Response Edit', data)

        connect_to = None
        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))
        dialogue = Dialogue.objects.get(
            conversation=conversation, slug=self.kwargs.get('dialogue_slug'))

        instance = DialogueResponse.objects.get(
            dialogue=dialogue, slug=self.kwargs.get('response_slug'))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')
        instance.order = self.request.data.get('order')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if data['connect_to_slug'] != '':
            instance.connect_to = Dialogue.objects.get(
                conversation=conversation, slug=data['connect_to_slug'])

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = DialogueResponseSerializer(
            instance, data=data, partial=partial, context={'request': request, 'dialogue': dialogue})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BackgroundCreateAPIView(generics.CreateAPIView):
    serializer_class = BackgroundSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = BackgroundSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BackgroundEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = BackgroundSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'background_slug'

    def get_object(self):
        return Background.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('background_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Background.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("background_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = BackgroundSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WeaponCreateAPIView(generics.CreateAPIView):
    serializer_class = WeaponSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = WeaponSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WeaponEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = WeaponSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'weapon_slug'

    def get_object(self):
        return Weapon.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('weapon_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Weapon.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("weapon_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = WeaponSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ItemCreateAPIView(generics.CreateAPIView):
    serializer_class = ItemSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        serializer = ItemSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ItemEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ItemSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'item_slug'

    def get_object(self):
        return Item.objects.filter(game__slug=self.kwargs.get("slug"), slug=self.kwargs.get('item_slug'), created_by=self.request.user)

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Item.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("item_slug"))
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = ItemSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


class PageTypesAPIView(generics.ListAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PageType.objects.all()
        else:
            return PageType.objects.all()


class PageActionTypesAPIView(generics.ListAPIView):
    serializer_class = PageActionTypeSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PageActionType.objects.all()
        else:
            return PageActionType.objects.all()


class PagesAPIView(generics.ListAPIView):
    serializer_class = PageSerializer
    permissions_classes = [IsOwnerOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            game = Game.objects.get(slug=self.kwargs.get('slug'))
            user = self.request.user
            return Page.objects.filter(game=game)
        else:
            game = Game.objects.get(slug=self.kwargs.get('slug'))
            user = self.request.user
            return Page.objects.filter(game=game)


class PagesNewAPIView(generics.ListCreateAPIView):
    serializer_class = PageSerializer
    permissions_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        user = None
        user = self.request.user
        data['created_by'] = user.pk

        serializer = PageSerializer(
            data=data, context={'request': request, 'image': image, 'game': game})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # print('Errors: ', serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PageAPIView(generics.RetrieveAPIView):
    serializer_class = PageSerializer
    permissions_classes = [IsOwnerOrReadOnly]
    lookup_field = 'page_slug'

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            game = Game.objects.filter(slug=self.kwargs.get('slug'))[0]
            return Page.objects.filter(game=game, slug=self.kwargs.get('page_slug'))[0]
        else:
            return Page.objects.filter(game=game, slug=self.kwargs.get('page_slug'))[0]


class PageEditAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PageSerializer
    permissions_classes = [IsOwnerOrReadOnly]
    lookup_field = 'page_slug'

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            game = Game.objects.filter(slug=self.kwargs.get('slug'))[0]
            return Page.objects.filter(game=game, slug=self.kwargs.get('page_slug'))[0]
        else:
            return Page.objects.filter(game=game, slug=self.kwargs.get('page_slug'))[0]

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = Page.objects.get(game__slug=self.kwargs.get(
            "slug"), slug=self.kwargs.get("page_slug"), created_by=self.request.user)
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if 'remove-image' in data:
            if data['remove-image'] == 'true':
                instance.image = None

        instance.save()
        user = None
        user = self.request.user
        data['created_by'] = user.pk

        partial = kwargs.pop('partial', False)

        serializer = PageSerializer(
            instance, data=data, partial=partial, context={'request': request, 'game': game})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PageActionsAPIView(generics.ListCreateAPIView):
    serializer_class = PageActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PageAction.objects.filter(page__game__slug=self.kwargs.get('slug'), page__slug=self.kwargs.get('page_slug'))
        else:
            return PageAction.objects.filter(page__game__slug=self.kwargs.get('slug'), page__slug=self.kwargs.get('page_slug'))

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        image = None
        if data['image'] != '':
            image = data['image']

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        page = None
        user = None
        game_action_type = None
        page_action_type = None
        go_to_page = None
        user = self.request.user

        if PageActionType.objects.filter(slug=self.request.data['actionTypeSlug']).exists():
            page_action_type = PageActionType.objects.get(
                slug=self.request.data['actionTypeSlug'])

        if Page.objects.filter(game=game, slug=data['pageSlug']).exists():
            page = Page.objects.get(game=game, slug=data['pageSlug'])

        if Page.objects.filter(game=game, slug=data['goToPageSlug']).exists():
            go_to_page = Page.objects.get(
                game=game, slug=data['goToPageSlug'])

        if GameActionType.objects.filter(slug=data['gameActionTypeSlug']).exists():
            game_action_type = GameActionType.objects.get(
                slug=data['gameActionTypeSlug'])

        serializer = PageActionSerializer(
            data=data, context={'request': request, 'game_action_type': game_action_type,  'page_action_type': page_action_type, 'page': page, 'go_to_page': go_to_page})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # print('Errors: ', serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PageActionGoToPageAPIView(generics.ListAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'action_slug'

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PageAction.objects.all()
        else:
            return PageAction.objects.all()

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PageAction.objects.none()
        else:
            return PageAction.objects.none()


class PageActionAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PageActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'action_slug'

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PageAction.objects.get(slug=self.kwargs.get('action_slug'), created_by=self.request.user)
        else:
            return PageAction.objects.none()

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        page = None
        user = None
        game_action_type = None
        page_action_type = None
        go_to_page = None
        user = self.request.user

        if PageActionType.objects.filter(slug=self.request.data['actionTypeSlug']).exists():
            page_action_type = PageActionType.objects.get(
                slug=self.request.data['actionTypeSlug'])

        if Page.objects.filter(game=game, slug=data['pageSlug']).exists():
            page = Page.objects.get(game=game, slug=data['pageSlug'])

        if Page.objects.filter(game=game, slug=data['goToPageSlug']).exists():
            go_to_page = Page.objects.get(
                game=game, slug=data['goToPageSlug'])

        if GameActionType.objects.filter(slug=data['gameActionTypeSlug']).exists():
            game_action_type = GameActionType.objects.get(
                slug=data['gameActionTypeSlug'])

        game = Game.objects.get(slug=self.kwargs.get('slug'))

        instance = PageAction.objects.get(page=page, slug=self.kwargs.get(
            "action_slug"), created_by=self.request.user)
        instance.name = self.request.data.get('name')
        instance.description = self.request.data.get('description')

        instance.page_action_type = page_action_type
        instance.page = page
        instance.go_to_page = go_to_page
        instance.game_action_type = game_action_type

        if 'image' in data:
            if data['image'] != '':
                # pass
                instance.image = data['image']
            else:
                data['image'] = instance.image
        elif instance.image:
            data['image'] = instance.image

        if 'remove-image' in data:
            if data['remove-image'] == 'true':
                instance.image = None

        instance.save()
        data['created_by'] = user.pk

        partial = kwargs.pop('partial', False)

        serializer = PageActionSerializer(
            instance, data=data, context={'request': request, 'game_action_type': game_action_type,  'page_action_type': page_action_type, 'page': page, 'go_to_page': go_to_page})
        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


class PlayerGameAPIView(generics.ListCreateAPIView):
    serializer_class = PlayerGameSerializer
    permissions_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            return PlayerGame.objects.filter(
                game__slug=self.kwargs.get('slug'), created_by=user
            )
        else:
            return PlayerGame.objects.none()

    def post(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        # print('Data: ', data)
        game = None
        character = None
        level = None
        cutscene = None
        room = None
        stage = None
        page = None

        player_game = None
        player_stage = None
        player_character = None
        player_level = None
        player_cutscene = None

        game = Game.objects.get(slug=self.kwargs.get('slug'))
        # character = Character.objects.get(
        #     slug=data['character_slug'], game=game)
        # level = Level.objects.get(game=game, order=1)
        # print('Character: ', character)

        # FOR CHOOSE YOUR OWN ADVENTURE
        if game.game_type.slug == 'choose-your-own-adventure':
            if PlayerGame.objects.filter(game=game, created_by=self.request.user, completed=False).exists():
                player_game = PlayerGame.objects.get(
                    game=game, created_by=self.request.user, completed=False)
                serializer = PlayerGameSerializer(
                    data=data, context={'request': request, 'game': game, 'page': page, 'player_stage': stage, 'character': character, 'room': room, 'level': level})
                # if serializer.is_valid():
                # serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                page = Page.objects.get(game=game, page_number=1)
                serializer = PlayerGameSerializer(
                    data=data, context={'request': request, 'game': game, 'page': page, 'player_stage': stage, 'character': character, 'room': room, 'level': level})
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    # print('Errors: ', serializer.errors)
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # FOR DUNGEON CRAWLER
        elif game.game_type.slug == 'dungeon-crawler':
            character = Character.objects.get(
                slug=data['character_slug'], game=game)
            level = Level.objects.get(game=game, order=1)
            if PlayerGame.objects.filter(game=game, created_by=self.request.user, completed=False).exists():
                player_game = PlayerGame.objects.get(
                    game=game, created_by=self.request.user, completed=False)
                # player_character = PlayerCharacter.objects.get(
                #     game=game, active=True, created_by=self.request.user
                # )
                # player_stage = None
                # player_stage = PlayerStage.objects.get(
                #     game=game, created_by=self.request.user)
                serializer = PlayerGameSerializer(
                    data=data, context={'request': request, 'player_stage': player_stage, 'game': game, 'character': character, 'level': level, 'room': room})
                if serializer.is_valid():
                    # serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                # level = Level.objects.get(game=game, order=1)
                # character = None
                # player_character = PlayerCharacter.objects.create(
                #     character=character, name=character.name, active=True, created_by=self.request.user
                # )
                # if Stage.objects.filter(game=game).exists():
                #     player_stage = PlayerStage.objects.create(
                #         game=game, created_by=self.request.user)
                # else:
                #     character =
                #     stage = Stage.objects.create(
                #         game=game, created_by=game.created_by
                #     )
                #     player_stage = PlayerStage.objects.create(
                #         game=game, created_by=self.request.user
                #     )
                serializer = PlayerGameSerializer(
                    data=data, context={'request': request, 'player_stage': player_stage, 'game': game, 'character': character, 'level': level, 'room': room})
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    # print('Errors: ', serializer.errors)
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # else:
            # print('Noothing!')


class PlayerGameUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerGameSerializer
    permissions_classes = [IsOwner]

    def get_object(self):
        user = None
        if self.request.user.is_authenticated:
            return PlayerGame.objects.get(game__slug=self.kwargs.get("slug"), completed=False, created_by=self.request.user)
        return PlayerGame.objects.none()

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        instance = PlayerGame.objects.get(game__slug=self.kwargs.get(
            "slug"), completed=False, created_by=self.request.user)

        instance.save()

        partial = kwargs.pop('partial', False)

        serializer = PlayerGameSerializer(
            instance, data=data, partial=partial, context={'request': request})

        if serializer.is_valid():
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}
            return Response(serializer.data)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PlayerCutsceneAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerCutsceneSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'cutscene_slug'

    def get_object(self, *args, **kwargs):
        game = None
        cutscene = None
        if PlayerGame.objects.filter(game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user
            )
            player_cutscene = PlayerCutscene.objects.get(
                game=player_game, active=True, created_by=self.request.user)
            if player_cutscene != '':
                return player_cutscene
            else:
                return PlayerCutscene.objects.none()
        else:
            return PlayerCutscene.objects.none()


class GetPlayerLevelAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerLevelSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'level_slug'

    def get_object(self, *args, **kwargs):
        game = None
        level = None
        if PlayerGame.objects.filter(game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user
            )
            if PlayerLevel.objects.filter(game=player_game, active=True, created_by=self.request.user).exists():
                return PlayerLevel.objects.get(game=player_game, active=True, created_by=self.request.user)
            else:
                return None
        else:
            return None


class GetPlayerCutsceneAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerCutsceneSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'cutscene_slug'

    def get_object(self, *args, **kwargs):
        player_game = None
        player_cutscene = None
        if PlayerGame.objects.filter(game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.get(game__slug=self.kwargs.get(
                'slug'), completed=False, created_by=self.request.user)
            if PlayerCutscene.objects.filter(game=player_game, active=True, created_by=self.request.user).exists():
                return PlayerCutscene.objects.get(game=player_game, active=True, created_by=self.request.user)
            else:
                return None
        else:
            return None


class PlayerLevelAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerLevelSerializer
    permissions_classes = [IsOwner]
    lookup_field = 'level_slug'

    def get_object(self, *args, **kwargs):
        game = None
        level = None
        if PlayerGame.objects.filter(game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user
            )
            level = Level.objects.get(game__slug=self.kwargs.get(
                'slug'), slug=self.kwargs.get('level_slug'))
            player_level = PlayerLevel.objects.get_or_create(
                game=player_game, level=level, active=True, created_by=self.request.user)
            if player_level != '':
                return player_level
            else:
                return PlayerLevel.objects.none()
        else:
            return PlayerLevel.objects.none()

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = None
        level = None
        player_level = None
        if PlayerGame.objects.filter(game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.get(game__slug=self.kwargs.get(
                'slug'), completed=False, created_by=self.request.user)
            level = Level.objects.get(game__slug=self.kwargs.get(
                'slug'), slug=self.kwargs.get('level_slug'))
            player_level = PlayerLevel.objects.get_or_create(
                game=player_game, level=level, active=True, created_by=self.request.user)
            # if player_level != '':
            # level = game.level.level
            #     return player_level
            # else:
            #     return None
        else:
            level = None

        # Update Info Here
        player_level.save()
        serializer = PlayerLevelSerializer(
            data=data, context={'request': request, 'player_level': player_level})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PlayerLevelCompleteAPIView(generics.ListAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        return LevelAction.objects.none()


class PlayerLevelActionsAPIView(generics.ListAPIView):
    serializer_class = LevelActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            actions = LevelAction.objects.filter(level__game__slug=self.kwargs.get(
                'slug'), level__slug=self.kwargs.get('level_slug'))
            return LevelAction.objects.filter(level__game__slug=self.kwargs.get('slug'), level__slug=self.kwargs.get('level_slug'))
        else:
            return NotFound()


class PlayerLevelActionAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerLevelSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=user)
            player_level = PlayerLevel.objects.get(
                game=player_game, active=True)
            if self.kwargs.get('action_slug') == 'complete-level':
                player_level.completed = True
                player_level.save()
            return player_level
        else:
            return PlayerLevel.objects.none()


class PlayerLevelChangeAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerLevelSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'new_level_slug'

    def get_object(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=user)
            player_level = PlayerLevel.objects.get(
                game=player_game, active=True)
            if self.kwargs.get('action_slug') == 'change-level':
                player_level.active = False
                player_level.save()
                level = Level.objects.get(game__slug=self.kwargs.get(
                    'slug'), slug=self.kwargs.get('new_level_slug'))
                new_player_level, created = PlayerLevel.objects.get_or_create(
                    game=player_game, level=level, created_by=user)
                new_player_level.active = True
                new_player_level.save()
                return new_player_level
            return player_level
        else:
            return PlayerLevel.objects.none()


class PlayerCutsceneLevelChangeAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerLevelSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'new_level_slug'

    def get_object(self, *args, **kwargs):
        user = None
        player_level = None
        player_cutscene = None
        if self.request.user.is_authenticated:
            user = self.request.user
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=user)
            player_cutscene = PlayerCutscene.objects.get(
                game=player_game, active=True)
            # player_level = PlayerLevel.objects.get(
            #     game=player_game, active=True)
            if self.kwargs.get('cutscene_action_type_slug') == 'change-level':
                # player_level.active = False
                # player_level.save()
                player_cutscene.active = False
                player_cutscene.save()
                level = Level.objects.get(game__slug=self.kwargs.get(
                    'slug'), slug=self.kwargs.get('new_level_slug'))
                new_player_level, created = PlayerLevel.objects.get_or_create(
                    game=player_game, level=level, created_by=user)
                new_player_level.active = True
                new_player_level.save()
                return new_player_level
            return PlayerLevel.objects.none()
        else:
            return PlayerLevel.objects.none()


class PlayerCutsceneChangeAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerCutsceneSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'cutscene_slug'

    def get_object(self, *args, **kwargs):
        player_level = None
        player_cutscene = None
        user = None
        if self.request.user.is_authenticated:
            user = self.request.user
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=user)
            if PlayerCutscene.objects.filter(
                    game=player_game, active=True).exists():
                player_cutscene = PlayerCutscene.objects.get(
                    game=player_game, active=True)
            if PlayerLevel.objects.filter(
                    game=player_game, active=True).exists():
                player_level = PlayerLevel.objects.get(
                    game=player_game, active=True)
            if self.kwargs.get('action_type_slug') == 'change-cutscene':
                if player_level != None:
                    player_level.active = False
                    player_level.save()
                if player_cutscene != None:
                    player_cutscene.active = False
                    player_cutscene.save()
                cutscene = Cutscene.objects.get(game__slug=self.kwargs.get(
                    'slug'), slug=self.kwargs.get('cutscene_slug'))
                new_player_cutscene, created = PlayerCutscene.objects.get_or_create(
                    game=player_game, cutscene=cutscene, created_by=user)
                new_player_cutscene.active = True
                new_player_cutscene.save()
                return new_player_cutscene
            return player_cutscene
        else:
            return PlayerCutscene.objects.none()


class PlayerCharacterAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerCharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'character_slug'

    def get_object(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            game = Game.objects.get(slug=self.kwargs.get('slug'))
            character = Character.objects.get(
                game=game, slug=self.kwargs.get('character_slug'))
            return PlayerCharacter.objects.get(character=character, active=True, created_by=self.request.user)
        else:
            return None


class PlayerCharacterUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerCharacterSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'character_slug'

    def get_object(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            game = Game.objects.get(slug=self.kwargs.get('slug'))
            character = Character.objects.get(
                game=game, slug=self.kwargs.get('character_slug'))
            player_character = PlayerCharacter.objects.get(
                character=character, active=True, created_by=self.request.user)
            player_character.hp = self.kwargs.get('player_health')
            player_character.save()
            return player_character
        else:
            return None

    def update(self, request, *args, **kwargs):
        if request.FILES:
            data = request.data
        else:
            data = request.data.copy()

        game = None
        character = None
        player_character = None
        if PlayerGame.objects.filter(game__slug=self.kwargs.get('slug'), completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.get(game__slug=self.kwargs.get(
                'slug'), completed=False, created_by=self.request.user)
            player_character = player_game.character
            player_character.hp = self.kwargs.get('player_health')
            player_character.save()

        serializer = PlayerCharacterSerializer(
            data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            # print('Errors: ', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PlayerPageAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerPageSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'player_page_slug'

    def get_object(self, *args, **kwargs):
        user = None
        if self.request.user.is_authenticated:
            user = None
            game = None
            page = None
            player_game = None
            user = self.request.user
            game = Game.objects.get(slug=self.kwargs.get('slug'))
            player_game = PlayerGame.objects.get(
                game__slug=self.kwargs.get('slug'), completed=False, created_by=user)
            if PlayerPage.objects.filter(player_game=player_game, active=True).exists():
                player_page = PlayerPage.objects.get(
                    player_game=player_game, active=True)
            else:
                page = Page.objects.get(game=game, page_number=1)
                player_page, created = PlayerPage.objects.get_or_create(
                    game=player_game.game, page=page, player_game=player_game, created_by=user)

            if self.kwargs.get('actionTyeSlug') == 'go-to-page':

                return player_page
            return player_page
        else:
            return PlayerPage.objects.none()


class PlayerPageActionsAPIView(generics.ListAPIView):
    serializer_class = PageActionSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        return PageAction.objects.filter(page__game__slug=self.kwargs.get('slug'), page__slug=self.kwargs.get('page_slug'))


class PlayerPageChangeAPIView(generics.RetrieveAPIView):
    serializer_class = PlayerPageSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, *args, **kwargs):
        game = None
        player_game = None
        current_page = None
        new_page = None
        current_player_page = None
        go_to_page = None
        if Game.objects.filter(slug=self.kwargs.get('slug')).exists():
            game = Game.objects.filter(slug=self.kwargs.get('slug'))[0]
        if PlayerGame.objects.filter(game=game, completed=False, created_by=self.request.user).exists():
            player_game = PlayerGame.objects.filter(
                game=game, completed=False, created_by=self.request.user)[0]
        if Page.objects.filter(game=player_game.game, slug=self.kwargs.get('page_slug')).exists():
            current_page = Page.objects.filter(
                game=game, slug=self.kwargs.get('page_slug'))[0]
        if Page.objects.filter(game=player_game.game, slug=self.kwargs.get('new_page_slug')).exists():
            new_page = Page.objects.filter(
                game=game, slug=self.kwargs.get('new_page_slug'))[0]
        if PlayerPage.objects.filter(game=player_game.game, player_game=player_game, page=current_page, active=True, created_by=self.request.user).exists():
            current_player_page = PlayerPage.objects.filter(
                game=player_game.game, player_game=player_game, page=current_page, active=True, created_by=self.request.user)[0]
            current_player_page.active = False
            current_player_page.save()
        go_to_page, created = PlayerPage.objects.get_or_create(
            game=player_game.game, player_game=player_game, page=new_page, created_by=self.request.user)
        if created == False:
            go_to_page.active = True
            go_to_page.save()
        return go_to_page


class PlayerGameChangeAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PlayerGameSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, *args, **kwargs):
        game = None
        player_game = None
        game = Game.objects.filter(slug=self.kwargs.get('slug'))[0]
        player_game = PlayerGame.objects.filter(
            game=game, created_by=self.request.user, completed=False)[0]

        if self.kwargs.get('action_type_slug') == 'win-game':
            player_game.completed = True
            player_game.completed_at = timezone.now()
            player_game.player_win = True
            player_game.save()
        elif self.kwargs.get('action_type_slug') == 'lose-game':
            player_game.completed = True
            player_game.completed_at = timezone.now()
            player_game.player_win = False
            player_game.save()
        # DELETE ALL GAME STUFF EXCEPT GAME (LEVELS, PAGES, ETC)
        player_game.character.delete()
        PlayerLevel.objects.filter(game=player_game).delete()
        PlayerPage.objects.filter(player_game=player_game).delete()
        return player_game


class PlayerConversationAPIView(generics.RetrieveAPIView):
    serializer_class = ConversationSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, *args, **kwargs):
        game = None
        conversation = None
        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))
        return conversation


class PlayerConversationStartAPIView(generics.RetrieveAPIView):
    serializer_class = DialogueSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, *args, **kwargs):
        game = None
        conversation = None
        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))
        dialogue = Dialogue.objects.get(
            conversation=conversation, opening_dialogue=True)
        return dialogue


class PlayerDialogueResponseListAPIView(generics.ListAPIView):
    serializer_class = DialogueResponseSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        # print('Getting queryset')
        user = None
        game = None
        conversation = None
        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))
        dialogue = Dialogue.objects.get(
            conversation=conversation, slug=self.kwargs.get('dialogue_slug'))
        if self.request.user.is_authenticated:
            user = self.request.user
            # print('Dialogue Response Query: ',
            #       DialogueResponse.objects.filter(dialogue=dialogue))
            return DialogueResponse.objects.filter(dialogue=dialogue)
        else:
            return DialogueResponse.objects.filter(dialogue=dialogue)


class PlayerConversationDialogueAPIView(generics.RetrieveAPIView):
    serializer_class = DialogueSerializer
    permissions_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, *args, **kwargs):
        game = None
        conversation = None
        game = Game.objects.get(slug=self.kwargs.get('slug'))
        conversation = Conversation.objects.get(
            game=game, slug=self.kwargs.get('conversation_slug'))
        dialogue = Dialogue.objects.get(
            conversation=conversation, slug=self.kwargs.get('dialogue_slug'))
        # print('Dialogue: ', dialogue)
        return dialogue
