from django.urls import path, include
# from . import views
from .views import (
    GamesListAPIView, GameDetailAPIView,
    GameCreateAPIView, GameEditAPIView,

    GameActionTypesListAPIView,

    GameTypesListAPIView,

    CharacterListAPIView, CharacterDetailAPIView,
    CharacterCreateAPIView, CharacterEditAPIView,

    NPCharacterListAPIView, NPCharacterDetailAPIView,
    NPCharacterCreateAPIView, NPCharacterEditAPIView,
    NPCharacterEditAttacksAPIView,

    NPCharacterAttackListAPIView, NPCharacterAttackDetailAPIView,
    NPCharacterAttackCreateAPIView, NPCharacterAttackEditAPIView,

    CutsceneListAPIView, CutsceneDetailAPIView,
    CutsceneCreateAPIView, CutsceneEditAPIView,

    CutsceneActionTypeListAPIView,

    CutsceneActionsListAPIView, CutsceneActionCreateAPIView,
    CutsceneActionEditAPIView,

    LevelListAPIView, LevelDetailAPIView,
    LevelCreateAPIView, LevelEditAPIView,

    TileListAPIView, TileDetailAPIView,
    TileCreateAPIView, TileEditAPIView,

    LevelTileListAPIView, LevelTileDetailAPIView,
    LevelTileCreateAPIView, LevelTileEditAPIView,

    LevelNPCharacterListAPIView, LevelNPCharacterDetailAPIView,
    LevelNPCharacterCreateAPIView, LevelNPCharacterEditAPIView,

    LevelActionTypesAPIView, LevelActionsAPIView, LevelActionAPIView, LevelActionEditAPIView,

    LevelTypesListAPIView,

    ConversationListAPIView, ConversationDetailAPIView,
    ConversationCreateAPIView, ConversationEditAPIView,

    DialogueListAPIView, DialogueDetailAPIView,
    DialogueCreateAPIView, DialogueEditAPIView,

    DialogueResponseListAPIView, DialogueResponseDetailAPIView,
    DialogueResponseCreateAPIView, DialogueResponseEditAPIView,

    BackgroundListAPIView, BackgroundDetailAPIView,
    BackgroundCreateAPIView, BackgroundEditAPIView,

    WeaponListAPIView, WeaponDetailAPIView,
    WeaponCreateAPIView, WeaponEditAPIView,

    ItemListAPIView, ItemDetailAPIView,
    ItemCreateAPIView, ItemEditAPIView,

    PageTypesAPIView, PageActionTypesAPIView,
    PagesNewAPIView,
    PagesAPIView, PageAPIView, PageEditAPIView,
    PageActionsAPIView, PageActionGoToPageAPIView,
    PageActionAPIView,

    PlayerGameAPIView, PlayerGameUpdateAPIView,

    PlayerPageAPIView, PlayerPageActionsAPIView, PlayerPageChangeAPIView,

    PlayerCharacterAPIView,

    PlayerCharacterUpdateAPIView,

    GetPlayerLevelAPIView,

    PlayerLevelAPIView,

    PlayerLevelActionsAPIView,

    PlayerLevelActionAPIView,

    PlayerLevelChangeAPIView, PlayerCutsceneLevelChangeAPIView,

    PlayerGameChangeAPIView,

    PlayerCutsceneAPIView, GetPlayerCutsceneAPIView, PlayerCutsceneChangeAPIView,

    PlayerConversationAPIView, PlayerConversationStartAPIView, PlayerDialogueResponseListAPIView,

    PlayerConversationDialogueAPIView
)

urlpatterns = [
    path('', GamesListAPIView.as_view(), name='games-list'),
    path('games/', GamesListAPIView.as_view(), name='games-list'),
    path('games/v/<str:slug>/', GameDetailAPIView.as_view(), name='game-detail'),

    path('games/new/', GameCreateAPIView.as_view(), name='game-create'),
    path('games/v/<str:slug>/edit/', GameEditAPIView.as_view(), name='game-edit'),


    path('games/types/', GameTypesListAPIView.as_view(), name='game-types'),
    path('games/actions/types/', GameActionTypesListAPIView.as_view(),
         name='game-action-types'),
    path('games/levels/types/', LevelTypesListAPIView.as_view(), name='level-types'),


    path('games/v/<str:slug>/characters/',
         CharacterListAPIView.as_view(), name='characters-list'),
    path('games/v/<str:slug>/characters/v/<str:character_slug>/',
         CharacterDetailAPIView.as_view(), name='character-detail'),

    path('games/v/<str:slug>/characters/new/',
         CharacterCreateAPIView.as_view(), name='character-create'),
    path('games/v/<str:slug>/characters/v/<str:character_slug>/edit/',
         CharacterEditAPIView.as_view(), name='character-edit'),


    path('games/v/<str:slug>/npcharacters/',
         NPCharacterListAPIView.as_view(), name='npcharacters-list'),
    path('games/v/<str:slug>/npcharacters/v/<str:npcharacter_slug>/',
         NPCharacterDetailAPIView.as_view(), name='npcharacter-detail'),

    path('games/v/<str:slug>/npcharacters/new/',
         NPCharacterCreateAPIView.as_view(), name='npcharacter-create'),
    path('games/v/<str:slug>/npcharacters/v/<str:npcharacter_slug>/edit/',
         NPCharacterEditAPIView.as_view(), name='npcharacter-edit'),
    path('games/v/<str:slug>/npcharacters/v/<str:npcharacter_slug>/attacks/',
         NPCharacterEditAttacksAPIView.as_view(), name='npcharacter-edit-attacks'),


    path('games/v/<str:slug>/npcharacters/attacks/',
         NPCharacterAttackListAPIView.as_view(), name='npcharacters-attacks-list'),
    path('games/v/<str:slug>/npcharacters/attacks/v/<str:npcharacter_attack_slug>/',
         NPCharacterAttackDetailAPIView.as_view(), name='npcharacter-attack-detail'),

    path('games/v/<str:slug>/npcharacters/attacks/new/',
         NPCharacterAttackCreateAPIView.as_view(), name='npcharacter-attack-create'),
    path('games/v/<str:slug>/npcharacters/attacks/v/<str:npcharacter_attack_slug>/edit/',
         NPCharacterAttackEditAPIView.as_view(), name='npcharacter-attack-edit'),


    path('games/v/<str:slug>/cutscenes/',
         CutsceneListAPIView.as_view(), name='cutscenes-list'),
    path('games/v/<str:slug>/cutscenes/v/<str:cutscene_slug>/',
         CutsceneDetailAPIView.as_view(), name='cutscene-detail'),

    path('games/v/<str:slug>/cutscenes/new/',
         CutsceneCreateAPIView.as_view(), name='cutscene-create'),
    path('games/v/<str:slug>/cutscenes/v/<str:cutscene_slug>/edit/',
         CutsceneEditAPIView.as_view(), name='cutscene-edit'),


    path('games/cutscenes/actions/types/',
         CutsceneActionTypeListAPIView.as_view(), name='cutscene-action-types-list'),

    path('games/v/<str:slug>/cutscenes/v/<str:cutscene_slug>/actions/',
         CutsceneActionsListAPIView.as_view(), name='cutscene-actions'),
    path('games/v/<str:slug>/cutscenes/v/<str:cutscene_slug>/actions/new/',
         CutsceneActionCreateAPIView.as_view(), name='cutscene-action-create'),
    path('games/v/<str:slug>/cutscenes/v/<str:cutscene_slug>/actions/v/<str:cutscene_action_slug>/edit/',
         CutsceneActionEditAPIView.as_view(), name='cutscene-action-edit'),



    path('games/v/<str:slug>/levels/',
         LevelListAPIView.as_view(), name='levels-list'),
    path('games/v/<str:slug>/levels/v/<str:level_slug>/',
         LevelDetailAPIView.as_view(), name='level-detail'),

    path('games/v/<str:slug>/levels/new/',
         LevelCreateAPIView.as_view(), name='level-create'),
    path('games/v/<str:slug>/levels/v/<str:level_slug>/edit/',
         LevelEditAPIView.as_view(), name='level-edit'),

    path('games/v/<str:slug>/tiles/',
         TileListAPIView.as_view(), name='tiles-list'),
    path('games/v/<str:slug>/tiles/v/<str:tile_slug>/',
         TileDetailAPIView.as_view(), name='tile-detail'),

    path('games/v/<str:slug>/tiles/new/',
         TileCreateAPIView.as_view(), name='tile-create'),
    path('games/v/<str:slug>/tiles/v/<str:tile_slug>/edit/',
         TileEditAPIView.as_view(), name='tile-edit'),

    path('games/v/<str:slug>/levels/v/<str:level_slug>/tiles/',
         LevelTileListAPIView.as_view(), name='level-tiles-list'),
    path('games/v/<str:slug>/levels/v/<str:level_slug>/tiles/v/<str:level_tile_slug>/',
         LevelTileDetailAPIView.as_view(), name='level-tile-detail'),

    path('games/v/<str:slug>/levels/v/<str:level_slug>/tiles/new/',
         LevelTileCreateAPIView.as_view(), name='level-tile-create'),
    path('games/v/<str:slug>/levels/v/<str:level_slug>/tiles/v/<str:level_tile_slug>/<str:uuid>/edit/',
         LevelTileEditAPIView.as_view(), name='level-tile-edit'),

    path('games/v/<str:slug>/levels/v/<str:level_slug>/npcharacters/',
         LevelNPCharacterListAPIView.as_view(), name='level-npcharacters-list'),
    path('games/v/<str:slug>/levels/v/<str:level_slug>/tiles/v/<str:level_npcharacter_slug>/',
         LevelNPCharacterDetailAPIView.as_view(), name='level-npcharacter-detail'),

    path('games/v/<str:slug>/levels/v/<str:level_slug>/npcharacters/new/',
         LevelNPCharacterCreateAPIView.as_view(), name='level-npcharacter-create'),
    path('games/v/<str:slug>/levels/v/<str:level_slug>/npcharacters/v/<str:level_npcharacter_slug>/<str:uuid>/edit/',
         LevelNPCharacterEditAPIView.as_view(), name='level-npcharacter-edit'),

    # path('games/v/<str:slug>/levels/types',
    #      LevelTypesListAPIView.as_view(), name='level-types'),
    # LIST ALL ACTION TYPES
    path('games/levels/actions/types/',
         LevelActionTypesAPIView.as_view(), name='level-action-types'),
    # LIST ALL ACTIONS FOR A LEVEL, CREATE AN ACTION FOR A LEVEL
    path('games/v/<str:slug>/levels/v/<str:level_slug>/actions/',
         LevelActionsAPIView.as_view(), name='level-actions'),
    # RETRIEVE ACTION FOR LEVEL
    path('games/v/<str:slug>/levels/v/<str:level_slug>/actions/v/<str:level_action_slug>/',
         LevelActionAPIView.as_view(), name='level-action'),
    # EDIT DELETE LEVEL ACTION
    path('games/v/<str:slug>/levels/v/<str:level_slug>/actions/v/<str:level_action_slug>/<str:uuid>/edit/',
         LevelActionEditAPIView.as_view(), name='level-action-edit'),






    path('games/v/<str:slug>/conversations/',
         ConversationListAPIView.as_view(), name='conversations-list'),
    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/',
         ConversationDetailAPIView.as_view(), name='conversation-detail'),

    path('games/v/<str:slug>/conversations/new/',
         ConversationCreateAPIView.as_view(), name='background-create'),
    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/edit/',
         ConversationEditAPIView.as_view(), name='conversation-edit'),





    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/',
         DialogueListAPIView.as_view(), name='dialogue-list'),
    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/',
         DialogueDetailAPIView.as_view(), name='dialogue-detail'),

    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/new/',
         DialogueCreateAPIView.as_view(), name='dialogue-create'),
    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/edit/',
         DialogueEditAPIView.as_view(), name='dialogue-edit'),



    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/responses/',
         DialogueResponseListAPIView.as_view(), name='dialogue-response-list'),
    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/responses/v/<str:response_slug>/',
         DialogueResponseDetailAPIView.as_view(), name='dialogue-response-detail'),

    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/responses/new/',
         DialogueResponseCreateAPIView.as_view(), name='dialogue-response-create'),
    path('games/v/<str:slug>/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/responses/v/<str:response_slug>/edit/',
         DialogueResponseEditAPIView.as_view(), name='dialogue-response-edit'),





    path('games/v/<str:slug>/backgrounds/',
         BackgroundListAPIView.as_view(), name='conversations-list'),
    path('games/v/<str:slug>/backgrounds/v/<str:background_slug>/',
         BackgroundDetailAPIView.as_view(), name='background-detail'),

    path('games/v/<str:slug>/backgrounds/new/',
         BackgroundCreateAPIView.as_view(), name='background-create'),
    path('games/v/<str:slug>/backgrounds/v/<str:background_slug>/edit/',
         BackgroundEditAPIView.as_view(), name='background-edit'),


    path('games/v/<str:slug>/weapons/',
         WeaponListAPIView.as_view(), name='weapons-list'),
    path('games/v/<str:slug>/weapons/v/<str:weapon_slug>/',
         WeaponDetailAPIView.as_view(), name='weapon-detail'),

    path('games/v/<str:slug>/weapons/new/',
         WeaponCreateAPIView.as_view(), name='weapon-new'),
    path('games/v/<str:slug>/weapons/v/<str:weapon_slug>/edit/',
         WeaponEditAPIView.as_view(), name='weapon-edit'),


    path('games/v/<str:slug>/items/',
         ItemListAPIView.as_view(), name='items-list'),
    path('games/v/<str:slug>/items/v/<str:item_slug>/',
         ItemDetailAPIView.as_view(), name='item-detail'),

    path('games/v/<str:slug>/items/new/',
         ItemCreateAPIView.as_view(), name='item-create'),
    path('games/v/<str:slug>/items/v/<str:item_slug>/edit/',
         ItemEditAPIView.as_view(), name='item-edit'),



    # CHOOSE YOUR OWN ADVENTURE
    path('games/pages/types/',
         PageTypesAPIView.as_view(), name='page-types-list'),
    path('games/pages/actions/types/',
         PageActionTypesAPIView.as_view(), name='page-action-types-list'),
    path('games/v/<str:slug>/pages/',
         PagesAPIView.as_view(), name='pages-list'),
    path('games/v/<str:slug>/pages/new/',
         PagesNewAPIView.as_view(), name='pages-new'),
    path('games/v/<str:slug>/pages/v/<str:page_slug>/',
         PageAPIView.as_view(), name='page-details'),
    path('games/v/<str:slug>/pages/v/<str:page_slug>/edit/',
         PageEditAPIView.as_view(), name='page-edit'),
    path('games/v/<str:slug>/pages/v/<str:page_slug>/actions/',
         PageActionsAPIView.as_view(), name='page-actions'),
    path('games/v/<str:slug>/pages/v/<str:page_slug>/actions/v/<str:action_slug>/to/<str:go_to_page_slug>/',
         PageActionGoToPageAPIView.as_view(), name='page-action-go-to-page'),
    path('games/v/<str:slug>/pages/v/<str:page_slug>/actions/v/<str:action_slug>/',
         PageActionAPIView.as_view(), name='page-action'),



    # PLAY GAME FUNCTIONS
    path('games/v/<str:slug>/plays/',
         PlayerGameAPIView.as_view(), name='all-games'),

    path('games/v/<str:slug>/play/',
         PlayerGameUpdateAPIView.as_view(), name='play-game'),
    path('games/v/<str:slug>/play/characters/v/<str:character_slug>/',
         PlayerCharacterAPIView.as_view(), name='player-character'),
    path('games/v/<str:slug>/play/characters/v/<str:character_slug>/update/<str:player_health>/',
         PlayerCharacterUpdateAPIView.as_view(), name='player-character-update'),

    #     path('games/v/<str:slug>/play/levels/change/<str:level_slug>/',
    #          PlayerLevelChangeAPIView.as_view(), name='change-level'),

    path('games/v/<str:slug>/play/page/',
         PlayerPageAPIView.as_view(), name='player-page'),

    path('games/v/<str:slug>/play/pages/v/<str:page_slug>/actions/',
         PlayerPageActionsAPIView.as_view(), name='player-page-actions'),

    path('games/v/<str:slug>/play/pages/v/<str:page_slug>/to/<str:new_page_slug>/',
         PlayerPageChangeAPIView.as_view(), name='player-page-change'),

    path('games/v/<str:slug>/play/level/',
         GetPlayerLevelAPIView.as_view(), name='get-player-level'),

    path('games/v/<str:slug>/play/levels/v/<str:level_slug>/',
         PlayerLevelAPIView.as_view(), name='player-level'),

    path('games/v/<str:slug>/play/levels/v/<str:level_slug>/actions/',
         PlayerLevelActionsAPIView.as_view(), name='player-level-actions'),

    path('games/v/<str:slug>/play/levels/v/<str:level_slug>/actions/v/<str:action_slug>/',
         PlayerLevelActionAPIView.as_view(), name='player-level-action'),

    path('games/v/<str:slug>/play/levels/v/<str:level_slug>/actions/v/<str:action_slug>/to/<str:new_level_slug>/',
         PlayerLevelChangeAPIView.as_view(), name='player-level-change'),

    path('games/v/<str:slug>/play/cutscenes/v/<str:cutscene_slug>/actions/v/<str:cutscene_action_type_slug>/to/<str:new_level_slug>/',
         PlayerCutsceneLevelChangeAPIView.as_view(), name='player-level-change'),

    # CHOOSE YOUR OWN ADVENTURE GAME
    path('games/v/<str:slug>/play/pages/v/<str:page_slug>/actions/v/<str:action_type_slug>/',
         PlayerGameChangeAPIView.as_view(), name='player-game-change'),
    # DUNGEON CRAWLER GAME
    path('games/v/<str:slug>/play/actions/v/<str:action_type_slug>/',
         PlayerGameChangeAPIView.as_view(), name='player-game-action'),

    path('games/v/<str:slug>/play/cutscenes/v/<str:cutscene_slug>/',
         PlayerCutsceneAPIView.as_view(), name='player-cutscene'),

    path('games/v/<str:slug>/play/cutscene/',
         GetPlayerCutsceneAPIView.as_view(), name='get-player-cutscene'),

    path('games/v/<str:slug>/play/cutscene/actions/v/<str:action_type_slug>/to/<str:cutscene_slug>/',
         PlayerCutsceneChangeAPIView.as_view(), name='change-player-cutscene'),

    path('games/v/<str:slug>/play/conversations/v/<str:conversation_slug>/',
         PlayerConversationAPIView.as_view(), name='player-conversation-view'),

    path('games/v/<str:slug>/play/conversations/v/<str:conversation_slug>/start/',
         PlayerConversationStartAPIView.as_view(), name='player-conversation-start'),

    path('games/v/<str:slug>/play/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/',
         PlayerConversationDialogueAPIView.as_view(), name='player-conversation-dialogue'),

    path('games/v/<str:slug>/play/conversations/v/<str:conversation_slug>/dialogue/v/<str:dialogue_slug>/responses/',
         PlayerDialogueResponseListAPIView.as_view(), name='player-conversation-dialogue-response')

    # path('games/v/<str:slug>/', game_view)
]
