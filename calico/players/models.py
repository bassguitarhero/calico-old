from django.db import models
from django.urls import reverse
from django.conf import settings
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete
from django.dispatch import receiver
import uuid

# from django.core.files.storage import default_storage as storage

from time import time

from engine.utils import unique_slug_generator

from PIL import Image, _imaging
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose, ResizeToFill

from engine.models import Game, Character, Level, Room, Page, Stage, Cutscene, Conversation

# Create your models here.


def get_game_upload_file_name(instance, filename):
    return settings.GAME_UPLOAD_FILE_PATTERN % (str(time()).replace('.', '_'), filename)


def pre_save_slug_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


def delete_file_from_s3(sender, instance, using, **kwargs):
    if instance.image:
        instance.image.delete(save=False)
    if instance.thumbnail:
        instance.thumbnail.delete(save=False)


class PlayerStage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='game_player_stages', null=True, on_delete=models.CASCADE
    )
    stage = models.ForeignKey(
        Stage, related_name='player_stages', null=True, on_delete=models.CASCADE
    )
    room = models.ForeignKey(
        Room, related_name='player_rooms', null=True, on_delete=models.CASCADE
    )
    completed = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.stage != None:
            return self.game.name
        else:
            return 'Player Stage'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PlayerCharacter(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4, editable=False)
    character = models.ForeignKey(
        Character, related_name='players', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    active = models.BooleanField(default=False)
    strength = models.IntegerField(default=5)
    skill = models.IntegerField(default=5)
    speed = models.IntegerField(default=5)
    health = models.IntegerField(default=5)
    wisdom = models.IntegerField(default=5)
    luck = models.IntegerField(default=5)
    hp = models.IntegerField(default=6)
    keys = models.IntegerField(default=0)
    bombs = models.IntegerField(default=0)
    damage = models.IntegerField(default=10)
    can_walk = models.BooleanField(default=True)
    can_fly = models.BooleanField(default=False)
    can_climb = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=PlayerCharacter)
post_delete.connect(delete_file_from_s3, sender=PlayerCharacter)


class PlayerGame(models.Model):
    game = models.ForeignKey(
        Game, related_name="games_player", on_delete=models.CASCADE)
    character = models.ForeignKey(
        PlayerCharacter, null=True, related_name="characters", on_delete=models.SET_NULL
    )
    stage = models.ForeignKey(
        PlayerStage, null=True, related_name='game_stages', on_delete=models.CASCADE
    )
    completed = models.BooleanField(default=False)
    completed_at = models.DateTimeField(null=True, blank=True)
    score = models.IntegerField(default=0)
    player_win = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.game != None:
            return self.game.name
        else:
            return 'Player Game'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PlayerPage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='game_player_pages', null=True, on_delete=models.CASCADE
    )
    player_game = models.ForeignKey(
        PlayerGame, related_name='game_player_pages', null=True, on_delete=models.CASCADE)
    page = models.ForeignKey(
        Page, related_name='page_players', null=True, on_delete=models.CASCADE
    )
    active = models.BooleanField(default=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.page != None:
            return '%s : Page %s' % (self.game.name, self.page.name)
        else:
            return 'Player Page'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PlayerLevel(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4, editable=False)
    game = models.ForeignKey(PlayerGame,
                             related_name='player_levels', null=True, on_delete=models.CASCADE)
    level = models.ForeignKey(
        Level, related_name='players', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.level != None:
            return self.level.name
        else:
            return 'Player Level'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PlayerCutscene(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4, editable=False)
    game = models.ForeignKey(PlayerGame,
                             related_name='player_cutscenes', null=True, on_delete=models.CASCADE)
    cutscene = models.ForeignKey(
        Cutscene, related_name='players', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.cutscene != None:
            return self.cutscene.name
        else:
            return 'Player Cutscene'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PlayerConversation(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4, editable=False)
    game = models.ForeignKey(PlayerGame,
                             related_name='player_conversations', null=True, on_delete=models.CASCADE)
    conversation = models.ForeignKey(
        Conversation, related_name='players', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.conversation != None:
            return self.conversation.name
        else:
            return 'Player Conversation'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PlayerRoom(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4, editable=False)
    player_level = models.ForeignKey(
        PlayerLevel, related_name='rooms', on_delete=models.CASCADE)
    room = models.ForeignKey(
        Room, related_name='players', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)
    door_top = models.IntegerField(default=0)
    door_right = models.IntegerField(default=0)
    door_bottom = models.IntegerField(default=0)
    door_left = models.IntegerField(default=0)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.player_level != None:
            return '%s : %s, %s' % (self.player_level.name, self.room.position_x, self.room.position_y)
        else:
            return 'Player Room'

    class Meta:
        ordering = ["-last_edited", "-created_at"]
