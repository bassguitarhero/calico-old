from django.contrib import admin
from .models import PlayerCharacter, PlayerLevel, PlayerRoom, PlayerGame, PlayerPage, PlayerCutscene

# Register your models here.
admin.site.register(PlayerCharacter)
admin.site.register(PlayerLevel)
admin.site.register(PlayerRoom)
admin.site.register(PlayerGame)
admin.site.register(PlayerPage)
admin.site.register(PlayerCutscene)
