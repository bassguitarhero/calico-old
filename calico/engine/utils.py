import random
import string
from django.utils.text import slugify

from itertools import tee

from urllib.parse import urlparse, parse_qs

from urllib.request import urlopen, HTTPError

import json


'''
This code is from

https://www.codingforentrepreneurs.com/blog/random-string-generator-in-python/
https://www.codingforentrepreneurs.com/blog/a-unique-slug-generator-for-django/

'''



def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def unique_email_code_generator(instance, new_slug=None):
    email_code = random_string_generator()

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(code=email_code).exists()
    if qs_exists:
        return unique_slug_generator(instance, new_slug=new_slug)
    return email_code

def comment_slug_generator(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance 
    has a model with a slug field and a name character (char) field.
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.comment)
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
                    slug=slug,
                    randstr=random_string_generator(size=4)
                )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug

def unique_slug_generator(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance 
    has a model with a slug field and a name character (char) field.
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.name)
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
                    slug=slug,
                    randstr=random_string_generator(size=4)
                )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug

def get_youtube_id(url=1):
#   REGEX CODE:
#   ((?<=(v|V)/)|(?<=be/)|(?<=(\?|\&)v=)|(?<=embed/))([\w-]+)
    u_pars = urlparse(url)
    quer_v = parse_qs(u_pars.query).get('v')
    if quer_v:
        return quer_v[0]
    pth = u_pars.path.split('/')
    if pth:
        return pth[-1]

def pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def getplace(lat, lon):
    key = "AIzaSyBH8eUndyrxLiXM9b4nc0ZL8kqoOFnOwzA"
    url = "https://maps.googleapis.com/maps/api/geocode/json?"
    url += "latlng=%s,%s&sensor=false&key=%s" % (lat, lon, key)
    v = urlopen(url).read()
    j = json.loads(v)
    if len(j['results']) > 0:
        components = j['results'][0]['address_components']
        country = city = None
        for c in components:
            if "country" in c['types']:
                country = c['long_name']
            if "locality" in c['types']:
                city = c['long_name']
        return city, country
    else:
        country = city = None
        return city, country 