from django.urls import path, include
# from . import views
from .views import home_view, game_view, game_api_view

urlpatterns = [
    path('api/games/v/<str:slug>/', game_api_view),
    path('games/v/<str:slug>/', game_view),
    path('', home_view)
]
