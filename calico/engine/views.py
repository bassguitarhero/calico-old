from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render

from .models import Game

# Create your views here.


def home_view(request, *args, **kwargs):
    games = Game.objects.all()
    args = {'games': games}
    return render(request, 'home.html', args)


def game_view(request, slug, *args, **kwargs):
    try:
        obj = Game.objects.get(slug=slug)
    except Game.DoesNotExist:
        raise Http404
    # return HttpResponse(f"Game slug: {obj.slug}")
    args = {'game': obj}
    return render(request, 'game.html', args)


def game_api_view(request, slug, *args, **kwargs):
    try:
        obj = Game.objects.get(slug=slug)
    except Game.DoesNotExist:
        return JsonResponse({"message": "Not Found"}, status_code=404)
    return JsonResponse({"id": obj.id})

# class HomeView():
#   pass
