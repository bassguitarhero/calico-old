from django.db import models
from django.urls import reverse
from django.conf import settings
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete
from django.dispatch import receiver
import uuid

from django.core.files.storage import default_storage as storage

from time import time

from .utils import unique_slug_generator

from PIL import Image, _imaging
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose, ResizeToFill

# Create your models here.


def get_game_upload_file_name(instance, filename):
    return settings.GAME_UPLOAD_FILE_PATTERN % (str(time()).replace('.', '_'), filename)


def pre_save_slug_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


def delete_file_from_s3(sender, instance, using, **kwargs):
    if instance.image:
        instance.image.delete(save=False)
    if instance.thumbnail:
        instance.thumbnail.delete(save=False)


class GameType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
                                Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class GameActionType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
                                Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    game_types = models.ManyToManyField(
        GameType, related_name='actions')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Game(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
                                Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    map_width = models.IntegerField(default=800)
    map_height = models.IntegerField(default=416)
    tile_width = models.IntegerField(default=32)
    tile_height = models.IntegerField(default=32)
    sprite_width = models.IntegerField(default=32)
    sprite_height = models.IntegerField(default=32)
    public = models.BooleanField(default=True)
    published = models.BooleanField(default=False)
    published_date = models.DateTimeField(null=True, blank=True)
    collaborators = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="collaborations")
    players = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="private_games")
    game_type = models.ForeignKey(
        GameType, related_name='games', on_delete=models.SET_NULL, null=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def stages_count(self):
        return self.stages.count()

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Game)
post_delete.connect(delete_file_from_s3, sender=Game)


class Weapon(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='weapons', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Weapon)
post_delete.connect(delete_file_from_s3, sender=Weapon)


class Item(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='items', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Item)
post_delete.connect(delete_file_from_s3, sender=Item)


# @receiver(post_delete, sender=Game)
class Character(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='characters', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    sprite_sheet = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='PNG', null=True, blank=True)
    sprite_sheet_thumbnail = ImageSpecField(source='sprite_sheet', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    base_strength = models.IntegerField(default=5)
    base_skill = models.IntegerField(default=5)
    base_speed = models.IntegerField(default=5)
    base_health = models.IntegerField(default=5)
    base_wisdom = models.IntegerField(default=5)
    base_luck = models.IntegerField(default=5)
    base_hp = models.IntegerField(default=6)
    base_keys = models.IntegerField(default=0)
    base_bombs = models.IntegerField(default=0)
    base_damage = models.IntegerField(default=10)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def game_slug(self):
        return self.game.slug

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Character)
post_delete.connect(delete_file_from_s3, sender=Character)

# @receiver(post_delete, sender=Character)


class NonPlayableCharacterAttack(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='npcharacter_attacks', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    sprite_sheet = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='PNG', null=True, blank=True)
    sprite_sheet_thumbnail = ImageSpecField(source='sprite_sheet', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    description = models.TextField(null=True, blank=True)
    level = models.IntegerField(default=1)
    contact_damage = models.BooleanField(default=True)
    contact_damage_amount = models.IntegerField(default=1)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=NonPlayableCharacterAttack)
post_delete.connect(delete_file_from_s3, sender=NonPlayableCharacterAttack)


class NonPlayableCharacter(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='npcharacters', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    sprite_sheet = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='PNG', null=True, blank=True)
    sprite_sheet_thumbnail = ImageSpecField(source='sprite_sheet', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    description = models.TextField(null=True, blank=True)
    health = models.IntegerField(default=0)
    speed = models.IntegerField(default=0)
    level = models.IntegerField(default=1)
    hostile = models.BooleanField(default=True)
    contact_damage = models.BooleanField(default=True)
    contact_damage_amount = models.IntegerField(default=1)
    can_walk = models.BooleanField(default=True)
    can_fly = models.BooleanField(default=False)
    can_attack = models.BooleanField(default=True)
    attacks = models.ManyToManyField(NonPlayableCharacterAttack)
    has_movement = models.BooleanField(default=False)
    movement_type = models.IntegerField(default=0)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=NonPlayableCharacter)
post_delete.connect(delete_file_from_s3, sender=NonPlayableCharacter)

# @receiver(post_delete, sender=NonPlayableCharacter)


class Conversation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='conversations', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    show_character = models.BooleanField(default=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


pre_save.connect(pre_save_slug_receiver, sender=Conversation)
post_delete.connect(delete_file_from_s3, sender=Conversation)


class Dialogue(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    conversation = models.ForeignKey(
        Conversation, null=True, related_name='conversation_dialogues', on_delete=models.CASCADE)
    opening_dialogue = models.BooleanField(default=False)
    ending_dialogue = models.BooleanField(default=False)
    embed_facebook = models.CharField(max_length=256, null=True)
    embed_twitter = models.TextField(null=True, blank=True)
    embed_youtube = models.CharField(max_length=256, null=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


pre_save.connect(pre_save_slug_receiver, sender=Dialogue)
post_delete.connect(delete_file_from_s3, sender=Dialogue)


class DialogueResponse(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    dialogue = models.ForeignKey(
        Dialogue, null=True, related_name='dialogue_responses', on_delete=models.CASCADE)
    connect_to = models.ForeignKey(
        Dialogue, null=True, related_name='dialogue_response_connections', on_delete=models.CASCADE)
    ending_response = models.BooleanField(default=False)
    order = models.IntegerField(default=1)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order", "-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=DialogueResponse)
post_delete.connect(delete_file_from_s3, sender=DialogueResponse)


class CutsceneType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


pre_save.connect(pre_save_slug_receiver, sender=CutsceneType)
post_delete.connect(delete_file_from_s3, sender=CutsceneType)


class CutsceneActionType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
                                Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    cutscene_types = models.ManyToManyField(
        CutsceneType, blank=True, related_name='actions')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


pre_save.connect(pre_save_slug_receiver, sender=CutsceneActionType)
post_delete.connect(delete_file_from_s3, sender=CutsceneActionType)


class Cutscene(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='cutscenes', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    conversation = models.ForeignKey(
        Conversation, null=True, on_delete=models.SET_NULL)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    url_youtube = models.CharField(max_length=256, null=True)
    url_vimeo = models.CharField(max_length=256, null=True)
    url_archive = models.CharField(max_length=256, null=True)
    complete_required = models.BooleanField(default=False)
    autoplay = models.BooleanField(default=True)
    loop = models.BooleanField(default=False)
    cutscene_type = models.ForeignKey(
        CutsceneType, related_name='cutscenes', null=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Cutscene)
post_delete.connect(delete_file_from_s3, sender=Cutscene)


class LevelType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class LevelActionType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    level_types = models.ManyToManyField(
        LevelType, related_name='actions')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Tile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='tiles', on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    # Change image to PNG to preserve transperancy
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='PNG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    walkable = models.BooleanField(default=True)
    climbable = models.BooleanField(default=False)
    swimmable = models.BooleanField(default=False)
    flyable = models.BooleanField(default=False)
    display = models.BooleanField(default=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Tile)
post_delete.connect(delete_file_from_s3, sender=Tile)


class LevelTile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='level_tiles', on_delete=models.CASCADE)
    tile = models.ForeignKey(
        Tile, related_name='level_tiles', on_delete=models.CASCADE)
    display = models.BooleanField(default=True)
    position_x = models.IntegerField(default=0)
    position_y = models.IntegerField(default=0)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.tile.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class LevelNPCharacter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    npcharacter = models.ForeignKey(
        NonPlayableCharacter, related_name='levels', on_delete=models.CASCADE)
    display = models.BooleanField(default=True)
    position_x = models.IntegerField(default=0)
    position_y = models.IntegerField(default=0)
    randomize = models.BooleanField(default=False)
    health = models.IntegerField(default=0)
    boss = models.BooleanField(default=False)
    hostile = models.BooleanField(default=True)
    conversation = models.ForeignKey(
        Conversation, null=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.npcharacter.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class Level(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='levels', on_delete=models.CASCADE)
    order = models.IntegerField(default=1)
    starting_level = models.BooleanField(default=False)
    starting_x = models.IntegerField(default=0)
    starting_y = models.IntegerField(default=0)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    level_type = models.ForeignKey(
        LevelType, related_name='levels', on_delete=models.SET_NULL, null=True)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    tile_width = models.IntegerField(default=0)
    tile_height = models.IntegerField(default=0)
    level_tiles = models.ManyToManyField(LevelTile, related_name='levels')
    level_npcharacters = models.ManyToManyField(
        LevelNPCharacter, related_name='levels')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Level)
post_delete.connect(delete_file_from_s3, sender=Level)


# @receiver(post_delete, sender=Level)


class Background(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='backgrounds', on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Background)
post_delete.connect(delete_file_from_s3, sender=Background)


class RoomType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class RoomActionType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    room_types = models.ManyToManyField(
        RoomType, related_name='actions')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]

# @receiver(post_delete, sender=Background)


class Room(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    level = models.ForeignKey(
        Level, related_name='rooms', on_delete=models.CASCADE, null=True, blank=True)
    background = models.ForeignKey(
        Background, related_name='rooms', on_delete=models.CASCADE)
    room_type = models.ForeignKey(
        RoomType, related_name='rooms', null=True, on_delete=models.SET_NULL)
    starting_room = models.BooleanField(default=False)
    position_x = models.IntegerField(null=True, blank=True)
    position_y = models.IntegerField(null=True, blank=True)
    door_top = models.IntegerField(default=0)
    door_right = models.IntegerField(default=0)
    door_bottom = models.IntegerField(default=0)
    door_left = models.IntegerField(default=0)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s : %s, %s' % (self.level.name, self.position_x, self.position_y)

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PageType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class PageActionType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    page_types = models.ManyToManyField(
        PageType, related_name='actions')
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Page(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(Game, related_name='pages',
                             null=True, on_delete=models.CASCADE)
    page_type = models.ForeignKey(
        PageType, related_name='pages', null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=256, null=True)
    slug = models.SlugField(unique=True, null=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], null=True, format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    youtube_url = models.CharField(max_length=256, null=True, blank=True)
    youtube_id = models.CharField(max_length=256, null=True, blank=True)
    page_number = models.IntegerField(default=1)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s : Page %s' % (self.game.name, self.page_number)

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=Page)
post_delete.connect(delete_file_from_s3, sender=Page)


class Stage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='stages', on_delete=models.CASCADE
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.game.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


# ACTIONS
class GameAction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    game = models.ForeignKey(
        Game, related_name='actions', null=True, on_delete=models.SET_NULL)
    action_type = models.ForeignKey(
        GameActionType, related_name='actions', null=True, on_delete=models.SET_NULL)
    complete_game = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Game Action'

    def name(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Game Action'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class CutsceneAction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=256, null=True)
    slug = models.SlugField(unique=True, null=True)
    game = models.ForeignKey(
        Game, related_name='cutscene_actions', null=True, on_delete=models.SET_NULL)
    cutscene = models.ForeignKey(
        Cutscene, related_name='cutscene_actions', null=True, on_delete=models.SET_NULL)
    action_type = models.ForeignKey(
        CutsceneActionType, related_name='cutscene_actions', null=True, on_delete=models.SET_NULL)
    game_action_type = models.ForeignKey(
        GameActionType, related_name='cutscene_game_action', null=True, on_delete=models.CASCADE)
    complete_required = models.BooleanField(default=False)
    change_level = models.ForeignKey(
        Level, null=True, related_name='cutscene_change_level', on_delete=models.SET_NULL
    )
    change_cutscene = models.ForeignKey(
        Cutscene, null=True, related_name='cutscene_change_cutscene', on_delete=models.SET_NULL)
    auto_select = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Cutscene Action'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=CutsceneAction)
# post_delete.connect(delete_file_from_s3, sender=Background)


class LevelAction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    action_type = models.ForeignKey(
        LevelActionType, related_name='actions', null=True, on_delete=models.SET_NULL)
    game_action_type = models.ForeignKey(
        GameActionType, related_name='level_game_action', null=True, on_delete=models.CASCADE)
    cutscene_action_type = models.ForeignKey(
        CutsceneActionType, related_name='level_cutscene_action', null=True, on_delete=models.SET_NULL)
    game_actions = models.ManyToManyField(
        GameAction, related_name='level_actions')
    level = models.ForeignKey(
        Level, related_name='level_actions', null=True, on_delete=models.SET_NULL
    )
    change_level = models.ForeignKey(
        Level, null=True, related_name='level_change_actions', on_delete=models.SET_NULL
    )
    change_cutscene = models.ForeignKey(
        Cutscene, null=True, related_name='level_change_cutscene', on_delete=models.SET_NULL)
    complete_level = models.BooleanField(default=False)
    complete_required = models.BooleanField(default=False)
    level_tile = models.ForeignKey(
        LevelTile, null=True, related_name='level_tile_actions', on_delete=models.SET_NULL)
    defeat_all_enemies = models.BooleanField(default=False)
    defeat_boss = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Level Action'

    def name(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Level Action'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class RoomAction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    action_type = models.ForeignKey(
        RoomActionType, related_name='actions', null=True, on_delete=models.SET_NULL)
    game_actions = models.ManyToManyField(
        GameAction, related_name='room_actions')
    level_actions = models.ManyToManyField(
        LevelAction, related_name="room_actions")
    room = models.ForeignKey(Room, related_name='actions',
                             null=True, on_delete=models.SET_NULL)
    complete_room = models.BooleanField(default=False)
    complete_required = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Room Action'

    def name(self):
        if self.action_type != None:
            return self.action_type.name
        else:
            return 'Room Action'

    class Meta:
        ordering = ["-last_edited", "-created_at"]


class PageAction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    action_type = models.ForeignKey(
        PageActionType, related_name='actions', null=True, on_delete=models.SET_NULL)
    game_action_type = models.ForeignKey(
        GameActionType, related_name='page_single_action_types', null=True, on_delete=models.CASCADE)
    game_actions = models.ManyToManyField(
        GameAction, related_name='page_multi_actions')
    page = models.ForeignKey(Page, related_name='actions',
                             null=True, on_delete=models.CASCADE)
    go_to_page = models.ForeignKey(
        Page, related_name='go_to_page', null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=256, null=True)
    slug = models.SlugField(unique=True, null=True)
    image = ProcessedImageField(upload_to=get_game_upload_file_name, processors=[
        Transpose(), ], null=True, format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    description = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_slug_receiver, sender=PageAction)
post_delete.connect(delete_file_from_s3, sender=PageAction)
