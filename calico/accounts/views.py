from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q, Max, Count
from django.core.mail import EmailMessage, send_mail
# from django.db.models import Avg, Max, Min, Sum

from django.shortcuts import get_object_or_404, render, HttpResponseRedirect
from django.template.context_processors import csrf
from django.core.mail import EmailMessage

from django.template.loader import get_template

from django.utils.dateparse import parse_datetime
import pytz

import datetime
import time
import decimal
from django.utils import timezone
import copy
import json

import csv
from io import TextIOWrapper

from rest_framework import generics, permissions, pagination, status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, ParseError, NotFound
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.parsers import FileUploadParser, MultiPartParser, JSONParser, FormParser
from knox.models import AuthToken
from knox.auth import TokenAuthentication

from PIL import Image

from .serializers import UserSerializer, RegisterSerializer, LoginSerializer, ResetPasswordRequestSerializer, ResetPasswordSerializer, ContactAppSerializer
from engine.utils import unique_email_code_generator
from profiles.models import Profile

# Create your views here.

# Register API


class RegisterAPIView(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        if request.data['termsConditions'] == True:
            serializer = RegisterSerializer(data=request.data, context={
                                            'termsConditions': request.data['termsConditions']})
            serializer.is_valid(raise_exception=True)
            user = serializer.save()
            profile = Profile.objects.create(
                created_by=user)

            verify_url = "http://127.0.0.1:8000/api/profiles/v/%s/verify/%s/" % (
                user.username, profile.code)
            verify_message = "Thanks for signing up with THIIS. Click %s to verify your email address." % (
                verify_url)
            template = get_template('contact_template.txt')
            context = {
                'name': user.username,
                'email': user.email,
                'message': verify_message,
            }
            content = template.render(context)
            email = EmailMessage(
                "Wandereo: New Account",
                content,
                "Wandereo" + '',
                [user.email],
                headers={'Reply-to': user.email
                         })
            email.send()

            return Response({
                "user": UserSerializer(user, context=self.get_serializer_context()).data,
                "token": AuthToken.objects.create(user)[1]
            })
        else:
            return None


# Login API
class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })

# Logout API
# class LogoutAPI(generics.GenericAPIView):
# 	permission_classes = (AllowAny,)

#     def post(self, request, format=None):
#         # simply delete the token to force a login
#         request.user.auth_token.delete()
#         return Response(status=status.HTTP_200_OK)


# Get User API
class UserAPIView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated]

    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

    # def get_object(self):
    # 	if self.request.user.is_authenticated:
    # 		return self.request.user
    # 	else:
    # 		return None

# class ResetPasswordRequestAPI(generics.GenericAPIView):
# 	serializer_class = ResetPasswordRequestSerializer
# 	permission_classes = [permissions.AllowAny,]

# 	def post(self, request, *args, **kwargs):
# 		serializer = self.get_serializer(data=request.data)
# 		serializer.is_valid(raise_exception=True)
# 		data = request.data
# 		if UserProfile.objects.filter(user__username=data['username'], user__email=data['email']).exists():
# 			profile = UserProfile.objects.get(user__username=data['username'], user__email=data['email'])
# 			profile.code = unique_email_code_generator(profile)
# 			profile.save()
# 			subject = "Wander: Reset Your Password."
# 			message = "You have requested to reset your password. Please visit https://wandereo.com/#/reset/%s/%s/ to reset your password." % (profile.user.username, profile.code)
# 			from_email = settings.EMAIL_HOST_USER
# 			to_list = [profile.user.email, settings.EMAIL_HOST_USER]
# 			send_mail(subject, message, from_email, to_list, fail_silently=True)
# 			return Response(serializer.data, status=status.HTTP_200_OK)
# 		return Response(serializer.data, status=status.HTTP_404_NOT_FOUND)

# class ResetPasswordAPI(generics.GenericAPIView):
# 	serializer_class = ResetPasswordSerializer
# 	permission_classes = [permissions.AllowAny,]

# 	def post(self, request, *args, **kwargs):
# 		serializer = self.get_serializer(data=request.data)
# 		serializer.is_valid(raise_exception=True)
# 		data = request.data
# 		if UserProfile.objects.filter(user__username=data['username'], code=data['code']).exists():
# 			profile = UserProfile.objects.get(user__username=data['username'], code=data['code'])
# 			user = profile.user
# 			user.password = make_password(data['password'])
# 			user.save()
# 			profile.code = unique_email_code_generator(profile)
# 			profile.save()
# 			return Response(serializer.data, status=status.HTTP_200_OK)
# 		return Response(serializer.data, status=status.HTTP_404_NOT_FOUND)


# class ContactAppAPI(generics.GenericAPIView):
# 	serializer_class = ContactAppSerializer
# 	permission_classes = [permissions.AllowAny,]

# 	def post(self, request, *args, **kwargs):
# 		serializer = self.get_serializer(data=request.data)
# 		serializer.is_valid(raise_exception=True)
# 		data = request.data
# 		contact_name = data['name']
# 		contact_email = data['email']
# 		contact_message = data['message']
# 		template = get_template('contact_template.txt')
# 		context = {
# 			'name': contact_name,
# 			'email': contact_email,
# 			'message': contact_message,
# 		}
# 		content = template.render(context)
# 		email = EmailMessage(
# 			"New Wandereo Contact",
# 			content,
# 			"Wandereo" +'',
# 			['jboyce.video@gmail.com'],
# 			headers = {'Reply-to': contact_email
# 		})
# 		email.send()
# 		return Response(serializer.data, status=status.HTTP_404_NOT_FOUND)


# def send_verification(request, user):
# 	subject = "Thank you for signing up."
# 	verify_code = user.user_profile.code
# 	verify_link = "profiles/v/%s/verify/%s" % (user.username, user.profile.code)
# 	message = "Thank you for signing up. Please follow the link to https://wandereo.com/api/%s/ to verify your account." % verify_link
# 	from_email = settings.EMAIL_HOST_USER
# 	to_list = [user.email, settings.EMAIL_HOST_USER]
# 	template = get_template('contact_template.txt')
# 	context = {
# 		'name': contact_name,
# 		'email': contact_email,
# 		'message': contact_message,
# 	}
# 	content = template.render(context)
# 	email = EmailMessage(
# 		"Update Reported",
# 		content,
# 		"Wandereo" +'',
# 		['jboyce.video@gmail.com'],
# 		headers = {'Reply-to': contact_email
# 	})
# 	email.send()

# 	send_mail(subject, message, from_email, to_list, fail_silently=True)

# 	messages.success(request, "Verification email sent.")
# 	return

# def re_verify_email(request):
# 	if request.user.is_authenticated():
# 		user_profile = UserProfile.objects.filter(user=request.user)[0]
# 		if not user_profile.code:
# 			code = user_profile.save()

# 		send_verification(request, request.user)
# 		messages.success(request, "Re-Verification email sent.")
# 		return redirect('/')
# 	else:
# 		messages.success(request, "Please login to send re-verification email.")
# 		return redirect('/')
