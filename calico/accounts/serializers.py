from django.contrib.auth import get_user_model, authenticate, login, logout
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
# User = get_user_model()

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, ParseError
from rest_framework.relations import HyperlinkedIdentityField
from rest_framework.reverse import reverse
from rest_framework.fields import CurrentUserDefault

from api.serializers import ProfileSerializer
from profiles.models import Profile

# User Serializer


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'profile')

# Register Serializer


class RegisterSerializer(serializers.ModelSerializer):
    termsConditions = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'termsConditions')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        if self.context['termsConditions'] == True:
            if User.objects.filter(username__iexact=validated_data['username']).exists():
                raise serializers.ValidationError(
                    "A user with this username already exists")
            if User.objects.filter(email__iexact=validated_data['email']).exists():
                raise serializers.ValidationError(
                    "A user with this email address already exists")
            user = User.objects.create_user(
                validated_data['username'], validated_data['email'], validated_data['password'])
            user.set_password(validated_data['password'])
            user.save()
            return user
        else:
            raise serializers.ValidationError(
                "Terms and Conditions not Agreed")

# Login Serializer


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        print("Hm?")
        user = authenticate(**data)
        if user and user.is_active:
            return UserSerializer(user=user)
        raise serializers.ValidationError("Incorrect Credentials")


class ResetPasswordRequestSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.CharField()


class ResetPasswordSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class ContactAppSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.CharField()
    message = serializers.CharField()
