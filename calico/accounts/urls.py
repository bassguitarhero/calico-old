from django.urls import path, include
# from . import views
from .views import UserAPIView, LoginAPIView, RegisterAPIView
from knox import views as knox_views

urlpatterns = [
    path('', include('knox.urls')),
    path('register/', RegisterAPIView.as_view()),
    path('login/', LoginAPIView.as_view()),
    path('user/', UserAPIView.as_view()),
    path('logout/', knox_views.LogoutView.as_view(), name='knox_logout'),
    # path('api/auth/password/reset/request/', ResetPasswordRequestAPI.as_view()),
    # path('api/auth/password/reset/', ResetPasswordAPI.as_view()),
    # path('api/contact-app/', ContactAppAPI.as_view())
]
