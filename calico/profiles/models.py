from django.db import models
from django.urls import reverse
from django.conf import settings
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete
from django.dispatch import receiver
import uuid

# from django.core.files.storage import default_storage as storage

from time import time

from PIL import Image, _imaging
from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import SmartResize, Transpose, ResizeToFill

from engine.utils import unique_email_code_generator

# Create your models here.


def get_profile_upload_file_name(instance, filename):
    return settings.PROFILE_UPLOAD_FILE_PATTERN % (str(time()).replace('.', '_'), filename)


def pre_save_user_profile(sender, instance, *args, **kwargs):
    if not instance.code:
        instance.code = unique_email_code_generator(instance)


def delete_file_from_s3(sender, instance, using, **kwargs):
    if instance.image:
        instance.image.delete(save=False)
    if instance.thumbnail:
        instance.thumbnail.delete(save=False)


class Profile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, related_name='profile')
    code = models.CharField(max_length=120, blank=True)
    image = ProcessedImageField(upload_to=get_profile_upload_file_name, processors=[
                                Transpose(), ], format='JPEG')
    thumbnail = ImageSpecField(source='image', processors=[ResizeToFill(
        400, 400)], format='JPEG', options={'quality': 60})
    content = models.TextField(null=True, blank=True)
    create_content = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profiles_created')
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)

    # def user(self):
    #     return self.created_by

    def __str__(self):
        return self.created_by.username

    class Meta:
        ordering = ["-last_edited", "-created_at"]


pre_save.connect(pre_save_user_profile, sender=Profile)
post_delete.connect(delete_file_from_s3, sender=Profile)

# @receiver(post_delete, sender=Profile)
