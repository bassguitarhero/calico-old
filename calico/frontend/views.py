from django.shortcuts import render

# Create your views here.


def index(request, *args, **kwargs):
    args = {}
    return render(request, 'index.html', args)
