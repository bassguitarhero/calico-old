import { 
  GET_PROFILE, 
  LOADING_PROFILE,
  EDIT_PROFILE
} from '../actions/types';

const initialState = {
  profileDetail: null,
  loadingProfile: true,
  editProfileObj: null
};

export default function(state = initialState, action) {
	switch(action.type){
		case GET_PROFILE:
			return {
        ...state,
        profileDetail: action.payload
      };

		case LOADING_PROFILE:
			return {
        ...state,
        loadingProfile: action.payload
      };

    case EDIT_PROFILE:
      return {
        ...state,
        editProfileObj: action.payload
      };
      
		default:
			return state;
	}
}