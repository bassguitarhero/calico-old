import { combineReducers } from 'redux';
import auth from './auth';
import game_admin from './game_admin';
import games from './games';
import messages from './messages';
import player from './player';
import profiles from './profiles';

export default combineReducers({
  auth,
  game_admin,
  games,
  messages,
  player,
  profiles
});

