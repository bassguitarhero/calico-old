import { 
  NEW_GAME,
  EDIT_GAME,
  DELETE_GAME,

  GET_GAME_TYPES,

  NEW_CHARACTER,
  EDIT_CHARACTER,
  DELETE_CHARACTER,

  NEW_NPCHARACTER,
  EDIT_NPCHARACTER,
  DELETE_NPCHARACTER,

  GET_LEVEL_TYPES,
  GET_LEVEL_ACTION_TYPES,

  GET_LEVEL_ACTIONS,
  LOADING_LEVEL_ACTIONS,
  NEW_LEVEL_ACTION,
  EDIT_LEVEL_ACTION,
	DELETE_LEVEL_ACTION,

  NEW_LEVEL,
  EDIT_LEVEL,
  DELETE_LEVEL,

  NEW_BACKGROUND,
  EDIT_BACKGROUND,
  DELETE_BACKGROUND,

  NEW_WEAPON,
  EDIT_WEAPON,
  DELETE_WEAPON,

  NEW_ITEM,
  EDIT_ITEM,
  DELETE_ITEM,

  NEW_PAGE,
  EDIT_PAGE,
  DELETE_PAGE,

  GET_PAGE_ACTION_TYPES,
  GET_PAGE_ACTIONS,
  LOADING_PAGE_ACTIONS,
	NEW_PAGE_ACTION,
	EDIT_PAGE_ACTION,
  DELETE_PAGE_ACTION,
  
  GET_GAME_ACTION_TYPES,
  LOADING_GAME_ACTION_TYPES,
  
  NEW_GAME_TILE,
	EDIT_GAME_TILE,
  DELETE_GAME_TILE,
  
  NEW_LEVEL_TILE,
	EDIT_LEVEL_TILE,
  DELETE_LEVEL_TILE,
  
  NEW_LEVEL_NPCHARACTER,
	EDIT_LEVEL_NPCHARACTER,
  DELETE_LEVEL_NPCHARACTER,
  
  NEW_CUTSCENE,
	EDIT_CUTSCENE,
	DELETE_CUTSCENE,

	GET_CUTSCENE_ACTION_TYPES,
	LOADING_CUTSCENE_ACTION_TYPES,

  NEW_CUTSCENE_ACTION, 
	EDIT_CUTSCENE_ACTION, 
	DELETE_CUTSCENE_ACTION,

  NEW_CONVERSATION,
	EDIT_CONVERSATION,
	DELETE_CONVERSATION,

  NEW_CONVERSATION_DIALOGUE,
  EDIT_CONVERSATION_DIALOGUE,
  DELETE_CONVERSATION_DIALOGUE,

  NEW_DIALOGUE_RESPONSE,
  EDIT_DIALOGUE_RESPONSE,
  DELETE_DIALOGUE_RESPONSE
} from '../actions/types';

const initialState = {
  newGameObj: null,
  editGameObj: null,
  deleteGameObj: null,
  
  gameTypes: null,

  newCharacterObj: null,
  editCharacterObj: null,
  deleteCharacterObj: null,

  newNPCharacterObj: null,
  editNPCharacterObj: null,
  deleteNPCharacterObj: null,

  newLevelObj: null,
  editLevelObj: null,
  deleteLevelObj: null,

  levelTypes: null,
  levelActionTypes: null,
  levelActions: null,
  loadingLevelActions: true,
  newLevelActionObj: null,
  editLevelActionObj: null,
  deleteLevelActionObj: null,

  newCutsceneObj: null,
  editCutsceneObj: null,
  deleteCutsceneObj: null,

  newConversationObj: null,
  editConversationObj: null,
  deleteConversationObj: null,

  newBackgroundObj: null,
  editBackgroundObj: null,
  deleteBackgroundObj: null,

  newWeaponObj: null,
  editWeaponObj: null,
  deleteWeaponObj: null,

  newItemObj: null,
  editItemObj: null,
  deleteItemObj: null,

  newPageObj: null,
  editPageObj: null,
  deletePageObj: null,

  pageActionTypes: null,
  pageActions: null,
  loadingPageActions: true,

  newPageActionObj: null,
  editPageActionObj: null,
  deletePageActionObj: null,

  gameActionTypes: null,
  loadingGameActionTypes: true,

  newGameTileObj: null,
  editGameTileObj: null,
  deleteGameTileObj: null,

  newLevelTileObj: null,
  editLevelTileObj: null,
  deleteLevelTileObj: null,

  newLevelNPCharacterObj: null,
  editLevelNPCharacterObj: null,
  deleteLevelNPCharacterObj: null,

  cutsceneActionTypes: null,
  loadingCutsceneActionTypes: true,

  newCutsceneActionObj: null,
  editCutsceneActionObj: null,
  deleteCutsceneActionObj: null,

  newConversationDialogueObj: null,
  editConversationDialogueObj: null,
  deleteConversationDialogueObj: null,

  newDialogueResponseObj: null,
  editDialogueResponseObj: null,
  deleteDialogueResponseObj: null
}

export default function(state = initialState, action) {
	switch(action.type) {
    // GAME
    case NEW_GAME:
			return {
				...state,
				newGameObj: action.payload
      };
    case EDIT_GAME:
      return {
        ...state,
        editGameObj: action.payload
      };
    case DELETE_GAME:
      return {
        ...state,
        deleteGameObj: action.payload
      };

    case GET_GAME_TYPES:
      return {
        ...state,
        gameTypes: action.payload
      }

    // CHARACTER
    case NEW_CHARACTER:
			return {
				...state,
				newCharacterObj: action.payload
      };
    case EDIT_CHARACTER:
      return {
        ...state,
        editCharacterObj: action.payload
      };
    case DELETE_CHARACTER:
      return {
        ...state,
        deleteCharacterObj: action.payload
      };


    // NPCHARACTER
    case NEW_NPCHARACTER:
			return {
				...state,
				newNPCharacterObj: action.payload
      };
    case EDIT_NPCHARACTER:
      return {
        ...state,
        editNPCharacterObj: action.payload
      };
    case DELETE_NPCHARACTER:
      return {
        ...state,
        deleteNPCharacterObj: action.payload
      };


    // LEVEL
    case NEW_LEVEL:
			return {
				...state,
				newLevelObj: action.payload
      };
    case EDIT_LEVEL:
      return {
        ...state,
        editLevelObj: action.payload
      };
    case DELETE_LEVEL:
      return {
        ...state,
        deleteLevelObj: action.payload
      };

    case GET_LEVEL_TYPES:
      return {
        ...state,
        levelTypes: action.payload
      }

    case GET_LEVEL_ACTION_TYPES:
      return {
        ...state,
        levelActionTypes: action.payload
      };

    case GET_LEVEL_ACTIONS:
      return {
        ...state,
        levelActions: action.payload
      }

    case LOADING_LEVEL_ACTIONS:
      return {
        ...state,
        loadingLevelActions: action.payload
      };

    case NEW_LEVEL_ACTION:
      return {
        ...state,
        levelAction: action.payload,
        newLevelActionObj: action.payload
      }

    case EDIT_LEVEL_ACTION:
      return {
        ...state,
        editLevelActionObj: action.payload
      };

    case DELETE_LEVEL_ACTION:
      return {
        ...state,
        deleteLevelActionObj: action.payload
      };

    // CUTSCENE
    case NEW_CUTSCENE:
			return {
				...state,
				newCutsceneObj: action.payload
      };
    case EDIT_CUTSCENE:
      return {
        ...state,
        editCutsceneObj: action.payload
      };
    case DELETE_CUTSCENE:
      return {
        ...state,
        deleteCutsceneObj: action.payload
      };

    // CONVERSATION
    case NEW_CONVERSATION:
			return {
				...state,
				newConversationObj: action.payload
      };
    case EDIT_CONVERSATION:
      return {
        ...state,
        editConversationObj: action.payload
      };
    case DELETE_CONVERSATION:
      return {
        ...state,
        deleteConversationObj: action.payload
      };

    // BACKGROUND
    case NEW_BACKGROUND:
			return {
				...state,
				newBackgroundObj: action.payload
      };
    case EDIT_BACKGROUND:
      return {
        ...state,
        editBackgroundObj: action.payload
      };
    case DELETE_BACKGROUND:
      return {
        ...state,
        deleteBackgroundObj: action.payload
      };
    

    // WEAPON
    case NEW_WEAPON:
			return {
				...state,
				newWeaponObj: action.payload
      };
    case EDIT_WEAPON:
      return {
        ...state,
        editWeaponObj: action.payload
      };
    case DELETE_WEAPON:
      return {
        ...state,
        deleteWeaponObj: action.payload
      };


    // ITEM
    case NEW_ITEM:
			return {
				...state,
				newItemObj: action.payload
      };
    case EDIT_ITEM:
      return {
        ...state,
        editItemObj: action.payload
      };
    case DELETE_ITEM:
      return {
        ...state,
        deleteItemObj: action.payload
      };

    // PAGE
    case NEW_PAGE:
			return {
				...state,
				newPageObj: action.payload
      };
    case EDIT_PAGE:
      return {
        ...state,
        editPageObj: action.payload
      };
    case DELETE_PAGE:
      return {
        ...state,
        deletePageObj: action.payload
      };

    case GET_PAGE_ACTION_TYPES:
      return {
        ...state,
        pageActionTypes: action.payload
      };
    case GET_PAGE_ACTIONS:
      return {
        ...state,
        pageActions: action.payload
      };
    case LOADING_PAGE_ACTIONS:
      return {
        ...state,
        loadingPageActions: action.payload
      };

    case NEW_PAGE_ACTION:
      return {
        ...state,
        newPageActionObj: action.payload
      };
    case EDIT_PAGE_ACTION:
      return {
        ...state,
        editPageActionObj: action.payload
      };
    case DELETE_PAGE_ACTION:
      return {
        ...state,
        deletePageActionObj: action.payload
      };




    case GET_GAME_ACTION_TYPES:
      return {
        ...state,
        gameActionTypes: action.payload
      };
    case LOADING_GAME_ACTION_TYPES:
      return {
        ...state,
        loadingGameActionTypes: action.payload
      };




    case NEW_GAME_TILE:
      return {
        ...state,
        newGameTileObj: action.payload
      };
    case EDIT_GAME_TILE:
      return {
        ...state,
        editGameTileObj: action.payload
      };
    case DELETE_GAME_TILE:
      return {
        ...state,
        deleteGameTileObj: action.payload
      };



    case NEW_LEVEL_TILE:
      return {
        ...state,
        newLevelTileObj: action.payload
      };
    case EDIT_LEVEL_TILE:
      return {
        ...state,
        editLevelTileObj: action.payload
      };
    case DELETE_LEVEL_TILE:
      return {
        ...state,
        deleteLevelTileObj: action.payload
      };

    case NEW_LEVEL_NPCHARACTER:
      return {
        ...state,
        newLevelNPCharacterObj: action.payload
      };
    case EDIT_LEVEL_NPCHARACTER:
      return {
        ...state,
        editLevelNPCharacterObj: action.payload
      };
    case DELETE_LEVEL_NPCHARACTER:
      return {
        ...state,
        deleteLevelNPCharacterObj: action.payload
      };

    case GET_CUTSCENE_ACTION_TYPES:
      return {
        ...state,
        cutsceneActionTypes: action.payload
      };
    case LOADING_CUTSCENE_ACTION_TYPES:
      return {
        ...state,
        loadingCutsceneActionTypes: action.payload
      };

    case NEW_CUTSCENE_ACTION:
      return {
        ...state,
        newCutsceneActionObj: action.payload
      };
    case EDIT_CUTSCENE_ACTION:
      return {
        ...state,
        editCutsceneActionObj: action.payload
      };
    case DELETE_CUTSCENE_ACTION:
      return {
        ...state,
        deleteCutsceneActionObj: action.payload
      };



    // CONVERSATION DIALOGUE
    case NEW_CONVERSATION_DIALOGUE:
			return {
				...state,
				newConversationDialogueObj: action.payload
      };
    case EDIT_CONVERSATION_DIALOGUE:
      return {
        ...state,
        editConversationDialogueObj: action.payload
      };
    case DELETE_CONVERSATION_DIALOGUE:
      return {
        ...state,
        deleteConversationDialogueObj: action.payload
      };


    // DIALOGUE RESPONSE
    case NEW_DIALOGUE_RESPONSE:
			return {
				...state,
				newDialogueResponseObj: action.payload
      };
    case EDIT_DIALOGUE_RESPONSE:
      return {
        ...state,
        editDialogueResponseObj: action.payload
      };
    case DELETE_DIALOGUE_RESPONSE:
      return {
        ...state,
        deleteDialogueResponseObj: action.payload
      };

    default: 
      return state;
  }
}