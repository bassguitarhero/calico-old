import { 
  GET_GAMES,
	LOADING_GAMES,
	GET_GAME_DETAIL,
	LOADING_GAME_DETAIL,
	GET_GAME_CHARACTERS,
	LOADING_GAME_CHARACTERS,
	GET_GAME_NP_CHARACTERS,
	LOADING_GAME_NP_CHARACTERS,
	GET_GAME_LEVELS,
	LOADING_GAME_LEVELS,
	GET_GAME_CUTSCENES,
	LOADING_GAME_CUTSCENES,
	GET_GAME_CONVERSATIONS,
  LOADING_GAME_CONVERSATIONS,
	GET_GAME_BACKGROUNDS,
	LOADING_GAME_BACKGROUNDS,
	GET_GAME_WEAPONS,
	LOADING_GAME_WEAPONS,
	GET_GAME_ITEMS,
	LOADING_GAME_ITEMS,
	GET_GAME_PAGES,
	LOADING_GAME_PAGES,
	GET_GAME_TILES,
	LOADING_GAME_TILES,
	GET_LEVEL_TILES,
	LOADING_LEVEL_TILES,
	GET_LEVEL_NPCHARACTERS, 
  LOADING_LEVEL_NPCHARACTERS,
	GET_CUTSCENE_DETAIL, 
	LOADING_CUTSCENE_DETAIL,
	GET_CUTSCENE_ACTIONS, 
  LOADING_CUTSCENE_ACTIONS,
	GET_GAME_CONVERSATION_DIALOGUES,
  LOADING_GAME_CONVERSATION_DIALOGUES,
  GET_DIALOGUE_RESPONSES,
  LOADING_DIALOGUE_RESPONSES
} from '../actions/types';

const initialState = {
  games: null,
	loadingGames: true,
	gameDetail: null,
	loadingGameDetail: true,
	gameCharacters: null,
	loadingGameCharacters: true,
	gameNPCharacters: null,
	loadingGameNPCharacters: true,
	gameLevels: null,
	loadingGameLevels: true,
	gameCutsceness: null,
	loadingGameCutscenes: true,
	gameConversations: null,
	loadingGameConversations: true,
	gameBackgrounds: null,
	loadingGameBackgrounds: true,
	gameWeapons: null,
	loadingGameWeapons: true,
	gameItems: null,
	loadingGameItems: true,
	gamePages: null,
	loadingGamePages: true,
	gameTiles: null,
	loadingGameTiles: true,
	levelTiles: null,
	loadingLevelTiles: true,
	levelNPCharacters: null,
	loadingLevelNPCharacters: true,
	cutsceneDetail: null,
	loadingCutsceneDetail: true,
	cutsceneActions: null,
	loadingCutsceneActions: true,
	gameConversationDialogues: null,
	loadingGameConversationDialogues: true,
	dialogueResponses: null,
	loadingDialogueResponses: true
}

export default function(state = initialState, action) {
	switch(action.type) {
		case GET_GAMES:
			return {
				...state,
				games: action.payload
			};
		case LOADING_GAMES:
			return {
				...state,
				loadingGames: action.payload
			};
			
		case GET_GAME_DETAIL:
			return {
				...state,
				gameDetail: action.payload
			};
		case LOADING_GAME_DETAIL:
			return {
				...state,
				loadingGameDetail: action.payload
			};

		case GET_GAME_CHARACTERS:
			return {
				...state,
				gameCharacters: action.payload
			};
		case LOADING_GAME_CHARACTERS:
			return {
				...state,
				loadingGameCharacters: action.payload
			};

		case GET_GAME_NP_CHARACTERS:
			return {
				...state,
				gameNPCharacters: action.payload
			};
		case LOADING_GAME_NP_CHARACTERS:
			return {
				...state,
				loadingGameNPCharacters: action.payload
			};

		case GET_GAME_LEVELS:
			return {
				...state,
				gameLevels: action.payload
			};
		case LOADING_GAME_LEVELS:
			return {
				...state,
				loadingGameLevels: action.payload
			};

		case GET_GAME_CUTSCENES:
			return {
				...state,
				gameCutscenes: action.payload
			};
		case LOADING_GAME_CUTSCENES:
			return {
				...state,
				loadingGameCutscenes: action.payload
			};

		case GET_GAME_CONVERSATIONS:
			return {
				...state,
				gameConversations: action.payload
			};
		case LOADING_GAME_CONVERSATIONS:
			return {
				...state,
				loadingGameConversations: action.payload
			};

		case GET_GAME_BACKGROUNDS:
			return {
				...state,
				gameBackgrounds: action.payload
			};
		case LOADING_GAME_BACKGROUNDS:
			return {
				...state,
				loadingGameBackgrounds: action.payload
			};

		case GET_GAME_WEAPONS:
			return {
				...state,
				gameWeapons: action.payload
			};
		case LOADING_GAME_WEAPONS:
			return {
				...state,
				loadingGameWeapons: action.payload
			};

		case GET_GAME_ITEMS:
			return {
				...state,
				gameItems: action.payload
			};
		case LOADING_GAME_ITEMS:
			return {
				...state,
				loadingGameItems: action.payload
			};

		case GET_GAME_PAGES:
			return {
				...state,
				gamePages: action.payload
			};
		case LOADING_GAME_PAGES:
			return {
				...state,
				loadingGamePages: action.payload
			};


		case GET_GAME_TILES:
			return {
				...state,
				gameTiles: action.payload
			};

		case LOADING_GAME_TILES:
			return {
				...state,
				loadingGameTiles: action.payload
			}


		case GET_LEVEL_TILES:
			return {
				...state,
				levelTiles: action.payload
			};

		case LOADING_LEVEL_TILES:
			return {
				...state,
				loadingLevelTiles: action.payload
			}

		case GET_LEVEL_NPCHARACTERS:
			return {
				...state,
				levelNPCharacters: action.payload
			};

		case LOADING_LEVEL_NPCHARACTERS:
			return {
				...state,
				loadingLevelNPCharacters: action.payload
			}

		case GET_CUTSCENE_DETAIL:
			return {
				...state,
				cutsceneDetail: action.payload
			};

		case LOADING_CUTSCENE_DETAIL:
			return {
				...state,
				loadingCutsceneDetail: action.payload
			}

		case GET_CUTSCENE_ACTIONS:
			return {
				...state,
				cutsceneActions: action.payload
			};

		case LOADING_CUTSCENE_ACTIONS:
			return {
				...state,
				loadingCutsceneActions: action.payload
			}


		case GET_GAME_CONVERSATION_DIALOGUES:
			return {
				...state,
				gameConversationDialogues: action.payload
			};

		case LOADING_GAME_CONVERSATION_DIALOGUES:
			return {
				...state,
				loadingGameConversationDialogues: action.payload
			}

		case GET_DIALOGUE_RESPONSES:
			return {
				...state,
				dialogueResponses: action.payload
			};

		case LOADING_DIALOGUE_RESPONSES:
			return {
				...state,
				loadingDialogueResponses: action.payload
			}
      
    default: 
      return state;
	}
}