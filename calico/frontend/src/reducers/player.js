import { 
  START_PLAYER_GAME,
	GET_PLAYER_GAMES,
	LOADING_PLAYER_GAMES,
	GET_PLAYER_GAME,
	LOADING_PLAYER_GAME,
	GET_PLAYER_LEVEL,
	LOADING_PLAYER_LEVEL,
	GET_PLAYER_LEVEL_ACTIONS,
  LOADING_PLAYER_LEVEL_ACTIONS,
  GET_PLAYER_PAGE,
  LOADING_PLAYER_PAGE,
  GET_PLAYER_PAGE_ACTIONS,
  LOADING_PLAYER_PAGE_ACTIONS,
  PLAY_GAME_ACTION_TYPE,
  LOADING_PLAY_GAME_ACTION_TYPES,
  GET_PLAYER_CHARACTER,
  LOADING_PLAYER_CHARACTER,
  PLAYER_LEVEL_ACTION,

  GET_PLAYER_POSITION,
  CHANGE_PLAYER_POSITION,
  LOADING_PLAYER_POSITION,
  CHANGE_PLAYER_ATTACK_POSITION,

  GET_PLAYER_SPRITE_POSITION,
  CHANGE_PLAYER_SPRITE_POSITION,
  LOADING_PLAYER_SPRITE_POSITION,

  TAKE_ACTION,
  COMPLETE_ACTION,
  GET_PLAYER_ATTACK,
  RESET_PLAYER_ATTACK,
  TAKE_DAMAGE,
  RESET_TAKE_DAMAGE,
  INITIATE_CONVERSATION, 
  UPDATE_PLAYER_HEALTH,

  GET_PLAYER_CUTSCENE,
  LOADING_PLAYER_CUTSCENE,

  GET_PLAYER_CONVERSATION_DIALOGUE, 
  LOADING_PLAYER_CONVERSATION_DIALOGUE, 
  GET_PLAYER_CONVERSATION_DIALOGUE_RESPONSES, 
  LOADING_PLAYER_CONVERSATION_DIALOGUE_RESPONSES
 } from '../actions/types';

const initialState = {
  playerGames: null,
	loadingPlayerGames: true,
	playerGame: null,
	loadingPlayerGame: true,
	playerLevel: null,
	loadingPlayerLevel: true,
	playerLevelActions: null,
  loadingPlayerLevelActions: true,
  playerPage: null,
  loadingPlayerPage: true,
  playerPageActions: null,
  loadingPlayerPageActions: true,

  playerCharacter: null,
  loadingPlayerCharacter: true,

  playerPosition: [0, 0],
  loadingPlayerPosition: true,

  playerSpritePosition: [0, 0],
  loadingPlayerSpritePosition: true,
  direction: "SOUTH",
  walkIndex: 0,
  playerAttackPosition: [0, 0],
  playerSpriteAttackPosition: [0, 0],
  attackKey: 'DOWN',
  attackIndex: 0,

  takeAction: false,
  actionPosition: [0, 0],
  takeDamage: false,
  damagePosition: [0, 0],

  playerCutscene: null,
  loadingPlayerCutscene: true,

  conversationPosition: [0, 0],
  initiateConversation: false,

  playerConversationDialogue: null,
  loadingPlayerConversationDialogue: true,

  playerConversationDialogueResponses: null,
  loadingPlayerConversationDialogueResponses: true
};

export default function(state = initialState, action) {
	switch(action.type){
    case GET_PLAYER_GAMES:
      return {
        ...state,
        playerGames: action.payload
      };
    case LOADING_PLAYER_GAMES:
      return {
        ...state,
        loadingPlayerGames: action.payload
      };

    case START_PLAYER_GAME:
      return {
        ...state,
        playerGame: action.payload
      };
    case GET_PLAYER_GAME:
      return {
        ...state,
        playerGame: action.payload
      };
    case LOADING_PLAYER_GAME:
      return {
        ...state,
        loadingPlayerGame: action.payload
      };

    case GET_PLAYER_LEVEL:
      return {
        ...state,
        playerLevel: action.payload,
        playerSpritePosition: [0, 0],
        direction: "SOUTH",
        walkIndex: 0,
      }
    case LOADING_PLAYER_LEVEL:
      return {
        ...state,
        loadingPlayerLevel: action.payload
      };

    case GET_PLAYER_LEVEL_ACTIONS:
      return {
        ...state,
        playerLevelActions: action.payload
      };

    case LOADING_PLAYER_LEVEL_ACTIONS:
      // console.log('Loading Player Level Actions: ', action.payload);
      return {
        ...state,
        loadingPlayerLevelActions: action.payload
      };



    case GET_PLAYER_PAGE:
      return {
        ...state,
        playerPage: action.payload
      };
    case LOADING_PLAYER_PAGE:
      return {
        ...state,
        loadingPlayerPage: action.payload
      };

    case GET_PLAYER_PAGE_ACTIONS:
      return {
        ...state,
        playerPageActions: action.payload
      };
    case LOADING_PLAYER_PAGE_ACTIONS:
      return {
        ...state,
        loadingPlayerPageActions: action.payload
      };



    case GET_PLAYER_CHARACTER:
      return {
        ...state,
        playerCharacter: action.payload
      };
    case LOADING_PLAYER_CHARACTER:
      return {
        ...state,
        loadingPlayerCharacter: action.payload
      };

    case PLAYER_LEVEL_ACTION:
      return {
        ...state,
        playerLevel: action.payload
      }



    // ALL THE PLAYER POSITION STUFF
    case GET_PLAYER_POSITION:
      return {
        ...state,
        playerPosition,
        direction,
        playerSpritePosition
      }

    case CHANGE_PLAYER_POSITION:
      // console.log('Player Sprite Position: ', action.payload.playerSpritePosition);
      // console.log('Player Walk Index: ', action.payload.walkIndex);
      return {
        ...state,
        playerPosition: [ action.payload.position[0], action.payload.position[1] ],
        direction: action.payload.direction,
        walkIndex: action.payload.walkIndex,
        playerSpritePosition: [ action.payload.playerSpritePosition[0], action.payload.playerSpritePosition[1] ]
      };

    case LOADING_PLAYER_POSITION:
      return {
        ...state,
        loadingPlayerPosition: action.payload
      };

    // ALL THE PLAYER SPRITE POSITION STUFF
    case GET_PLAYER_SPRITE_POSITION:
      return {
        ...state,
        playerSpritePosition: [0, 0]
      };

    case CHANGE_PLAYER_SPRITE_POSITION:
      return {
        ...state,
        direction: action.payload.direction,
        playerSpritePosition: [ action.payload.playerSpritePosition[0], action.payload.playerSpritePosition[1] ]
      };

    case LOADING_PLAYER_SPRITE_POSITION:
      return {
        ...state,
        loadingPlayerSpritePosition: action.payload
      };

    case CHANGE_PLAYER_ATTACK_POSITION:
      // console.log("I'm good, bruh");
      return {
        ...state,
        playerAttackPosition: [ action.payload.playerAttackPosition[0], action.payload.playerAttackPosition[1] ],
        attackKey: action.payload.attackKey,
        attackIndex: action.payload.attackIndex,
        playerSpriteAttackPosition: [ action.payload.playerSpriteAttackPosition[0], action.payload.playerSpriteAttackPosition[1] ]
      };



    // ACTION STUFF
    case TAKE_ACTION:
      return {
        ...state,
        takeAction: true,
        actionPosition: [ action.payload.position[0], action.payload.position[1] ]
      };

    case COMPLETE_ACTION:
      return {
        ...state,
        takeAction: false,
        actionPosition: [0, 0]
      };

    // DAMAGE STUFF
    case TAKE_DAMAGE:
      return {
        ...state,
        playerPosition: [ action.payload.playerPosition[0], action.payload.playerPosition[1] ],
        takeDamage: true,
        damagePosition: [ action.payload.damagePosition[0], action.payload.damagePosition[1] ]
      };

    case RESET_TAKE_DAMAGE:
      return {
        ...state,
        takeDamage: false,
        damagePosition: [ 0, 0 ]
      };


    case GET_PLAYER_ATTACK:
      return {
        ...state
      }

    case RESET_PLAYER_ATTACK:
      return {
        ...state
      }

    case INITIATE_CONVERSATION:
      return {
        ...state,
        conversationPosition: [ action.payload.conversationPosition[0], action.payload.conversationPosition[1] ],
        initiateConversation: action.payload.initiateConversation
      }

    case UPDATE_PLAYER_HEALTH:
      return {
        ...state,
        playerCharacter: action.payload
      }

    case GET_PLAYER_CUTSCENE:
      return {
        ...state,
        playerCutscene: action.payload
      };

    case LOADING_PLAYER_CUTSCENE:
      return {
        ...state,
        loadingPlayerCutscene: action.payload
      };


    case GET_PLAYER_CONVERSATION_DIALOGUE:
      // console.log('Get Player Conversation Dialogue Reducer: ', action.payload);
      return {
        ...state,
        playerConversationDialogue: action.payload
      };
    case LOADING_PLAYER_CONVERSATION_DIALOGUE:
      return {
        ...state,
        loadingPlayerConversationDialogue: action.payload
      };
    case GET_PLAYER_CONVERSATION_DIALOGUE_RESPONSES:
      // console.log('Get Player Conversation Dialogue Responses Reducer: ', action.payload);
      return {
        ...state,
        playerConversationDialogueResponses: action.payload
      };
    case LOADING_PLAYER_CONVERSATION_DIALOGUE_RESPONSES:
      return {
        ...state,
        loadingPlayerConversationDialogueResponses: action.payload
      };





		default:
			return state;
	}
}