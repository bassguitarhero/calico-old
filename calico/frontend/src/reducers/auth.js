import { 
	LOADING_USER,
  AUTH_ERROR,
  USER_LOADED,
  ERROR_401,
	LOGIN_SUCCESS,
	REGISTER_SUCCESS,
	LOGIN_FAIL,
	REGISTER_FAIL,

	LOGOUT_SUCCESS
} from '../actions/types';

const initialState = {
	token: localStorage.getItem("token"),
  isAuthenticated: null,
  loadingUser: false,
  user: null,
};

export default function(state = initialState, action) {
	switch(action.type){
		case LOADING_USER:
      return {
        ...state,
        loadingUser: true
      };
    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        loadingUser: false,
        user: action.payload
      };
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      localStorage.setItem("token", action.payload.token);
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        loadingUser: false
			};
		case ERROR_401:
    case AUTH_ERROR:
    case LOGIN_FAIL:
    case LOGOUT_SUCCESS:
    case REGISTER_FAIL:
      localStorage.removeItem("token");
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        loadingUser: false
      };
		
		default:
			return state;
	}
}