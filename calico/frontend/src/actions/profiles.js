import axios from 'axios';
import { bindActionCreators } from 'redux';
import { URL, tokenConfig } from './common';
import { createMessage, returnErrors } from "./messages";

import { 
  GET_PROFILE,
  LOADING_PROFILE
} from './types';

export const getUserProfile = (profileSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/profiles/v/${profileSlug}/`, tokenConfig(getState))
    .then(res => {
      console.log('New Res: ', res);
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PROFILE,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetUserProfile = () => (dispatch) => {
  dispatch({
    type: GET_PROFILE,
    payload: null
  });
  dispatch({
    type: LOADING_PROFILE,
    payload: true
  });
}

export const editUserProfile = (profileSlug, profileData) => (dispatch, getState) => {
  console.log('Profile Slug: ', profileSlug);
  console.log('Profile Data: ', profileData);
  var editProfileData = new FormData();
  editProfileData.set('slug', slug);
  if (profileData.imageFile !== null) {
    editProfileData.append('image', profileData.imageFile);
  }

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .put(URL + `/api/profiles/v/${profileSlug}/`, editProfileData, config)
    .then(res => {
      dispatch(createMessage({ characterEdited: "Profile successfully edited" }))
      console.log('Edit Res: ', res);
      dispatch({
        type: EDIT_PROFILE,
        payload: res.data
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetEditUserProfile = () => (dispatch) => {
  dispatch({
    type: EDIT_PROFILE,
    payload: null
  });
}