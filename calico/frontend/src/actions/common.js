export const URL = 'http://178.128.72.158';

// Setup config with token 
export const tokenConfig = getState => {
	// Get token from state
	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}
	
	return config;
};

export const sendFacebookToProps = (facebook) => (dispatch, getState) => {
	dispatch({
		type: SEND_FACEBOOK_TO_PROPS,
		payload: facebook
	});
}

export const getTimestamp = (lat, lon) => (dispatch, getState) => {
	const API_KEY = 'Z4U9A3IOX5IO'
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	axios
    .get(`https://cors-anywhere.herokuapp.com/https://api.timezonedb.com/v2.1/get-time-zone?key=${API_KEY}&format=json&by=position&lat=${lat}&lng=${lon}`, config)
    .then(res => {
      dispatch({
        type: GET_TIMESTAMP,
        payload: res.data
      });
    })
    .then(() => {
      dispatch({
        type: TIMESTAMP_LOADING,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}