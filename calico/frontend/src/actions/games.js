import axios from 'axios';
import { bindActionCreators } from 'redux';
import { URL, tokenConfig } from './common';
import { createMessage, returnErrors } from "./messages";

import { 
  GET_GAMES,
  LOADING_GAMES,

  GET_GAME_DETAIL,
  LOADING_GAME_DETAIL,

  GET_GAME_CHARACTERS,
  LOADING_GAME_CHARACTERS,

  GET_GAME_NP_CHARACTERS,
  LOADING_GAME_NP_CHARACTERS,

  GET_GAME_LEVELS,
  LOADING_GAME_LEVELS,

  GET_GAME_CUTSCENES,
	LOADING_GAME_CUTSCENES,

  GET_GAME_CONVERSATIONS,
  LOADING_GAME_CONVERSATIONS,
  
  GET_GAME_BACKGROUNDS,
  LOADING_GAME_BACKGROUNDS,

  GET_GAME_WEAPONS,
  LOADING_GAME_WEAPONS,

  GET_GAME_ITEMS,
  LOADING_GAME_ITEMS,

  GET_GAME_PAGES,
  LOADING_GAME_PAGES,

  GET_GAME_TILES,
  LOADING_GAME_TILES,
  
  GET_LEVEL_TILES,
  LOADING_LEVEL_TILES,

  GET_LEVEL_NPCHARACTERS, 
  LOADING_LEVEL_NPCHARACTERS,
  GET_LEVEL_ACTIONS,

  GET_CUTSCENE_DETAIL, 
  LOADING_CUTSCENE_DETAIL,

  GET_CUTSCENE_ACTIONS, 
  LOADING_CUTSCENE_ACTIONS,

  GET_GAME_CONVERSATION_DIALOGUES,
  LOADING_GAME_CONVERSATION_DIALOGUES,

  GET_DIALOGUE_RESPONSES,
  LOADING_DIALOGUE_RESPONSES
} from './types';

export const getGames = () => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAMES,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAMES,
        payload: false
      })
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const clearGames = () => (dispatch) => {
  dispatch({
    type: GET_GAMES,
    payload: null
  });
  dispatch({
    type: LOADING_GAMES,
    payload: true
  });
}

export const getGameDetail = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_DETAIL,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_DETAIL,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameDetail = () => (dispatch) => {
  dispatch({
    type: GET_GAME_DETAIL,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_DETAIL,
    payload: true
  });
}

export const editGame = (game) => (dispatch) => {

}

export const getGameCharacters = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/characters/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_CHARACTERS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_CHARACTERS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameCharacters = () => (dispatch) => {
  dispatch({
    type: GET_GAME_CHARACTERS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_CHARACTERS,
    payload: true
  });
}

export const getGameNPCharacters = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/npcharacters/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_NP_CHARACTERS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_NP_CHARACTERS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameNPCharacters = () => (dispatch) => {
  dispatch({
    type: GET_GAME_NP_CHARACTERS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_NP_CHARACTERS,
    payload: true
  });
}

export const getGameLevels = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/levels/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_LEVELS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_LEVELS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameLevels = () => (dispatch) => {
  dispatch({
    type: GET_GAME_LEVELS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_LEVELS,
    payload: true
  });
}

export const getGameCutscenes = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/cutscenes/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_CUTSCENES,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_CUTSCENES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameCutscenes = () => (dispatch) => {
  dispatch({
    type: GET_GAME_CUTSCENES,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_CUTSCENES,
    payload: true
  });
}

export const getGameConversations = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/conversations/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_CONVERSATIONS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_CONVERSATIONS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameConversations = () => (dispatch) => {
  dispatch({
    type: GET_GAME_CONVERSATIONS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_CONVERSATIONS,
    payload: true
  });
}

export const getGameBackgrounds = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/backgrounds/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_BACKGROUNDS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_BACKGROUNDS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameBackgrounds = () => (dispatch) => {
  dispatch({
    type: GET_GAME_BACKGROUNDS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_BACKGROUNDS,
    payload: true
  });
}

export const getGameWeapons = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/weapons/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_WEAPONS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_WEAPONS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameWeapons = () => (dispatch) => {
  dispatch({
    type: GET_GAME_WEAPONS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_WEAPONS,
    payload: true
  });
}

export const getGameItems = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/items/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_ITEMS,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_ITEMS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameItems = () => (dispatch) => {
  dispatch({
    type: GET_GAME_ITEMS,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_ITEMS,
    payload: true
  });
}

export const getGamePages = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/pages/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_PAGES,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_PAGES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGamePages = () => (dispatch) => {
  dispatch({
    type: GET_GAME_PAGES,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_PAGES,
    payload: true
  });
}

export const getGameTiles = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/tiles/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_TILES,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_TILES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearGameTiles = () => (dispatch) => {
  dispatch({
    type: GET_GAME_TILES,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_TILES,
    payload: true
  });
}

export const getLevelTiles = (slug, levelSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/tiles/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_LEVEL_TILES,
        payload: res.data
      });
      dispatch({
        type: LOADING_LEVEL_TILES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearLevelTiles = () => (dispatch) => {
  dispatch({
    type: GET_LEVEL_TILES,
    payload: null
  });
  dispatch({
    type: LOADING_LEVEL_TILES,
    payload: true
  });
}

export const getLevelNPCharacters = (slug, levelSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/npcharacters/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_LEVEL_NPCHARACTERS,
        payload: res.data
      });
      dispatch({
        type: LOADING_LEVEL_NPCHARACTERS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearLevelNPCharacters = () => (dispatch) => {
  dispatch({
    type: GET_LEVEL_NPCHARACTERS,
    payload: null
  });
  dispatch({
    type: LOADING_LEVEL_NPCHARACTERS,
    payload: true
  });
}

// export const getLevelActions = (slug, levelSlug) => (dispatch, getState) => {
//   axios
//     .get(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/actions/`, tokenConfig(getState))
//     .then(res => {
//       dispatch({
//         type: GET_LEVEL_ACTIONS,
//         payload: res.data
//       });
//       dispatch({
//         type: LOADING_LEVEL_ACTIONS,
//         payload: false
//       });
//     })
//     .catch(err => 
//     	dispatch(returnErrors(err.response.data, err.response.status))
//     );
// }

// export const resetLevelActions = () => (dispatch) => {
//   dispatch({
//     type: GET_LEVEL_ACTIONS,
//     payload: null
//   });
//   dispatch({
//     type: LOADING_LEVEL_ACTIONS,
//     payload: true
//   });
// }


export const getCutsceneDetail = (slug, cutsceneSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/cutscenes/v/${cutsceneSlug}/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_CUTSCENE_DETAIL,
        payload: res.data
      });
      dispatch({
        type: LOADING_CUTSCENE_DETAIL,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetCutsceneDetail = () => (dispatch) => {
  dispatch({
    type: GET_CUTSCENE_DETAIL,
    payload: null
  });
  dispatch({
    type: LOADING_CUTSCENE_DETAIL,
    payload: true
  });
}

export const getCutsceneActions = (slug, cutsceneSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/cutscenes/v/${cutsceneSlug}/actions/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_CUTSCENE_ACTIONS,
        payload: res.data
      });
      dispatch({
        type: LOADING_CUTSCENE_ACTIONS,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetCutsceneActions = () => (dispatch) => {
  dispatch({
    type: GET_CUTSCENE_ACTIONS,
    payload: null
  });
  dispatch({
    type: LOADING_CUTSCENE_ACTIONS,
    payload: true
  });
}





export const getGameConversationDialogues = (gameSlug, conversationSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_CONVERSATION_DIALOGUES,
        payload: res.data
      });
      dispatch({
        type: LOADING_GAME_CONVERSATION_DIALOGUES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetGameConversationDialogues = () => (dispatch) => {
  dispatch({
    type: GET_GAME_CONVERSATION_DIALOGUES,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_CONVERSATION_DIALOGUES,
    payload: true
  });
}




export const getDialogueResponses = (gameSlug, conversationSlug, dialogueSlug) => (dispatch, getState) => {
  dispatch({
    type: GET_DIALOGUE_RESPONSES,
    payload: null
  });
  dispatch({
    type: LOADING_DIALOGUE_RESPONSES,
    payload: true
  })

  // console.log('Get Dialogue Responses -- Game Slug: ', gameSlug, 'Conversation Slug: ', conversationSlug, 'Dialogue Slug: ', dialogueSlug);

  axios
    .get(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/responses/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_DIALOGUE_RESPONSES,
        payload: res.data
      });
      dispatch({
        type: LOADING_DIALOGUE_RESPONSES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetDialogueResponses = () => (dispatch) => {
  dispatch({
    type: GET_DIALOGUE_RESPONSES,
    payload: null
  });
  dispatch({
    type: LOADING_DIALOGUE_RESPONSES,
    payload: true
  })
}