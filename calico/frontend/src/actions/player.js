import axios from 'axios';
import { bindActionCreators } from 'redux';
import { URL, tokenConfig } from './common';
import { createMessage, returnErrors } from "./messages";

import { 
  GET_PLAYER_GAMES,
  LOADING_PLAYER_GAMES,

  START_PLAYER_GAME,
  GET_PLAYER_GAME,
  LOADING_PLAYER_GAME,

  GET_PLAYER_LEVEL,
  LOADING_PLAYER_LEVEL,

  GET_PLAYER_LEVEL_ACTIONS,
  LOADING_PLAYER_LEVEL_ACTIONS,

  GET_PLAYER_PAGE,
  LOADING_PLAYER_PAGE,

  GET_PLAYER_PAGE_ACTIONS,
  LOADING_PLAYER_PAGE_ACTIONS,
  LOADING_GAME_ACTION_TYPES,

  PLAY_GAME_ACTION_TYPE,
  LOADING_PLAY_GAME_ACTION_TYPES,

  PLAYER_LEVEL_ACTION,
  UPDATE_PLAYER_HEALTH, 

  GET_PLAYER_CHARACTER,
  LOADING_PLAYER_CHARACTER,


  GET_PLAYER_POSITION,
  CHANGE_PLAYER_POSITION,
  LOADING_PLAYER_POSITION,

  GET_PLAYER_SPRITE_POSITION,
  CHANGE_PLAYER_SPRITE_POSITION,
  LOADING_PLAYER_SPRITE_POSITION,
  
  TAKE_ACTION,
  COMPLETE_ACTION,
  CHANGE_PLAYER_ATTACK_POSITION,
  RESET_PLAYER_ATTACK,
  RESET_TAKE_DAMAGE,

  GET_PLAYER_CUTSCENE,
  LOADING_PLAYER_CUTSCENE,
  INITIATE_CONVERSATION,

  GET_PLAYER_CONVERSATION_DIALOGUE, 
  LOADING_PLAYER_CONVERSATION_DIALOGUE, 
  GET_PLAYER_CONVERSATION_DIALOGUE_RESPONSES, 
  LOADING_PLAYER_CONVERSATION_DIALOGUE_RESPONSES
} from './types';

export const getPlayerGames = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/plays/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_PLAYER_GAMES,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_GAMES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerGames = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_GAMES,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_GAMES,
    payload: true
  });
}

export const startPlayerGame = (gameSlug, characterSlug) => (dispatch, getState) => {
  var newGameData = new FormData();
  newGameData.set('slug', gameSlug);
  if (characterSlug !== '' && characterSlug !== undefined) {
    newGameData.set('character_slug', characterSlug);
  } else {
    newGameData.set('character_slug', '');
  }

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/plays/`, newGameData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Game successfully started" }))
			dispatch({
				type: START_PLAYER_GAME,
				payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_GAME,
        payload: false
      });
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetStartGame = () => (dispatch) => {
  dispatch({
    type: START_PLAYER_GAME,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_GAME,
    payload: true
  });
}

export const getPlayerGame = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/play/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_PLAYER_GAME,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_GAME,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerGame = () => (dispatch) => {
  dispatch({
    type: START_PLAYER_GAME,
    payload: null
  });
  dispatch({
    type: GET_PLAYER_GAME,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_GAME,
    payload: true
  });
}

export const getActivePlayerLevel = (slug) => (dispatch, getState) => {
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading+... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${slug}/play/level/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_LEVEL,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_LEVEL,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const getPlayerLevel = (slug, playerLevel, completed) => (dispatch, getState) => {
  var levelData = new FormData();
  levelData.set('slug', slug);
  levelData.set('level_completed', completed);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading+... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${slug}/play/levels/v/${playerLevel.level.slug}/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_LEVEL,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_LEVEL,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const clearPlayerLevelData = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_LEVEL,
    payload: null
  });
}

export const getPlayerLevelActions = (playerLevel) => (dispatch, getState) => {
  axios
  .get(URL + `/api/games/v/${playerLevel.level.game.slug}/play/levels/v/${playerLevel.level.slug}/actions/`, tokenConfig(getState))
  .then(res => {
    dispatch({
      type: GET_PLAYER_LEVEL_ACTIONS,
      payload: res.data
    });
    dispatch({
      type: LOADING_PLAYER_LEVEL_ACTIONS,
      payload: false
    });
  })
  .catch(err => 
    dispatch(returnErrors(err.response.data, err.response.status))
  );
}

export const resetPlayerLevelActions = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_LEVEL_ACTIONS,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_LEVEL_ACTIONS,
    payload: true
  });
}

export const changeCutsceneLevel = (slug, cutsceneAction) => (dispatch, getState) => {
  dispatch({
    type: LOADING_PLAYER_LEVEL,
    payload: true
  });

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${slug}/play/cutscenes/v/${cutsceneAction.cutscene.slug}/actions/v/${cutsceneAction.action_type.slug}/to/${cutsceneAction.change_level.slug}/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_LEVEL,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_LEVEL,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const changeLevel = (slug, levelAction) => (dispatch, getState) => {
  dispatch({
    type: LOADING_PLAYER_LEVEL,
    payload: true
  });

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${slug}/play/levels/v/${levelAction.level.slug}/actions/v/${levelAction.action_type.slug}/to/${levelAction.change_level.slug}/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_LEVEL,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_LEVEL,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetChangeLevel = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_LEVEL,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_LEVEL,
    payload: true
  });
}

export const playLevelAction = (playerLevel, levelAction) => (dispatch, getState) => {
  dispatch({
    type: LOADING_PLAYER_LEVEL,
    payload: true
  });
  // dispatch({
  //   type: LOADING_PLAYER_LEVEL_ACTIONS,
  //   payload: true
  // });

  if (levelAction.action_type.slug === 'change-level') {
    axios
    .get(URL + `/api/games/v/${playerLevel.level.game.slug}/play/levels/v/${playerLevel.level.slug}/actions/v/${levelAction.action_type.slug}/to/${levelAction.change_level.slug}`, tokenConfig(getState))
      .then(res => {
        dispatch({
          type: PLAYER_CHANGE_LEVEL,
          payload: res.data
        });
      })
      .catch(err => 
        dispatch(returnErrors(err.response.data, err.response.status))
      );
  } else {
    axios
    .get(URL + `/api/games/v/${playerLevel.level.game.slug}/play/levels/v/${playerLevel.level.slug}/actions/v/${levelAction.action_type.slug}/`, tokenConfig(getState))
      .then(res => {
        dispatch({
          type: PLAYER_LEVEL_ACTION,
          payload: res.data
        });
        dispatch({
          type: LOADING_PLAYER_LEVEL,
          payload: false
        });
      })
      .catch(err => 
        dispatch(returnErrors(err.response.data, err.response.status))
      );
  }
}


export const getPlayerCharacter = (gameSlug, characterSlug) => (dispatch, getState) => {
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${gameSlug}/play/characters/v/${characterSlug}/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_PLAYER_CHARACTER,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CHARACTER,
        payload: false
      });
    })
    .catch(err => 
      dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerCharacter = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_CHARACTER,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_CHARACTER,
    payload: true
  });
}



export const getPlayerPage = (slug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/play/page/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_PLAYER_PAGE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_PAGE,
        payload: false
      });
    })
    .catch(err => 
      dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerPage = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_PAGE,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_PAGE,
    payload: true
  });
}

export const getPlayerPageActions = (slug, pageSlug) => (dispatch, getState) => {
  axios
    .get(URL + `/api/games/v/${slug}/play/pages/v/${pageSlug}/actions/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_PLAYER_PAGE_ACTIONS,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_PAGE_ACTIONS,
        payload: false
      });
    })
    .catch(err => 
      dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerPageActions = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_PAGE_ACTIONS,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_PAGE_ACTIONS,
    payload: true
  });
}

export const goToPage = (slug, pageSlug, newPageSlug) => (dispatch, getState) => {
  dispatch({
    type: LOADING_PLAYER_PAGE,
    payload: true,
  });
  dispatch({
    type: LOADING_PLAYER_PAGE_ACTIONS,
    payload: true
  })
  axios
    .get(URL + `/api/games/v/${slug}/play/pages/v/${pageSlug}/to/${newPageSlug}/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_PLAYER_PAGE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_PAGE,
        payload: false
      });
    })
    .catch(err => 
      dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetGoToPage = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_PAGE,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_PAGE,
    payload: true
  });
}

export const playGameAction = (gameDetail, slug, gameActionType) => (dispatch, getState) => {
  dispatch({
    type: GET_PLAYER_GAME,
    payload: null
  })
  dispatch({
    type: LOADING_PLAYER_GAME,
    payload: true
  });
  if (gameDetail.game_type.slug === 'dungeon-crawler') {
    axios
      .get(URL + `/api/games/v/${gameDetail.slug}/play/actions/v/${gameActionType}/`, tokenConfig(getState))
      .then(res => {
        dispatch({
          type: GET_PLAYER_GAME,
          payload: res.data
        });
        dispatch({
          type: LOADING_PLAYER_GAME,
          payload: false
        });
      })
      .catch(err => 
        dispatch(returnErrors(err.response.data, err.response.status))
      );
  } else if (gameDetail.game_type.slug === 'choose-your-own-adventure') {
    axios
      .get(URL + `/api/games/v/${gameDetail.slug}/play/pages/v/${slug}/actions/v/${gameActionType}/`, tokenConfig(getState))
      .then(res => {
        dispatch({
          type: GET_PLAYER_GAME,
          payload: res.data
        });
        dispatch({
          type: LOADING_PLAYER_GAME,
          payload: false
        });
      })
      .catch(err => 
        dispatch(returnErrors(err.response.data, err.response.status))
      );
  }
  
}

export const resetGameAction = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_GAME,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_GAME,
    payload: true
  });
}






export const getPlayerPosition = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_POSITION,
    payload: [0, 0]
  });
  dispatch({
    type: LOADING_PLAYER_POSITION,
    payload: false
  })
}

export const changePlayerPosition = (x, y) => (dispatch) => {
  dispatch({
    type: CHANGE_PLAYER_POSITION,
    payload: [x, y]
  });
  dispatch({
    type: LOADING_PLAYER_POSITION,
    payload: false
  })
}

export const resetPlayerPosition = () => (dispatch) => {
  dispatch({
    type: CHANGE_PLAYER_POSITION,
    payload: {
      position: [0, 0],
      direction: 'SOUTH',
      playerSpritePosition: [0, 0],
      walkIndex: 0
    }
  });
  dispatch({
    type: LOADING_PLAYER_POSITION,
    payload: true
  })
}

export const completeAction = () => (dispatch) => {
  dispatch({
    type: COMPLETE_ACTION
  });
}

export const resetPlayerAttack = () => (dispatch) => {
  dispatch({
    type: CHANGE_PLAYER_ATTACK_POSITION,
    payload: {
      playerAttackPosition: [0, 0],
      playerSpriteAttackPosition: [0, 0],
      attackKey: 'DOWN',
      attackIndex: 0,
    }
  });
}

export const resetTakeDamage = () => (dispatch) => {
  dispatch({
    type: RESET_TAKE_DAMAGE
  });
}

// export const resetPlayerAttack = () => (dispatch) => {
//   dispatch({
//     type: RESET_PLAYER_ATTACK
//   })
// }

export const updatePlayerHealth = (gameSlug, characterSlug, health) => (dispatch, getState) => {
  dispatch({
    type: LOADING_PLAYER_CHARACTER,
    payload: true
  });

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${gameSlug}/play/characters/v/${characterSlug}/update/${health}/`, config)
    .then(res => {
      dispatch({
        type: UPDATE_PLAYER_HEALTH,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CHARACTER,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const getActivePlayerCutscene = (slug) => (dispatch, getState) => {
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading+... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${slug}/play/cutscene/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_CUTSCENE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CUTSCENE,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const changePlayerCutscene = (slug, actionTypeSlug, cutsceneSlug) => (dispatch, getState) => {
  dispatch({
    type: LOADING_PLAYER_CUTSCENE,
    payload: true
  });

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${slug}/play/cutscene/actions/v/${actionTypeSlug}/to/${cutsceneSlug}/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_CUTSCENE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CUTSCENE,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetChangePlayerCutscene = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_CUTSCENE,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_CUTSCENE,
    payload: true
  });
}


export const resetPlayerConversation = () => (dispatch) => {
  dispatch({
    type: INITIATE_CONVERSATION,
    payload: {
      conversationPosition: [0, 0],
      initiateConversation: false
    }
  });
}

export const startPlayerConversation = (gameSlug, conversationSlug) => (dispatch, getState) => {
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${gameSlug}/play/conversations/v/${conversationSlug}/start/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_CONVERSATION_DIALOGUE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CONVERSATION_DIALOGUE,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const getPlayerConversationDialogue = (gameSlug, conversationSlug, dialogueSlug) => (dispatch, getState) => {
  dispatch({
    type: GET_PLAYER_CONVERSATION_DIALOGUE,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_CONVERSATION_DIALOGUE,
    payload: true
  })
  
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${gameSlug}/play/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/`, config)
    .then(res => {
      dispatch({
        type: GET_PLAYER_CONVERSATION_DIALOGUE,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CONVERSATION_DIALOGUE,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerConversationDialogue = () => (dispatch) => {
  dispatch({
    type: GET_PLAYER_CONVERSATION_DIALOGUE,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_CONVERSATION_DIALOGUE,
    payload: true
  })
}

export const getPlayerConversationDialogueResponses = (gameSlug, conversationSlug, dialogueSlug) => (dispatch, getState) => {
  // console.log('Get Player Conversation Dialogue Responses');
  // console.log('Game Slug: ', gameSlug, 'Conversation Slug: ', conversationSlug, 'Dialogue Slug: ', dialogueSlug);
  dispatch({
    type: GET_PLAYER_CONVERSATION_DIALOGUE_RESPONSES,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_CONVERSATION_DIALOGUE_RESPONSES,
    payload: true
  })
  
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		// onUploadProgress: function (progressEvent) {
	  //   let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	  //   document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  // }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
  }
  
  axios
    .get(URL + `/api/games/v/${gameSlug}/play/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/responses/`, config)
    .then(res => {
      // console.log('Dialogue Responses in Actions: ', res);
      dispatch({
        type: GET_PLAYER_CONVERSATION_DIALOGUE_RESPONSES,
        payload: res.data
      });
      dispatch({
        type: LOADING_PLAYER_CONVERSATION_DIALOGUE_RESPONSES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetPlayerConversationDialogueResponses = () => (dispatch) => {
  // console.log('Reset Player Conversation Dialogue Responses');
  dispatch({
    type: GET_PLAYER_CONVERSATION_DIALOGUE_RESPONSES,
    payload: null
  });
  dispatch({
    type: LOADING_PLAYER_CONVERSATION_DIALOGUE_RESPONSES,
    payload: true
  })
}