import axios from 'axios';
import { createMessage, returnErrors } from "./messages";
import { URL, tokenConfig } from './common';

import { 
  LOADING_USER,
  AUTH_ERROR,
  USER_LOADED,
  ERROR_401,

  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  LOGIN_FAIL,
  REGISTER_FAIL,

  LOGOUT_SUCCESS
} from './types';

// CHECK TOKEN AND LOAD USER 
export const loadUser = () => (dispatch, getState) => {
	// User Loading
	dispatch({type: LOADING_USER});

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}
	if (token === null ) { 
		dispatch({
			type: AUTH_ERROR
		})
	} else {
		axios.get(URL + '/api/auth/user/', config)
			.then(res => {
				if (res.data.code == 401) {
					dispatch({
						type: AUTH_ERROR
					})
				} else {
					dispatch({
						type: USER_LOADED,
						payload: res.data
					});
				}
			}).catch(err => {
				if (err.response.status == 401) {
					dispatch({
						type: ERROR_401
					});
				} else {
					dispatch(returnErrors(err.response.data, err.response.status));
					dispatch({
						type: AUTH_ERROR
					});
				}
			});
	}
};

// LOGIN USER 1
export const login = (username, password) => dispatch => {
  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json"
    },
    auth: {
      "username": username,
      "password": password
    }
  };

  // Request Body
  const data = JSON.stringify({ username, password });

  axios
    .post(URL + '/api/auth/login/', null, config)
    .then(res => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
			});
			dispatch(createMessage({ logoutRequest: "Successfully logged in."}));
    })
    .catch(err => {
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

// LOGIN USER 2
export const login2 = (username, password) => (dispatch) => {
	// Request Headers
	 const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  // Request Body
  const data = JSON.stringify({ username, password });

  axios
    .post(URL + '/api/login/', data, config)
    .then(res => {
    	dispatch(createMessage({loginSuccessful: "Login Successful"}));
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data
      });
      dispatch({
      	type: REBUILD_NAV_BAR,
      	payload: true
      })
    })
    .catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

// REGISTER USER
export const register = ({ username, password, email, privateProfile, termsConditions }) => dispatch => {
	// Headers
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	}

	// Request body
	const body = JSON.stringify({ username, password, email, privateProfile, termsConditions });

	axios
		.post(URL + '/api/auth/register/', body, config)
		.then(res => {
			dispatch({
				type: REGISTER_SUCCESS,
				payload: res.data
			});
			dispatch(createMessage({ registerRequest: "Successfully registered."}));
		}).catch(err => {
			dispatch(returnErrors(err.response.data, err.response.status));
			dispatch({
				type: REGISTER_FAIL
			});
		});
};

// LOGOUT USER
export const logout = () => (dispatch, getState) => {
  axios
    .post(URL + '/api/auth/logout/', null, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: LOGOUT_SUCCESS
      });
      dispatch(createMessage({ logoutRequest: "Successfully logged out."}));
    })
    .catch(err => {
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};