import axios from 'axios';
import { URL, tokenConfig } from './common';
import { createMessage, returnErrors } from "./messages";

import { 
  NEW_GAME,
  EDIT_GAME,
	DELETE_GAME,
	
	GET_GAME_TYPES,

  NEW_CHARACTER,
  EDIT_CHARACTER,
  DELETE_CHARACTER,

  NEW_NPCHARACTER,
  EDIT_NPCHARACTER,
  DELETE_NPCHARACTER,

  NEW_LEVEL,
  EDIT_LEVEL,
	DELETE_LEVEL,
	
	GET_LEVEL_TYPES,
	GET_LEVEL_ACTION_TYPES,

	GET_LEVEL_ACTIONS,
	LOADING_LEVEL_ACTIONS,
	NEW_LEVEL_ACTION,
	EDIT_LEVEL_ACTION,
	DELETE_LEVEL_ACTION,

  NEW_BACKGROUND,
  EDIT_BACKGROUND,
  DELETE_BACKGROUND,

  NEW_WEAPON,
  EDIT_WEAPON,
  DELETE_WEAPON,

  NEW_ITEM,
  EDIT_ITEM,
	DELETE_ITEM,
	
	NEW_PAGE,
	EDIT_PAGE,
	DELETE_PAGE,

	GET_PAGE_ACTION_TYPES,
	GET_PAGE_ACTIONS,
	LOADING_PAGE_ACTIONS,
	NEW_PAGE_ACTION,
	EDIT_PAGE_ACTION,
	DELETE_PAGE_ACTION,

	GET_GAME_ACTION_TYPES,
	LOADING_GAME_ACTION_TYPES,

	NEW_GAME_TILE,
	EDIT_GAME_TILE,
	DELETE_GAME_TILE,

	NEW_LEVEL_TILE,
	EDIT_LEVEL_TILE,
	DELETE_LEVEL_TILE,

	NEW_LEVEL_NPCHARACTER,
	EDIT_LEVEL_NPCHARACTER,
	DELETE_LEVEL_NPCHARACTER,

	NEW_CUTSCENE,
	EDIT_CUTSCENE,
	DELETE_CUTSCENE,

	GET_CUTSCENE_ACTION_TYPES,
	LOADING_CUTSCENE_ACTION_TYPES,

	NEW_CUTSCENE_ACTION, 
	EDIT_CUTSCENE_ACTION, 
	DELETE_CUTSCENE_ACTION,

	NEW_CONVERSATION,
	EDIT_CONVERSATION,
	DELETE_CONVERSATION,

	NEW_CONVERSATION_DIALOGUE,
  EDIT_CONVERSATION_DIALOGUE,
  DELETE_CONVERSATION_DIALOGUE,

  NEW_DIALOGUE_RESPONSE,
  EDIT_DIALOGUE_RESPONSE,
  DELETE_DIALOGUE_RESPONSE
} from './types';

export const getGameTypes = () => (dispatch, getState) => {
	axios
    .get(URL + `/api/games/types/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_GAME_TYPES,
        payload: res.data
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetGameTypes = () => (dispatch) => {
	dispatch({
		type: GET_GAME_TYPES,
		payload: null
	});
}

export const newGame = (game) => (dispatch, getState) => {
  var gameData = new FormData();
	gameData.set('name', game.name);
	gameData.append('description', game.description);
	if (game.imageFile !== null) {
		gameData.append('image', game.imageFile);
	} else {
		gameData.append('image', '');
	}
	gameData.append('public', game.publicAccess);
	gameData.append('published', false);
	gameData.append('game_type_slug', game.gameTypeSlug);
	gameData.append('map_width', game.mapWidth);
	gameData.append('map_height', game.mapHeight);
	gameData.append('tile_width', game.tileWidth);
	gameData.append('tile_height', game.tileHeight);
	gameData.append('sprite_width', game.spriteWidth);
	gameData.append('sprite_height', game.spriteHeight);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + '/api/games/new/', gameData, config)
		.then(res => {
			dispatch(createMessage({ gameCreated: "New Game successfully created"}))
			dispatch({
				type: NEW_GAME,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetCreateNewGame = () => (dispatch) => {
	dispatch({
		type: NEW_GAME,
		payload: null
	})
}

export const newCharacter = (slug, character) => (dispatch, getState) => {
  var characterData = new FormData();
	characterData.set('name', character.name);
	characterData.append('description', character.description);
	if (character.imageFile !== null) {
		characterData.append('image', character.imageFile);
	} else {
		characterData.append('image', '');
	}
	if (character.spriteSheetFile !== null) {
		characterData.append('sprite_sheet', character.spriteSheetFile);
	} else {
		characterData.append('sprite_sheet', '');
	}
	characterData.append('width', character.width);
	characterData.append('height', character.height);
	characterData.append('base_strength', character.baseStrength);
	characterData.append('base_skill', character.baseSkill);
	characterData.append('base_speed', character.baseSpeed);
	characterData.append('base_health', character.baseHealth);
	characterData.append('base_wisdom', character.baseWisdom);
	characterData.append('base_luck', character.baseLuck);
	characterData.append('base_hp', character.baseHP);
	characterData.append('base_keys', character.baseKeys);
	characterData.append('base_bombs', character.baseBombs);
	characterData.append('base_damage', character.baseDamage);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/characters/new/`, characterData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Character successfully created"}))
			dispatch({
				type: NEW_CHARACTER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewCharacter = () => (dispatch, getState) => {
	dispatch({
		type: NEW_CHARACTER,
		payload: null
	});
}

export const newNPCharacter = (slug, npCharacter) => (dispatch, getState) => {
	var npCharacterData = new FormData();
	npCharacterData.set('name', npCharacter.name);
	npCharacterData.append('description', npCharacter.description);
	if (npCharacter.imageFile !== null) {
		npCharacterData.append('image', npCharacter.imageFile);
	} else {
		npCharacterData.append('image', '');
	}
	if (npCharacter.spriteSheetFile !== null) {
		npCharacterData.append('sprite_sheet', npCharacter.spriteSheetFile);
	} else {
		npCharacterData.append('sprite_sheet', '');
	}
	npCharacterData.append('height', npCharacter.height);
	npCharacterData.append('width', npCharacter.width);
	npCharacterData.append('health', npCharacter.health);
	npCharacterData.append('speed', npCharacter.speed);
	npCharacterData.append('level', npCharacter.level);
	npCharacterData.append('hostile', npCharacter.hostile);
	npCharacterData.append('contact_damage', npCharacter.contactDamage);
	npCharacterData.append('contact_damage_amount', npCharacter.contactDamageAmount);
	npCharacterData.append('can_walk', npCharacter.canWalk);
	npCharacterData.append('can_fly', npCharacter.canFly);
	npCharacterData.append('can_attack', npCharacter.canAttack);
	// npCharacterData.append('description', npCharacter.attacks);
	npCharacterData.append('has_movement', npCharacter.hasMovement);
	npCharacterData.append('movement_type', npCharacter.movementType);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/npcharacters/new/`, npCharacterData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Non-Playable Character successfully created"}))
			dispatch({
				type: NEW_NPCHARACTER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewNPCharacter = () => (dispatch) => {
	dispatch({
		type: NEW_NPCHARACTER,
		payload: null
	});
}

export const getLevelActionTypes = () => (dispatch, getState) => {
	axios
    .get(URL + `/api/games/levels/actions/types/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_LEVEL_ACTION_TYPES,
        payload: res.data
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetLevelActionTypes = () => (dispatch) => {
	dispatch({
		type: GET_LEVEL_ACTION_TYPES,
		payload: null
	});
}

export const getLevelTypes = () => (dispatch, getState) => {
	axios
    .get(URL + `/api/games/levels/types/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_LEVEL_TYPES,
        payload: res.data
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetLevelTypes = () => (dispatch) => {
	dispatch({
		type: GET_LEVEL_TYPES,
		payload: null
	});
}

export const newLevel = (slug, level) => (dispatch, getState) => {
	var levelData = new FormData();
	levelData.set('name', level.name);
	levelData.append('description', level.description);
	if (level.imageFile !== null) {
		levelData.append('image', level.imageFile);
	} else {
		levelData.append('image', '');
	}
	levelData.append('level_type_slug', level.levelTypeSlug);
	levelData.append('width', level.width);
	levelData.append('height', level.height);
	levelData.append('tile_width', level.tileWidth);
	levelData.append('tile_height', level.tileHeight);
	levelData.append('starting_level', level.startingLevel);
	levelData.append('starting_x', level.startingX);
	levelData.append('starting_y', level.startingY);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/levels/new/`, levelData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Level successfully created"}))
			dispatch({
				type: NEW_LEVEL,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewLevel = () => (dispatch) => {
	dispatch({
		type: NEW_LEVEL,
		payload: null
	});
}

export const getLevelActions = (slug, level) => (dispatch, getState) => {
	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.get(URL + `/api/games/v/${slug}/levels/v/${level.slug}/actions/`, config)
		.then(res => {
			dispatch({
				type: GET_LEVEL_ACTIONS,
				payload: res.data
			});
			dispatch({
				type: LOADING_LEVEL_ACTIONS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetLevelActions = () => (dispatch) => {
	dispatch({
		type: GET_LEVEL_ACTIONS,
		payload: null
	});
	dispatch({
		type: LOADING_LEVEL_ACTIONS,
		payload: true
	});
}

export const newLevelAction = (gameSlug, levelSlug, levelTileData, levelActionData) => (dispatch, getState) => {
	var levelActionForm = new FormData();
	levelActionForm.set('tile_slug', levelTileData.tileSlug);
	levelActionForm.append('position_x', levelTileData.positionX);
	levelActionForm.append('position_y', levelTileData.positionY);
	levelActionForm.append('display', levelTileData.display);
	levelActionForm.append('action_type', levelActionData.levelActionTypeSlug);
	levelActionForm.append('game_action_type', levelActionData.gameActionTypeSlug);
	levelActionForm.append('change_cutscene', levelActionData.cutsceneSlug);
	levelActionForm.append('change_level', levelActionData.levelChangeSlug);
	levelActionForm.append('require_complete', levelActionData.requireComplete);
	levelActionForm.append('defeat_all_enemies', levelActionData.defeatAllEnemies);
	levelActionForm.append('defeat_boss', levelActionData.defeatBoss);
	
  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/levels/v/${levelSlug}/actions/`, levelActionForm, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Level Action successfully created"}))
			dispatch({
				type: NEW_LEVEL_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewLevelAction = () => (dispatch) => {
	dispatch({
		type: NEW_LEVEL_ACTION,
		payload: null
	});
}

export const newBackground = (slug, background) => (dispatch, getState) => {
	var backgroundData = new FormData();
	backgroundData.set('name', background.name);
	backgroundData.append('description', background.description);
	if (background.imageFile !== null) {
		backgroundData.append('image', background.imageFile);
	} else {
		backgroundData.append('image', '');
	}

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/backgrounds/new/`, backgroundData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Background successfully created"}))
			dispatch({
				type: NEW_BACKGROUND,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewBackground = () => (dispatch) => {
	dispatch({
		type: NEW_BACKGROUND,
		payload: null
	});
}

export const newWeapon = (slug, weapon) => (dispatch, getState) => {
	var weaponData = new FormData();
	weaponData.set('name', weapon.name);
	weaponData.append('description', weapon.description);
	if (weapon.imageFile !== null) {
		weaponData.append('image', weapon.imageFile);
	} else {
		weaponData.append('image', '');
	}

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/weapons/new/`, weaponData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Weapon successfully created"}))
			dispatch({
				type: NEW_WEAPON,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewWeapon = () => (dispatch) => {
	dispatch({
		type: NEW_WEAPON,
		payload: null
	});
}

export const newItem = (slug, item) => (dispatch, getState) => {
	var itemData = new FormData();
	itemData.set('name', item.name);
	itemData.append('description', item.description);
	if (item.imageFile !== null) {
		itemData.append('image', item.imageFile);
	} else {
		itemData.append('image', '');
	}

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/items/new/`, itemData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Item successfully created"}))
			dispatch({
				type: NEW_ITEM,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewItem = () => (dispatch) => {
	dispatch({
		type: NEW_ITEM,
		payload: null
	});
}

export const newPage = (slug, page) => (dispatch, getState) => {
	var pageData = new FormData();
	pageData.set('name', page.name);
	pageData.append('description', page.description);
	if (page.imageFile !== null) {
		pageData.append('image', page.imageFile);
	} else {
		pageData.append('image', '');
	}
	pageData.append('youtube_id', page.youtubeID);
	pageData.append('youtube_url', page.youtubeURL);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/pages/new/`, pageData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Page successfully created"}))
			dispatch({
				type: NEW_PAGE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewPage = () => (dispatch) => {
	dispatch({
		type: NEW_PAGE,
		payload: null
	});
}

// EDIT
export const editGame = (slug, game) => (dispatch, getState) => {
  var gameData = new FormData();
	gameData.set('name', game.name);
	gameData.append('description', game.description);
	if (game.imageFile !== null) {
		gameData.append('image', game.imageFile);
	} else {
		gameData.append('image', '');
	}
	gameData.append('public', game.publicAccess);
	gameData.append('published', game.published);
	gameData.append('game_type_slug', game.gameTypeSlug);
	gameData.append('remove-image', game.removeImage);
	gameData.append('map_width', game.mapWidth);
	gameData.append('map_height', game.mapHeight);
	gameData.append('tile_width', game.tileWidth);
	gameData.append('tile_height', game.tileHeight);
	gameData.append('sprite_width', game.spriteWidth);
	gameData.append('sprite_height', game.spriteHeight);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/edit/`, gameData, config)
		.then(res => {
			dispatch(createMessage({ gameEdited: "Game successfully edited"}))
			dispatch({
				type: EDIT_GAME,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditGame = () => (dispatch) => {
	dispatch({
		type: EDIT_GAME,
		payload: null
	});
}

export const editCharacter = (slug, character) => (dispatch, getState) => {
	var characterData = new FormData();
	characterData.set('name', character.name);
	characterData.append('description', character.description);
	if (character.imageFile !== null) {
		characterData.append('image', character.imageFile);
	} else {
		characterData.append('image', '');
	}
	if (character.spriteSheetFile !== null) {
		characterData.append('sprite_sheet', character.spriteSheetFile);
	} else {
		characterData.append('sprite_sheet', '');
	}
	characterData.append('width', character.width);
	characterData.append('height', character.height);
	characterData.append('base_strength', character.baseStrength);
	characterData.append('base_skill', character.baseSkill);
	characterData.append('base_speed', character.baseSpeed);
	characterData.append('base_health', character.baseHealth);
	characterData.append('base_wisdom', character.baseWisdom);
	characterData.append('base_luck', character.baseLuck);
	characterData.append('base_hp', character.baseHP);
	characterData.append('base_keys', character.baseKeys);
	characterData.append('base_bombs', character.baseBombs);
	characterData.append('base_damage', character.baseDamage);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/characters/v/${character.slug}/edit/`, characterData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Character successfully edited"}))
			dispatch({
				type: EDIT_CHARACTER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditCharacter = () => (dispatch) => {
	dispatch({
		type: EDIT_CHARACTER,
		payload: null
	});
}

export const editNPCharacter = (slug, npCharacter) => (dispatch, getState) => {
	var npCharacterData = new FormData();
	npCharacterData.set('name', npCharacter.name);
	npCharacterData.append('description', npCharacter.description);
	if (npCharacter.imageFile !== null) {
		npCharacterData.append('image', npCharacter.imageFile);
	} else {
		npCharacterData.append('image', '');
	}
	if (npCharacter.spriteSheetFile !== null) {
		npCharacterData.append('sprite_sheet', npCharacter.spriteSheetFile);
	} else {
		npCharacterData.append('sprite_sheet', '');
	}
	npCharacterData.append('height', npCharacter.height);
	npCharacterData.append('width', npCharacter.width);
	npCharacterData.append('health', npCharacter.health);
	npCharacterData.append('speed', npCharacter.speed);
	npCharacterData.append('level', npCharacter.level);
	npCharacterData.append('hostile', npCharacter.hostile);
	npCharacterData.append('contact_damage', npCharacter.contactDamage);
	npCharacterData.append('contact_damage_amount', npCharacter.contactDamageAmount);
	npCharacterData.append('can_walk', npCharacter.canWalk);
	npCharacterData.append('can_fly', npCharacter.canFly);
	npCharacterData.append('can_attack', npCharacter.canAttack);
	npCharacterData.append('has_movement', npCharacter.hasMovement);
	npCharacterData.append('movement_type', npCharacter.movementType);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/npcharacters/v/${npCharacter.slug}/edit/`, npCharacterData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Non-Playable Character successfully edited"}))
			dispatch({
				type: EDIT_NPCHARACTER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditNPCharacter = () => (dispatch) => {
	dispatch({
		type: EDIT_NPCHARACTER,
		payload: null
	});
}

export const editLevel = (slug, level) => (dispatch, getState) => {
	var levelData = new FormData();
	levelData.set('name', level.name);
	levelData.append('description', level.description);
	if (level.imageFile !== null) {
		levelData.append('image', level.imageFile);
	} else {
		levelData.append('image', '');
	}
	levelData.append('level_type_slug', level.levelTypeSlug);
	levelData.append('width', level.width);
	levelData.append('height', level.height);
	levelData.append('tile_width', level.tileWidth);
	levelData.append('tile_height', level.tileHeight);
	levelData.append('starting_level', level.startingLevel);
	levelData.append('starting_x', level.startingX);
	levelData.append('starting_y', level.startingY);
	levelData.append('order', level.order);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/levels/v/${level.slug}/edit/`, levelData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Level successfully edited"}))
			dispatch({
				type: EDIT_LEVEL,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditLevel = () => (dispatch) => {
	dispatch({
		type: EDIT_LEVEL,
		payload: null
	});
}

export const editBackground = (slug, background) => (dispatch, getState) => {
	var backgroundData = new FormData();
	backgroundData.set('name', background.name);
	backgroundData.append('description', background.description);
	if (background.imageFile !== null) {
		backgroundData.append('image', background.imageFile);
	} else {
		backgroundData.append('image', '');
	}

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/backgrounds/v/${background.slug}/edit/`, backgroundData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Background successfully edited"}))
			dispatch({
				type: EDIT_BACKGROUND,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditBackground = () => (dispatch) => {
	dispatch({
		type: EDIT_BACKGROUND,
		payload: null
	});
}

export const editWeapon = (slug, weapon) => (dispatch, getState) => {
	var weaponData = new FormData();
	weaponData.set('name', weapon.name);
	weaponData.append('description', weapon.description);
	if (weapon.imageFile !== null) {
		weaponData.append('image', weapon.imageFile);
	} else {
		weaponData.append('image', '');
	}

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/weapons/v/${weapon.slug}/edit/`, weaponData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Weapon successfully edited"}))
			dispatch({
				type: EDIT_WEAPON,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditWeapon = () => (dispatch) => {
	dispatch({
		type: EDIT_WEAPON,
		payload: null
	});
}

export const editItem = (slug, item) => (dispatch, getState) => {
	var itemData = new FormData();
	itemData.set('name', item.name);
	itemData.append('description', item.description);
	if (item.imageFile !== null) {
		itemData.append('image', item.imageFile);
	} else {
		itemData.append('image', '');
	}

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/items/v/${item.slug}/edit/`, itemData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Item successfully edited"}))
			dispatch({
				type: EDIT_ITEM,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditItem = () => (dispatch) => {
	dispatch({
		type: EDIT_ITEM,
		payload: null
	});
}

export const editPage = (slug, page) => (dispatch, getState) => {
	var pageData = new FormData();
	pageData.set('name', page.name);
	pageData.append('description', page.description);
	if (page.imageFile !== null) {
		pageData.append('image', page.imageFile);
	} else {
		pageData.append('image', '');
	}
	pageData.append('youtube_id', page.youtubeID);
	pageData.append('youtube_url', page.youtubeURL);
	pageData.append('remove-image', page.removeImage);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/pages/v/${page.slug}/edit/`, pageData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Page successfully edited"}))
			dispatch({
				type: EDIT_PAGE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditPage = () => (dispatch) => {
	dispatch({
		type: EDIT_PAGE,
		payload: null
	});
}

// DELETE
export const deleteGame = (slug) => (dispatch, getState) => {
  const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
	  slug: slug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Game successfully deleted"}))
			dispatch({
				type: DELETE_GAME,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteGame = () => (dispatch) => {
	dispatch({
		type: DELETE_GAME,
		payload: null
	})
}

export const deleteCharacter = (slug, characterSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		characterSlug: characterSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/characters/v/${characterSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Character successfully deleted"}))
			dispatch({
				type: DELETE_CHARACTER,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteCharacter = () => (dispatch) => {
	dispatch({
		type: DELETE_CHARACTER, 
		payload: null
	});
}

export const deleteNPCharacter = (slug, npCharacterSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		npCharacterSlug: npCharacterSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/npcharacters/v/${npCharacterSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Non-Playable Character successfully deleted"}))
			dispatch({
				type: DELETE_NPCHARACTER,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteNPCharacter = () => (dispatch) => {
	dispatch({
		type: DELETE_NPCHARACTER,
		payload: null
	});
}

export const deleteLevel = (slug, levelSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		levelSlug: levelSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Level successfully deleted"}))
			dispatch({
				type: DELETE_LEVEL,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteLevel = () => (dispatch) => {
	dispatch({
		type: DELETE_LEVEL,
		payload: null
	});
}

export const deleteBackground = (slug, backgroundSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		backgroundSlug: backgroundSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/backgrounds/v/${backgroundSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Background successfully deleted"}))
			dispatch({
				type: DELETE_BACKGROUND,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteBackground = () => (dispatch) => {
	dispatch({
		type: DELETE_BACKGROUND,
		payload: null
	});
}

export const deleteWeapon = (slug, weaponSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		weaponSlug: weaponSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/weapons/v/${weaponSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Weapon successfully deleted"}))
			dispatch({
				type: DELETE_WEAPON,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteWeapon = () => (dispatch) => {
	dispatch({
		type: DELETE_WEAPON,
		payload: null
	});
}

export const deleteItem = (slug, itemSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		itemSlug: itemSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/items/v/${itemSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Item successfully deleted"}))
			dispatch({
				type: DELETE_ITEM,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteItem = () => (dispatch) => {
	dispatch({
		type: DELETE_ITEM,
		payload: null
	});
}

export const deletePage = (slug, pageSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		page_slug: pageSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/pages/v/${pageSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Page successfully deleted"}))
			dispatch({
				type: DELETE_PAGE,
				payload: slug
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeletePage = () => (dispatch) => {
	dispatch({
		type: DELETE_PAGE,
		payload: null
	});
}

export const getPageActionTypes = () => (dispatch, getState) => {
	axios
		.get(URL + `/api/games/pages/actions/types/`, tokenConfig(getState))
		.then(res  => {
			dispatch({
				type: GET_PAGE_ACTION_TYPES,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetPageActionTypes = () => (dispatch) => {
	dispatch({
		type: GET_PAGE_ACTION_TYPES,
		payload: null
	});
}

export const getPageActions = (gameSlug, pageSlug) => (dispatch, getState) => {
	axios
		.get(URL + `/api/games/v/${gameSlug}/pages/v/${pageSlug}/actions/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_PAGE_ACTIONS,
				payload: res.data
			});
			dispatch({
				type: LOADING_PAGE_ACTIONS,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetPageActions = () => (dispatch) => {
	dispatch({
		type: GET_PAGE_ACTIONS,
		payload: null
	});
	dispatch({
		type: LOADING_PAGE_ACTIONS,
		payload: true
	});
}

export const newPageAction = (slug, pageAction) => (dispatch, getState) => {
	var pageActionData = new FormData();
	pageActionData.set('actionTypeSlug', pageAction.actionTypeSlug)
	pageActionData.append('pageSlug', pageAction.page.slug);
	pageActionData.append('goToPageSlug', pageAction.goToPageSlug);
	pageActionData.append('name', pageAction.name);
	pageActionData.append('description', pageAction.description);
	pageActionData.append('image', '');
	pageActionData.append('gameActionTypeSlug', pageAction.gameActionTypeSlug);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/pages/v/${pageAction.page.slug}/actions/`, pageActionData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Page Action successfully created"}))
			dispatch({
				type: NEW_PAGE_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewPageAction = () => (dispatch) => {
	dispatch({
		type: NEW_PAGE_ACTION,
		payload: null
	});
}

export const editPageAction = (slug, pageAction) => (dispatch, getState) => {
	var pageActionData = new FormData();
	pageActionData.set('actionTypeSlug', pageAction.actionTypeSlug)
	pageActionData.append('pageSlug', pageAction.page.slug);
	pageActionData.append('goToPageSlug', pageAction.goToPageSlug);
	pageActionData.append('name', pageAction.name);
	pageActionData.append('description', pageAction.description);
	pageActionData.append('image', '');
	pageActionData.append('gameActionTypeSlug', pageAction.gameActionTypeSlug);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/pages/v/${pageAction.page.slug}/actions/v/${pageAction.actionSlug}/`, pageActionData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "Page Action successfully edited"}))
			dispatch({
				type: EDIT_PAGE_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditPageAction = () => (dispatch) => {
	dispatch({
		type: EDIT_PAGE_ACTION,
		payload: null
	});
}

export const deletePageAction = (slug, pageSlug, actionSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		page_slug: pageSlug,
		action_slug: actionSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/pages/v/${pageSlug}/actions/v/${actionSlug}/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Page Action successfully deleted"}))
			dispatch({
				type: DELETE_PAGE_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeletePageAction = () => (dispatch) => {
	dispatch({
		type: DELETE_PAGE_ACTION,
		payload: null
	});
}

export const getGameActionTypes = () => (dispatch, getState) => {
	axios
		.get(URL + `/api/games/actions/types/`, tokenConfig(getState))
		.then(res => {
			dispatch({
				type: GET_GAME_ACTION_TYPES,
				payload: res.data
			});
			dispatch({
				type: LOADING_GAME_ACTION_TYPES,
				payload: false
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetGameActionTypes = () => (dispatch) => {
  dispatch({
    type: GET_GAME_ACTION_TYPES,
    payload: null
  });
  dispatch({
    type: LOADING_GAME_ACTION_TYPES,
    payload: true
  });
}






export const newTile = (slug, tile) => (dispatch, getState) => {
	var tileData = new FormData();
	tileData.set('name', tile.name);
	if (tile.imageFile !== null) {
		tileData.append('image', tile.imageFile);
	} else {
		tileData.append('image', '');
	}
	tileData.append('height', tile.height);
	tileData.append('width', tile.width);
	tileData.append('walkable', tile.walkable);
	tileData.append('display', tile.display);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/tiles/new/`, tileData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Tile successfully created"}))
			dispatch({
				type: NEW_GAME_TILE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewTile = () => (dispatch) => {
	dispatch({
		type: NEW_GAME_TILE,
		payload: null
	});
}

export const editTile = (slug, tile) => (dispatch, getState) => {
	var tileData = new FormData();
	tileData.set('name', tile.name);
	if (tile.imageFile !== null) {
		tileData.append('image', tile.imageFile);
	} else {
		tileData.append('image', '');
	}
	tileData.append('height', tile.height);
	tileData.append('width', tile.width);
	tileData.append('walkable', tile.walkable);
	tileData.append('display', tile.display);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/tiles/v/${tile.slug}/edit/`, tileData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Tile successfully edited"}))
			dispatch({
				type: EDIT_GAME_TILE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditTile = () => (dispatch) => {
	dispatch({
		type: EDIT_GAME_TILE,
		payload: null
	});
}

export const deleteTile = (slug, tileSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		tileSlug: tileSlug
	}

	axios
		.delete(URL + `/api/games/v/${slug}/tiles/v/${tileSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Tile successfully deleted"}))
			dispatch({
				type: DELETE_GAME_TILE,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteTile = () => (dispatch) => {
	dispatch({
		type: DELETE_GAME_TILE,
		payload: null
	});
}











export const newLevelTile = (slug, levelSlug, tileSlug, positionData, display) => (dispatch, getState) => {
	var levelTileData = new FormData();
	levelTileData.set('tile_slug', tileSlug);
	levelTileData.append('position_x', positionData.positionX);
	levelTileData.append('position_y', positionData.positionY);
	levelTileData.append('display', display);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/tiles/new/`, levelTileData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Level Tile successfully created"}))
			dispatch({
				type: NEW_LEVEL_TILE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewLevelTile = () => (dispatch) => {
	dispatch({
		type: NEW_LEVEL_TILE,
		payload: null
	});
}

export const editLevelTile = (slug, levelSlug, tileSlug, positionData, id, display) => (dispatch, getState) => {
	var levelTileData = new FormData();
	levelTileData.set('tile_slug', tileSlug);
	levelTileData.append('position_x', positionData.positionX);
	levelTileData.append('position_y', positionData.positionY);
	levelTileData.append('display', display);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/tiles/v/${tileSlug}/${id}/edit/`, levelTileData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Level Tile successfully edited"}))
			dispatch({
				type: EDIT_LEVEL_TILE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditLevelTile = () => (dispatch) => {
	dispatch({
		type: EDIT_LEVEL_TILE,
		payload: null
	});
}

export const deleteLevelTile = (slug, levelSlug, tileSlug, id) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		levelSlug: levelSlug,
		tileSlug: tileSlug,
		id: id
	}

	axios
		.delete(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/tiles/v/${tileSlug}/${id}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Level Tile successfully deleted"}))
			dispatch({
				type: DELETE_LEVEL_TILE,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteLevelTile = () => (dispatch) => {
	dispatch({
		type: DELETE_LEVEL_TILE,
		payload: null
	});
}




export const newLevelNPCharacter = (slug, levelSlug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug) => (dispatch, getState) => {
	var npCharacterData = new FormData();
	npCharacterData.set('npcharacter_slug', npCharacterSlug);
	npCharacterData.append('position_x', positionData.positionX);
	npCharacterData.append('position_y', positionData.positionY);
	npCharacterData.append('health', health);
	npCharacterData.append('hostile', hostile);
	npCharacterData.append('display', display);
	npCharacterData.append('randomize', randomize);
	npCharacterData.append('boss', boss);
	npCharacterData.append('conversation_slug', conversationSlug);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/npcharacters/new/`, npCharacterData, config)
		.then(res => {
			dispatch(createMessage({ characterCreated: "New Level NPCharacter successfully created"}))
			dispatch({
				type: NEW_LEVEL_NPCHARACTER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewLevelNPCharacter = () => (dispatch) => {
	dispatch({
		type: NEW_LEVEL_NPCHARACTER,
		payload: null
	});
}

export const editLevelNPCharacter = (slug, levelSlug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug, id) => (dispatch, getState) => {
	var npCharacterData = new FormData();
	npCharacterData.set('npcharacter_slug', npCharacterSlug);
	npCharacterData.append('position_x', positionData.positionX);
	npCharacterData.append('position_y', positionData.positionY);
	npCharacterData.append('health', health);
	npCharacterData.append('hostile', hostile);
	npCharacterData.append('display', display);
	npCharacterData.append('randomize', randomize);
	npCharacterData.append('boss', boss);
	npCharacterData.append('conversation_slug', conversationSlug);

  const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/npcharacters/v/${npCharacterSlug}/${id}/edit/`, npCharacterData, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Level NPCharacter successfully edited"}))
			dispatch({
				type: EDIT_LEVEL_NPCHARACTER,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditLevelNPCharacter = () => (dispatch) => {
	dispatch({
		type: EDIT_LEVEL_NPCHARACTER,
		payload: null
	});
}

export const deleteLevelNPCharacter = (slug, levelSlug, npCharacterSlug, id) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		slug: slug,
		levelSlug: levelSlug,
		npCharacterSlug: npCharacterSlug,
		id: id
	}

	axios
		.delete(URL + `/api/games/v/${slug}/levels/v/${levelSlug}/npcharacters/v/${npCharacterSlug}/${id}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Level NPCharacter successfully deleted"}))
			dispatch({
				type: DELETE_LEVEL_NPCHARACTER,
				payload: slug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteLevelNPCharacter = () => (dispatch) => {
	dispatch({
		type: DELETE_LEVEL_NPCHARACTER,
		payload: null
	});
}

export const editLevelAction = (gameSlug, levelSlug, levelTileData, levelActionData, id) => (dispatch, getState) => {
	var levelActionForm = new FormData();
	levelActionForm.set('tile_slug', levelTileData.tileSlug);
	levelActionForm.append('position_x', levelTileData.positionX);
	levelActionForm.append('position_y', levelTileData.positionY);
	levelActionForm.append('display', levelTileData.display);
	levelActionForm.append('action_type', levelActionData.levelActionTypeSlug);
	levelActionForm.append('game_action_type', levelActionData.gameActionTypeSlug);
	levelActionForm.append('change_cutscene', levelActionData.cutsceneSlug);
	levelActionForm.append('change_level', levelActionData.levelChangeSlug);
	levelActionForm.append('require_complete', levelActionData.requireComplete);
	levelActionForm.append('defeat_all_enemies', levelActionData.defeatAllEnemies);
	levelActionForm.append('defeat_boss', levelActionData.defeatBoss);
	levelActionForm.append('id', id);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${gameSlug}/levels/v/${levelSlug}/actions/v/${levelActionData.levelActionTypeSlug}/${id}/edit/`, levelActionForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Level Action successfully edited"}))
			dispatch({
				type: EDIT_LEVEL_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditLevelAction = () => (dispatch) => {
	dispatch({
		type: EDIT_LEVEL_ACTION,
		payload: null
	});
}

export const deleteLevelAction = (gameSlug, levelSlug, actionTypeSlug, id) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		gameSlug: gameSlug,
		levelSlug: levelSlug,
		actionTypeSlug: actionTypeSlug,
		id: id
	}

	axios
		.delete(URL + `/api/games/v/${gameSlug}/levels/v/${levelSlug}/actions/v/${actionTypeSlug}/${id}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Level Action successfully deleted"}))
			dispatch({
				type: DELETE_LEVEL_ACTION,
				payload: gameSlug
			});
		})
		// .then(() => {
		// 	window.location = `/#/journals/`
		// })
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteLevelAction = () => (dispatch) => {
	dispatch({
		type: DELETE_LEVEL_ACTION,
		payload: null
	});
}


// CUTSCENES
export const newCutscene = (gameSlug, cutsceneData) => (dispatch, getState) => {
	var cutsceneForm = new FormData();
	cutsceneForm.set('name', cutsceneData.name);
	cutsceneForm.append('description', cutsceneData.description);
	if (cutsceneData.imageFile !== null) {
		cutsceneForm.append('image', cutsceneData.imageFile);
	} else {
		cutsceneForm.append('image', '');
	}
	cutsceneForm.append('url_youtube', cutsceneData.fieldYouTube);
	cutsceneForm.append('url_vimeo', cutsceneData.fieldVimeo);
	cutsceneForm.append('url_archive', cutsceneData.fieldArchive);
	cutsceneForm.append('width', cutsceneData.width);
	cutsceneForm.append('height', cutsceneData.height);
	cutsceneForm.append('autoplay', cutsceneData.autoplay);
	cutsceneForm.append('loop', cutsceneData.loop);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/cutscenes/new/`, cutsceneForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Cutscene successfully created"}))
			dispatch({
				type: NEW_CUTSCENE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewCutscene = () => (dispatch) => {
	dispatch({
		type: NEW_CUTSCENE,
		payload: null
	});
}

export const editCutscene = (gameSlug, cutsceneData) => (dispatch, getState) => {
	var cutsceneForm = new FormData();
	cutsceneForm.set('name', cutsceneData.name);
	cutsceneForm.append('description', cutsceneData.description);
	if (cutsceneData.imageFile !== null) {
		cutsceneForm.append('image', cutsceneData.imageFile);
	} else {
		cutsceneForm.append('image', '');
	}
	cutsceneForm.append('url_youtube', cutsceneData.fieldYouTube);
	cutsceneForm.append('url_vimeo', cutsceneData.fieldVimeo);
	cutsceneForm.append('url_archive', cutsceneData.fieldArchive);
	cutsceneForm.append('width', cutsceneData.width);
	cutsceneForm.append('height', cutsceneData.height);
	cutsceneForm.append('autoplay', cutsceneData.autoplay);
	cutsceneForm.append('loop', cutsceneData.loop);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${gameSlug}/cutscenes/v/${cutsceneData.slug}/edit/`, cutsceneForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Cutscene successfully edited"}))
			dispatch({
				type: EDIT_CUTSCENE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditCutscene = () => (dispatch) => {
	dispatch({
		type: EDIT_CUTSCENE,
		payload: null
	});
}

export const deleteCutscene = (gameSlug, cutsceneSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		gameSlug: gameSlug,
		cutsceneSlug: cutsceneSlug
	}

	axios
		.delete(URL + `/api/games/v/${gameSlug}/cutscenes/v/${cutsceneSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Cutscene successfully deleted."}))
			dispatch({
				type: DELETE_CUTSCENE,
				payload: gameSlug
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteCutscene = () => (dispatch) => {
	dispatch({
		type: DELETE_CUTSCENE,
		payload: null
	});
}

export const getCutsceneActionTypes = () => (dispatch, getState) => {
	axios
    .get(URL + `/api/games/cutscenes/actions/types/`, tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_CUTSCENE_ACTION_TYPES,
        payload: res.data
      });
			dispatch({
        type: LOADING_CUTSCENE_ACTION_TYPES,
        payload: false
      });
    })
    .catch(err => 
    	dispatch(returnErrors(err.response.data, err.response.status))
    );
}

export const resetCutsceneActionTypes = () => (dispatch) => {
	dispatch({
		type: GET_CUTSCENE_ACTION_TYPES,
		payload: null
	});
	dispatch({
		type: LOADING_CUTSCENE_ACTION_TYPES,
		payload: true
	});
}

export const newCutsceneAction = (gameSlug, cutsceneActionData) => (dispatch, getState) => {
	var cutsceneActionForm = new FormData();
	cutsceneActionForm.set('cutscene_slug', cutsceneActionData.cutscene.slug);
	cutsceneActionForm.append('name', cutsceneActionData.actionName);
	cutsceneActionForm.append('action_type_slug', cutsceneActionData.cutsceneActionTypeSlug);
	if (cutsceneActionData.levelSlug !== '') {
		cutsceneActionForm.append('level_slug', cutsceneActionData.levelSlug);
	} else {
		cutsceneActionForm.append('level_slug', '');
	}
	if (cutsceneActionData.changeCutsceneSlug !== '') {
		cutsceneActionForm.append('change_cutscene_slug', cutsceneActionData.changeCutsceneSlug);
	} else {
		cutsceneActionForm.append('change_cutscene_slug', '');
	}
	if (cutsceneActionData.gameActionTypeSlug !== '') {
		cutsceneActionForm.append('game_action_type_slug', cutsceneActionData.gameActionTypeSlug);
	} else {
		cutsceneActionForm.append('game_action_type_slug', '');
	}
	cutsceneActionForm.append('require_complete', cutsceneActionData.requireComplete);
	cutsceneActionForm.append('auto_select', cutsceneActionData.autoSelect);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/cutscenes/v/${cutsceneActionData.cutscene.slug}/actions/new/`, cutsceneActionForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Cutscene Action successfully created"}))
			dispatch({
				type: NEW_CUTSCENE_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewCutsceneAction = () => (dispatch) => {
	dispatch({
		type: NEW_CUTSCENE_ACTION,
		payload: null
	});
}

export const editCutsceneAction = (gameSlug, cutsceneActionData) => (dispatch, getState) => {
	var cutsceneActionForm = new FormData();
	cutsceneActionForm.set('cutscene_slug', cutsceneActionData.cutsceneAction.cutscene.slug);
	cutsceneActionForm.append('name', cutsceneActionData.name);
	cutsceneActionForm.append('action_type_slug', cutsceneActionData.cutsceneActionTypeSlug);
	if (cutsceneActionData.levelSlug !== '') {
		cutsceneActionForm.append('level_slug', cutsceneActionData.levelSlug);
	} else {
		cutsceneActionForm.append('level_slug', '');
	}
	if (cutsceneActionData.changeCutsceneSlug !== '') {
		cutsceneActionForm.append('change_cutscene_slug', cutsceneActionData.changeCutsceneSlug);
	} else {
		cutsceneActionForm.append('change_cutscene_slug', '');
	}
	if (cutsceneActionData.gameActionTypeSlug !== '') {
		cutsceneActionForm.append('game_action_type_slug', cutsceneActionData.gameActionTypeSlug);
	} else {
		cutsceneActionForm.append('game_action_type_slug', '');
	}
	cutsceneActionForm.append('require_complete', cutsceneActionData.requireComplete);
	cutsceneActionForm.append('auto_select', cutsceneActionData.autoSelect);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${gameSlug}/cutscenes/v/${cutsceneActionData.cutsceneAction.cutscene.slug}/actions/v/${cutsceneActionData.cutsceneAction.slug}/edit/`, cutsceneActionForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Cutscene Action successfully edited"}))
			dispatch({
				type: EDIT_CUTSCENE_ACTION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditCutsceneAction = () => (dispatch) => {
	dispatch({
		type: EDIT_CUTSCENE_ACTION,
		payload: null
	});
}

export const deleteCutsceneAction = (gameSlug, cutsceneSlug, cutsceneActionSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		gameSlug: gameSlug,
		cutsceneSlug: cutsceneSlug,
		cutsceneActionSlug: cutsceneActionSlug
	}

	axios
		.delete(URL + `/api/games/v/${gameSlug}/cutscenes/v/${cutsceneSlug}/actions/v/${cutsceneActionSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Cutscene Action successfully deleted."}))
			dispatch({
				type: DELETE_CUTSCENE,
				payload: gameSlug
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteCutsceneAction = () => (dispatch) => {
	dispatch({
		type: DELETE_CUTSCENE_ACTION,
		payload: null
	});
}




// CONVERSATIONS
export const newConversation = (gameSlug, conversationData) => (dispatch, getState) => {
	var conversationForm = new FormData();
	conversationForm.set('name', conversationData.name);
	conversationForm.append('description', conversationData.description);
	if (conversationData.imageFile !== null) {
		conversationForm.append('image', conversationData.imageFile);
	} else {
		conversationForm.append('image', '');
	}
	conversationForm.append('url_youtube', conversationData.fieldYouTube);
	conversationForm.append('url_vimeo', conversationData.fieldVimeo);
	conversationForm.append('url_archive', conversationData.fieldArchive);
	conversationForm.append('width', conversationData.width);
	conversationForm.append('height', conversationData.height);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/conversations/new/`, conversationForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Conversation successfully created"}))
			dispatch({
				type: NEW_CONVERSATION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewConversation = () => (dispatch) => {
	dispatch({
		type: NEW_CONVERSATION,
		payload: null
	});
}

export const editConversation = (gameSlug, conversationData) => (dispatch, getState) => {
	var conversationForm = new FormData();
	conversationForm.set('name', conversationData.name);
	conversationForm.append('description', conversationData.description);
	if (conversationData.imageFile !== null) {
		conversationForm.append('image', conversationData.imageFile);
	} else {
		conversationForm.append('image', '');
	}
	conversationForm.append('url_youtube', conversationData.fieldYouTube);
	conversationForm.append('url_vimeo', conversationData.fieldVimeo);
	conversationForm.append('url_archive', conversationData.fieldArchive);
	conversationForm.append('width', conversationData.width);
	conversationForm.append('height', conversationData.height);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationData.slug}/edit/`, conversationForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Conversation successfully edited"}))
			dispatch({
				type: EDIT_CONVERSATION,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditConversation = () => (dispatch) => {
	dispatch({
		type: EDIT_CONVERSATION,
		payload: null
	});
}

export const deleteConversation = (gameSlug, conversationSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		gameSlug: gameSlug,
		conversationSlug: conversationSlug
	}

	axios
		.delete(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Conversation successfully deleted."}))
			dispatch({
				type: DELETE_CONVERSATION,
				payload: gameSlug
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteConversation = () => (dispatch) => {
	dispatch({
		type: DELETE_CONVERSATION,
		payload: null
	});
}

export const newConversationDialogue = (gameSlug, conversationSlug, dialogueData) => (dispatch, getState) => {
	var dialogueDataForm = new FormData();
	dialogueDataForm.set('name', dialogueData.name);
	dialogueDataForm.append('description', dialogueData.description);
	if (dialogueData.imageFile !== null) {
		dialogueDataForm.append('image', dialogueData.imageFile);
	} else {
		dialogueDataForm.append('image', '');
	}
	dialogueDataForm.append('opening_dialogue', dialogueData.openingDialogue);
	dialogueDataForm.append('closing_dialogue', dialogueData.closingDialogue);
	dialogueDataForm.append('embed_facebook', dialogueData.embedFacebook);
	dialogueDataForm.append('embed_twitter', dialogueData.embedTwitter);
	dialogueDataForm.append('embed_youtube', dialogueData.embedYouTube);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/new/`, dialogueDataForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Conversation Dialogue successfully created"}))
			dispatch({
				type: NEW_CONVERSATION_DIALOGUE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewConversationDialogue = () => (dispatch) => {
	dispatch({
		type: NEW_CONVERSATION_DIALOGUE,
		payload: null
	});
}

export const editConversationDialogue = (gameSlug, conversationSlug, dialogueData) => (dispatch, getState) => {
	var dialogueDataForm = new FormData();
	dialogueDataForm.set('name', dialogueData.name);
	dialogueDataForm.append('description', dialogueData.description);
	if (dialogueData.imageFile !== null) {
		dialogueDataForm.append('image', dialogueData.imageFile);
	} else {
		dialogueDataForm.append('image', '');
	}
	dialogueDataForm.append('opening_dialogue', dialogueData.openingDialogue);
	dialogueDataForm.append('closing_dialogue', dialogueData.closingDialogue);
	dialogueDataForm.append('embed_facebook', dialogueData.embedFacebook);
	dialogueDataForm.append('embed_twitter', dialogueData.embedTwitter);
	dialogueDataForm.append('embed_youtube', dialogueData.embedYouTube);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/v/${dialogueData.slug}/edit/`, dialogueDataForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Conversation Dialogue successfully edited"}))
			dispatch({
				type: EDIT_CONVERSATION_DIALOGUE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditConversationDialogue = () => (dispatch) => {
	dispatch({
		type: EDIT_CONVERSATION_DIALOGUE,
		payload: null
	});
}

export const deleteConversationDialogue = (gameSlug, conversationSlug, dialogueSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		gameSlug: gameSlug,
		conversationSlug: conversationSlug,
		dialogueSlug: dialogueSlug
	}

	axios
		.delete(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Dialogue successfully deleted."}))
			dispatch({
				type: DELETE_CONVERSATION_DIALOGUE,
				payload: dialogueSlug
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteConversationDialogue = () => (dispatch) => {
	dispatch({
		type: DELETE_CONVERSATION_DIALOGUE,
		payload: null
	});
}

export const newDialogueResponse = (gameSlug, conversationSlug, dialogueSlug, dialogueResponseData) => (dispatch, getState) => {
	var dialogueResponseDataForm = new FormData();
	dialogueResponseDataForm.set('name', dialogueResponseData.name);
	dialogueResponseDataForm.append('description', dialogueResponseData.description);
	if (dialogueResponseData.imageFile !== null) {
		dialogueResponseDataForm.append('image', dialogueResponseData.imageFile);
	} else {
		dialogueResponseDataForm.append('image', '');
	}
	dialogueResponseDataForm.append('dialogue_slug', dialogueResponseData.dialogueSlug);
	dialogueResponseDataForm.append('connect_to_slug', dialogueResponseData.connectToSlug);
	if (dialogueResponseData.connectToSlug !== '') {
		dialogueResponseDataForm.append('ending_response', false);
	} else {
		dialogueResponseDataForm.append('ending_response', dialogueResponseData.endingResponse);
	}
	dialogueResponseDataForm.append('order', dialogueResponseData.order);

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.post(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/responses/new/`, dialogueResponseDataForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Dialogue Response successfully created"}))
			dispatch({
				type: NEW_DIALOGUE_RESPONSE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetNewDialogueResponse = () => (dispatch) => {
	dispatch({
		type: NEW_DIALOGUE_RESPONSE,
		payload: null
	});
}

export const editDialogueResponse = (gameSlug, conversationSlug, dialogueSlug, dialogueResponseData) => (dispatch, getState) => {
	var dialogueResponseDataForm = new FormData();
	dialogueResponseDataForm.set('name', dialogueResponseData.name);
	dialogueResponseDataForm.append('description', dialogueResponseData.description);
	if (dialogueResponseData.imageFile !== null) {
		dialogueResponseDataForm.append('image', dialogueResponseData.imageFile);
	} else {
		dialogueResponseDataForm.append('image', '');
	}
	dialogueResponseDataForm.append('dialogue_slug', dialogueResponseData.dialogueSlug);
	dialogueResponseDataForm.append('connect_to_slug', dialogueResponseData.connectToSlug);
	if (dialogueResponseData.connectToSlug !== '') {
		dialogueResponseDataForm.append('ending_response', false);
	} else {
		dialogueResponseDataForm.append('ending_response', dialogueResponseData.endingResponse);
	}
	dialogueResponseDataForm.append('order', dialogueResponseData.order);
	

	const token = getState().auth.token;

	// Headers
	const config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		},
		onUploadProgress: function (progressEvent) {
	    let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
	    document.getElementById('uploadProgress').innerHTML = 'Uploading... ' + percentCompleted + '%';
	  }
	}

	// If Token
	if(token) {
		config.headers['Authorization'] = `Token ${token}`;
	}

	axios
		.put(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/responses/v/${dialogueResponseData.slug}/edit/`, dialogueResponseDataForm, config)
		.then(res => {
			dispatch(createMessage({ characterEdited: "Dialogue Response successfully edited"}))
			dispatch({
				type: EDIT_DIALOGUE_RESPONSE,
				payload: res.data
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetEditDialogueResponse = () => (dispatch) => {
	dispatch({
		type: EDIT_DIALOGUE_RESPONSE,
		payload: null
	});
}

export const deleteDialogueResponse = (gameSlug, conversationSlug, dialogueSlug, responseSlug) => (dispatch, getState) => {
	const token = getState().auth.token;

	const headers = {
		'Content-Type': 'application/json'
	}

	if(token) {
		headers['Authorization'] = `Token ${token}`
	}

	const data = {
		gameSlug: gameSlug,
		conversationSlug: conversationSlug,
		dialogueSlug: dialogueSlug,
		responseSlug: responseSlug
	}

	axios
		.delete(URL + `/api/games/v/${gameSlug}/conversations/v/${conversationSlug}/dialogue/v/${dialogueSlug}/responses/v/${responseSlug}/edit/`, {headers, data})
		.then(res => {
			dispatch(createMessage({ gameDeleted: "Dialogue Response successfully deleted."}))
			dispatch({
				type: DELETE_DIALOGUE_RESPONSE,
				payload: responseSlug
			});
		})
		.catch(err => 
			dispatch(returnErrors(err.response.data, err.response.status))
		);
}

export const resetDeleteDialogueResponse = () => (dispatch) => {
	dispatch({
		type: DELETE_DIALOGUE_RESPONSE,
		payload: null
	});
}