import React, { Component } from 'react'
import { connect } from 'react-redux'

import Login from '../components/auth/Login';
import Logout from '../components/auth/Logout';

import Games from '../components/games/Games';

import Header from '../components/layout/Header';

import UserProfiles from '../components/profiles/UserProfiles';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLogin: false,
      showLogout: false,
      showGames: true,
      showUserProfiles: false,
      gameSlug: '',
      profileSlug: ''
    }
  }

  handleShowLogin = () => {
    this.setState({
      showLogin: true,
      showLogout: false,
      showGames: false,
      showUserProfiles: false
    });
  }
  
  handleShowLogout = () => {
    this.setState({
      showLogin: false,
      showLogout: true,
      showGames: false,
      showUserProfiles: false
    });
  }

  handleShowGames = () => {
    this.setState({
      showLogin: false,
      showLogout: false,
      showGames: true,
      showUserProfiles: false
    });
  }

  handleShowUserProfiles = () => {
    this.setState({
      showLogin: false,
      showLogout: false,
      showGames: false,
      showUserProfiles: true
    });
  }

  render() {
    const { showLogin, showLogout, showGames, showUserProfiles } = this.state;
    const { gameSlug, profileSlug } = this.state;
    const { isAuthenticated } = this.props;

    return (
      <div>
        <Header showLogin={this.handleShowLogin} showLogout={this.handleShowLogout} showGames={this.handleShowGames} />
        <div>
          {/* {showLogin && 
            <Login showGames={this.handleShowGames} />
          } */}
          {showLogout && 
            <Logout showGames={this.handleShowGames} />
          }
          {isAuthenticated ? 
            <div>
              {showGames && 
                <Games gameSlug={gameSlug} />
              }
              {showUserProfiles && 
                <UserProfiles profileSlug={profileSlug} />
              }
            </div> : 
            <div>
              {showLogin ? 
                <Login showGames={this.handleShowGames} /> :
                <div>
                  <span>Coming Soon</span>
                </div>
              }
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
