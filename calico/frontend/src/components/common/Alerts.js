import React, { Component, Fragment } from "react";
import { withAlert } from "react-alert";
import { connect } from "react-redux";
import PropTypes from "prop-types";

export class Alerts extends Component {
  static propTypes = {
    // error: PropTypes.object.isRequired,
    message: PropTypes.object.isRequired
  };
  // componentDidMount() {
  //   const { alert } = this.props;
  //   alert.success('hi');
  // }

  componentDidUpdate(prevProps) {
    const { error, alert, message } = this.props;
    if (error !== prevProps.error) {
      console.log('What');
      // if (error.msg.name) alert.error(`Name: ${error.msg.name.join()}`);
      // if (error.msg.email) alert.error(`Email: ${error.msg.email.join()}`);
      // if (error.msg.message)
      //   alert.error(`Message: ${error.msg.message.join()}`);
      // if (error.msg.non_field_errors)
      //   alert.error(error.msg.non_field_errors.join());
      // if (error.msg.username) alert.error(error.msg.username.join());
      if (error.msg && error.status) alert.error(error.status, error.msg);
    }

    if (message !== prevProps.message) {
      if (message.loginRequest) alert.success(message.loginRequest);
      if (message.logoutRequest) alert.success(message.logoutRequest);

      if (message.gameCreated) alert.success(message.gameCreated);
      if (message.gameEdited) alert.success(message.gameEdited);
      if (message.gameDeleted) alert.success(message.gameDeleted);
      if (message.characterCreated) alert.success(message.characterCreated);
      if (message.characterEdited) alert.success(message.characterEdited);
      if (message.characterDeleted) alert.success(message.characterDeleted);
    }
  }

  render() {
    return <Fragment />;
  }
}

const mapStateToProps = state => ({
  error: state.errors,
  message: state.messages
});

export default connect(mapStateToProps)(withAlert()(Alerts));