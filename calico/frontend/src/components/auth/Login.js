import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../layout/Styles';

import { login } from '../../actions/auth';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  handleCancel = () => {
    this.props.showGames();
  }

  handleLogin = () => {
		this.props.login(this.state.username, this.state.password);
	}

  onChange = e => this.setState({
		[e.target.name]: e.target.value
  });
  
  componentDidUpdate(lastProps, lastState) {
    if (this.props.isAuthenticated === true && lastProps.isAuthenticated === false) {
      this.props.showGames();
    }
  }

  render() {
    const { username, password } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <input 
            type="text" 
            className="form-control" 
            style={styles.textInput}
            name="username" 
            onChange={this.onChange} 
            value={username} 
            placeholder="User Name" 
          />
        </div>
        <div style={{paddingBottom: 10}}>
          <input 
            type="password" 
            className="form-control" 
            style={styles.textInput}
            name="password" 
            onChange={this.onChange} 
            value={password} 
            placeholder="Password" 
          />
        </div>
        <div style={{display: 'flex'}}>
          <div style={{flex: 1, textAlign: 'center'}}>
            <button style={styles.buttonDefault} onClick={this.handleLogin}>
              Login
            </button>
          </div>
          <div style={{flex: 1, textAlign: 'center'}}>
            <button style={styles.buttonDefault} onClick={this.handleCancel}>
              Cancel
            </button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = {
  login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
