import React, { Component } from 'react'
import { connect } from 'react-redux'

import { logout } from '../../actions/auth';

export class Logout extends Component {
  handleLogout = () => {
    this.props.logout();
  }

  handleCancel = () => {
    this.props.showGames();
  }

  componentDidUpdate(lastProps) {
    if (this.props.isAuthenticated === false && lastProps.isAuthenticated === true) {
      this.props.showGames();
    }
  }

  render() {
    return (
      <div>
        <div>Logout?</div>
        <div><button onClick={this.handleLogout}>Yes</button></div>
        <div><button onClick={this.handleCancel}>Cancel</button></div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = {
  logout
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)
