import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import store from '../store';

import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import Alerts from './common/Alerts';

import HomeScreen from '../screens/HomeScreen';

import { loadUser } from '../actions/auth';

import './layout/fonts.css';
import './layout/styles.css';
import { styles } from './layout/Styles';

// Alert options
const alertOptions = {
	timeout: 3000,
	position: 'top center'
}

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUser());
  }
  
  render() {
    return (
      <div style={{flex: 1, display: 'flex'}}>
        <div style={{flex: 1}} />
        <div style={{flex: 8}}>
          <Provider store={store}>
            <AlertProvider template={AlertTemplate} {...alertOptions}>
              <div>
                <Alerts />
                <HomeScreen />
              </div>
            </AlertProvider>
          </Provider>
        </div>
        <div style={{flex: 1}} />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'));