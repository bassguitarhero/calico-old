import React, { Component } from 'react'
import { connect } from 'react-redux'
import { styles } from './Styles';

class Header extends Component {
  handleShowLogin = () => {
    this.props.showLogin();
  }

  handleShowLogout = () => {
    this.props.showLogout();
  }

  render() {
    const { isAuthenticated } = this.props;

    return (
      <div style={{flex: 1, display: "flex"}}>
        <div style={{paddingTop: 20}}>
          {isAuthenticated ? 
            <span style={styles.headerText}>Calico</span> : 
            <span style={styles.headerText}>Coming Soon</span>
          }
        </div>
        <div style={{flex: 1, alignItems: 'flex-end', paddingTop: 10, paddingBottom: 10}}>
          {(isAuthenticated === true) ? 
            <div style={{textAlign: 'right'}}>
              <button style={styles.button, styles.buttonDefault} onClick={this.handleShowLogout}>Logout</button>
            </div> :
            <div style={{textAlign: 'right'}}>
              <button style={styles.button, styles.buttonDefault} onClick={this.handleShowLogin}>Login</button>
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
