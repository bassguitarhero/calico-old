export const styles = {
  actionsContainer: {
    paddingTop: 10,
    flex: 1
  },
  actionContainer: {
    padding: 10,
    flex: 1,
    textAlign: 'center'
  },
  button: {
    display: 'inlineBlock',
    margin: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#000',
    textAlign: 'center',
    transition: 'all 0.2s',
  },
  buttonAction: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#000',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: '#FFF',
    fontSize: 16
  },
  buttonBack: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#000',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: '#FFF',
  },
  buttonCheckbox: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#000',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: '#FFF',
    fontSize: 14
  },
  buttonDefault: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#FFFFFF',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: '#4eb5f1',
  },
  buttonEdit: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#FFFFFF',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: 'green'
  },
  buttonNav: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#000',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: '#999'
  },
  buttonNew: {
    display: 'inlineBlock',
    padding: 10,
    borderRadius: '2em',
    boxSizing: 'borderBox',
    textDecoration: 'none',
    fontWeight: 300,
    color: '#FFFFFF',
    textAlign: 'center',
    transition: 'all 0.2s',
    backgroundColor: 'red'
  },
  characterImageFull: {
    width: '100%',
    height: 'auto'
  },
  characterImageThumb: {
    width: 64,
    height: 64
  },
  container: {
    flex: 1
  },
  defaultTextContainer: {
    padding: 5
  },
  defaultText: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    whiteSpace: 'pre-line'
  },
  descriptionContainer: {
    padding: 5
  },
  descriptionText: {
    fontSize: 18,
    fontFamily: 'sans-serif',
    whiteSpace: 'pre-line'
  },
  gamePagesContainer: {
    padding: 5
  },
  gamePagesText: {
    fontSize: 16,
    fontFamily: 'sans-serif'
  },
  gamesListHR: {
    width: '100%'
  },
  gameTypeContainer: {
    padding: 5
  },
  gameTypeText: {
    fontSize: 16,
    fontFamily: 'sans-serif'
  },
  headerTextContainer: {
    padding: 10
  },
  headerText: {
    fontFamily: 'sans-serif',
    fontWeight: 'bold',
    fontSize: 24,
  },
  lastEditedContainer: {
    padding: 5
  },
  lastEditedText: {
    fontSize: 16,
    fontStyle: 'italic',
    fontFamily: 'sans-serif'
  },
  loadingText: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'sans-serif'
  },
  loadingTextContainer: {
    padding: 5
  },
  modal: {
		width: '60%',
		backgroundColor: '#FFF',
		boxShadow: '0 2px 8px rgba(0, 0, 0, .3)',
		borderRadius: 25,
		position: 'fixed',
		top: '4vh',
		left: '20%',
		maxHeight: '100vh',
		minHeight: '80vh',
		overflowY: 'auto',
		overflowScrolling: "touch",
		WebkitOverflowScrolling: "touch",
		zIndex: 1000,
	},
  nameContainer: {
    padding: 5
  },
  nameText: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'sans-serif'
  },
  newGameButton: {
    position: 'fixed',
    bottom: '5%',
    right: '11%'
  },
  newGameButtonPlaceholder: {
    position: 'relative'
  },
  pageTextContainer: {
    padding: 5
  },
  pageText: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    whiteSpace: 'pre-line'
  },
  selectInput: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    padding: 5,
    width: '60%'
  },
  textareaInput: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    padding: 5,
    width: '60%'
  },
  textareaPageInput: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    padding: 5,
    width: '95%'
  },
  textInput: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    padding: 5,
    width: '60%'
  },
	titleContainer: {
    padding: 5
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'sans-serif'
  },
  usernameContainer: {
    padding: 5
  },
  usernameText: {
    fontSize: 16,
    fontFamily: 'sans-serif'
  },
  viewGameContainer: {
    padding: 5
  },
  youtubeContainer: {
    padding: 5
  }
}