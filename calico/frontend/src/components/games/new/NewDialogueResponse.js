import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../../layout/Styles';

import { newDialogueResponse, resetNewDialogueResponse } from '../../../actions/game_admin';
import { getDialogueResponses, resetDialogueResponses } from '../../../actions/games';

export class NewDialogueResponse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      image: '',
      imageFile: '',
      description: '',
      dialogueSlug: '',
      connectToSlug: '',
      endingResponse: false,
      uploadingData: false,
      conversationDialogueOptions: [],
      order: 1
    }
  }

  handleCreateNewDialogueResponse = () => {
    const { dialogue } = this.props;
    const { name, description, image, imageFile, dialogueSlug, connectToSlug, endingResponse, order } = this.state;
    const dialogueResponseData = { name, description, image, imageFile, dialogueSlug, connectToSlug, endingResponse, order };
    if (name !== '') {
			if (window.confirm("Create New Dialogue Response?")) {
        this.props.newDialogueResponse(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug, dialogueResponseData);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Dialogue Responses must have a Name.");
		}
  }

  handleSelectChange = (e) => {
    if (e.target.value !== '') {
      this.setState({
        connectToSlug: e.target.value,
        endingResponse: false
      });
    } else {
      this.setState({
        connectToSlug: e.target.value
      });
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
      [e.target.name]: !this.state[e.target.name]
    });
  }
  
  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'actionName'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleGetOrder = () => {
    const { dialogueResponses, loadingDialogueResponses } = this.props;
    // const { order } = this.state;
    var newOrder = dialogueResponses.length + 1;
    console.log('New Order: ', newOrder);
    this.setState({order: newOrder});
  }

  handleSetUpConversationDialogueOptions = () => {
    const { dialogue, gameConversationDialogues } = this.props;
    var conversationDialogueOptions = [];
    for (var i = 0; i < gameConversationDialogues.length; i++) {
      if (gameConversationDialogues[i].slug !== dialogue.slug) {
        conversationDialogueOptions.push(gameConversationDialogues[i]);
      }
    }
    this.setState({
      conversationDialogueOptions
    });
  }

  componentDidMount() {
    const { dialogue } = this.props;
    this.setState({
      dialogueSlug: dialogue.slug
    });
    this.handleSetUpConversationDialogueOptions();
    this.handleGetOrder();
  }

  componentDidUpdate(lastProps) {
    if (this.props.newDialogueResponseObj !== null && lastProps.newDialogueResponseObj === null) {
      const { dialogue } = this.props;
      this.props.resetNewDialogueResponse();
      this.props.resetDialogueResponses();
      this.props.getDialogueResponses(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug)
      this.setState({uploadingData: false});
      this.props.closeNewDialogueResponse();
    }
  }
  
  render() {
    const { name, description, image, imageFile, uploadingData, endingResponse, conversationDialogueOptions, connectToSlug, order } = this.state;
    const { dialogueResponses, loadingDialogueResponses } = this.props;

    return (
      <div>
        <div style={{paddingBottom: 20}}>
          <div style={{paddingBottom: 10}}><span style={styles.nameText}>New Dialogue Response:</span></div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Name:</span>
            </div>
            <div style={{flex: 4}}>
              <input 
                name="name"
                value={name}
                placeholder="Name"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Description:</span>
            </div>
            <div style={{flex: 4}}>
              <textarea 
                className="updateFormTextArea" 
                id="description"
                name="description" 
                onChange={this.handleChange} 
                value={description} 
                placeholder="Description:"
                style={styles.textareaInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}> 
              <label for="image" style={styles.defaultText}>Image:</label>
            </div>
            <div style={{flex: 4}}>
              <input 
                style={styles.fileInput}
                type="file" 
                name="image" 
                onChange={this.handleImageChange}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Dialogue: </span>
            </div>
            <div style={{flex: 3}}>
              {conversationDialogueOptions.length > 0 ? 
                <select 
                  name="conversationDialogueOptions" 
                  id="conversationDialogueOptions"
                  className="conversationDialogueOptions"
                  onChange={this.handleSelectChange.bind(this)}
                  style={styles.selectInput}
                >
                  <option value="">Connect to Dialogue</option>
                  {conversationDialogueOptions.map((dialogue, index) => (
                    <option 
                      key={index} 
                      value={dialogue.slug}
                    >
                      {dialogue.name}
                    </option>
                  ))}
                </select> :
                <div>
                  <span style={styles.defaultText}>No Dialogue to Connect...</span>
                </div>
              }
            </div>
          </div>
          {connectToSlug == '' && 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Ending Response:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="endingResponse" 
                  id="endingResponse" 
                  checked={endingResponse} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          }
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Order:</span>
            </div>
            <div style={{flex: 4}}>
              <input 
                name="order"
                value={order}
                placeholder="Order"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewDialogueResponse}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewDialogueResponse}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  dialogueResponses: state.games.dialogueResponses,
	loadingDialogueResponses: state.games.loadingDialogueResponses,

  newDialogueResponseObj: state.game_admin.newDialogueResponseObj,

  gameConversationDialogues: state.games.gameConversationDialogues,
  loadingGameConversationDialogues: state.games.loadingGameConversationDialogues,
})

const mapDispatchToProps = {
  newDialogueResponse, resetNewDialogueResponse,
  getDialogueResponses, resetDialogueResponses
}

export default connect(mapStateToProps, mapDispatchToProps)(NewDialogueResponse)
