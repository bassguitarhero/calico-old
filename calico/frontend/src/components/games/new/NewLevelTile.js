import React, { Component } from 'react'
import { connect } from 'react-redux'

import InlineTile from '../edit/InlineTile';
import { styles } from '../../layout/Styles';

import { getLevelTiles, clearLevelTiles } from '../../../actions/games';
import { newLevelTile, resetNewLevelTile } from '../../../actions/game_admin';

export class NewLevelTile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positionX: 0,
      positionY: 0,
      tileSlug: '',
      uploadingData: false,
      display: true
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleSelectTile = (tileSlug) => {
    this.setState({
      tileSlug: tileSlug
    });
  }

  handleCreateNewLevelTile = () => {
    const { gameDetail, level } = this.props;
    const { positionX, positionY, tileSlug, display } = this.state;
    const positionData = { positionX, positionY }
    if (tileSlug !== '') {
			if (window.confirm("Create New Level Tile?")) {
        this.props.newLevelTile(gameDetail.slug, level.slug, tileSlug, positionData, display);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Level Tiles must have a Tile.");
		}
  }

  handleSelectChange = (e) => {
    this.setState({
      tileSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    // console.log('Game Tiles for New Level Tile: ', this.props.gameTiles);
    const { level, selectedColumn, selectedRow } = this.props;
    this.setState({
      positionY: level.tile_width * selectedRow,
      positionX: level.tile_height * selectedColumn
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.newLevelTileObj !== null && lastProps.newLevelTileObj === null) {
      this.props.resetNewLevelTile();
      this.props.clearLevelTiles();
      this.props.getLevelTiles(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeNewLevelTile();
    }
  }

  render() {
    const { level, gameTiles, loadingGameTiles } = this.props;
    const { positionX, positionY, uploadingData, display } = this.state;

    return (
      <div style={{margin: '20px auto', width: `${level.width}px`}}>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>New Level Tile:</span>
        </div>
        <div style={{paddingBottom: 25, display: 'flex'}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position X:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionX"
                  value={positionX}
                  placeholder="Position X"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position Y:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionY"
                  value={positionY}
                  placeholder="Position Y"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Display:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
          <div style={{flex: 1}}>
            {!loadingGameTiles ? 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Tile:</span>
                </div>
                <div style={{flex: 3, paddingLeft: 24}}>
                  {gameTiles !== null ? 
                    <select 
                      name="gameTiles" 
                      id="gameTiles"
                      style={styles.selectInput}
                      onChange={this.handleSelectChange.bind(this)}
                    >
                      <option value="">Select a Tile</option>
                      {gameTiles.map((tile, index) => (
                        <option 
                          key={index} 
                          value={tile.slug}
                        >
                          {tile.name}
                        </option>
                      ))}
                    </select> :
                    <div>
                      <span style={styles.defaultText}>Loading...</span>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game Tiles...</span>
              </div>
            }
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewLevelTile}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewLevelTile}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  gameTiles: state.games.gameTiles,
  loadingGameTiles: state.games.loadingGameTiles,

  newLevelTileObj: state.game_admin.newLevelTileObj
})

const mapDispatchToProps = {
  getLevelTiles, clearLevelTiles, newLevelTile, resetNewLevelTile
}

export default connect(mapStateToProps, mapDispatchToProps)(NewLevelTile)
