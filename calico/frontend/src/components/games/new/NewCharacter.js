import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { newCharacter, resetNewCharacter } from '../../../actions/game_admin';
import { clearGameCharacters, getGameCharacters } from '../../../actions/games';

export class NewCharacter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: [],
      spriteSheet: '',
      spriteSheetFile: [],
      description: '',
      baseStrength: 5,
      baseSkill: 5,
      baseSpeed: 5,
      baseHealth: 5,
      baseWisdom: 5,
      baseLuck: 5,
      baseHP: 6,
      baseKeys: 0,
      baseBombs: 0,
      baseDamage: 10,
      uploadingData: false,
      width: 0,
      height: 0
    }
  }

  handleSpriteSheetChange = e => this.setState({
		spriteSheetFile: e.target.files[0]
	});

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewCharacter = () => {
    const { name, description, imageFile, spriteSheet, spriteSheetFile, width, height } = this.state;
    const { baseStrength, baseSpeed, baseSkill, baseWisdom, baseHealth, baseLuck, baseHP, baseBombs, baseKeys, baseDamage } = this.state;
    const character = { name, description, imageFile, spriteSheet, spriteSheetFile, width, height, baseStrength, baseSpeed, baseSkill, baseWisdom, baseHealth, baseLuck, baseHP, baseBombs, baseKeys, baseDamage };
		if (name !== '') {
			if (window.confirm("Create New Character?")) {
				this.props.newCharacter(this.props.gameDetail.slug, character);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Characters must have a Name.");
		}
  }

  componentDidMount() {
    const { gameDetail } = this.props;
    this.setState({
      width: gameDetail.sprite_width,
      height: gameDetail.sprite_height
    })
  }

  componentDidUpdate(lastProps) {
    if (this.props.newCharacterObj !== null && lastProps.newCharacterObj === null) {
      this.props.resetNewCharacter();
      this.props.clearGameCharacters();
      this.props.getGameCharacters(this.props.gameDetail.slug);
      this.props.closeNewCharacter();
    }
  }

  render() {
    const { name, image, imageFile, description, uploadingData, spriteSheet, spriteSheetFile } = this.state;
    const { baseStrength, baseSkill, baseSpeed, baseHealth, baseWisdom, baseLuck } = this.state;
    const { baseHP, baseKeys, baseBombs, baseDamage } = this.state;
    const { width, height } = this.state;
    
    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>New Character:</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 20}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Name:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Description:</span>
              </div>
              <div style={{flex: 3}}>
                <textarea 
                  style={styles.textareaInput}
                  className="updateFormTextArea" 
                  id="description"
                  name="description" 
                  onChange={this.handleChange} 
                  value={description} 
                  placeholder="Description" 
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <label for="image" style={styles.defaultText}>Image:</label>
              </div>
              <div style={{flex: 3}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <label for="spriteSheet" style={styles.defaultText}>Sprite Sheet:</label>
              </div>
              <div style={{flex: 3}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="spriteSheet" 
                  onChange={this.handleSpriteSheetChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Sprite Width:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="width"
                  value={width}
                  placeholder="Width"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Sprite Height:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="height"
                  value={height}
                  placeholder="Height"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div style={{display: 'flex', paddingTop: 20, paddingBottom: 25}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Strength:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseStrength"
                  value={baseStrength}
                  placeholder="Base Strength"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Skill:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseSkill"
                  value={baseSkill}
                  placeholder="Base Skill"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Speed:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseSpeed"
                  value={baseSpeed}
                  placeholder="Base Speed"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Health:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseHealth"
                  value={baseHealth}
                  placeholder="Base Health"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Wisdom:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseWisdom"
                  value={baseWisdom}
                  placeholder="Base Wisdom"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Luck:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseLuck"
                  value={baseLuck}
                  placeholder="Base Luck"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>HP:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseHP"
                  value={baseHP}
                  placeholder="Base HP"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Keys:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseKeys"
                  value={baseKeys}
                  placeholder="Base Keys"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Bombs:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseBombs"
                  value={baseBombs}
                  placeholder="Base Bombs"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Damage:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseDamage"
                  value={baseDamage}
                  placeholder="Base Damage"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewCharacter}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewCharacter}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newCharacterObj: state.game_admin.newCharacterObj
})

const mapDispatchToProps = {
  newCharacter,
  resetNewCharacter,
  clearGameCharacters,
  getGameCharacters
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCharacter)
