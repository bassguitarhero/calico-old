import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { newCutscene, resetNewCutscene } from '../../../actions/game_admin';
import { clearGameCutscenes, getGameCutscenes } from '../../../actions/games';

export class NewCutscene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      fieldYouTube: '',
      fieldVimeo: '',
      fieldArchive: '',
      width: 0,
      height: 0,
      autoplay: true,
      loop: false
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewCutscene = () => {
    const { name, description, imageFile, fieldYouTube, fieldVimeo, fieldArchive, width, height, autoplay, loop } = this.state;
    const cutscene = { name, description, imageFile, fieldYouTube, fieldVimeo, fieldArchive, width, height, autoplay, loop  };
		if (name !== '') {
			if (window.confirm("Create New Cutscene?")) {
				this.props.newCutscene(this.props.gameDetail.slug, cutscene);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Cutscenes must have a Name.");
		}
  }

  componentDidUpdate(lastProps) {
    if (this.props.newCutsceneObj !== null && lastProps.newCutsceneObj === null) {
      this.props.resetNewCutscene();
      this.props.clearGameCutscenes();
      this.props.getGameCutscenes(this.props.gameDetail.slug);
      this.props.closeNewCutscene();
    }
  }

  render() {
    const { name, description, image, imageFile, uploadingData, fieldYouTube, fieldVimeo, fieldArchive, width, height, autoplay, loop } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span>New Cutscene</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Name:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Description </span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              style={styles.textareaInput}
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Describe your Cutscene"
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <label for="image" style={styles.defaultText}>Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>YouTube:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="fieldYouTube"
              value={fieldYouTube}
              placeholder="YouTube"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Vimeo:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="fieldVimeo"
              value={fieldVimeo}
              placeholder="Vimeo"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Internet Archive:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="fieldArchive"
              value={fieldArchive}
              placeholder="Internet Archive"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Width:</span>
          </div>
          <div style={{flex: 3}}>
            <input 
              name="width"
              value={width}
              placeholder="Width"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Height:</span>
          </div>
          <div style={{flex: 3}}>
            <input 
              name="height"
              value={height}
              placeholder="Height"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
            <span style={styles.defaultText}>Auto Play:</span>
          </div>
          <div style={{flex: 1}}>
            <input 
              type="checkbox" 
              name="autoplay" 
              id="autoplay" 
              checked={autoplay} 
              onChange={this.handleCheckboxChange.bind(this)} 
            />
          </div>
        </div> 
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
            <span style={styles.defaultText}>Loop:</span>
          </div>
          <div style={{flex: 1}}>
            <input 
              type="checkbox" 
              name="loop" 
              id="loop" 
              checked={loop} 
              onChange={this.handleCheckboxChange.bind(this)} 
            />
          </div>
        </div> 
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewCutscene}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewCutscene}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newCutsceneObj: state.game_admin.newCutsceneObj
})

const mapDispatchToProps = {
  newCutscene,
  resetNewCutscene,
  clearGameCutscenes,
  getGameCutscenes
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCutscene)
