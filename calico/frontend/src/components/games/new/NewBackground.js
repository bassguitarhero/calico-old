import React, { Component } from 'react'
import { connect } from 'react-redux'

import { newBackground, resetNewBackground } from '../../../actions/game_admin';
import { clearGameBackgrounds, getGameBackgrounds } from '../../../actions/games';

export class NewBackground extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewBackground = () => {
    const { name, description, imageFile } = this.state;
    const background = { name, description, imageFile };
		if (name !== '') {
			if (window.confirm("Create New Background?")) {
				this.props.newBackground(this.props.gameDetail.slug, background);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Backgrounds must have a Name.");
		}
  }

  componentDidUpdate(lastProps) {
    if (this.props.newBackgroundObj !== null && lastProps.newBackgroundObj === null) {
      this.props.resetNewBackground();
      this.props.clearGameBackgrounds();
      this.props.getGameBackgrounds(this.props.gameDetail.slug);
      this.props.closeNewBackground();
    }
  }

  render() {
    const { name, description, image, imageFile, uploadingData } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span>New Background</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span>Name: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span>Description: </span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              style={styles.textareaInput}
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Describe your Background"
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <label for="image">Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleCreateNewBackground}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.props.closeNewBackground}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newBackgroundObj: state.game_admin.newBackgroundObj
})

const mapDispatchToProps = {
  newBackground,
  resetNewBackground,
  clearGameBackgrounds,
  getGameBackgrounds
}

export default connect(mapStateToProps, mapDispatchToProps)(NewBackground)

const styles = {
  textInput: {
    size: 40
  },
  textareaInput: {

  },
  fileInput: {

  },
  checkboxInput: {

  }
}