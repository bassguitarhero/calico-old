import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../../layout/Styles';

import { newConversationDialogue, resetNewConversationDialogue } from '../../../actions/game_admin';
import { getGameConversationDialogues, resetGameConversationDialogues } from '../../../actions/games';

export class NewDialogue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      openingDialogue: false,
      endingDialogue: false,
      embedFacebook: '',
      embedTwitter: '',
      embedYouTube: '',
      conversationSlug: '',
      uploadingData: false
    }
  }

  handleCreateNewDialogue = () => {
    const { gameDetail, conversation } = this.props;
    const { name, description, image, imageFile, openingDialogue, endingDialogue, embedFacebook, embedTwitter, embedYouTube } = this.state;
    const dialogueData = { name, description, image, imageFile, openingDialogue, endingDialogue, embedFacebook, embedTwitter, embedYouTube };
    if (name !== '') {
			if (window.confirm("Create New Dialogue?")) {
				this.props.newConversationDialogue(gameDetail.slug, conversation.slug, dialogueData);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Dialogues must have a Name.");
		}
  }

  handleCheckboxChange = (e) => {
    this.setState({
      [e.target.name]: !this.state[e.target.name]
    });
  }
  
  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'actionName'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    const { gameConversationDialogues } = this.props;
    if (gameConversationDialogues.length > 0) {
      var openingCount = 0;
      for (var u = 0; u < gameConversationDialogues.length; u++) {
        if (gameConversationDialogues[u].opening_dialogue === true) {
          openingCount += 1;
        }
      }
      if (openingCount === 0) {
        this.setState({
          openingDialogue: true
        });
      }
    } else {
      this.setState({
        openingDialogue: true
      });
    }
  }

  componentDidUpdate(lastProps) {
    if (this.props.newConversationDialogueObj !== null && lastProps.newConversationDialogueObj === null) {
      const { conversation } = this.props;
      this.setState({uploadingData: false});
      this.props.resetNewConversationDialogue();
      this.props.resetGameConversationDialogues();
      this.props.getGameConversationDialogues(conversation.game.slug, conversation.slug);
      this.props.closeNewConversationDialogue();
    }
  }

  render() {
    const { name, description, image, imageFile, openingDialogue, closingDialogue, embedFacebook, embedTwitter, embedYouTube, uploadingData } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>New Dialogue:</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Name:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Description:</span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              className="updateFormTextArea" 
              id="description"
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Description:"
              style={styles.textareaInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}> 
            <label for="image" style={styles.defaultText}>Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
            <span style={styles.defaultText}>Opening Dialogue:</span>
          </div>
          <div style={{flex: 1}}>
            <input 
              type="checkbox" 
              name="openingDialogue" 
              id="openingDialogue" 
              checked={openingDialogue} 
              onChange={this.handleCheckboxChange.bind(this)} 
            />
          </div>
        </div> 
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
            <span style={styles.defaultText}>Closing Dialogue:</span>
          </div>
          <div style={{flex: 1}}>
            <input 
              type="checkbox" 
              name="closingDialogue" 
              id="closingDialogue" 
              checked={closingDialogue} 
              onChange={this.handleCheckboxChange.bind(this)} 
            />
          </div>        
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Facebook:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="embedFacebook"
              value={embedFacebook}
              placeholder="Embed Facebook"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Twitter:</span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              className="updateFormTextArea" 
              id="embedTwitter"
              name="embedTwitter" 
              onChange={this.handleChange} 
              value={embedTwitter} 
              placeholder="Embed Twitter"
              style={styles.textareaInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>YouTube:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="embedYouTube"
              value={embedYouTube}
              placeholder="Embed YouTube"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewDialogue}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewConversationDialogue}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newConversationDialogueObj: state.game_admin.newConversationDialogueObj,
})

const mapDispatchToProps = {
  newConversationDialogue, resetNewConversationDialogue,
  getGameConversationDialogues, resetGameConversationDialogues
}

export default connect(mapStateToProps, mapDispatchToProps)(NewDialogue)
