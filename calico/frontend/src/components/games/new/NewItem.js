import React, { Component } from 'react'
import { connect } from 'react-redux'

import { newItem, resetNewItem } from '../../../actions/game_admin';
import { clearGameItems, getGameItems } from '../../../actions/games';

export class NewItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewItem = () => {
    const { name, description, imageFile } = this.state;
    const item = { name, description, imageFile };
		if (name !== '') {
			if (window.confirm("Create New Item?")) {
				this.props.newItem(this.props.gameDetail.slug, item);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Items must have a Name.");
		}
  }

  componentDidUpdate(lastProps) {
    if (this.props.newItemObj !== null && lastProps.newItemObj === null) {
      this.props.resetNewItem();
      this.props.clearGameItems();
      this.props.getGameItems(this.props.gameDetail.slug);
      this.props.closeNewItem();
    }
  }

  render() {
    const { name, description, image, imageFile, uploadingData } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span>New Item</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span>Name: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span>Description: </span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              style={styles.textareaInput}
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Describe your Item"
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <label for="image">Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        {uploadingData ? 
          <div id='uploadProgress' /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleCreateNewItem}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.props.closeNewItem}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newItemObj: state.game_admin.newItemObj
})

const mapDispatchToProps = {
  newItem,
  resetNewItem,
  clearGameItems,
  getGameItems
}

export default connect(mapStateToProps, mapDispatchToProps)(NewItem)

const styles = {
  textInput: {
    size: 40
  },
  textareaInput: {

  },
  fileInput: {

  },
  checkboxInput: {

  }
}