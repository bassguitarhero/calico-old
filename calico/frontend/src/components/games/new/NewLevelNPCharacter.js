import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getLevelNPCharacters, clearLevelNPCharacters, getGameNPCharacters, clearGameNPCharacters } from '../../../actions/games';
import { newLevelNPCharacter, resetNewLevelNPCharacter } from '../../../actions/game_admin';

export class NewLevelNPCharacter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positionX: 0,
      positionY: 0,
      npCharacterSlug: '',
      conversationSlug: '',
      uploadingData: false,
      health: 0,
      hostile: true,
      display: true,
      randomize: false,
      boss: false
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleCreateNewLevelNPCharacter = () => {
    const { gameDetail, level } = this.props;
    const { positionX, positionY, npCharacterSlug, health, hostile, display, randomize, boss, conversationSlug } = this.state;
    const positionData = { positionX, positionY }
    if (npCharacterSlug !== '') {
      if (conversationSlug != '') {
        if (boss == false && hostile == false) {
          if (window.confirm("Create New Level NPCharacter?")) {
            this.props.newLevelNPCharacter(gameDetail.slug, level.slug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug);
            this.setState({uploadingData: true});
          }
        } else {
          alert('Conversation NPCharacters cannot be hostile or bosses.');
        }
      } else if (boss == true) {
        if (hostile == true) {
         if (window.confirm("Create New Level NPCharacter?")) {
            this.props.newLevelNPCharacter(gameDetail.slug, level.slug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug);
            this.setState({uploadingData: true});
          }
        } else {
          alert('Bosses must be hostile');
        }
      } else {
        if (window.confirm("Create New Level NPCharacter?")) {
          this.props.newLevelNPCharacter(gameDetail.slug, level.slug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug);
          this.setState({uploadingData: true});
        }
      }
		} else {
			alert("Level NPCharacters must have an NPCharacter.");
		}
  }

  handleSelectNPCharacter = (e) => {
    const { gameNPCharacters } = this.props;
    for (var t = 0; t < gameNPCharacters.length; t++) {
      if (gameNPCharacters[t].id === e.target.value) {
        this.setState({
          npCharacterSlug: gameNPCharacters[t].slug,
          health: gameNPCharacters[t].health,
          hostile: gameNPCharacters[t].hostile
        })
      }
    }
  }

  handleSelectConversation = (e) => {
    this.setState({
      conversationSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    const { level, selectedColumn, selectedRow } = this.props;
    this.setState({
      positionY: level.tile_width * selectedRow,
      positionX: level.tile_height * selectedColumn
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.newLevelNPCharacterObj !== null && lastProps.newLevelNPCharacterObj === null) {
      this.props.resetNewLevelNPCharacter();
      this.props.clearLevelNPCharacters();
      this.props.getLevelNPCharacters(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeNewLevelNPCharacter();
    }
  }

  render() {
    const { level, gameNPCharacters, loadingGameNPCharacters, gameConversations, loadingGameConversations } = this.props;
    const { positionX, positionY, uploadingData, health, hostile, display, randomize, boss, conversationSlug } = this.state;

    return (
      <div style={{margin: '20px auto', width: `${level.width}px`}}>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>New Level NPCharacter:</span>
        </div>
        <div style={{paddingBottom: 25, display: 'flex'}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position X:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionX"
                  value={positionX}
                  placeholder="Position X"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position Y:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionY"
                  value={positionY}
                  placeholder="Position Y"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Randomize X/Y:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="randomize" 
                  id="randomize" 
                  checked={randomize} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
          <div style={{flex: 1}}>
            {!loadingGameNPCharacters ? 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>NPCharacter:</span>
                </div>
                <div style={{flex: 3}}>
                  {gameNPCharacters !== null ? 
                    <select 
                      name="gameNPCharacters" 
                      id="gameNPCharacters"
                      style={styles.selectInput}
                      onChange={this.handleSelectNPCharacter.bind(this)}
                    >
                      <option value="">Select an NPCharacter</option>
                      {gameNPCharacters.map((gameNPCharacter, index) => (
                        <option 
                          key={index} 
                          value={gameNPCharacter.id}
                        >
                          {gameNPCharacter.name}
                        </option>
                      ))}
                    </select> :
                    <div>
                      <span style={styles.defaultText}>Loading...</span>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game NPCharacters...</span>
              </div>
            }
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Health:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="health"
                  value={health}
                  placeholder="Health"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Hostile:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="hostile" 
                  id="hostile" 
                  checked={hostile} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Display:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Boss:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="boss" 
                  id="boss" 
                  checked={boss} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            {!loadingGameConversations ? 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Conversation:</span>
                </div>
                <div style={{flex: 3}}>
                  {gameConversations !== null ? 
                    <select 
                      name="gameConversations" 
                      id="gameConversations"
                      style={styles.selectInput}
                      onChange={this.handleSelectConversation.bind(this)}
                    >
                      <option value="">Select a Conversation</option>
                      {gameConversations.map((gameConversation, index) => (
                        <option 
                          key={index} 
                          value={gameConversation.slug}
                        >
                          {gameConversation.name}
                        </option>
                      ))}
                    </select> :
                    <div>
                      <span style={styles.defaultText}>Loading...</span>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game NPCharacters...</span>
              </div>
            }
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewLevelNPCharacter}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewLevelNPCharacter}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 
  gameConversations: state.games.gameConversations,
  loadingGameConversations: state.games.loadingGameConversations,

  gameNPCharacters: state.games.gameNPCharacters,
  loadingGameNPCharacters: state.games.loadingGameNPCharacters,

  newLevelNPCharacterObj: state.game_admin.newLevelNPCharacterObj
})

const mapDispatchToProps = {
  getGameNPCharacters, clearGameNPCharacters, getLevelNPCharacters, clearLevelNPCharacters, newLevelNPCharacter, resetNewLevelNPCharacter
}

export default connect(mapStateToProps, mapDispatchToProps)(NewLevelNPCharacter)
