import React, { Component } from 'react'
import { connect } from 'react-redux'

import { newWeapon, resetNewWeapon } from '../../../actions/game_admin';
import { clearGameWeapons, getGameWeapons } from '../../../actions/games';

export class NewWeapon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewWeapon = () => {
    const { name, description, imageFile } = this.state;
    const weapon = { name, description, imageFile };
		if (name !== '') {
			if (window.confirm("Create New Weapon?")) {
				this.props.newWeapon(this.props.gameDetail.slug, weapon);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Weapons must have a Name.");
		}
  }

  componentDidUpdate(lastProps) {
    if (this.props.newWeaponObj !== null && lastProps.newWeaponObj === null) {
      this.props.resetNewWeapon();
      this.props.clearGameWeapons();
      this.props.getGameWeapons(this.props.gameDetail.slug);
      this.props.closeNewWeapon();
    }
  }

  render() {
    const { name, description, image, imageFile, uploadingData } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span>New Weapon</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span>Name: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span>Description: </span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              style={styles.textareaInput}
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Describe your Weapon"
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <label for="image">Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleCreateNewWeapon}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.props.closeNewWeapon}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newWeaponObj: state.game_admin.newWeaponObj
})

const mapDispatchToProps = {
  newWeapon,
  resetNewWeapon,
  clearGameWeapons,
  getGameWeapons
}

export default connect(mapStateToProps, mapDispatchToProps)(NewWeapon)

const styles = {
  textInput: {
    size: 40
  },
  textareaInput: {

  },
  fileInput: {

  },
  checkboxInput: {

  }
}