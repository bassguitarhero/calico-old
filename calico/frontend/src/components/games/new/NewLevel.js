import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { newLevel, resetNewLevel, getLevelTypes } from '../../../actions/game_admin';
import { clearGameLevels, getGameLevels } from '../../../actions/games';

export class NewLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      levelTypes: null,
      levelTypeSlug: '',
      width: 0,
      height: 0,
      tileWidth: 0,
      tileHeight: 0,
      startingLevel: false,
      startingX: 0,
      startingY: 0,
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleSelectChange = (e) => {
    this.setState({
      levelTypeSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleCreateNewLevel = () => {
    const { name, description, imageFile, levelTypeSlug, width, height, tileWidth, tileHeight, startingLevel, startingX, startingY } = this.state;
    const level = { name, description, imageFile, levelTypeSlug, width, height, tileWidth, tileHeight, startingLevel, startingX, startingY };
		if (name !== '') {
			if (window.confirm("Create New Level?")) {
				this.props.newLevel(this.props.gameDetail.slug, level);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Levels must have a Name.");
		}
  }

  componentDidMount() {
    this.props.getLevelTypes();
    const { gameDetail } = this.props;
    this.setState({
      width: gameDetail.map_width,
      height: gameDetail.map_height,
      tileWidth: gameDetail.tile_width,
      tileHeight: gameDetail.tile_height
    })
  }

  componentDidUpdate(lastProps) {
    if (this.props.newLevelObj !== null && lastProps.newLevelObj === null) {
      this.props.resetNewLevel();
      this.props.clearGameLevels();
      this.props.getGameLevels(this.props.gameDetail.slug);
      this.props.closeNewLevel();
    }
  }

  render() {
    const { name, description, uploadingData, width, height, tileWidth, tileHeight } = this.state;
    const { levelTypes } = this.props;
    const { startingLevel, startingX, startingY } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>New Level:</span>
        </div>
        <div style={{paddingBottom: 25}}>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Name:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="name"
                value={name}
                placeholder="Name"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Description:</span>
            </div>
            <div style={{flex: 3}}>
              <textarea 
                style={styles.textareaInput}
                type="textArea" 
                name="description" 
                onChange={this.handleChange} 
                value={description} 
                placeholder="Describe your Level"
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Type:</span>
            </div>
            <div style={{flex: 3}}>
              {levelTypes !== null ? 
                <select 
                  name="levelTypes" 
                  id="levelTypes"
                  style={styles.selectInput}
                  onChange={this.handleSelectChange.bind(this)}
                >
                  <option value="">...</option>
                  {levelTypes.map((levelType, index) => (
                    <option 
                      key={index} 
                      value={levelType.slug}
                    >
                      {levelType.name}
                    </option>
                  ))}
                </select> :
                <div>
                  Loading...
                </div>
              }
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <label for="image" style={styles.defaultText}>Image:</label>
            </div>
            <div style={{flex: 3}}>
              <input 
                style={styles.fileInput}
                type="file" 
                name="image" 
                onChange={this.handleImageChange}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Map Width:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="width"
                value={width}
                placeholder="Width"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Map Height:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="height"
                value={height}
                placeholder="Height"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Tile Width:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="tileWidth"
                value={tileWidth}
                placeholder="Tile Width"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Tile Height:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="tileHeight"
                value={tileHeight}
                placeholder="Tile Height"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Starting X:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="startingX"
                value={startingX}
                placeholder="Starting X"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Starting Y:</span>
            </div>
            <div style={{flex: 3}}>
              <input 
                name="startingY"
                value={startingY}
                placeholder="Starting Y"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
              <span style={styles.defaultText}>Starting Level:</span>
            </div>
            <div style={{flex: 4}}>
              <input 
                type="checkbox" 
                name="startingLevel" 
                id="startingLevel" 
                checked={startingLevel} 
                onChange={this.handleCheckboxChange.bind(this)} 
              />
            </div>
          </div> 
        </div> 
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewLevel}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewLevel}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newLevelObj: state.game_admin.newLevelObj,

  levelTypes: state.game_admin.levelTypes
})

const mapDispatchToProps = {
  newLevel,
  resetNewLevel,
  clearGameLevels,
  getGameLevels,
  getLevelTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(NewLevel)
