import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { newGame, resetCreateNewGame, getGameTypes, resetGameTypes } from '../../../actions/game_admin';
import { clearGames, getGames } from '../../../actions/games';

export class NewGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      gameTypes: null,
      imageFile: null,
      publicAccess: true,
      published: false,
      uploadingData: false,
      gameTypeSlug: '',
      mapWidth: 800,
      mapHeight: 416,
      tileWidth: 32,
      tileHeight: 32,
      spriteWidth: 32,
      spriteHeight: 32
    }
  }

  handleSelectChange = (e) => {
    this.setState({
      gameTypeSlug: e.target.value
    });
  }

  handlePublicAccessChange = () => {
    this.setState({
      publicAccess: !this.state.publicAccess
    })
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		})
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewGame = () => {
    const { name, description, imageFile, publicAccess, gameTypeSlug, mapWidth, mapHeight, tileWidth, tileHeight, spriteWidth, spriteHeight } = this.state;
    const game = { name, description, imageFile, publicAccess, gameTypeSlug, mapWidth, mapHeight, tileWidth, tileHeight, spriteWidth, spriteHeight };
		if (name !== '') {
      if (gameTypeSlug !== '') {
        if (window.confirm("Create New Game?")) {
          this.props.newGame(game);
          this.setState({uploadingData: true});
        }
      } else {
        alert("Games must have a Type.");
      }
		} else {
			alert("Games must have a Name.");
		}
  }

  componentDidMount() {
    this.props.getGameTypes();
  }

  componentDidUpdate(lastProps) {
    if (this.props.newGameObj !== null && lastProps.newGameObj === null) {
      this.props.clearGames();
      this.setState({uploadingData: false});
      this.props.resetCreateNewGame();
      this.props.closeNewGame();
    }
  }

  componentWillUnmount() {
    this.props.resetGameTypes();
  }

  render() {
    const { name, description, image, imageFile, publicAccess, published, uploadingData } = this.state;
    const { mapWidth, mapHeight, tileWidth, tileHeight, spriteWidth, spriteHeight } = this.state;
    const { gameTypes } = this.props;
    
    return (
      <div>
        <div style={{paddingBottom: 15}}>
          <span style={styles.titleText}>New Game</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Name: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name" 
              value={name} 
              placeholder="Name your Game" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Description: </span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              style={styles.textareaInput}
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Describe your Game"
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Type: </span>
          </div>
          <div style={{flex: 4}}>
            {gameTypes !== null ? 
              <select 
                name="gameTypes" 
                id="gameTypes"
                onChange={this.handleSelectChange.bind(this)} 
                style={styles.selectInput}
              >
                <option value="">...</option>
                {gameTypes.map((gameType, index) => (
                  <option key={index} value={gameType.slug}>{gameType.name}</option>
                ))}
              </select> :
              <div style={styles.loadingTextContainer}>
                <span style={styles.loadingText}>Loading...</span>
              </div>
            }
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <label style={styles.defaultText} for="image">Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Map Width: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="mapWidth" 
              value={mapWidth} 
              placeholder="Map Width" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Map Height: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="mapHeight" 
              value={mapHeight} 
              placeholder="Map Height" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Tile Width: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="tileWidth" 
              value={tileWidth} 
              placeholder="Tile Width" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Tile Height: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="tileHeight" 
              value={tileHeight} 
              placeholder="Tile Height" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Sprite Width: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="spriteWidth" 
              value={spriteWidth} 
              placeholder="Sprite Width" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Sprite Height: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="spriteHeight" 
              value={spriteHeight} 
              placeholder="Sprite Height" 
              onChange={this.handleChange} 
              style={styles.textInput} 
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1, textAlign: 'right', paddingTop: 20, paddingRight: 10}}>
            <input type="checkbox" name="publicAccess" id="publicAccess" checked={publicAccess} onChange={this.handleCheckboxChange.bind(this)} />
          </div>
          <div style={{flex: 4, paddingTop: 10}}>
            <button style={styles.buttonCheckbox} onClick={this.handlePublicAccessChange}>Public</button>
          </div>
        </div>
        {uploadingData ? 
          <div style={{paddingTop: 10}}>
            <div style={styles.defaultText} id="uploadProgress" />
          </div> :
          <div style={{paddingTop: 10, display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewGame}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewGame}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  newGameObj: state.game_admin.newGameObj,

  gameTypes: state.game_admin.gameTypes
})

const mapDispatchToProps = {
  newGame,
  resetCreateNewGame,
  clearGames, 
  getGames,
  getGameTypes,
  resetGameTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(NewGame)

