import React, { Component } from 'react'
import { connect } from 'react-redux'

import { newTile, resetNewTile } from '../../../actions/game_admin';
import { getGameTiles, clearGameTiles } from '../../../actions/games';

import { styles } from '../../layout/Styles';

export class NewTile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      image: '',
      imageFile: null,
      height: 0,
      width: 0,
      walkable: true,
      uploadingData: false,
      display: true
    }
  }

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
  });
  
  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleWalkableChange = () => {
    this.setState({
      walkable: !this.state.walkable
    });
  }

  handleCreateNewTile = () => {
    const { name, imageFile, height, width, walkable, display } = this.state;
    const tile = { name, imageFile, height, width, walkable, display };
    if (name !== '') {
			if (window.confirm("Create New Tile?")) {
				this.props.newTile(this.props.gameDetail.slug, tile);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Tiles must have a Name.");
		}
  }

  componentDidMount() {
    const { gameDetail } = this.props;
    this.setState({
      width: gameDetail.tile_width,
      height: gameDetail.tile_height
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.newGameTileObj !== null && lastProps.newGameTileObj === null) {
      this.props.resetNewTile();
      this.props.clearGameTiles();
      this.props.getGameTiles(this.props.gameDetail.slug);
      this.props.closeNewTile();
    }
  }

  render() {
    const { name, uploadingData, width, height, walkable, display } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>New Tile:</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 25}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Name:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <label for="image">Image:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Height: </span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="height"
                  value={height}
                  placeholder="Height"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Width: </span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="width"
                  value={width}
                  placeholder="Width"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <label for="display" style={styles.defaultText}>Display:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <label for="walkable" style={styles.defaultText}>Walkable:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  type="checkbox" 
                  name="walkable" 
                  id="walkable" 
                  checked={walkable} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewTile}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewTile}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  newGameTileObj: state.game_admin.newGameTileObj
})

const mapDispatchToProps = {
  newTile,
  resetNewTile,
  getGameTiles, 
  clearGameTiles
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTile)
