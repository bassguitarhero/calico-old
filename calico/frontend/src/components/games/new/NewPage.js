import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { newPage, resetNewPage } from '../../../actions/game_admin';
import { clearGamePages, getGamePages } from '../../../actions/games';

export class NewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      youtubeURL: '',
      youtubeID: ''
    }
  }

  handleCloseNewPage = () => {
    this.props.closeNewPage();
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleCreateNewPage = () => {
    const { name, description, imageFile, youtubeID, youtubeURL } = this.state;
    const page = { name, description, imageFile, youtubeID, youtubeURL };
		if (name !== '') {
			if (window.confirm("Create New Page?")) {
				this.props.newPage(this.props.gameDetail.slug, page);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Pages must have a Name.");
		}
  }

  componentDidUpdate(lastProps) {
    if (this.props.newPageObj !== null && lastProps.newPageObj === null) {
      this.props.resetNewPage();
      this.props.clearGamePages();
      this.props.getGamePages(this.props.gameDetail.slug);
      this.props.closeNewPage();
    }
  }

  render() {
    const { name, description, image, imageFile, uploadingData, youtubeID, youtubeURL } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 15}}>
          <span style={styles.titleText}>New Page</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Name: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
          <span style={styles.defaultText}>Story: </span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              style={styles.textareaPageInput}
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Story"
              rows="10"
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <label style={styles.defaultText} for="image">Image:</label>
          </div>
          <div style={{flex: 4}}>
            <input 
              style={styles.fileInput}
              type="file" 
              name="image" 
              onChange={this.handleImageChange}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
          <span style={styles.defaultText}>YouTube ID: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="youtubeID"
              value={youtubeID}
              placeholder="YouTube ID"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        {uploadingData ? 
          <div style={{paddingTop: 10}}>
            <div style={styles.defaultText} id='uploadProgress' />
          </div> :
          <div style={{display: 'flex', paddingTop: 10}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewPage}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCloseNewPage}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 
  
  newPageObj: state.game_admin.newPageObj
})

const mapDispatchToProps = {
  newPage, 
  resetNewPage,
  clearGamePages, 
  getGamePages
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPage)

