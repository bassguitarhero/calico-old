import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { newLevelAction, resetNewLevelAction, getLevelActions, resetLevelActions } from '../../../actions/game_admin';
import { getGameActionTypes, resetGameActionTypes, getCutsceneActionTypes, resetCutsceneActionTypes } from '../../../actions/game_admin';

export class NewLevelAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positionX: 0,
      positionY: 0,
      tileSlug: '',
      uploadingData: false,
      display: true,
      levelChangeSlug: '',
      levelActionTypeSlug: '',
      gameActionTypeSlug: '',
      cutsceneSlug: '',
      levelOptions: [],
      requireComplete: false,
      defeatAllEnemies: false,
      defeatBoss: false
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleSelectTile = (tileSlug) => {
    this.setState({
      tileSlug: tileSlug
    });
  }

  handleCreateNewLevelAction = () => {
    const { gameDetail, level } = this.props;
    const { positionX, positionY, tileSlug, display } = this.state;
    const { requireComplete, levelChangeSlug, levelActionTypeSlug, gameActionTypeSlug, cutsceneSlug, defeatAllEnemies, defeatBoss } = this.state;
    const levelTileData = { tileSlug, positionX, positionY, display };
    const levelActionData = { requireComplete, levelChangeSlug, levelActionTypeSlug, gameActionTypeSlug, cutsceneSlug, defeatAllEnemies, defeatBoss };
    if (tileSlug !== '') {
      if (levelActionTypeSlug !== '') {
        if (levelActionTypeSlug === 'change-level' && levelChangeSlug === '') {
          alert('Level Changes must have a new Level');
        } else {
          if (window.confirm("Create New Level Action?")) {
            this.props.newLevelAction(gameDetail.slug, level.slug, levelTileData, levelActionData);
            this.setState({uploadingData: true});
          }
        }
      } else {
        alert('Level Actions must have an Action.');
      }
		} else {
      if (levelActionTypeSlug === 'game-action') {
        if (window.confirm("Create New Level Action?")) {
          this.props.newLevelAction(gameDetail.slug, level.slug, levelTileData, levelActionData);
          this.setState({uploadingData: true});
        }
      } else {
        alert("Level Actions must have a Tile.");
      }
		}
  }

  handleSelectGameLevel = (e) => {
    this.setState({
      levelChangeSlug: e.target.value,
      cutsceneSlug: '',
      gameActionTypeSlug: ''
    });
  }

  handleSelectGameCutscene = (e) => {
    this.setState({
      levelChangeSlug: '',
      cutsceneSlug: e.target.value,
      gameActionTypeSlug: ''
    });
  }

  handleSelectGameActionType = (e) => {
    this.setState({
      levelChangeSlug: '',
      cutsceneSlug: '',
      gameActionTypeSlug: e.target.value
    });
  }

  handleSelectChange = (e) => {
    this.setState({
      tileSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleSelectLevelActionType = (e) => {
    this.setState({
      levelActionTypeSlug: e.target.value
    })
  }

  handleSetGameCutscenes = () => {
    const { level, gameCutscenes } = this.props;
    const cutsceneOptions = [];
    for (var y = 0; y < gameCutscenes.length; y++) {
      cutsceneOptions.push(gameCutscenes[y]);
    }
    this.setState({cutsceneOptions})
  }

  handleGetGameLevels = () => {
    const { level, gameLevels, levelActions } = this.props;
    var levelOptions = [];
    var match = 0;
    for (var i = 0; i < gameLevels.length; i++) {
      if (gameLevels[i].slug !== level.slug) {
        for (var j = 0; j < levelActions.length; j++) {
          if (levelActions[j].change_level !== null) {
            if (gameLevels[i].slug === levelActions[j].change_level.slug) {
              match += 1;
            }
          }
        }
        if (match === 0) {
          levelOptions.push(gameLevels[i]);
        }
        match = 0;
      }
    }
    this.setState({levelOptions});
  }

  componentDidMount() {
    const { level, selectedColumn, selectedRow } = this.props;
    this.setState({
      positionY: level.tile_width * selectedRow,
      positionX: level.tile_height * selectedColumn
    });
    this.handleGetGameLevels();
    this.props.getGameActionTypes();
    this.props.getCutsceneActionTypes();
  }

  componentDidUpdate(lastProps) {
    if (this.props.newLevelActionObj !== null && lastProps.newLevelActionObj === null) {
      this.props.resetNewLevelAction();
      this.props.resetLevelActions();
      this.props.getLevelActions(this.props.gameDetail.slug, this.props.level);
      this.setState({uploadingData: false});
      this.props.closeNewLevelAction();
    }
  }

  componentWillUnmount() {
    this.props.resetGameActionTypes();
  }

  render() {
    const { level, gameTiles, loadingGameTiles, levelActionTypes, gameActionTypes, loadingGameActionTypes, cutsceneActionTypes, loadingCutsceneActionTypes, gameCutscenes, loadingGameCutscenes } = this.props;
    const { positionX, positionY, uploadingData, display, levelOptions, levelActionTypeSlug, gameActionTypeSlug, cutsceneActionTypeSlug, requireComplete, defeatAllEnemies, defeatBoss } = this.state;

    return (
      <div style={{margin: '20px auto', width: `${level.width}px`}}>
        <div style={{padding: 10}}>
          <span style={styles.nameText}>New Level Action:</span>
        </div>
        <div style={{padding: 25, display: 'flex'}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Position X: </span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionX"
                  value={positionX}
                  placeholder="Position X"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Position Y: </span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionY"
                  value={positionY}
                  placeholder="Position Y"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            {!loadingGameTiles ? 
              <div>
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1}}>
                    <span style={styles.defaultText}>Tile: </span>
                  </div>
                  <div style={{flex: 3}}>
                    {gameTiles !== null ? 
                      <select 
                        name="gameTiles" 
                        id="gameTiles"
                        className="gameTilesSelect"
                        onChange={this.handleSelectChange.bind(this)}
                        style={styles.selectInput}
                      >
                        <option value="">Select a Tile</option>
                        {gameTiles.map((tile, index) => (
                          <option 
                            key={index} 
                            value={tile.slug}
                          >
                            {tile.name}
                          </option>
                        ))}
                      </select> :
                      <div>
                        <span style={styles.defaultText}>Loading Game Tiles...</span>
                      </div>
                    }
                  </div>
                </div>
                <div style={{paddingBottom: 10}}>
                  <span style={styles.defaultText}>Leave Tile Blank for Defeat All Enemies / Defeat Boss</span>
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game Tiles</span>
              </div>
            }
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Display: </span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right',  paddingRight: 10}}>
                <span style={styles.defaultText}>Defeat All Enemies: </span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="defeatAllEnemies" 
                  id="defeatAllEnemies" 
                  checked={defeatAllEnemies} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Defeat Boss: </span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="defeatBoss" 
                  id="defeatBoss" 
                  checked={defeatBoss} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
          <div style={{flex: 1}}>
            {levelActionTypes !== null ? 
              <div style={{paddingBottom: 10}}>
                <div style={{paddingBottom: 10, display: 'flex'}}>
                  <div style={{flex: 1}}>
                    <span style={styles.defaultText}>Action:</span>
                  </div>
                  <div style={{flex: 1}}>
                    <select 
                      name="levelActionTypes" 
                      id="levelActionTypes"
                      className="levelActionTypesSelect"
                      onChange={this.handleSelectLevelActionType.bind(this)}
                      style={styles.selectInput}
                    >
                      <option value="">...</option>
                      {levelActionTypes.map((actionType, index) => (
                        <option 
                          key={index} 
                          value={actionType.slug}
                        >
                          {actionType.name}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                {levelActionTypeSlug === "change-level" && 
                  <div style={{paddingBottom: 10}}>
                    <div style={{paddingBottom: 10, display: 'flex'}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Select Level:</span>
                      </div>
                      <div style={{flex: 1}}>
                        {levelOptions.length > 0 ? 
                          <select 
                            name="gameLevels" 
                            id="gameLevels"
                            className="gameLevelSelect"
                            onChange={this.handleSelectGameLevel.bind(this)}
                            style={styles.selectInput}
                          >
                            <option value="">...</option>
                            {levelOptions.map((levelOption, index) => (
                              <option 
                                value={levelOption.slug}
                                key={index}
                              >
                                {levelOption.name}
                              </option>
                            ))}
                          </select> :
                          <div>
                            <span style={styles.defaultText}>No Levels to Select</span>
                          </div>
                        }
                      </div>
                    </div>
                  </div>
                }
                {levelActionTypeSlug === "game-action" && 
                  <div style={{paddingBottom: 10}}>
                    <div style={{paddingBottom: 10, display: 'flex'}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Game Action:</span>
                      </div>
                      <div style={{flex: 1}}>
                        {!loadingGameActionTypes ? 
                          <select
                            name="gameActionTypes" 
                            id="gameActionTypes"
                            className="gameActionTypeSelect"
                            onChange={this.handleSelectGameActionType.bind(this)}
                            style={styles.selectInput}
                          >
                            <option value="">...</option>
                            {gameActionTypes.map((gameActionType, index) => (
                              <option 
                                key={index}
                                value={gameActionType.slug}
                              >
                                {gameActionType.name}
                              </option>
                            ))}
                          </select> :
                          <div>
                            <span style={styles.defaultText}>Loading Game Action Types...</span>
                          </div>
                        }
                      </div>
                    </div>
                  </div>
                }
                {levelActionTypeSlug === "change-cutscene" && 
                  <div style={{paddingBottom: 10}}>
                    <div style={{paddingBottom: 10, display: 'flex'}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Select Cutscene:</span>
                      </div>
                      <div style={{flex: 1}}>
                        {!loadingGameCutscenes ? 
                          <div>
                            {gameCutscenes.length > 0 ? 
                              <select
                                name="gameCutscene" 
                                id="gameCutscene"
                                className="gameCutsceneSelect"
                                onChange={this.handleSelectGameCutscene.bind(this)}
                                style={styles.selectInput}
                              >
                                <option value="">...</option>
                                {gameCutscenes.map((cutscene, index) => (
                                  <option 
                                    key={index}
                                    value={cutscene.slug}
                                  >
                                    {cutscene.name}
                                  </option>
                                ))}
                              </select> :
                              <div>
                                <span style={styles.defaultText}>No Cutscenes</span>
                              </div>
                            }
                          </div> :
                          <div>
                            <span style={styles.defaultText}>Loading Cutscenes...</span>
                          </div>
                        }
                      </div>
                    </div>
                  </div>
                }
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Level Action Types...</span>
              </div>
            }
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Require Complete:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="requireComplete" 
                  id="requireComplete" 
                  checked={requireComplete} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCreateNewLevelAction}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeNewLevelAction}>Cancel</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 
  gameLevels: state.games.gameLevels,
  gameCutscenes: state.games.gameCutscenes,
  loadingGameCutscenes: state.games.loadingGameCutscenes,

  gameTiles: state.games.gameTiles,
  loadingGameTiles: state.games.loadingGameTiles,

  levelActions: state.game_admin.levelActions,
  loadingLevelActions: state.game_admin.loadingLevelActions, 

  levelActionTypes: state.game_admin.levelActionTypes,

  newLevelActionObj: state.game_admin.newLevelActionObj,

  gameActionTypes: state.game_admin.gameActionTypes,
  loadingGameActionTypes: state.game_admin.loadingGameActionTypes,

  cutsceneActionTypes: state.game_admin.cutsceneActionTypes,
  loadingCutsceneActionTypes: state.game_admin.loadingCutsceneActionTypes
})

const mapDispatchToProps = {
  newLevelAction, resetNewLevelAction,getLevelActions, resetLevelActions, getGameActionTypes, resetGameActionTypes, getCutsceneActionTypes, resetCutsceneActionTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(NewLevelAction)
