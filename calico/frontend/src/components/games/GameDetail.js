import React, { Component } from 'react'
import { connect } from 'react-redux';

import { styles } from '../layout/Styles';

import { getGameDetail, getGameCharacters, getGameNPCharacters, getGameCutscenes, getGameConversations, getGameLevels, getGameBackgrounds, getGameWeapons, getGameItems } from '../../actions/games';
import { getGamePages, clearGamePages, clearGameDetail } from '../../actions/games';

import EditGame from './edit/EditGame';
import PlayerGames from './player/PlayerGames';
import PlayerGame from './player/PlayerGame';
import GameStatsModal from './GameStatsModal';

class GameDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showGameDetail: true,
      showGameEdit: false,
      showPlayerGame: false,
      defaultCharacterSlug: '',
      showGameStatsModal: false,
      winCount: 0,
      lossCount: 0
    }
  }

  setGameWinsAndLosses = (wins, losses) => {
    this.setState({
      winCount: wins,
      lossCount: losses
    });
  }

  handleCloseGameStatsModal = () => {
    this.setState({
      showGameStatsModal: false
    })
  }

  handleOpenGameStatsModal = () => {
    this.setState({
      showGameStatsModal: true
    })
  }

  handleShowGameEdit = () => {
    this.setState({
      showGameDetail: false,
      showGameEdit: true,
      showPlayerGame: false
    })
  }

  handleShowGameDetail = () => {
    this.setState({
      showGameDetail: true,
      showGameEdit: false,
      showPlayerGame: false
    })
  }

  handleShowPlayerGame = (slug) => {
    this.setState({
      showGameDetail: false,
      showGameEdit: false,
      showPlayerGame: true
    })
  }

  handleShowGames = () => {
    this.props.showGamesList();
  }

  getDefaultCharacter = (defaultCharacterSlug) => {
    this.setState({
      defaultCharacterSlug
    })
  }

  componentDidMount() {
    this.props.getGameDetail(this.props.slug);
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingGameDetail === false && lastProps.loadingGameDetail === true) {
      const { gameDetail } = this.props;
      if (gameDetail.game_type !== null) {
        if (gameDetail.game_type.slug === 'choose-your-own-adventure') {
          this.props.getGamePages(gameDetail.slug);
        }
        if (gameDetail.game_type.slug === 'dungeon-crawler') {
          this.props.getGameCharacters(gameDetail.slug);
          this.props.getGameNPCharacters(gameDetail.slug);
          this.props.getGameLevels(gameDetail.slug);
          this.props.getGameCutscenes(gameDetail.slug);
          this.props.getGameConversations(gameDetail.slug);
          this.props.getGameBackgrounds(gameDetail.slug);
          this.props.getGameWeapons(gameDetail.slug);
          this.props.getGameItems(gameDetail.slug);
        }
      }
    }
    if (this.props.loadingGamePages === false && lastProps.loadingGamePages === true) {

    }
    if (this.props.loadingGameCharacters === false && lastProps.loadingGameCharacters === true) {
      if (this.props.gameCharacters.length === 1) {
        const { gameCharacters } = this.props;
        this.setState({
          defaultCharacterSlug: gameCharacters[0].slug
        })
      }
    }
  }

  componentWillUnmount() {
    this.props.clearGameDetail();
  }

  render() {
    function setTimestamp(s) {
      let fields = s.split('T');
      let date = fields[0].split('-');
      let timestamp = fields[1].split('.')[0];
      let time = timestamp.split(':');
      let period = '';
      if (time[0] > 12) {
        time[0] = time[0] - 12;
        period = "pm";
      } else if (time[0] == 12) {
        period = "pm";
      } else if (time[0] == 0) {
        period = "pm";
        time[0] = 12;
      } else {
        period = "am";
      }
      return date[1] + '/' + date[2] + '/' + date[0] + ', ' + time[0] + ':' + time[1] + ' ' + period;
    }
    
    const { gameDetail, loadingGameDetail, gameCharacters, loadingGameCharacters } = this.props;
    const { loadingGameNPCharacters } = this.props;
    const { gameLevels, loadingGameLevels, loadingGameBackgrounds } = this.props;
    const { loadingGameWeapons, loadingGameItems } = this.props;
    const { showGameDetail, showGameEdit, showPlayerGame, defaultCharacterSlug } = this.state;
    const { gamePages, loadingGamePages } = this.props;
    const { showGameStatsModal } = this.state;
    const { winCount, lossCount } = this.state;

    return (
      <div>
        {showGameStatsModal && 
          <GameStatsModal winCount={winCount} lossCount={lossCount} closeGameStatsModal={this.handleCloseGameStatsModal} />
        }
        {showPlayerGame && 
          <PlayerGame endGame={this.handleShowGameDetail} />
        }
        {showGameEdit && 
          <EditGame closeEditGame={this.handleShowGameDetail} />
        }
        {showGameDetail && 
          <div>
            {!loadingGameDetail ? 
              <div style={{display: 'flex'}}>
                <div style={{flex: 2}}>
                  <img src={gameDetail.thumbnail} style={{width: 200, height: 200}} />
                </div>
                <div style={{flex: 8, paddingLeft: 32}}>
                  <div style={{display: 'flex'}}>
                    <div style={{flex: 9}}>
                      <div style={styles.titleContainer}><span style={styles.titleText}>{gameDetail.name}</span></div>
                    </div>
                    <div style={{flex: 1, textAlign: 'right'}}>
                      <div><button style={styles.button, styles.buttonBack} onClick={this.handleShowGames}>Back</button></div>
                    </div>
                  </div>
                  <div style={styles.descriptionContainer}><span style={styles.descriptionText}>{gameDetail.description}</span></div>
                  {gameDetail.game_type !== null && 
                    <div style={styles.gameTypeContainer}><span style={styles.gameTypeText}>Type: {gameDetail.game_type.name}</span></div>
                  }
                  <div style={{paddingBottom: 25}}>
                    <div style={styles.lastEditedContainer}>
                      <span style={styles.usernameText}>by {gameDetail.created_by.username}, </span>
                      <span style={styles.lastEditedText}>{setTimestamp(gameDetail.last_edited)}</span>
                    </div>
                  </div>

                  {/* Choose Your Own Adventure */}
                  {gameDetail.game_type.slug === 'choose-your-own-adventure' && 
                    <div>
                      <div style={{display: 'flex'}}>
                        <div style={{flex: 2}}>
                          {!loadingGamePages && 
                              <div>
                                {gamePages.length > 0 ? 
                                  <PlayerGames setGameWinsAndLosses={this.setGameWinsAndLosses} defaultCharacterSlug={defaultCharacterSlug} showPlayerGame={this.handleShowPlayerGame} /> :
                                  <div>
                                    <span style={styles.defaultText}>Games must have Pages to play.</span>
                                  </div>
                                }
                              </div>
                            }
                        </div>
                        <div style={{flex: 2}}>
                          {!loadingGamePages ? 
                            <div style={styles.defaultTextContainer}>
                              <button style={styles.buttonDefault} onClick={this.handleOpenGameStatsModal}>Stats</button>
                            </div> :
                            <div style={{padding: 10}}>
                              <span style={styles.defaultText}>Loading...</span>
                            </div>
                          }
                        </div>
                        <div style={{flex: 1, textAlign: 'right'}}>
                          {!loadingGameDetail && 
                            <div>
                              {gameDetail.owner && 
                                <div style={styles.defaultTextContainer}>
                                  <button style={styles.button, styles.buttonEdit} onClick={this.handleShowGameEdit}>Edit Game</button>
                                </div>
                              }
                            </div>
                          }
                        </div>
                      </div>
                    </div>
                  }

                  {/* Dungeon Crawler */}
                  {gameDetail.game_type.slug === 'dungeon-crawler' && 
                    <div>
                      <div style={{display: 'flex'}}>
                        <div style={{flex: 2}}>
                          {(!loadingGameLevels && !loadingGameCharacters) ?  
                            <div>
                              {(gameLevels.length > 0 && gameCharacters.length > 0) ? 
                                <PlayerGames setGameWinsAndLosses={this.setGameWinsAndLosses} defaultCharacterSlug={defaultCharacterSlug} showPlayerGame={this.handleShowPlayerGame} /> :
                                <div style={styles.defaultTextContainer}>
                                  <span style={styles.defaultText}>Games must have a Level &amp; Character to play.</span>
                                </div>
                              }
                            </div> : 
                            <div style={{padding: 10}}>
                              <span style={styles.defaultText}>Loading Game Details...</span>
                            </div>
                          }
                        </div>
                        <div style={{flex: 2, textAlign: 'center'}}>
                          {!loadingGameDetail && !loadingGameCharacters && !loadingGameNPCharacters && !loadingGameLevels && !loadingGameBackgrounds && !loadingGameWeapons && !loadingGameItems ?
                            <div style={styles.defaultTextContainer}>
                              <button style={styles.buttonDefault} onClick={this.handleOpenGameStatsModal}>Stats</button>
                            </div> :
                            <div style={{padding: 10}}>
                              <span style={styles.defaultText}>Loading Stats...</span>
                            </div>
                          }
                        </div>
                        <div style={{flex: 2, textAlign: 'right'}}>
                          {!loadingGameDetail && 
                            <div>
                              {gameDetail.owner && 
                                <div style={styles.defaultTextContainer}>
                                  <button style={styles.button, styles.buttonEdit} onClick={this.handleShowGameEdit}>Edit Game</button>
                                </div>
                              }
                            </div>
                          }
                        </div>
                      </div>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading...</span>
              </div>
            }
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    gameDetail: state.games.gameDetail,
    loadingGameDetail: state.games.loadingGameDetail,

    gameCharacters: state.games.gameCharacters,
    loadingGameCharacters: state.games.loadingGameCharacters,

    gameNPCharacters: state.games.gameNPCharacters,
    loadingGameNPCharacters: state.games.loadingGameNPCharacters,

    gameLevels: state.games.gameLevels,
    loadingGameLevels: state.games.loadingGameLevels,

    gameCutscenes: state.games.gameCutscenes,
    loadingGameCutscenes: state.games.loadingGameCutscenes,

    gameConversations: state.games.gameConversations,
    loadingGameConversations: state.games.loadingGameConversations,
    
    gameBackgrounds: state.games.gameBackgrounds,
    loadingGameBackgrounds: state.games.loadingGameBackgrounds,

    gameWeapons: state.games.gameWeapons,
    loadingGameWeapons: state.games.loadingGameWeapons,

    gameItems: state.games.gameItems,
    loadingGameItems: state.games.loadingGameItems,

    gamePages: state.games.gamePages,
    loadingGamePages: state.games.loadingGamePages
  }
}

const mapDispatchToProps = {
  getGameDetail,
  getGameCharacters,
  getGameNPCharacters,
  getGameLevels,
  getGameCutscenes,
  getGameConversations,
  getGameBackgrounds,
  getGameWeapons,
  getGameItems,
  getGamePages, 
  clearGamePages,
  clearGameDetail
}

export default connect(mapStateToProps, mapDispatchToProps)(GameDetail);