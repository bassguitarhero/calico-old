import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getPlayerLevel, resetChangeLevel, getPlayerLevelActions, resetPlayerLevelActions, changeLevel, playLevelAction, getActivePlayerLevel, resetPlayerPosition } from '../../../actions/player';

export class PlayerActionsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: false,
      showComplete: false
    }
  }

  handleLevelAction = (levelAction) => {
    const { playerLevel } = this.props;
    this.props.playLevelAction(playerLevel, levelAction);
    this.props.resetPlayerLevelActions();
    this.props.closeActionsModal();
  }

  handleCompleteLevel = () => {
    this.setState({
      showComplete: true
    });
  }

  handleChangeLevel = (levelAction) => {
    const { gameDetail } = this.props;
    this.props.changeLevel(gameDetail.slug, levelAction);
    this.props.resetPlayerPosition();
    this.props.resetPlayerLevelActions();
    this.props.closeActionsModal();
  }

  componentDidMount() {
    this.setState({
      completed: this.props.playerLevel.completed
    });
  }

  render() {
    const { playerLevel, playerLevelActions, loadingPlayerLevelActions } = this.props;
    const { completed, showComplete } = this.state;
    
    return (
      <div style={styles.modal}>
        <div style={{padding: 20}}>
          <div style={{display: 'flex'}}>
            <div style={{flex: 9, padding: 10}}>
               <span style={styles.nameText}>Level: {playerLevel.level.name}</span>
            </div>
            <div style={{flex: 1, textAlign: 'right', padding: 10}}>
              <button style={styles.buttonDefault} onClick={this.props.closeActionsModal}>Close</button>
            </div>
          </div>
          <div style={{padding: 10}}>
            <span style={styles.nameText}>Level Actions:</span>
          </div>
          {completed ? 
            <div style={styles.gameTypeContainer}>
              <div>
                <span style={styles.gameTypeText}>Completed</span>
              </div>
              <div>
                {!loadingPlayerLevelActions ? 
                  <div>
                    {playerLevelActions.length > 0 ? 
                      <div>
                        {playerLevelActions.map((levelAction, index) => (
                          <div key={index} style={{padding: 10}}>
                            {levelAction.action_type.slug === 'change-level' ? 
                              <button style={styles.buttonDefault} onClick={this.handleChangeLevel.bind(this, levelAction)}>{levelAction.action_type.name}: {levelAction.change_level.name}</button> :
                              <div>
                                {levelAction.action_type.slug !== 'complete-level' && 
                                  <div>
                                    {levelAction.action_type.slug === 'game-action' ? 
                                      <button style={styles.buttonDefault} onClick={this.handleLevelAction.bind(this, levelAction)}>{levelAction.action_type.name}: {levelAction.game_action_type.name}</button> : 
                                      <button style={styles.buttonDefault} onClick={this.handleLevelAction.bind(this, levelAction)}>{levelAction.action_type.name}</button>
                                    }
                                  </div>
                                }
                              </div>
                            }
                          </div>  
                        ))}
                      </div> :
                      <div>
                        <span style={{padding: 10}}>Loading...</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span style={{padding: 10}}>No Actions</span>
                  </div> 
                  }
              </div>  
            </div> :
            <div>
              <div style={{padding: 10}}><span>Not Completed</span></div>
              {!loadingPlayerLevelActions ? 
                <div>
                  {playerLevelActions.length > 0 ? 
                    <div>
                      {playerLevelActions.map((levelAction, index) => (
                        <div key={index}>
                          {!levelAction.complete_required && 
                            <div style={{padding: 10}}>
                              {levelAction.action_type.slug === 'change-level' ? 
                                <button style={styles.buttonDefault} onClick={this.handleChangeLevel.bind(this, levelAction)}>{levelAction.action_type.name}: {levelAction.change_level.name}</button> :
                                <div>
                                  {levelAction.action_type.slug === 'complete-level' ? 
                                    <div>
                                      {showComplete ? 
                                        <button style={styles.buttonDefault} onClick={this.handleLevelAction.bind(this, levelAction)}>{levelAction.action_type.name}</button> :
                                        <button style={styles.buttonDefault} onClick={this.handleCompleteLevel}>Handle Complete</button>
                                      }
                                    </div> : 
                                    <div>
                                      {levelAction.action_type.slug === 'game-action' ? 
                                        <button  style={styles.buttonDefault}onClick={this.handleLevelAction.bind(this, levelAction)}>{levelAction.action_type.name}: {levelAction.game_action_type.name}</button> : 
                                        <button style={styles.buttonDefault} onClick={this.handleLevelAction.bind(this, levelAction)}>{levelAction.action_type.name}</button>
                                      }
                                    </div>
                                  }
                                </div>
                              }
                            </div>
                          }
                        </div>
                      ))}
                    </div> : 
                    <div style={{padding: 10}}>
                      <span>No Actions</span>
                    </div>
                  }
                </div>
                  :
                <div style={{padding: 10}}>
                  <span>Loading...</span>
                </div>
              }
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  playerLevel: state.player.playerLevel,
  loadingPlayerLevel: state.player.loadingPlayerLevel,

  playerLevelActions: state.player.playerLevelActions,
  loadingPlayerLevelActions: state.player.loadingPlayerLevelActions
})

const mapDispatchToProps = {
  getPlayerLevel, resetChangeLevel, getPlayerLevelActions, changeLevel, playLevelAction, getActivePlayerLevel, resetPlayerPosition, resetPlayerLevelActions
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerActionsModal)
