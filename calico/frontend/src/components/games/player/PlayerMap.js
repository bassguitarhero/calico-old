import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getLevelTiles, clearLevelTiles, getLevelNPCharacters, clearLevelNPCharacters } from '../../../actions/games';

export class PlayerMap extends Component {
  componentDidMount() {
    this.props.getLevelTiles(this.props.gameDetail.slug, this.props.playerLevel.level.slug);
    this.props.getLevelNPCharacters(this.props.gameDetail.slug, this.props.playerLevel.level.slug);
  }
  
  render() {
    const { playerLevel, loadingPlayerLevel, levelTiles, loadingLevelTiles, levelNPCharacters, loadingLevelNPCharacters, playerLevelActions, loadingPlayerLevelActions } = this.props;
    const { npcharacters } = this.props;

    if (!loadingPlayerLevel) {
      return (
        <div>
          <div 
            style={{
              position: 'relative',
              top: '0px',
              left: '0px',
              width: `${playerLevel.level.width}px`,
              height: `${playerLevel.level.height}px`,
              backgroundColor: 'black',
              backgroundImage: `url(${playerLevel.level.image})`,
            }}
          />
          {!loadingLevelTiles ? 
            <div>
              {levelTiles.length > 0 ? 
                <div>
                  {levelTiles.map((levelTile, index) => (
                    <div 
                      style={{
                        position: 'absolute',
                        top: `${levelTile.position_y}px`,
                        left: `${levelTile.position_x}px`,
                        backgroundImage: `url(${levelTile.display && levelTile.tile.display ? levelTile.tile.image : ''})`,
                        backgroundPosition: '0 0',
                        width: `${levelTile.tile.width}px`,
                        height: `${levelTile.tile.height}px`
                      }}
                      key={index}
                    />
                  ))}
                </div> :
                <div>
                  {/* <span>No Level Tiles</span> */}
                </div>
              }
            </div> :
            <div>
              <span>Loading Tiles...</span>
            </div>
          }
          {!loadingLevelNPCharacters ? 
            <div>
              {npcharacters.length > 0 ? 
                <div>
                  {npcharacters.map((levelNPCharacter, index) => (
                    <div 
                      style={{
                        position: 'absolute',
                        top: `${levelNPCharacter.position_y}px`,
                        left: `${levelNPCharacter.position_x}px`,
                        backgroundImage: `url(${levelNPCharacter.display ? levelNPCharacter.npcharacter.sprite_sheet : ''})`,
                        backgroundPosition: '0 0',
                        width: `${levelNPCharacter.npcharacter.width}px`,
                        height: `${levelNPCharacter.npcharacter.height}px`
                      }}
                      key={index}
                    >
                      <span>{levelNPCharacter.health}</span>
                    </div>
                  ))}
                </div> :
                <div>
                  {/* <span>No Level NPCharacters</span> */}
                </div>
              }
            </div> :
            <div>
              <span>Loading NPCharacters...</span>
            </div>
          }
          {!loadingPlayerLevelActions ? 
            <div>
              {playerLevelActions.length > 0 ? 
                <div>
                  {playerLevelActions.map((levelAction, index) => (
                    <div key={index}>
                      {levelAction.level_tile !== null && 
                        <div>
                          {!levelAction.complete_required ? 
                            <div 
                              style={{
                                position: 'absolute',
                                top: `${levelAction.level_tile.position_y}px`,
                                left: `${levelAction.level_tile.position_x}px`,
                                backgroundImage: `url(${levelAction.level_tile.display ? levelAction.level_tile.tile.image : ''})`,
                                backgroundPosition: '0 0',
                                width: `${levelAction.level_tile.tile.width}px`,
                                height: `${levelAction.level_tile.tile.height}px`
                              }}
                            /> : 
                            <div>
                              {playerLevel.completed && 
                                <div 
                                  style={{
                                    position: 'absolute',
                                    top: `${levelAction.level_tile.position_y}px`,
                                    left: `${levelAction.level_tile.position_x}px`,
                                    backgroundImage: `url(${levelAction.level_tile.display ? levelAction.level_tile.tile.image : ''})`,
                                    backgroundPosition: '0 0',
                                    width: `${levelAction.level_tile.tile.width}px`,
                                    height: `${levelAction.level_tile.tile.height}px`
                                  }}
                                />
                              }
                            </div>
                          }
                        </div>
                      }
                    </div>
                  ))}
                </div> :
                <div>
                  {/* <span>No Level Actions</span> */}
                </div>
              }
            </div> :
            <div>
              <span>Loading Level Actions...</span>
            </div>
          }
        </div>
      )
    } else {
      return (
        <div>
          <span>Loading Map...</span>
        </div>
      )
    }

  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  levelTiles: state.games.levelTiles,
  loadingLevelTiles: state.games.loadingLevelTiles, 

  playerLevel: state.player.playerLevel,
  loadingPlayerLevel: state.player.loadingPlayerLevel,

  levelNPCharacters: state.games.levelNPCharacters,
  loadingLevelNPCharacters: state.games.loadingLevelNPCharacters,

  playerLevelActions: state.player.playerLevelActions,
  loadingPlayerLevelActions: state.player.loadingPlayerLevelActions,
})

const mapDispatchToProps = {
  getLevelTiles, clearLevelTiles, getLevelNPCharacters, clearLevelNPCharacters
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerMap)
