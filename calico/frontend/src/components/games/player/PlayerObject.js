import React from 'react';

function PlayerObject(props) {
  return (
    <div
      style={{
        position: 'absolute',
        left: props.playerPosition[0],
        top: props.playerPosition[1],
        backgroundImage: `url(${props.playerCharacter.character.sprite_sheet})`,
        // This has to be a negative number for some reason.
        backgroundPosition: `-${props.playerSpritePosition[1]}px -${props.playerSpritePosition[0]}px`,
        width: `${props.playerCharacter.character.width}px`,
        height: `${props.playerCharacter.character.height}px`
      }}
    />
  )
}

export default PlayerObject;