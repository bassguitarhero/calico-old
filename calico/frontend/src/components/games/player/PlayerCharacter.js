import React, { Component } from 'react'
import { connect } from 'react-redux'
import { styles } from '../../layout/Styles';

import { getPlayerCharacter, resetPlayerCharacter } from '../../../actions/player';

export class PlayerCharacter extends Component {
  handleShowStatsModal = () => {
    this.props.showModal();
  }

  handleShowTeaser = () => {
    this.setState({
      showTeaser: true,
      showDetails: false
    });
  }

  handleShowDetails = () => {
    this.setState({
      showTeaser: false,
      showDetails: true
    });
  }

  componentDidMount() {
    const { playerGame } = this.props;
    this.props.getPlayerCharacter(playerGame.game.slug, playerGame.character.character.slug);
  }

  render() {
    const { playerCharacter, loadingPlayerCharacter } = this.props;

    return (
      <div>
        {!loadingPlayerCharacter ? 
          <div style={{textAlign: 'center'}}>
            <span style={{paddingLeft: 20}}><button style={styles.buttonDefault} onClick={this.handleShowStatsModal}>Stats</button></span>
          </div> :
          <div style={styles.loadingTextContainer}>
            <span style={styles.loadingText}>Loading...</span>
          </div>
        }
      </div>      
    )
  }
}

const mapStateToProps = (state) => ({
  playerGame: state.player.playerGame, 

  playerCharacter: state.player.playerCharacter,
  loadingPlayerCharacter: state.player.loadingPlayerCharacter
})

const mapDispatchToProps = {
  getPlayerCharacter,
  resetPlayerCharacter
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerCharacter)
