import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getPlayerGames, startPlayerGame, getPlayerGame, resetPlayerGames } from '../../../actions/player';
import { styles } from '../../layout/Styles';

export class PlayerGames extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadingData: false,
      openGames: 0,
      characterSlug: ''
    }
  }

  handlePlayGame = () => {
    this.props.getPlayerGame(this.props.gameDetail.slug);
    this.setState({
      uploadingData: true
    });
    this.props.showPlayerGame();
  }

  handleStartGame = () => {
    const { gameDetail, defaultCharacterSlug } = this.props;
    const { characterSlug } = this.state;
    this.props.startPlayerGame(gameDetail.slug, characterSlug);
    this.setState({
      uploadingData: true
    });
    this.props.showPlayerGame();
  }

  componentDidMount() {
    this.props.getPlayerGames(this.props.gameDetail.slug);
    if (this.props.loadingPlayerGames === false) {
      var lossCount = 0;
      var winCount = 0;
      var openGames = 0;
      for (let game of this.props.playerGames) {
        if (game.completed === false) {
          openGames += 1;
        } else {
          if (game.player_win) {
            winCount += 1;
          }  else {
            lossCount += 1;
          }
        }
      }
      this.setState({openGames});
      this.props.setGameWinsAndLosses(winCount, lossCount);
    }
    this.setState({characterSlug: this.props.defaultCharacterSlug});
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingPlayerGames === false && lastProps.loadingPlayerGames === true) {
      const { playerGames } = this.props;
      var openGames = 0;
      var winCount = 0;
      var lossCount = 0;
      for (let game of playerGames) {
        if (game.completed === false) {
          openGames += 1;
        } else {
          if (game.player_win) {
            winCount += 1;
          }  else {
            lossCount += 1;
          }
        }
      }
      this.setState({openGames});
      this.props.setGameWinsAndLosses(winCount, lossCount);
    }
    if (this.props.loadingPlayerGames === true && lastProps.loadingPlayerGames === false) {
      this.props.getPlayerGames(this.props.gameDetail.slug);
    }
  }

  componentWillUnmount() {
    this.setState({
      openGames: 0,
      winCount: 0,
      lossCount: 0,
      characterSlug: ''
    })
    this.props.resetPlayerGames();
  }
  
  render() {
    const { loadingPlayerGames, playerGames } = this.props;
    const { uploadingData, openGames } = this.state;

    return (
      <div style={{flex: 1}}>
        {uploadingData &&
          <div style={styles.defaultTextContainer}>
            <div style={styles.defaultText} id="uploadProgress" />
          </div>
        }
        {!loadingPlayerGames ? 
          <div>
            {playerGames.length > 0 ? 
              <div style={styles.defaultTextContainer}>
                {openGames === 0 ?  
                  <div>
                    <div><button style={styles.button, styles.buttonDefault} onClick={this.handleStartGame}>Start Game</button></div>
                  </div> : 
                  <div>
                    <div><button style={styles.button, styles.buttonDefault} onClick={this.handlePlayGame}>Play Game</button></div>
                  </div>
                }
              </div> :
              <div>
                <div style={styles.gameTypeContainer}><span style={styles.gameTypeText}>No Games Played</span></div>
                <div><button style={styles.button, styles.buttonDefault} onClick={this.handleStartGame}>Start Game</button></div>
              </div>
            }
          </div> :
          <div style={styles.loadingTextContainer}>
            <span style={styles.defaultText}>Loading...</span>
          </div>
        }
    </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  playerGames: state.player.playerGames,
  loadingPlayerGames: state.player.loadingPlayerGames,

  playerGame: state.player.playerGame,
  loadingPlayerGame: state.player.loadingPlayerGame
})

const mapDispatchToProps = {
  getPlayerGames,
  startPlayerGame,
  getPlayerGame,
  resetPlayerGames
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerGames)
