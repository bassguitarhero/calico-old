import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { resetPlayerPosition, resetPlayerAttack, completeAction, playLevelAction, changeLevel, resetPlayerLevelActions } from '../../../actions/player';
import { playGameAction, resetGameAction, resetTakeDamage } from '../../../actions/player';
import { updatePlayerHealth, resetPlayerConversation } from '../../../actions/player';
import { changePlayerCutscene, resetChangePlayerCutscene, clearPlayerLevelData } from '../../../actions/player';

import handleAction from '../../functions/action';
import handleMovement from '../../functions/movement';

import PlayerConversationModal from './PlayerConversationModal';
import PlayerCutscene from './PlayerCutscene';
import PlayerMap from './PlayerMap';
import PlayerObject from './PlayerObject';

export class PlayerStage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      npcharacters: [],
      playerHealth: 0,
      showPlayerLevel: false,
      showPlayerCutscene: false,
      playerCutsceneSlug: null,
      showPlayerConversationModal: false,
      conversation: null,
      npcharacterConversation: null
    }
  }

  handlePlayerConversation = () => {
    const { npcharacters } = this.state;
    const { conversationPosition } = this.props;
    for (var y = 0; y < npcharacters.length; y++) {
      if (conversationPosition[0] == npcharacters[y].position_x && conversationPosition[1] == npcharacters[y].position_y) {
        this.setState({conversation: npcharacters[y].conversation, npcharacterConversation: npcharacters[y]});
        this.handlePlayerConversationModal(true);
        // PAUSE GAME
        window.removeEventListener('keydown', this.handleKeyDown);
      }
    }
  }

  handlePlayerConversationModal = (state) => {
    this.setState({
      showPlayerConversationModal: state
    });
    if (state === false) {
      // RESTART GAME
      window.addEventListener('keydown', this.handleKeyDown);
    }
  }

  handleShowLevelDisplay = () => {
    this.props.showLevelDisplay();
  }

  handleChangePlayerCutscene = (playerLevelAction) => {
    const { gameDetail } = this.props;
    this.props.changePlayerCutscene(gameDetail.slug, playerLevelAction.action_type.slug, playerLevelAction.change_cutscene.slug);
    this.handleShowPlayerCutscene();
    this.props.hideLevelDisplay();
  }

  handleShowPlayerCutscene = () => {
    this.setState({
      showPlayerLevel: false,
      showPlayerCutscene: true
    });
    this.props.resetPlayerPosition();
  }

  handleShowPlayerLevel = () => {
    this.setState({
      showPlayerLevel: true,
      showPlayerCutscene: false
    });
  }

  handleUpdatePlayerHealth = () => {
    const { playerHealth } = this.state;
    const { gameDetail, playerCharacter } = this.props;
    this.props.updatePlayerHealth(gameDetail.slug, playerCharacter.character.slug, playerHealth);
  }

  handleTakeDamage = () => {
    const { playerHealth, npcharacters } = this.state;
    const { damagePosition } = this.props;
    var newPlayerHealth = playerHealth;
    for (var y = 0; y < npcharacters.length; y++) {
      if (damagePosition[0] == npcharacters[y].position_x && damagePosition[1] == npcharacters[y].position_y) {
        if (npcharacters[y].hostile && npcharacters[y].npcharacter.contact_damage) {
          if (playerHealth - npcharacters[y].npcharacter.contact_damage_amount <= 0) {
            newPlayerHealth = 0;
            const { gameDetail, playerLevel } = this.props;
            this.props.resetPlayerLevelActions();
            this.props.playGameAction(gameDetail, playerLevel.level.slug, 'lose-game');
          } else {
            // Handle Knockback
            newPlayerHealth = playerHealth - npcharacters[y].npcharacter.contact_damage_amount;
          }
        }
        this.setState({
          playerHealth: newPlayerHealth
        });
        this.props.setPlayerHealth(newPlayerHealth);
      }
    }
  }

  handleAddNPCharactersToStage = () => {
    const { playerLevel } = this.props;
    const npcharacters = [];
    if (!playerLevel.completed) {
      // console.log('Level Not Completed: ');
      for (var i = 0; i < playerLevel.level.level_npcharacters.length; i++) {
        // console.log('Not Completed NPCharacter: ', playerLevel.level.level_npcharacters[i]);
        npcharacters.push(playerLevel.level.level_npcharacters[i]);
      }
    } else {
      // console.log('Level Completed: ');
      for (var i = 0; i < playerLevel.level.level_npcharacters.length; i++) {
        // console.log('Completed NP Character: ', playerLevel.level.level_npcharacters[i]);
        if (!playerLevel.level.level_npcharacters[i].hostile) {
          // console.log('Completed NP Character Not Hostile: ', playerLevel.level.level_npcharacters[i]);
          npcharacters.push(playerLevel.level.level_npcharacters[i]);
        }
      }
    }
    this.setState({
      npcharacters
    });
    this.props.setNPCharacters(npcharacters);
  }

  handlePlayerAttack = () => {
    const { npcharacters } = this.state;
    const { playerAttackPosition, playerCharacter, playerLevelActions } = this.props;
    const newNPCharacters = [];
    for (var i = 0; i < npcharacters.length; i++) {
      if (playerAttackPosition[0] == npcharacters[i].position_x && playerAttackPosition[1] == npcharacters[i].position_y) {
        if (npcharacters[i].health - playerCharacter.damage >= 1) {
          npcharacters[i].health = npcharacters[i].health - playerCharacter.damage;
        } else {
          npcharacters[i].health = 0;
          if (npcharacters[i].boss) {
            for (var n = 0; n < playerLevelActions.length; n++) {
              if (playerLevelActions[n].defeat_boss) {
                console.log('Run this action for killing a boss: ', playerLevelActions[n]);
                if (playerLevelActions[n].action_type.slug === 'game-action') {
                  this.handleGameAction(playerLevelActions[n]);
                } else {
                  this.handleLevelAction(playerLevelActions[n]);
                }
              }
            }
          }
        }
      }
    }
    for (var j = 0; j < npcharacters.length; j++) {
      if (npcharacters[j].health > 0) {
        newNPCharacters.push(npcharacters[j]);
      }
    }
    this.setState({npcharacters: newNPCharacters});
    this.props.setNPCharacters(newNPCharacters);
    if (newNPCharacters.length === 0) {
      for (var k = 0; k < playerLevelActions.length; k++) {
        if (playerLevelActions[k].defeat_all_enemies) {
          if (playerLevelActions[k].action_type.slug === 'game-action') {
            this.handleGameAction(playerLevelActions[k]);
          } else {
            this.handleLevelAction(playerLevelActions[k]);
          }
        }
      }
    }
  }

  handleGameAction = (levelAction) => {
    const { gameDetail, playerLevel } = this.props;
    this.props.resetPlayerLevelActions();
    this.props.playGameAction(gameDetail, playerLevel.level.slug, levelAction.game_action_type.slug);
  }

  handleLevelAction = (levelAction) => {
    const { playerLevel } = this.props;
    this.props.resetPlayerLevelActions();
    this.handleUpdatePlayerHealth();
    this.props.playLevelAction(playerLevel, levelAction);
  }

  handleChangeLevel = (levelAction) => {
    const { gameDetail } = this.props;
    this.props.resetPlayerPosition();
    this.props.resetPlayerLevelActions();
    this.handleUpdatePlayerHealth();
    this.props.changeLevel(gameDetail.slug, levelAction);
  }

  handlePlayerAction = () => {
    const { actionPosition, playerLevelActions } = this.props;
    for (var i = 0; i < playerLevelActions.length; i++) {
      if (playerLevelActions[i].level_tile !== null) {
        if (playerLevelActions[i].level_tile.position_x == actionPosition[0] && playerLevelActions[i].level_tile.position_y == actionPosition[1]) {
          if (playerLevelActions[i].action_type.slug == 'change-level') {
            this.handleChangeLevel(playerLevelActions[i]);
          } else if (playerLevelActions[i].action_type.slug == 'game-action') {
            this.handleGameAction(playerLevelActions[i]);
          } else if (playerLevelActions[i].action_type.slug == 'change-cutscene') {
            this.handleChangePlayerCutscene(playerLevelActions[i]);
          } else {
            this.handleLevelAction(playerLevelActions[i]);
          }
        }
      }
    }
  }

  handleKeyDown = (e) => {
    e.preventDefault();
    const { playerCharacter, playerLevel, movementGrid, npcharacterGrid } = this.props;
    // console.log('KeyCode: ', e.keyCode);
    switch(e.keyCode) {
      // MOVEMENT
      case 65:
        return handleMovement('WEST', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, playerCharacter.can_walk, playerCharacter.can_fly, playerCharacter.can_climb, npcharacterGrid);
      case 68:
        return handleMovement('EAST', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, playerCharacter.can_walk, playerCharacter.can_fly, playerCharacter.can_climb, npcharacterGrid);
      case 83:
        return handleMovement('SOUTH', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, playerCharacter.can_walk, playerCharacter.can_fly, playerCharacter.can_climb, npcharacterGrid);
      case 87:
        return handleMovement('NORTH', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, playerCharacter.can_walk, playerCharacter.can_fly, playerCharacter.can_climb, npcharacterGrid);

      // ACTIONS
      case 38:
        return handleAction('UP', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, npcharacterGrid);
      case 37:
        return handleAction('LEFT', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, npcharacterGrid);
      case 40:
        return handleAction('DOWN', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, npcharacterGrid);
      case 39:
        return handleAction('RIGHT', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, npcharacterGrid);
      case 69:
        return handleAction('E', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, npcharacterGrid);
      case 17:
        return handleAction('LEFT CTRL', playerCharacter.character.width, playerCharacter.character.height, playerLevel.level.width, playerLevel.level.height, playerLevel.level.tile_width, playerLevel.level.tile_height, movementGrid, npcharacterGrid);
    }
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
    const { playerCharacter, playerLevel } = this.props;
    this.setState({
      playerHealth: playerCharacter.hp
    });
    this.props.setPlayerHealth(playerCharacter.hp);
    if (playerLevel.active === true) {
      this.setState({
        showPlayerCutscene: false,
        showPlayerLevel: true
      });
      this.handleAddNPCharactersToStage();
    } else if (playerLevel.active === false) {
      this.setState({
        showPlayerCutscene: true,
        showPlayerLevel: false
      });
    }
  }

  componentDidUpdate(lastProps) {
    if (this.props.takeAction === true && lastProps.takeAction === false) {
      this.handlePlayerAction();
      this.props.completeAction();
    }
    if (this.props.playerAttackPosition !== lastProps.playerAttackPosition) {
      this.handlePlayerAttack();
    }
    if (this.props.takeDamage === true && lastProps.takeDamage === false) {
      this.handleTakeDamage();
      this.props.resetTakeDamage();
    }
    if (this.props.loadingPlayerCharacter === false && lastProps.loadingPlayerCharacter === true) {
      const { playerCharacter } = this.props;
      this.setState({playerHealth: playerCharacter.hp});
      this.props.setPlayerHealth(playerCharacter.hp);
    }
    if (this.props.initiateConversation === true && lastProps.initiateConversation === false) {
      this.handlePlayerConversation();
      this.props.resetPlayerConversation();
    }
    // if (this.props.loadingPlayerCutscene === false && lastProps.loadingPlayerCutscene === true) {
    //   this.handleShowPlayerCutscene();
    // }
  }

  componentWillUnmount() {
    this.props.resetPlayerAttack();
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  render() {
    const { playerSpritePosition, playerPosition, playerCharacter, loadingPlayerCharacter, playerLevel, loadingPlayerLevel } = this.props;
    const { npcharacters, showPlayerCutscene, showPlayerLevel, playerCutsceneSlug, showPlayerConversationModal } = this.state;
    const { conversation, npcharacterConversation } = this.state;

    return (
      <div>
        {showPlayerConversationModal && 
          <PlayerConversationModal npcharacterConversation={npcharacterConversation} conversation={conversation} closePlayerConversationModal={this.handlePlayerConversationModal} />
        }
        {showPlayerCutscene && 
          <PlayerCutscene showLevelDisplay={this.handleShowLevelDisplay} closeCutscene={this.handleShowPlayerLevel} cutsceneSlug={playerCutsceneSlug} />
        }
        {showPlayerLevel && 
          <div>
            {!loadingPlayerCharacter && !loadingPlayerLevel && 
              <div 
              style={{
                position: 'relative',
                width: `${playerLevel.level.width}px`,
                height: `${playerLevel.level.height}px`,
                margin: '20px auto'
              }}
            >
              <PlayerMap npcharacters={npcharacters} />
              <PlayerObject playerSpritePosition={playerSpritePosition} playerPosition={playerPosition} playerCharacter={playerCharacter} />
            </div>
            }
          </div>
        }
      </div>
    )
  }    
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  playerPosition: state.player.playerPosition,
  playerSpritePosition: state.player.playerSpritePosition,
  direction: state.player.direction,
  playerAttackPosition: state.player.playerAttackPosition, 
  playerSpriteAttackPosition: state.player.playerSpriteAttackPosition,
  attackKey: state.player.attackKey,
  attackIndex: state.player.attackIndex,

  playerCharacter: state.player.playerCharacter,
  loadingPlayerCharacter: state.player.loadingPlayerCharacter,

  playerLevel: state.player.playerLevel,
  loadingPlayerLevel: state.player.loadingPlayerLevel,

  takeAction: state.player.takeAction,
  actionPosition: state.player.actionPosition,
  takeDamage: state.player.takeDamage,
  damagePosition: state.player.damagePosition,

  playerLevelActions: state.player.playerLevelActions,
  loadingPlayerLevelActions: state.player.loadingPlayerLevelActions,

  playerCutscene: state.player.playerCutscene,
  loadingPlayerCutscene: state.player.loadingPlayerCutscene,

  initiateConversation: state.player.initiateConversation,
  conversationPosition: state.player.conversationPosition
})

const mapDispatchToProps = {
  resetPlayerPosition, resetPlayerAttack, completeAction, playLevelAction, changeLevel, resetPlayerLevelActions, playGameAction, resetGameAction, resetTakeDamage, updatePlayerHealth, changePlayerCutscene, resetChangePlayerCutscene, clearPlayerLevelData, resetPlayerConversation
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerStage)
