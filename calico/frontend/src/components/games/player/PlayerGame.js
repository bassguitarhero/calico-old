import React, { Component } from 'react'
import { connect } from 'react-redux'

import { resetPlayerGames, resetPlayerGame, resetPlayerPage, resetPlayerPageActions, getPlayerCharacter, resetPlayerCharacter, resetPlayerLevelActions, resetPlayerPosition } from '../../../actions/player';
import { styles } from '../../layout/Styles';

import PlayerActionsModal from './PlayerActionsModal';
import PlayerCharacter from './PlayerCharacter';
import PlayerLevel  from './PlayerLevel';
import PlayerPage from './PlayerPage';
import PlayerStage from './PlayerStage';
import PlayerStatsModal from './PlayerStatsModal';

export class PlayerGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: {
        strength: 0,
        skill: 0,
        speed: 0,
        health: 0,
        wisdom: 0,
        luck: 0,
        hp: 0,
        keys: 0,
        bombs: 0,
        damage: 0,
      },
      showStatsModal: false,
      showActionsModal: false,
      npcharacters: [],
      movementGrid: [],
      npcharacterGrid: [],
      levelDisplay: true
    }
  }

  handleShowLevelDisplay = () => {
    this.setState({
      levelDisplay: true
    })
  }

  handleHideLevelDisplay = () => {
    this.setState({
      levelDisplay: false
    });
  }

  handleSetPlayerHealth = (newPlayerHealth) => {
    this.setState({
      player: {
        hp: newPlayerHealth
      }
    });
  }

  handleHideActionsModal = () => {
    this.setState({
      showActionsModal: false
    });
  }

  handleShowActionsModal = () => {
    this.setState({
      showActionsModal: true
    });
  }

  handleHideStatsModal = () => {
    this.setState({
      showStatsModal: false
    });
  }

  handleShowStatsModal = () => {
    this.setState({
      showStatsModal: true
    });
  }

  setNPCharacters = (npcharacters) => {
    this.setState({npcharacters});
  }

  setNPCharacterGrid = (npcharacterGrid) => {
    this.setState({
      npcharacterGrid
    });
  }

  setMovementGrid = (movementGrid) => {
    this.setState({
      movementGrid
    });
  }

  handleExitGame = () => {
    this.props.resetPlayerGames();
    this.props.resetPlayerGame();
    this.props.resetPlayerPage();
    this.props.resetPlayerPageActions();
    this.props.resetPlayerCharacter();
    this.props.resetPlayerPosition();
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingPlayerGame === false && lastProps.loadingPlayerGame === true) {
      const { playerGame } = this.props;
      if (playerGame.character !== null) {
        this.setState({
          player: {
            strength: playerGame.character.strength,
            skill: playerGame.character.skill,
            speed: playerGame.character.speed,
            health: playerGame.character.health,
            wisdom: playerGame.character.wisdom,
            luck: playerGame.character.luck,
            hp: playerGame.character.hp,
            keys: playerGame.character.keys,
            bombs: playerGame.character.bombs,
            damage: playerGame.character.damage
          }
        });
      }
    }
    if (this.props.playerCharacter === null && lastProps.playerCharacter !== null) {
      this.props.resetPlayerLevelActions();
      this.props.endGame();
    }
  }

  render() {
    const { gameDetail, playerGame, loadingPlayerGame, loadingPlayerCharacter, loadingPlayerLevel } = this.props;
    const { movementGrid, npcharacterGrid, showStatsModal, showActionsModal, player } = this.state;
    const { npcharacters, levelDisplay } = this.state;

    return (
      <div style={{flex: 1}}>
        {!loadingPlayerGame && !playerGame.completed && 
          <div>
            {showStatsModal && 
              <PlayerStatsModal closeModal={this.handleHideStatsModal} />
            }
            {showActionsModal && 
              <PlayerActionsModal closeActionsModal={this.handleHideActionsModal} />
            }
          </div>
        }
        {!loadingPlayerGame ? 
          <div>
            <div style={{display: 'flex', padding:5}}>
              {/* GAME TITLE */}
              <div style={{flex: 5}}>
                <div style={{paddingTop: 10, paddingBottom: 5}}>
                  <span style={styles.titleText}>{gameDetail.name}</span>
                </div>
              </div>
              {/* HP */}
              <div style={{flex: 1, textAlign: 'center'}}>
                {(!loadingPlayerCharacter && !playerGame.completed && gameDetail.game_type.slug === 'dungeon-crawler') && 
                  <div style={{paddingTop: 10, paddingBottom: 5}}>
                    <span style={styles.gameTypeText}>HP: {player.hp}</span>
                  </div>
                }
              </div>
              {/* LEVEL BUTTON */}
              <div style={{flex: 1, textAlign: 'center'}}>
                {!playerGame.completed && gameDetail.game_type.slug === 'dungeon-crawler' &&
                  <PlayerLevel levelDisplay={levelDisplay} npcharacters={npcharacters} showActionsModal={this.handleShowActionsModal} setMovementGrid={this.setMovementGrid} setNPCharacterGrid={this.setNPCharacterGrid} />
                }
              </div>
              {/* STATS BUTTON */}
              <div style={{flex: 1, textAlign: 'center'}}>
                {!playerGame.completed && gameDetail.game_type.slug === 'dungeon-crawler' &&
                  <PlayerCharacter setPlayerHealth={this.handleSetPlayerHealth} showModal={this.handleShowStatsModal} />
                }
              </div>
              {/* EXIT BUTTON */}
              <div style={{flex: 1, textAlign: 'right'}}>
                <button style={styles.buttonDefault} onClick={this.handleExitGame}>Exit Game</button>
              </div>
            </div>
            {!playerGame.completed ? 
              <div>
                {gameDetail.game_type.slug === 'dungeon-crawler' && 
                  <div>
                    {!loadingPlayerLevel && !loadingPlayerCharacter ? 
                      <PlayerStage showLevelDisplay={this.handleShowLevelDisplay} hideLevelDisplay={this.handleHideLevelDisplay} setPlayerHealth={this.handleSetPlayerHealth} setNPCharacters={this.setNPCharacters} movementGrid={movementGrid} npcharacterGrid={npcharacterGrid} /> :
                      <div><span>Loading...</span></div>
                    }
                  </div>
                }
                {gameDetail.game_type.slug === 'choose-your-own-adventure' && 
                  <PlayerPage />
                }
              </div> :
              <div>
                <div style={{paddingTop: 5, paddingBottom: 5, paddingLeft: 5}}>
                  <span style={styles.defaultText}>Game Over: </span> 
                  {playerGame.player_win ? 
                    <span style={styles.defaultText}>You Win</span> : <span style={styles.defaultText}>You Lose</span>
                  }
                </div>
                <div style={{paddingTop: 10, paddingLeft: 5}}>
                  <button style={styles.buttonAction} onClick={this.handleExitGame}>
                    <div style={{padding: 5}}>Exit Game</div>
                  </button>
                </div>
              </div>
            }
          </div> :
          <div style={styles.loadingTextContainer}>
            <span style={styles.loadingText}>Loading... <div id="uploadProgress" /></span>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  loadingPlayerGame: state.player.loadingPlayerGame,
  playerGame: state.player.playerGame,

  loadingPlayerLevel: state.player.loadingPlayerLevel,

  playerCharacter: state.player.playerCharacter,
  loadingPlayerCharacter: state.player.loadingPlayerCharacter,

  playerPosition: state.player.playerPosition,
  loadingPlayerPosition: state.player.loadingPlayerPosition
})

const mapDispatchToProps = {
  resetPlayerGames,
  resetPlayerGame,
  resetPlayerPage, 
  resetPlayerPageActions,
  getPlayerCharacter,
  resetPlayerCharacter,
  resetPlayerLevelActions,
  resetPlayerPosition
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerGame)
