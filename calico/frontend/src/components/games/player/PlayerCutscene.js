import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getCutsceneDetail, resetCutsceneDetail, getCutsceneActions, resetCutsceneActions } from '../../../actions/games';
import { changeCutsceneLevel, getActivePlayerCutscene } from '../../../actions/player';

let loadYT;

export class PlayerCutscene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      youtubeID: '',
      vimeoID: '',
      archiveID: '',
      cutsceneComplete: false,
      youtubePlayer: {}
    }
  }

  handleCompleteCutscene = () => {
    const { cutsceneActions } = this.props;
    for (var t = 0; t < cutsceneActions.length; t++) {
      if (cutsceneActions[t].auto_select === true) {
        this.handleCutsceneAction(cutsceneActions[t]);
      }
    }
    this.setState({
      cutsceneComplete: true
    });
  }

  handleChangeCutsceneLevel = (cutsceneAction) => {
    const { gameDetail } = this.props;
    this.setState({uploadingData: true});
    this.props.changeCutsceneLevel(gameDetail.slug, cutsceneAction);
  }

  handleCutsceneAction = (cutsceneAction) => {
    const { gameDetail } = this.props;
    if (cutsceneAction.action_type.slug === 'change-level') {
      if (cutsceneAction.change_level !== null) {
        this.handleChangeCutsceneLevel(cutsceneAction);
      }
    }
    if (cutsceneAction.action_type.slug === 'change-cutscene') {
      if (cutsceneAction.change_cutscene !== null) {
        
      }
    }
    if (cutsceneAction.action_type.slug === 'game-action') {
      if (cutsceneAction.game_action_type !== null) {

      }
    }
  }

  handleCloseCutscene = () => {
    this.props.handleShowPlayerLevel();
  }

  parse_youtube = (url) => {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
  }

  handleBuildYouTubePlayer = (youtubeURL) => {
    const { playerCutscene } = this.props;
    var youtubePlayer = {};
    var youtubeID = '';
    youtubeID = this.parse_youtube(youtubeURL);
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var youtubePlayer;
    function onYouTubeIframeAPIReady() {
      youtubePlayer = new YT.Player('youtubePlayer', {
        height: `${playerCutscene.cutscene.height}`,
        width: `${playerCutscene.cutscene.width}`,
        videoId: youtubeID,
        events: {
          'onReady': onPlayerReady,
          'onStageChange': onPlayerStateChange
        }
      });
    }

    function onPlayerReady(event) {
      event.target.playVideo();
    }

    // var done = false;
    // function onPlayerStateChange(event) {
    //   if (event.data == YT.PlayerState.PLAYING && !done) {
    //     setTimeout(stopVideo, 6000);
    //     done = true;
    //   }
    // }
    function onPlayerStateChange(event) {
      if (event.data === 0) {
        alert('done');
      }
    }

    function stopVideo() {
      youtubePlayer.stopVideo();
    }

    this.setState({
      youtubePlayer
    });
  }

  handleLoadYouTube = (youtubeURL) => {
    const { playerCutscene } = this.props;
    var youtubeID = '';
    youtubeID = this.parse_youtube(youtubeURL);
    if (!loadYT) {
      loadYT = new Promise((resolve) => {
        const tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/iframe_api';
        const firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        window.onYouTubeIframeAPIReady = () => resolve(window.YT);
      });
    }
    loadYT.then((YT) => {
      this.youtubePlayer = new YT.Player(this.youtubePlayerAnchor, {
        height: playerCutscene.cutscene.height,
        width: playerCutscene.cutscene.width,
        videoId: youtubeID,
        events: {
          onStateChange: this.onPlayerStateChange
        }
      });
    });
  }

  onPlayerStateChange = (e) => {
    if (typeof this.props.onStateChange === 'function') {
      this.props.onStateChange(e);  
    }
    if (e.data === 0 || e.data === 2) {
      // alert('done');
      this.setState({
        cutsceneComplete: true
      });
    } else if (e.data === 1) {
      this.setState({
        cutsceneComplete: false
      });
    }
  }

  handleLoadCutscene = () => {
    const { playerCutscene } = this.props;
    var fields = '';
    var youtubeID = null;
    var vimeoID = null;
    var archiveID = null;
    if (playerCutscene.cutscene.url_youtube !== null || playerCutscene.cutscene.url_vimeo !== null || playerCutscene.cutscene.url_archive !== null) {
      if (playerCutscene.cutscene.url_youtube !== null) {
        this.handleLoadYouTube(playerCutscene.cutscene.url_youtube);
        youtubeID = this.parse_youtube(playerCutscene.cutscene.url_youtube);
      } else if (playerCutscene.cutscene.url_vimeo !== null) {
        fields = videoID.split('/');
        vimeoID = fields[3];
      } else if (playerCutscene.cutscene.url_archive !== null) {
        fields = videoID.split('/');
        archiveID = fields[4];
      }
    }
    this.setState({
      youtubeID: youtubeID,
      vimeoID: vimeoID,
      archiveID: archiveID
    })
  }
  
  componentDidMount() {
    this.props.getActivePlayerCutscene(this.props.gameDetail.slug);
    // console.log('Player Cutscene: ', this.props.playerCutscene);
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingPlayerCutscene === false && lastProps.loadingPlayerCutscene === true) {
      this.props.getCutsceneActions(this.props.gameDetail.slug, this.props.playerCutscene.cutscene.slug);
    }
    if (this.props.loadingCutsceneActions === false && lastProps.loadingCutsceneActions === true) {
      this.handleLoadCutscene();
    }
  }

  componentWillUnmount() {
    // this.props.resetCutsceneDetail();
    this.props.resetCutsceneActions();
  }

  render() {
    const { playerCutscene, loadingPlayerCutscene, cutsceneActions, loadingCutsceneActions } = this.props;
    const { youtubeID, vimeoID, archiveID, cutsceneComplete } = this.state;

    return (
      <div>
        {!loadingPlayerCutscene ? 
          <div>
            {(youtubeID !== null || vimeoID !== null || archiveID !== null) ? 
              <div style={{margin: '20px auto', width: `${playerCutscene.cutscene.width}px`, height: `${playerCutscene.cutscene.height}px`, maxWidth: '100%'}}>
                {(youtubeID !== null && youtubeID !== "null") && 
                    // <iframe src={`https://www.youtube.com/embed/${youtubeID}?autoplay=${playerCutscene.cutscene.autoplay ? 1 : 0}&loop=${playerCutscene.cutscene.loop ? 1 : 0}`}
                    //   frameBorder='0' 
                    //   allow='autoplay; encrypted-media' 
                    //   allowFullScreen 
                    //   title='video' 
                    //   width={playerCutscene.cutscene.width}
                    //   height={playerCutscene.cutscene.height}
                    // />
                    // <div id="youtubePlayer"></div>
                    <div ref={(r) => {this.youtubePlayerAnchor = r}}></div>
                }
                {(vimeoID !== null && vimeoID !== "null") && 
                  <div>
                    <div><span>Vimeo:</span></div>
                    <div><span>{playerCutscene.cutscene.url_vimeo}</span></div>
                  </div>
                }
                {(archiveID !== null && archiveID !== "null") && 
                  <div>
                    <div><span>Internet Archive:</span></div>
                    <div><span>{playerCutscene.cutscene.url_archive}</span></div>
                  </div>
                }
              </div> :
              <div>
                <span>No Video Fields</span>
              </div>
            }
          </div> : 
          <div>
            <span>Loading Cutscene...</span>
          </div>
        }
        {!loadingCutsceneActions ? 
          <div>
            {cutsceneActions.length > 0 ? 
              <div>
                {cutsceneActions.map((cutsceneAction, index) => (
                  <div key={index}>
                    {(cutsceneAction.complete_required && cutsceneComplete) || (!cutsceneAction.complete_required) && 
                      <button onClick={this.handleCutsceneAction.bind(this, cutsceneAction)} style={styles.buttonDefault}>
                        {cutsceneAction.name}
                      </button>
                    }
                  </div>
                ))}
              </div> :
              <div>
                <span>No Cutscene Actions</span>
              </div>
            }
          </div> :
          <div>
            <span>Loading Cutscene Actions...</span>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  cutsceneDetail: state.games.cutsceneDetail,
	loadingCutsceneDetail: state.games.loadingCutsceneDetail,

  cutsceneActions: state.games.cutsceneActions,
  loadingCutsceneActions: state.games.loadingCutsceneActions,

  playerCutscene: state.player.playerCutscene,
  loadingPlayerCutscene: state.player.loadingPlayerCutscene
})

const mapDispatchToProps = {
  getCutsceneDetail, resetCutsceneDetail, getCutsceneActions, resetCutsceneActions,
  changeCutsceneLevel, getActivePlayerCutscene
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerCutscene)
