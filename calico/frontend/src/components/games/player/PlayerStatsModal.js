import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

export class PlayerStatsModal extends Component {
  render() {
    const { loadingPlayerCharacter, playerCharacter } = this.props;

    return (
      <div style={styles.modal}>
        <div style={{padding: 20}}>
          {!loadingPlayerCharacter ? 
            <div>
              <div style={{display: 'flex'}}>
                <div style={{flex: 9, padding: 10}}>
                  <span style={styles.nameText}>Player Stats:</span>
                </div>
                <div style={{flex: 1, textAlign: 'right', padding: 10}}>
                  <button style={styles.buttonDefault} onClick={this.props.closeModal}>Close</button>
                </div>
              </div>
              <div style={styles.nameContainer}>
                <span style={styles.gameTypeText}>Name: {playerCharacter.name}</span>
              </div>
              <div style={{display: 'flex'}}>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Thumb: </span><img src={playerCharacter.character.thumbnail} style={styles.characterImageThumb} />
                </div>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Sprite Sheet Thumb: </span><img src={playerCharacter.character.sprite_sheet_thumbnail} style={styles.characterImageThumb} />
                </div>
              </div>
              <div style={{display: 'flex'}}>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Strength: {playerCharacter.strength}</span>
                </div>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Skill: {playerCharacter.skill}</span>
                </div>
              </div>
              <div style={{display: 'flex'}}>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Speed: {playerCharacter.speed}</span>
                </div>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Health: {playerCharacter.health}</span>
                </div>
              </div>
              <div style={{display: 'flex'}}>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Wisdom: {playerCharacter.wisdom}</span>
                </div>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Luck: {playerCharacter.luck}</span>
                </div>
              </div>
              <div style={{display: 'flex'}}>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>HP: {playerCharacter.hp}</span>
                </div>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Keys: {playerCharacter.keys}</span>
                </div>
              </div>
              <div style={{display: 'flex'}}>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Bombs: {playerCharacter.bombs}</span>
                </div>
                <div style={{padding: 10, flex: 1}}>
                  <span style={styles.gameTypeText}>Damage: {playerCharacter.damage}</span>
                </div>
              </div>
            </div> :
            <div style={styles.loadingTextContainer}>
              <span style={styles.loadingText}>Loading...</span>
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  playerCharacter: state.player.playerCharacter,
  loadingPlayerCharacter: state.player.loadingPlayerCharacter
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerStatsModal)
