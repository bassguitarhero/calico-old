import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { startPlayerConversation, getPlayerConversationDialogue, resetPlayerConversationDialogue, getPlayerConversationDialogueResponses, resetPlayerConversationDialogueResponses } from '../../../actions/player';

export class PlayerConversationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      facebookID: '',
      twitterID: '',
      youtubeID: ''
    }
  }

  handleClosePlayerConversationModal = () => {
    this.props.closePlayerConversationModal(false);
  }

  handleDialogueResponseAction = (response) => {
    // console.log('Response: ', response);
    if (response.ending_response == true) {
      this.handleClosePlayerConversationModal();
    } else if (response.connect_to !== null) {
      const { gameDetail, conversation } = this.props;
      this.props.resetPlayerConversationDialogue();
      this.props.resetPlayerConversationDialogueResponses();
      this.props.getPlayerConversationDialogue(gameDetail.slug, conversation.slug, response.connect_to.slug);
    }
  }

  parse_facebook = (url) => {
    // return 'facebook: ' + url;
  }

  parse_twitter = (url) => {
    // return 'twitter: ' + url;
  }

  parse_youtube = (url) => {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
  }

  handleGetFacebookConversation = () => {
    const { playerConversationDialogue } = this.props;
    // var facebookID;
    // facebookID = this.parse_facebook(playerConversationDialogue.embed_facebook);
    // console.log('Facebook: ', facebookID);
    // this.setState({
    //   facebookID
    // });
  }

  handleGetTwitterConversation = () => {
    const { playerConversationDialogue } = this.props;
    // console.log('Twitter Embed: ', playerConversationDialogue.embed_twitter);
    // var twitterID;
    // twitterID = this.parse_twitter(playerConversationDialogue.embed_twitter);
    // console.log('Twitter: ', twitterID);
    // this.setState({
    //   twitterID
    // });
  }

  handleGetYouTubeConversation = () => {
    const { playerConversationDialogue } = this.props;
    var youtubeID;
    youtubeID = this.parse_youtube(playerConversationDialogue.embed_youtube);
    // console.log('YouTube: ', youtubeID);
    this.setState({
      youtubeID
    });
  }

  componentDidMount() {
    const { gameDetail, conversation, npcharacterConversation } = this.props;
    this.props.startPlayerConversation(gameDetail.slug, conversation.slug);
    // console.log('Conversation: ', conversation);
    // console.log('npcharacterConversation: ', npcharacterConversation);
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingPlayerConversationDialogue === false && lastProps.loadingPlayerConversationDialogue === true) {
      const { gameDetail, conversation, playerConversationDialogue } = this.props;
      // console.log('Dialogue: ', this.props.playerConversationDialogue);
      if (this.props.playerConversationDialogue.embed_facebook !== '' && this.props.playerConversationDialogue.embed_facebook !== null && this.props.playerConversationDialogue.embed_facebook !== 'null') {
        this.handleGetFacebookConversation();
      }
      if (this.props.playerConversationDialogue.embed_twitter !== '' && this.props.playerConversationDialogue.embed_twitter !== null && this.props.playerConversationDialogue.embed_twitter !== 'null') {
        this.handleGetTwitterConversation();
      }
      if (this.props.playerConversationDialogue.embed_youtube !== '' && this.props.playerConversationDialogue.embed_youtube !== null && this.props.playerConversationDialogue.embed_youtube !== 'null') {
        this.handleGetYouTubeConversation();
      }
      this.props.getPlayerConversationDialogueResponses(gameDetail.slug, conversation.slug, playerConversationDialogue.slug);
    }
    // if (this.props.loadingPlayerConversationDialogueResponses === false && lastProps.loadingPlayerConversationDialogueResponses) {
    //   console.log('Responses: ', this.props.playerConversationDialogueResponses);
    // }
  }

  componentWillUnmount() {
    this.props.resetPlayerConversationDialogue();
    this.props.resetPlayerConversationDialogueResponses();
  }
  
  render() {
    const { conversation, npcharacterConversation } = this.props;
    const { playerConversationDialogue, loadingPlayerConversationDialogue, playerConversationDialogueResponses, loadingPlayerConversationDialogueResponses } = this.props;
    const { youtubeID } = this.state;

    return (
      <div style={styles.modal}>
        <div style={{padding: 20}}>
          {conversation !== null ? 
            <div>
              {conversation.show_character ? 
                <div style={{display: 'flex'}}>
                  <div style={{flex: 1}}>
                    <div>
                      <div>
                        {npcharacterConversation.npcharacter.thumbnail !== null && 
                          <img 
                            src={npcharacterConversation.npcharacter.thumbnail}
                            style={{width: 100, height: 100, padding: 5}}
                          />
                        }
                      </div>
                      <div style={{padding: 5}}><span>{npcharacterConversation.npcharacter.name}</span></div>
                    </div>
                  </div>
                  <div style={{flex: 9}}>
                    {!loadingPlayerConversationDialogue ? 
                      <div>
                        <div style={{padding: 5}}>
                          <span>{playerConversationDialogue.name}</span>
                        </div>
                        {playerConversationDialogue.description && 
                          <div style={{padding: 5}}>
                            <span>{playerConversationDialogue.description}</span>
                          </div>
                        }
                        {(playerConversationDialogue.embed_facebook !== null) && (playerConversationDialogue.embed_facebook !== '') && (playerConversationDialogue.embed_facebook !== 'null') && 
                          // <iframe src={playerConversationDialogue.embed_facebook}
                          //   frameBorder='0' 
                          //   title='Facebook'
                          //   width='960'
                          //   height='540'
                          // />
                          <div style={{padding: 5}}>
                            <span>{playerConversationDialogue.embed_facebook}</span>
                          </div>
                        }
                        {(playerConversationDialogue.embed_twitter !== null) && (playerConversationDialogue.embed_twitter !== '') && (playerConversationDialogue.embed_twitter !== 'null') && 
                          // <div style={{padding: 5}}>
                          //   <span>{playerConversationDialogue.embed_twitter}</span>
                          // </div>
                          <div style={{padding: 5}} dangerouslySetInnerHTML={{"__html": playerConversationDialogue.embed_twitter}} />
                        }
                        {playerConversationDialogue.embed_youtube && youtubeID !== '' && 
                           <div style={{margin: '10px auto', width: `480px`, height: `270px`, maxWidth: '100%'}}>
                            <iframe src={`https://www.youtube.com/embed/${youtubeID}?autoplay=1}`}
                              frameBorder='0' 
                              allow='autoplay; encrypted-media' 
                              allowFullScreen 
                              title='video' 
                              width='480'
                              height='270'
                            />
                          </div>
                        }
                      </div> :
                      <div style={{paddingBottom: 10}}>
                        <span style={styles.loadingText}>Loading Dialogue...</span>
                      </div>
                    }
                    {!loadingPlayerConversationDialogueResponses ? 
                      <div style={{padding: 5}}>
                        {playerConversationDialogueResponses.length > 0 ? 
                          <div>
                            {playerConversationDialogueResponses.map((response, index) => (
                              <div key={index} style={{padding: 5, float: 'left', width: '24%'}}>
                                <button onClick={this.handleDialogueResponseAction.bind(this, response)} style={styles.buttonDefault}>{response.name}</button>
                              </div>
                            ))}
                          </div> :
                          <div>
                            <button onClick={this.handleClosePlayerConversationModal} style={styles.buttonDefault}>
                              Close
                            </button>
                          </div>
                        }
                      </div> :
                      <div style={{paddingBottom: 10}}>
                        <span style={styles.loadingText}>Loading Responses...</span>
                      </div>
                    }
                  </div>
                </div> :
                <div>
                  {!loadingPlayerConversationDialogue ? 
                    <div>
                      <div style={{padding: 5}}><span>{playerConversationDialogue.name}</span></div>
                      <div style={{padding: 5}}><span>{playerConversationDialogue.description}</span></div>
                    </div> :
                    <div>
                      <span>Loading Dialogue...</span>
                    </div>
                  }
                  {!loadingPlayerConversationDialogueResponses ? 
                    <div style={{padding: 5}}>
                      {playerConversationDialogueResponses.length > 0 ? 
                        <div>
                          {playerConversationDialogueResponses.map((response, index) => (
                            <div key={index} style={{padding: 5, float: 'left', width: '24%'}}>
                              <button onClick={this.handleDialogueResponseAction.bind(this, response)} style={styles.buttonDefault}>{response.name}</button>
                            </div>
                          ))}
                        </div> :
                        <div>
                          <button onClick={this.handleClosePlayerConversationModal} style={styles.buttonDefault}>
                            Close
                          </button>
                        </div>
                      }
                    </div> :
                    <div style={{paddingBottom: 10}}>
                      <span style={styles.loadingText}>Loading Responses...</span>
                    </div>
                  }
                </div>
              }
            </div> :
            <div style={{paddingBottom: 10}}>
              <span style={styles.loadingText}>No Conversation Found.</span>
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  playerConversationDialogue: state.player.playerConversationDialogue,
  loadingPlayerConversationDialogue: state.player.loadingPlayerConversationDialogue,

  playerConversationDialogueResponses: state.player.playerConversationDialogueResponses,
  loadingPlayerConversationDialogueResponses: state.player.loadingPlayerConversationDialogueResponses
})

const mapDispatchToProps = {
  startPlayerConversation, getPlayerConversationDialogue, resetPlayerConversationDialogue, getPlayerConversationDialogueResponses, resetPlayerConversationDialogueResponses
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerConversationModal)
