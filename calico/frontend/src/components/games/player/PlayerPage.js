import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getPlayerPage, resetPlayerPage, getPlayerPageActions, resetPlayerPageActions } from '../../../actions/player';
import { goToPage, resetGoToPage } from '../../../actions/player';
import { playGameAction, resetGameAction } from '../../../actions/player';

export class PlayerPage extends Component {
  handleGameAction = (action) => {
    const { gameDetail, playerPage } = this.props;
    this.props.playGameAction(gameDetail, playerPage.page.slug, action.game_action_type.slug);
  }

  handleChangePage = (action) => {
    const { gameDetail, playerPage } = this.props;
    this.props.goToPage(gameDetail.slug, playerPage.page.slug, action.go_to_page.slug);
  }

  componentDidMount() {
    this.props.getPlayerPage(this.props.gameDetail.slug);
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingPlayerPage === false && lastProps.loadingPlayerPage === true) {
      const { gameDetail, playerPage } = this.props;
      this.props.getPlayerPageActions(gameDetail.slug, playerPage.page.slug);
    }
    if (this.props.loadingPlayerPageActions === false && lastProps.loadingPlayerPageActions === true) {

    }
  }

  render() {
    const { playerPage, loadingPlayerPage } = this.props;
    const { playerPageActions, loadingPlayerPageActions } = this.props;

    return (
      <div style={styles.container}>
        {!loadingPlayerPage ? 
          <div>
            {(playerPage.page.image !== '' && playerPage.page.image !== null)  || (playerPage.page.youtube_id !== '' && playerPage.page.youtube_id !== null && playerPage.page.youtube_id !== 'null') ? 
              <div>
                <div style={styles.nameContainer}>
                  <span style={styles.nameText}>{playerPage.page.name}</span>
                </div>
                <div style={{display: 'flex'}}>
                  <div style={{flex: 1}}>
                    {(playerPage.page.youtube_id !== '' && playerPage.page.youtube_id !== null && playerPage.page.youtube_id !== 'null') && 
                      <div style={styles.youtubeContainer}>
                        <iframe src={`https://www.youtube.com/embed/${playerPage.page.youtube_id}`}
                          frameBorder='0'
                          allow='autoplay; encrypted-media'
                          allowFullScreen
                          title='video' 
                        />
                      </div>
                    }
                    {(playerPage.page.image !== '' && playerPage.page.image !== null) && 
                      <div style={{padding: 5}}>
                        <img src={playerPage.page.image} style={{width: '100%', height: 'auto'}} />
                      </div>
                    }
                  </div>
                  <div style={{flex: 3}}>
                    <div style={styles.pageTextContainer}>
                      <span style={styles.pageText}>{playerPage.page.description}</span>
                    </div>
                  </div>
                </div>
              </div> :
              <div>
                <div style={{display: 'flex'}}>
                  <div style={{flex: 1}} />
                  <div style={{flex: 5}}>
                    <div style={styles.nameContainer}>
                      <span style={styles.nameText}>{playerPage.page.name}</span>
                    </div>
                    <div style={styles.pageTextContainer}>
                      <span style={styles.pageText}>{playerPage.page.description}</span>
                    </div>
                  </div>
                  <div style={{flex: 1}} />
                </div>   
              </div>
                    
            }
            {!loadingPlayerPageActions ? 
              <div style={styles.actionsContainer}>
                {playerPageActions.length > 0 ? 
                  <div style={{display: 'flex', flexWrap: 'wrap'}}>
                    {playerPageActions.map((action, index) => (
                      <div key={index} style={styles.actionContainer}>
                        {action.go_to_page !== null && 
                          <button style={styles.buttonAction} onClick={this.handleChangePage.bind(this, action)}>
                            {(action.name !== null || action.description !== null) ? 
                              <div>
                                {action.name !== null && 
                                  <div style={{padding: 5}}>
                                    <span>{action.name}</span>
                                  </div>
                                }
                                {action.description !== null &&
                                  <div style={{padding: 5}}>
                                    <span>{action.description}</span>
                                  </div>
                                }
                              </div> : 
                              <div style={{padding: 5}}>
                                {action.action_type.name}: {action.go_to_page.name}
                              </div>
                            }                            
                          </button>
                        }
                        {action.game_action_type !== null && 
                          <button style={styles.buttonAction} onClick={this.handleGameAction.bind(this, action)}>
                            {(action.name !== null || action.description !== null) ? 
                              <div>
                                {action.name !== null && 
                                  <div style={{padding: 5}}>
                                    <span>{action.name}</span>
                                  </div>
                                }
                                {action.description !== null &&
                                  <div style={{padding: 5}}>
                                    <span>{action.description}</span>
                                  </div>
                                }
                              </div> :
                              <div style={{padding: 5}}>
                                {action.game_action_type.name}
                              </div>
                            }
                          </button>
                        }
                      </div>
                    ))}
                  </div> :
                  <div>
                    <span>No Page Actions</span>
                  </div>
                }
              </div> :
              <div style={{flex: 1, textAlign: 'center', padding: 5}}>
                <span style={styles.loadingText}>Loading Actions...</span>
              </div>
            }
          </div> :
          <div style={styles.loadingTextContainer}>
            <span style={styles.loadingText}>Loading Page...</span>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  playerPage: state.player.playerPage,
  loadingPlayerPage: state.player.loadingPlayerPage,

  playerPageActions: state.player.playerPageActions,
  loadingPlayerPageActions: state.player.loadingPlayerPageActions
})

const mapDispatchToProps = {
  getPlayerPage,
  resetPlayerPage,
  getPlayerPageActions,
  resetPlayerPageActions,
  goToPage,
  resetGoToPage,
  playGameAction, 
  resetGameAction
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerPage)
