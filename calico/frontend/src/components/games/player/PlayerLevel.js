import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getPlayerLevel, resetChangeLevel, resetPlayerLevelActions, getPlayerLevelActions, changeLevel, playLevelAction, getActivePlayerLevel, getActivePlayerCutscene } from '../../../actions/player';

export class PlayerLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: false,
      uploadingData: false,
      showComplete: false,
      movementGrid: [],
      npcharacterGrid: []
    }
  }

  handleShowActionsModal = () => {
    this.props.showActionsModal();
  }

  handleAddActionsToMovementGrid = () => {
    const { movementGrid } = this.state;
    const { playerLevelActions, playerLevel } = this.props;
    if (playerLevelActions.length > 0) {
      for (var n = 0; n < playerLevelActions.length; n++) {
        if (playerLevelActions[n].level_tile !== null) {
          if (playerLevelActions[n].complete_required) {
            if (playerLevel.completed) {
              if (!playerLevelActions[n].level_tile.tile.walkable) {
                movementGrid[
                  playerLevelActions[n].level_tile.position_y !== 0 ? (playerLevelActions[n].level_tile.position_y / playerLevel.level.tile_height) : 0
                ].row[
                  playerLevelActions[n].level_tile.position_x !== 0 ? (playerLevelActions[n].level_tile.position_x / playerLevel.level.tile_width) : 0
                ] = 9;
              } else {
                movementGrid[
                  playerLevelActions[n].level_tile.position_y !== 0 ? (playerLevelActions[n].level_tile.position_y / playerLevel.level.tile_height) : 0
                ].row[
                  playerLevelActions[n].level_tile.position_x !== 0 ? (playerLevelActions[n].level_tile.position_x / playerLevel.level.tile_width) : 0
                ] = 9;
              }
            }
          } else {
            if (!playerLevelActions[n].level_tile.tile.walkable) {
              movementGrid[
                playerLevelActions[n].level_tile.position_y !== 0 ? (playerLevelActions[n].level_tile.position_y / playerLevel.level.tile_height) : 0
              ].row[
                playerLevelActions[n].level_tile.position_x !== 0 ? (playerLevelActions[n].level_tile.position_x / playerLevel.level.tile_width) : 0
              ] = 9;
            } else {
              movementGrid[
                playerLevelActions[n].level_tile.position_y !== 0 ? (playerLevelActions[n].level_tile.position_y / playerLevel.level.tile_height) : 0
              ].row[
                playerLevelActions[n].level_tile.position_x !== 0 ? (playerLevelActions[n].level_tile.position_x / playerLevel.level.tile_width) : 0
              ] = 9;
            }
          }
        }
      }
    }
    this.setState({
      movementGrid
    });
    this.props.setMovementGrid(movementGrid);
  }

  handleBuildNPCharacterGrid = () => {
    // console.log('Handle Build NPCharacter Grid: ');
    const { playerLevel, npcharacters } = this.props;
    var npcharacterGrid = [];
    var row = [];
    for (var i = 0; i < (playerLevel.level.height / playerLevel.level.tile_height); i++) {
      for (var j = 0; j < (playerLevel.level.width / playerLevel.level.tile_width); j++) {
        row.push(0);
      }
      npcharacterGrid.push({row});
      row = [];
    }
    // Add the NP Characters to the '5' list for now
    if (npcharacters.length > 0) {
      for (var m = 0; m < npcharacters.length; m++) {
        if (npcharacters[m].conversation !== null) {
          npcharacterGrid[
            npcharacters[m].position_y !== 0 ? (npcharacters[m].position_y / playerLevel.level.tile_height) : 0
          ].row[
            npcharacters[m].position_x !== 0 ? (npcharacters[m].position_x / playerLevel.level.tile_width) : 0
          ] = 9;
        } else if (npcharacters[m].hostile === true) {
          npcharacterGrid[
            npcharacters[m].position_y !== 0 ? (npcharacters[m].position_y / playerLevel.level.tile_height) : 0
          ].row[
            npcharacters[m].position_x !== 0 ? (npcharacters[m].position_x / playerLevel.level.tile_width) : 0
          ] = 5;
        }
      }
    }
    this.setState({
      npcharacterGrid
    });
    this.props.setNPCharacterGrid(npcharacterGrid);
  }

  handleBuildMovementGrid = () => {
    const { playerLevel } = this.props;
    var movementGrid = [];
    var row = [];
    for (var i = 0; i < (playerLevel.level.height / playerLevel.level.tile_height); i++) {
      for (var j = 0; j < (playerLevel.level.width / playerLevel.level.tile_width); j++) {
        row.push(0);
      }
      movementGrid.push({row});
      row = [];
    }
    // Add the Tiles to the 2 list if they are not walkable
    if (playerLevel.level.level_tiles.length > 0) {
      for (var k = 0; k < playerLevel.level.level_tiles.length; k++) {
        if (!playerLevel.level.level_tiles[k].tile.walkable) {
          movementGrid[
            playerLevel.level.level_tiles[k].position_y !== 0 ? (playerLevel.level.level_tiles[k].position_y / playerLevel.level.tile_height) : 0
          ].row[
            playerLevel.level.level_tiles[k].position_x !== 0 ? (playerLevel.level.level_tiles[k].position_x / playerLevel.level.tile_width) : 0
          ] = 2;
        }
      }
    }
    this.setState({
      movementGrid
    });
    this.props.setMovementGrid(movementGrid);
  }

  handleLevelAction = (levelAction) => {
    const { playerLevel } = this.props;
    this.props.resetPlayerLevelActions();
    this.props.playLevelAction(playerLevel, levelAction);
  }

  handleCompleteLevel = () => {
    this.setState({
      showComplete: true
    });
  }

  handleChangeLevel = (levelAction) => {
    const { gameDetail } = this.props;
    this.setState({uploadingData: true});
    this.props.resetPlayerLevelActions();
    this.props.changeLevel(gameDetail.slug, levelAction);
  }

  componentDidMount() {
    const { gameDetail } = this.props;
    this.props.getActivePlayerLevel(gameDetail.slug);
  }

  componentDidUpdate(lastProps) {
    if (this.props.loadingPlayerLevel === false && lastProps.loadingPlayerLevel === true) {
      const { playerLevel } = this.props;
      if (playerLevel.active === true) {
        this.props.getPlayerLevelActions(this.props.playerLevel);
        this.setState({
          uploadingData: false,
          completed: this.props.playerLevel.completed
        });
        this.handleBuildMovementGrid();
        this.handleBuildNPCharacterGrid();
      }
    }
    if (this.props.loadingPlayerLevelActions === false && lastProps.loadingPlayerLevelActions === true) {
      this.handleAddActionsToMovementGrid();
    }
    if (this.props.npcharacters !== lastProps.npcharacters) {
      // console.log('Updating NPCharacters');
      this.handleBuildNPCharacterGrid();
      // if (!this.props.loadingPlayerLevel && !this.props.loadingPlayerLevelActions) {
      //   this.handleBuildNPCharacterGrid();
      // }
    }
  }

  componentWillUnmount() {
    this.props.resetChangeLevel();
  }
  
  render() {
    const { uploadingData } = this.state;
    const { playerLevel, loadingPlayerLevel, levelDisplay } = this.props;

    return (
      <div>
        {uploadingData ? 
          <div>
            <div id="uploadProgress" />
          </div> :
          <div>
            {!loadingPlayerLevel ? 
              <div>
                {playerLevel.active && levelDisplay && 
                  <div style={{textAlign: 'center'}}>
                    <span style={{paddingLeft: 20}}><button style={styles.buttonDefault} onClick={this.handleShowActionsModal}>Level</button></span>
                  </div>
                }
              </div> : 
              <div style={styles.loadingTextContainer}>
                <span style={styles.loadingText}>Loading...</span>
              </div>
            }
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  playerGame: state.player.playerGame,

  playerLevel: state.player.playerLevel,
  loadingPlayerLevel: state.player.loadingPlayerLevel,

  playerLevelActions: state.player.playerLevelActions,
  loadingPlayerLevelActions: state.player.loadingPlayerLevelActions,

  playerCharacter: state.player.playerCharacter,
  loadingPlayerCharacter: state.player.loadingPlayerCharacter
})

const mapDispatchToProps = {
  changeLevel,
  getPlayerLevel,
  resetPlayerLevelActions,
  getPlayerLevelActions,
  resetChangeLevel,
  playLevelAction,
  getActivePlayerLevel,
  getActivePlayerCutscene
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerLevel)
