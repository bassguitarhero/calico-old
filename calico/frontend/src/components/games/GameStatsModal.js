import React, { Component } from 'react'
import { connect } from 'react-redux'
import { styles } from '../layout/Styles';

export class GameStatsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wins: 0,
      losses: 0
    }
  }

  handleCloseGameStatsModal = () => {
    this.props.closeGameStatsModal();
  }

  render() {
    function setTimestamp(s) {
      let fields = s.split('T');
      let date = fields[0].split('-');
      let timestamp = fields[1].split('.')[0];
      let time = timestamp.split(':');
      let period = '';
      if (time[0] > 12) {
        time[0] = time[0] - 12;
        period = "pm";
      } else if (time[0] == 12) {
        period = "pm";
      } else if (time[0] == 0) {
        period = "pm";
        time[0] = 12;
      } else {
        period = "am";
      }
      return date[1] + '/' + date[2] + '/' + date[0] + ', ' + time[0] + ':' + time[1] + ' ' + period;
    }

    const { gamePages, loadingGamePages } = this.props;
    const { gameDetail, gameCharacters, loadingGameCharacters } = this.props;
    const { gameNPCharacters, loadingGameNPCharacters } = this.props;
    const { gameLevels, loadingGameLevels, gameBackgrounds, loadingGameBackgrounds } = this.props;
    const { gameConversations, loadingGameConversations } = this.props;
    const { gameCutscenes, loadingGameCutscenes } = this.props;
    const { gameWeapons, loadingGameWeapons, gameItems, loadingGameItems } = this.props;
    const { playerGames, loadingPlayerGames } = this.props;
    const { winCount, lossCount } = this.props;

    return (
      <div style={styles.modal}>
        <div style={{padding: 20}}>
          <div style={{display: 'flex'}}>
            <div style={{flex: 9, padding: 10}}>
              <span style={styles.nameText}>{gameDetail.name}</span>
            </div>
            <div style={{flex: 1, textAlign: 'right', padding: 10}}>
              <button style={styles.buttonDefault} onClick={this.handleCloseGameStatsModal}>Close</button>
            </div>
          </div>

          {/* Choose Your Own Adventure */}
          {gameDetail.game_type.slug === 'choose-your-own-adventure' && 
            <div>
              <div style={{padding: 10}}>
                {!loadingGamePages && 
                    <div>
                      {gamePages.length > 0 ? 
                        <div>
                          <span style={styles.gameTypeText}>{gamePages.length + 1} Pages</span>
                        </div> :
                        <div>
                          <span style={styles.gameTypeText}>No Pages in Game</span>
                        </div>
                      }
                    </div>
                  }
              </div>
            </div>
          }

          {/* Dungeon Crawler */}
          {gameDetail.game_type.slug === 'dungeon-crawler' && 
            <div>
              <div style={{display: 'flex'}}>
                {!loadingGameCharacters ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameCharacters.length > 0 ? 
                      <div>
                        {gameCharacters.map((character, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span style={styles.gameTypeText}>Character: {character.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Playable Characters</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
                {!loadingGameNPCharacters ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameNPCharacters.length > 0 ? 
                      <div>
                        {gameNPCharacters.map((npCharacter, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span style={styles.gameTypeText}>NPCharacter: {npCharacter.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Non-Playable Characters</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
              </div>
              <div style={{display: 'flex'}}>
                {!loadingGameLevels ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameLevels.length > 0 ? 
                      <div>
                        {gameLevels.map((level, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span style={styles.gameTypeText}>Level: {level.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Levels</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
                {!loadingGameCutscenes ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameCutscenes.length > 0 ? 
                      <div>
                        {gameCutscenes.map((cutscene, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span style={styles.gameTypeText}>Cutscene: {cutscene.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Cutscenes</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
              </div>
              <div style={{display: 'flex'}}>
              {!loadingGameConversations ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameConversations.length > 0 ? 
                      <div>
                        {gameConversations.map((conversation, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span style={styles.gameTypeText}>Conversation: {conversation.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Conversations</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
              </div>
              <div style={{display: 'flex'}}>
                {!loadingGameWeapons ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameWeapons.length > 0 ? 
                      <div>
                        {gameWeapons.map((weapon, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span style={styles.gameTypeText}>Weapon: {weapon.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Weapons</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
                {!loadingGameItems ? 
                  <div style={{padding: 10, flex: 1}}>
                    {gameItems.length > 0 ? 
                      <div>
                        {gameItems.map((item, index) => (
                          <div style={styles.gameTypeContainer} key={index}>
                            <span>Item: {item.name}</span>
                          </div>
                        ))}
                      </div> :
                      <div style={styles.gameTypeContainer}>
                        <span style={styles.gameTypeText}>No Game Items</span>
                      </div>
                    }
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingText}>Loading...</span>
                  </div>
                }
              </div>
            </div>
          }

          {/* Player Stats */}
          {!loadingPlayerGames ? 
            <div>
              {playerGames.length > 0 ? 
                <div>
                  <div style={{display: 'flex'}}>
                    <div style={{padding: 10, flex: 1}}>
                      <span style={styles.gameTypeText}>Wins: {winCount}</span>
                    </div>
                    <div style={{padding: 10, flex: 1}}>
                      <span style={styles.gameTypeText}>Losses: {lossCount}</span>
                    </div>
                  </div>
                  <div style={{padding: 10}}>
                    {playerGames[0].completed ? 
                      <span style={styles.gameTypeText}>Last Game Finished: {setTimestamp(playerGames[0].last_edited)}</span> : 
                      <span style={styles.gameTypeText}>Current Game Last Played: {setTimestamp(playerGames[0].last_edited)}</span>
                    }
                  </div>
                </div> :
                <div>
                  <span>No Games Played...</span>
                </div>
              }
            </div> :
            <div>
              <span>Loading Player Stats...</span>
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,
  loadingGameDetail: state.games.loadingGameDetail,

  gameCharacters: state.games.gameCharacters,
  loadingGameCharacters: state.games.loadingGameCharacters,

  gameNPCharacters: state.games.gameNPCharacters,
  loadingGameNPCharacters: state.games.loadingGameNPCharacters,

  gameLevels: state.games.gameLevels,
  loadingGameLevels: state.games.loadingGameLevels,

  gameCutscenes: state.games.gameCutscenes,
  loadingGameCutscenes: state.games.loadingGameCutscenes,

  gameConversations: state.games.gameConversations,
  loadingGameConversations: state.games.loadingGameConversations,
  
  gameBackgrounds: state.games.gameBackgrounds,
  loadingGameBackgrounds: state.games.loadingGameBackgrounds,

  gameWeapons: state.games.gameWeapons,
  loadingGameWeapons: state.games.loadingGameWeapons,

  gameItems: state.games.gameItems,
  loadingGameItems: state.games.loadingGameItems,

  gamePages: state.games.gamePages,
  loadingGamePages: state.games.loadingGamePages,

  playerGames: state.player.playerGames,
  loadingPlayerGames: state.player.loadingPlayerGames,
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(GameStatsModal)
