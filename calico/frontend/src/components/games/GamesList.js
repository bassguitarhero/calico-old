import React, { Component } from 'react'
import { connect } from 'react-redux';

import { getGames } from '../../actions/games';

import GameInline from './GameInline';

class GamesList extends Component {
  componentDidMount() {
    this.props.getGames();
  }

  handleGetGame = (game) => {
    this.props.showGameDetail(game.slug);
  }

  render() {
    const { games, loadingGames } = this.props;

    return (
      <div style={{flex: 1}}>
        {!loadingGames ? 
          <div>
            {games.length > 0 ? 
              <div>
                {games.map((game, index) => (
                  <GameInline key={index} handleGetGame={this.handleGetGame} game={game} />
                ))}
              </div> :
              <div>
                No Games!
              </div>
            }
          </div> :
          <div>
            Loading Games...
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    games: state.games.games,
    loadingGames: state.games.loadingGames
  }
}

const mapDispatchToProps = {
  getGames
}

export default connect(mapStateToProps, mapDispatchToProps)(GamesList);
