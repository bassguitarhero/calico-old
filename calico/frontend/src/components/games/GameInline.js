import React, { Component } from 'react';
import { styles } from '../layout/Styles';

class GameInline extends Component {
  handleGetGame = (game) => {
    this.props.handleGetGame(game);
  }

  componentDidMount() {

  }

  render() {
    function setTimestamp(s) {
      let fields = s.split('T');
      let date = fields[0].split('-');
      let timestamp = fields[1].split('.')[0];
      let time = timestamp.split(':');
      let period = '';
      if (time[0] > 12) {
        time[0] = time[0] - 12;
        period = "pm";
      } else if (time[0] == 12) {
        period = "pm";
      } else if (time[0] == 0) {
        period = "pm";
        time[0] = 12;
      } else {
        period = "am";
      }
      return date[1] + '/' + date[2] + '/' + date[0] + ', ' + time[0] + ':' + time[1] + ' ' + period;
    }

    const { game } = this.props;

    return (
      <div>
        <div style={{flex: 1, display: 'flex'}}>
          <div style={{flex: 1}}>
            <img src={game.image} style={{height: 150, width: 150}} />
          </div>
          <div style={{flex: 4}}> 
            <div style={styles.titleContainer}><span style={styles.titleText}>{game.name}</span></div>
            <div style={styles.descriptionContainer}><span style={styles.descriptionText}>{game.description}</span></div>
            {game.game_type !== null && 
              <div style={styles.gameTypeContainer}><span style={styles.gameTypeText}>Type: {game.game_type.name}</span></div>
            }
            <div style={styles.lastEditedContainer}>
              <span style={styles.usernameText}>by {game.created_by.username}, </span>
              <span style={styles.lastEditedText}>{setTimestamp(game.last_edited)}</span>
            </div>
            <div style={styles.viewGameContainer}>
              <button style={styles.button, styles.buttonDefault} onClick={this.handleGetGame.bind(this, game)}>
                <span>View Game</span>            
              </button>
            </div>
          </div>
        </div>
        <hr style={styles.gamesListHR} />
      </div>
    )
  }
}

export default GameInline;
