import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../layout/Styles';

import GameDetail from './GameDetail';
import GamesList from './GamesList';

import NewGame from './new/NewGame';

class Games extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slug: '',
      showGamesList: true,
      showGameDetail: false,
      showNewGame: false
    }
  }

  handleCloseNewGame = () => {
    this.setState({
      slug: '',
      showGamesList: true,
      showGameDetail: false,
      showNewGame: false
    })
  }

  handleShowNewGame = () => {
    this.setState({
      slug: '',
      showGamesList: false,
      showGameDetail: false,
      showNewGame: true
    })
  }

  handleShowGamesList = () => {
    this.setState({
      slug: '',
      showGamesList: true,
      showGameDetail: false,
      showNewGame: false
    })
  }

  handleShowGameDetail = (slug) => {
    this.setState({
      slug: slug,
      showGamesList: false,
      showGameDetail: true,
      showNewGame: false
    })
  }

  componentDidMount() {

  }

  render() {
    const { showGamesList, showGameDetail, showNewGame } = this.state;
    const { slug } = this.state;
    const { isAuthenticated, user } = this.props

    return (
      <div style={{flex: 1, padding: 5}}>
        {showNewGame && 
          <NewGame closeNewGame={this.handleCloseNewGame} />
        }
        {showGamesList && 
          <GamesList showGameDetail={this.handleShowGameDetail} />
        }
        {showGameDetail && 
          <GameDetail slug={slug} showGamesList={this.handleShowGamesList} />
        }
        {(isAuthenticated && showGamesList && user.profile) && 
          <div>
            {user.profile.create_content && 
              <div style={styles.newGameButtonPlaceholder}>
                <div style={styles.newGameButton}>
                  <button style={styles.button, styles.buttonNew} onClick={this.handleShowNewGame}>New</button>
                </div>
              </div>
            }
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user
});

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Games);