import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditItem from './EditItem';
import InlineItem from './InlineItem';

import NewItem from '../new/NewItem';

export class EditItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showItems: true,
      showEditItem: false,
      showNewItem: false,
      editItem: null
    }
  }

  handleCloseNewItem = () => {
    this.setState({
      showItems: true,
      showEditItem: false,
      showNewItem: false,
      editItem: null
    })
  }

  handleShowNewItem = () => {
    this.setState({
      showItems: false,
      showEditItem: false,
      showNewItem: true,
      editItem: null
    })
  }

  handleShowItems = () => {
    this.setState({
      showItems: true,
      showEditItem: false,
      showNewItem: false,
      editItem: null
    })
  }

  handleShowEditItem = (item) => {
    this.setState({
      showItems: false,
      showEditItem: true,
      showNewItem: false,
      editItem: item
    })
  }
  
  render() {
    const { gameItems, loadingGameItems } = this.props;
    const { showItems, showEditItem, showNewItem, editItem } = this.state;
    
    return (
      <div>
        <div style={{paddingBottom: 24}}>
          <button onClick={this.handleShowNewItem}>New</button>
        </div>
        {showItems && 
          <div>
            {!loadingGameItems ? 
              <div>
                {gameItems.length > 0 ? 
                  <div>
                    {gameItems.map((item, index) => (
                      <button key={index} style={styles.button} onClick={this.handleShowEditItem.bind(this, item)}>
                        <InlineItem item={item} />
                      </button>
                    ))}
                  </div> :
                  <div>
                    <span>No Game Items</span>
                  </div>
                }
              </div> :
              <div>
                <span>Loading...</span>
              </div>
            }
          </div>
        }
        {showEditItem && 
          <EditItem showItems={this.handleShowItems} item={editItem} />
        }
        {showNewItem && 
          <NewItem closeNewItem={this.handleCloseNewItem} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameItems: state.games.gameItems,
  loadingGameItems: state.games.loadingGameItems
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItems)

const styles = {
  button: {
    fontSize: '100%',
    fontFamily: 'inherit',
    border: 0,
    padding: 0
  }
}