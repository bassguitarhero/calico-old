import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getPageActionTypes, resetPageActionTypes, getPageActions, resetPageActions } from '../../../actions/game_admin';
import { newPageAction, resetNewPageAction, editPageAction, resetEditPageAction, deletePageAction, resetDeletePageAction } from '../../../actions/game_admin';
import { getGameActionTypes, resetGameActionTypes } from '../../../actions/game_admin';

import EditPageAction from './EditPageAction';
import InlinePageAction from './InlinePageAction';

export class EditPageActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actionTypeSlug: '',
      goToPageSlug: '',
      availablePages: [],
      name: '',
      description: '',
      uploadingData: false,
      gameActionTypeSlug: '',
      showPageActions: true,
      showEditPageAction: false,
      editActionObj: ''
    }
  }

  handleShowPageActions = () => {
    this.setState({
      showPageActions: true,
      showEditPageAction: false,
      editActionObj: ''
    });
  }

  handleShowEditPageAction = (actionObj) => {
    this.setState({
      showPageActions: false,
      showEditPageAction: true,
      editActionObj: actionObj
    });
  }

  handleDelete = () => {
    if (window.confirm("Delete Page Action?")) {
      this.props.deletePageAction(this.props.gameDetail.slug, this.props.pageAction.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditPageAction = () => {
    const { gameDetail } = this.props;
    const { name, description, actionTypeSlug, goToPageSlug, gameActionTypeSlug } = this.state;
    const pageAction = { name, description, actionTypeSlug, goToPageSlug, gameActionTypeSlug };
		if (name !== '') {
			if (window.confirm("Edit Page Action?")) {
				this.props.editPageAction(gameDetail.slug, pageAction);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Page Actions must have a Name.");
		}
  }

  handleCreatePageAction = () => {
    const { gameDetail, page } = this.props;
    const { name, description, actionTypeSlug, goToPageSlug, gameActionTypeSlug } = this.state;
    const pageAction = { name, description, page, actionTypeSlug, goToPageSlug, gameActionTypeSlug };
		if (name !== '') {
			if (window.confirm("Create New Page Action?")) {
				this.props.newPageAction(gameDetail.slug, pageAction);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Page Actions must have a Name.");
		}
  }

  handleSelectGoToPage = (e) => {
    this.setState({
      goToPageSlug: e.target.value
    });
  }

  handleSelectGameActionType = (e) => {
    this.setState({
      gameActionTypeSlug: e.target.value
    });
  }

  handleSelectPageActionType = (e) => {
    this.setState({
      actionTypeSlug: e.target.value
    });
  }

  componentDidMount() {
    const { gameDetail, gamePages, page } = this.props;
    this.props.getPageActionTypes();
    this.props.getPageActions(gameDetail.slug, page.slug);
    this.props.getGameActionTypes();
    var availablePages = [];
    for (var i = 0; i < gamePages.length; i++) {
      if (gamePages[i].slug !== page.slug) {
        availablePages.push(gamePages[i]);
      }
    }
    this.setState({
      availablePages
    });
  }

  componentDidUpdate(lastProps, lastState) {
    if (this.props.loadingPageActions === false && lastProps.loadingPageActions === true) {

    }
    if (this.props.newPageActionObj !== null && lastProps.newPageActionObj === null) {
      var availablePages = [];
      this.props.resetPageActions();
      this.props.getPageActions(this.props.gameDetail.slug, this.props.page.slug);
      this.setState({
        uploadingData: false,
        actionTypeSlug: '',
        goToPageSlug: '',
        availablePages: [],
        name: '',
        description: '',
        gameActionTypeSlug: '',
      });
    }
    if (this.state.showEditPageAction === false && lastState.showEditPageAction === true) {
      const { gamePages, page } = this.props;
      var availablePages = [];
      for (var i = 0; i < gamePages.length; i++) {
        if (gamePages[i].slug !== page.slug) {
          availablePages.push(gamePages[i]);
        }
      }
      this.setState({
        uploadingData: false,
        actionTypeSlug: '',
        goToPageSlug: '',
        availablePages: availablePages,
        name: '',
        description: '',
        gameActionTypeSlug: '',
      });
    }
  }

  render() {
    const { page, loadingPageActions, pageActionTypes, pageActions } = this.props;
    const { availablePages, actionTypeSlug, goToPageSlug } = this.state;
    const { name, description, uploadingData, gameActionTypeSlug } = this.state;
    const { gameActionTypes, loadingGameActionTypes } = this.props;
    const { showPageActions, showEditPageAction } = this.state;
    const { editActionObj } = this.state;

    return (
      <div>
        {showPageActions && 
          <div>
            <div>
              <span style={styles.titleText}>Page Actions:</span>
            </div>
            {!loadingPageActions ? 
              <div>
                {pageActions.length > 0 ? 
                  <div>
                    {pageActions.map((actionObj, index) => (
                      <InlinePageAction showEditAction={this.handleShowEditPageAction} key={index} actionObj={actionObj} />
                    ))}
                  </div> :
                  <div style={styles.defaultTextContainer}>
                    <span style={styles.defaultText}>No Page Actions</span>
                  </div>
                }
              </div> :
              <div style={styles.defaultTextContainer}>
                <span style={styles.defaultText}>Loading Page Actions...</span>
              </div>
            }
            {pageActionTypes !== null ? 
              <div>
                <div style={{paddingTop: 10, paddingBottom: 15}}>
                  <span style={styles.titleText}>New Action:</span>
                </div>
                <select 
                  name="pageActionTypes" 
                  id="pageActionTypes"
                  onChange={this.handleSelectPageActionType.bind(this)}
                  style={styles.selectInput}
                  value={actionTypeSlug}
                >
                  <option value="">...</option>
                  {pageActionTypes.map((actionType, index) => (
                    <option key={index} value={actionType.slug}>{actionType.name}</option>
                  ))}
                </select>
                {actionTypeSlug === "go-to-page" && 
                  <div>
                    <select 
                      name="availablePages" 
                      id="availablePages"
                      onChange={this.handleSelectGoToPage.bind(this)}
                      style={styles.selectInput}
                      value={goToPageSlug}
                    >
                      <option value="">Select Page:</option>
                      {availablePages.map((availablePage, index) => (
                        <option value={availablePage.slug}>{availablePage.name}</option>
                      ))}
                    </select>
                    <div style={{display: 'flex', paddingTop: 15, paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Name: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="name"
                          value={name}
                          placeholder="Name"
                          onChange={this.handleChange}
                          style={styles.textInput}
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Description:</span>
                      </div>
                      <div style={{flex: 4}}>
                        <textarea 
                          type="textArea" 
                          name="description" 
                          onChange={this.handleChange} 
                          value={description} 
                          placeholder="Describe your Page Action"
                          style={styles.textareaInput}
                        />
                      </div>
                    </div>
                    <div>
                      {uploadingData ? 
                        <div style={defaultTextContainer}>
                          <div style={styles.defaultText} id="uploadProgress" />
                        </div> :
                        <div style={{textAlign: 'center'}}>
                          <button style={styles.buttonDefault} onClick={this.handleCreatePageAction}>
                            New Page Action
                          </button>
                        </div>
                      }
                    </div>
                  </div>
                }
                {actionTypeSlug === 'game-action' && 
                  <div>
                    {!loadingGameActionTypes ? 
                      <div>
                        <div style={{paddingBottom: 15, paddingTop: 10}}>
                          <span style={styles.titleText}>Game Actions:</span>
                        </div>
                        {gameActionTypes.length > 0 ? 
                          <div>
                            <select 
                              name="gameActionTypes" 
                              id="gameActionTypes"
                              className="gameActionTypesSelect"
                              onChange={this.handleSelectGameActionType.bind(this)}
                              style={styles.selectInput}
                              value={gameActionTypeSlug}
                            >
                              <option value="">...</option>
                              {gameActionTypes.map((gameActionType, index) => (
                                <option key={index} value={gameActionType.slug}>{gameActionType.name}</option>
                              ))}
                            </select>
                            <div style={{display: 'flex', paddingTop: 15, paddingBottom: 10}}>
                              <div style={{flex: 1}}>
                                <span style={styles.defaultText}>Name: </span>
                              </div>
                              <div style={{flex: 4}}>
                                <input 
                                  name="name"
                                  value={name}
                                  placeholder="Name"
                                  onChange={this.handleChange}
                                  style={styles.textInput}
                                />
                              </div>
                            </div>
                            <div style={{display: 'flex', paddingBottom: 10}}>
                              <div style={{flex: 1}}>
                                <span style={styles.defaultText}>Description:</span>
                              </div>
                              <div style={{flex: 4}}>
                                <textarea 
                                  style={styles.textareaInput}
                                  type="textArea" 
                                  name="description" 
                                  onChange={this.handleChange} 
                                  value={description} 
                                  placeholder="Describe your Page Action"
                                />
                              </div>
                            </div>
                            <div>
                              {uploadingData ? 
                                <div style={styles.defaultTextContainer}>
                                  <div style={styles.defaultText} id="uploadProgress" />
                                </div> : 
                                <div style={{textAlign: 'center'}}>
                                  <button style={styles.buttonDefault} onClick={this.handleCreatePageAction}>
                                    New Page Action
                                  </button>
                                </div>
                              }
                            </div>  
                          </div> :
                          <div style={styles.defaultTextContainer}>
                            <span style={styles.defaultText}>No Game Action Types</span>
                          </div>
                        }
                      </div> :
                      <div style={styles.defaultTextContainer}>
                        <span style={styles.defaultText}>Loading Game Actions...</span>
                      </div>
                    }
                  </div>
                }
              </div> :
              <div style={styles.defaultTextContainer}>
                <span styke={styles.defaultText}>Loading Action Types...</span>
              </div>
            }
          </div>
        }
        {showEditPageAction && 
          <EditPageAction page={page} editActionObj={editActionObj} closeEditPageAction={this.handleShowPageActions} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,
  gamePages: state.games.gamePages,

  pageActionTypes: state.game_admin.pageActionTypes,

  pageActions: state.game_admin.pageActions,
  loadingPageActions: state.game_admin.loadingPageActions,

  newPageActionObj: state.game_admin.newPageActionObj,

  gameActionTypes: state.game_admin.gameActionTypes,
  loadingGameActionTypes: state.game_admin.loadingGameActionTypes
})

const mapDispatchToProps = {
  getPageActionTypes, 
  resetPageActionTypes, 
  getPageActions, 
  resetPageActions,
  newPageAction, 
  resetNewPageAction, 
  editPageAction, 
  resetEditPageAction, 
  deletePageAction, 
  resetDeletePageAction,
  getGameActionTypes, 
  resetGameActionTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPageActions)
