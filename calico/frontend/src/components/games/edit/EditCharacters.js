import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditCharacter from './EditCharacter';
import EditNPCharacter from './EditNPCharacter';
import InlineCharacter from './InlineCharacter';
import InlineNPCharacter from './InlineNPCharacter';

import NewCharacter from '../new/NewCharacter';
import NewNPCharacter from '../new/NewNPCharacter';

import { styles } from '../../layout/Styles';

export class EditCharacters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPlayableCharacters: true,
      showNonPlayableCharacters: false,
      showCharacters: true,
      showEditCharacter: false,
      showNewCharacter: false,
      showNPCharacters: true,
      showEditNPCharacter: false,
      showNewNPCharacter: false,
      editCharacter: null,
      editNPCharacter: null
    }
  }

  handleShowCharacters = () => {
    this.setState({
      showCharacters: true,
      showEditCharacter: false,
      showNewCharacter: false,
      editCharacter: null
    });
  }

  handleShowEditCharacter = (character) => {
    this.setState({
      showCharacters: false,
      showEditCharacter: true,
      showNewCharacter: false,
      editCharacter: character
    });
  }

  handleShowNewCharacter = () => {
    this.setState({
      showCharacters: false,
      showEditCharacter: false,
      showNewCharacter: true,
      editCharacter: null
    });
  }

  handleCloseNewCharacter = () => {
    this.setState({
      showCharacters: true,
      showEditCharacter: false,
      showNewCharacter: false,
      editCharacter: null
    });
  }

  handleShowNPCharacters = () => {
    this.setState({
      showNPCharacters: true,
      showEditNPCharacter: false,
      showNewNPCharacter: false,
      editNPCharacter: null
    });
  }

  handleShowEditNPCharacter = (npCharacter) => {
    this.setState({
      showNPCharacters: false,
      showEditNPCharacter: true,
      showNewNPCharacter: false,
      editNPCharacter: npCharacter
    });
  }

  handleShowNewNPCharacter = () => {
    this.setState({
      showNPCharacters: false,
      showEditNPCharacter: false,
      showNewNPCharacter: true,
      editNPCharacter: null
    });
  }

  handleCloseNewNPCharacter = () => {
    this.setState({
      showNPCharacters: true,
      showEditNPCharacter: false,
      showNewNPCharacter: false,
      editNPCharacter: null
    });
  }

  handleShowCharactersSection = () => {
    this.setState({
      showPlayableCharacters: true,
      showNonPlayableCharacters: false
    })
  }

  handleShowNPCharactersSection = () => {
    this.setState({
      showPlayableCharacters: false,
      showNonPlayableCharacters: true
    })
  }

  render() {
    const { gameCharacters, loadingGameCharacters } = this.props;
    const { gameNPCharacters, loadingGameNPCharacters } = this.props;
    const { showPlayableCharacters, showNonPlayableCharacters } = this.state;
    const { showCharacters, showEditCharacter, showNPCharacters, showEditNPCharacter } = this.state;
    const { editCharacter, editNPCharacter } = this.state;
    const { showNewCharacter, showNewNPCharacter } = this.state;

    return (
      <div>
        <div style={{display: 'flex', paddingBottom:  24}}>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showPlayableCharacters ? 
              <div style={{padding: 10}}><span style={styles.nameText}>Playable Characters</span></div> :
              <button style={styles.buttonAction} onClick={this.handleShowCharactersSection}>Playable Characters</button>
            }
          </div>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showNonPlayableCharacters ? 
              <div style={{padding: 10}}><span style={styles.nameText}>Non-Playable Characters</span></div> :
              <button style={styles.buttonAction} onClick={this.handleShowNPCharactersSection}>Non-Playable Characters</button>
            }
          </div>
        </div>
        {showPlayableCharacters && 
          <div>
            {!showNewCharacter && !showEditCharacter && 
              <div style={{paddingBottom: 24}}>
                <button style={styles.buttonDefault} onClick={this.handleShowNewCharacter}>New</button>
              </div>
            }
            {showCharacters && 
              <div>
                {!loadingGameCharacters ? 
                  <div>
                    {gameCharacters.length > 0 ? 
                      <div> 
                        {gameCharacters.map((character, index) => (
                          <button key={index} style={styles.buttonDefault } onClick={this.handleShowEditCharacter.bind(this, character)}>
                            <InlineCharacter character={character} />
                          </button>
                        ))}
                      </div> :
                      <div>
                        <span>No Playable Characters</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span>Loading...</span>
                  </div>
                }
              </div>
            }
            {showEditCharacter && 
              <EditCharacter showCharacters={this.handleShowCharacters} character={editCharacter} />
            }
            {showNewCharacter && 
              <NewCharacter closeNewCharacter={this.handleCloseNewCharacter} />
            }
          </div>
        }
        {showNonPlayableCharacters && 
          <div>
            {!showNewNPCharacter && !showEditNPCharacter && 
              <div style={{paddingBottom: 24}}>
                <button style={styles.buttonDefault} onClick={this.handleShowNewNPCharacter}>New</button>
              </div>
            }
            {showNPCharacters && 
              <div>
                {!loadingGameNPCharacters ? 
                  <div>
                    {gameNPCharacters.length > 0 ? 
                      <div> 
                        {gameNPCharacters.map((npCharacter, index) => (
                          <button key={index} style={styles.buttonDefault} onClick={this.handleShowEditNPCharacter.bind(this, npCharacter)}>
                            <InlineNPCharacter npCharacter={npCharacter} />
                          </button>
                        ))}
                      </div> :
                      <div>
                        <span>No Non-Playable Characters</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span>Loading...</span>
                  </div>
                }
              </div>
            }
            {showEditNPCharacter && 
              <EditNPCharacter showNPCharacters={this.handleShowNPCharacters} npCharacter={editNPCharacter} />
            }
            {showNewNPCharacter && 
              <NewNPCharacter closeNewNPCharacter={this.handleCloseNewNPCharacter} />
            }
          </div>
        }
      </div>
      
    )
  }
}

const mapStateToProps = (state) => ({
  gameCharacters: state.games.gameCharacters,
  loadingGameCharacters: state.games.loadingGameCharacters,

  gameNPCharacters: state.games.gameNPCharacters,
  loadingGameNPCharacters: state.games.loadingGameNPCharacters
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCharacters);