import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editLevelAction, resetEditLevelAction, deleteLevelAction, resetDeleteLevelAction, getLevelActions, resetLevelActions, deleteLevelTile, resetDeleteLevelTile } from '../../../actions/game_admin';
import { getGameActionTypes, resetGameActionTypes, getCutsceneActionTypes, resetCutsceneActionTypes } from '../../../actions/game_admin';
import { getLevelTiles, clearLevelTiles } from '../../../actions/games';

export class EditLevelAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positionX: 0,
      positionY: 0,
      tileSlug: '',
      id: '',
      uploadingData: false,
      display: true,
      levelChangeSlug: '',
      levelActionTypeSlug: '',
      gameActionTypeSlug: '',
      cutsceneSlug: '',
      levelOptions: [],
      requireComplete: false,
      defeatAllEnemies: false,
      defeatBoss: false
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		})
  }

  handleDelete = () => {
    if (window.confirm("Delete Level Action?")) {
      this.props.deleteLevelAction(this.props.gameDetail.slug, this.props.level.slug, this.props.levelAction.action_type.slug, this.props.levelAction.id);
    }
  }

  handleSelectTile = (tileSlug) => {
    this.setState({
      tileSlug: tileSlug
    });
  }

  handleSelectGameLevel = (e) => {
    this.setState({
      levelChangeSlug: e.target.value,
      cutsceneSlug: '',
      gameActionTypeSlug: ''
    });
  }

  handleSelectGameCutscene = (e) => {
    this.setState({
      levelChangeSlug: '',
      cutsceneSlug: e.target.value,
      gameActionTypeSlug: ''
    });
  }

  handleSelectGameActionType = (e) => {
    this.setState({
      levelChangeSlug: '',
      cutsceneSlug: '',
      gameActionTypeSlug: e.target.value
    });
  }

  handleEditLevelAction = () => {
    const { gameDetail, level } = this.props;
    const { positionX, positionY, tileSlug, id, display, requireComplete } = this.state;
    const { levelChangeSlug, levelActionTypeSlug, gameActionTypeSlug, cutsceneSlug, defeatAllEnemies, defeatBoss } = this.state;
    const levelTileData = { tileSlug, positionX, positionY, display };
    const levelActionData = { requireComplete, levelChangeSlug, levelActionTypeSlug, gameActionTypeSlug, cutsceneSlug, defeatAllEnemies, defeatBoss };
    if (levelActionTypeSlug === 'complete-level') {
      if (window.confirm("Edit Level Action?")) {
        this.props.editLevelAction(gameDetail.slug, level.slug, levelTileData, levelActionData, id);
        this.setState({uploadingData: true});
      }
    } else {
      if (tileSlug !== '') {
        if (levelActionTypeSlug !== '') {
          if (levelActionTypeSlug === 'change-level' && levelChangeSlug === '') {
            alert('Level Changes must have a new Level');
          } else {
            if (window.confirm("Edit Level Action?")) {
              this.props.editLevelAction(gameDetail.slug, level.slug, levelTileData, levelActionData, id);
              this.setState({uploadingData: true});
            }
          }
        } else {
          alert('Level Actions must have an Action.');
        }
      } else {
        if (levelActionTypeSlug === 'game-action') {
          if (window.confirm("Edit Level Action?")) {
            this.props.editLevelAction(gameDetail.slug, level.slug, levelTileData, levelActionData, id);
            this.setState({uploadingData: true});
          }
        } else {
          alert("Level Actions must have a Tile.");
        }
      }
    } 
  }

  handleSelectChange = (e) => {
    this.setState({
      tileSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleSelectLevelActionType = (e) => {
    this.setState({
      levelActionTypeSlug: e.target.value
    })
  }

  handleGetGameLevels = () => {
    const { level, gameLevels } = this.props;
    var levelOptions = [];
    var match = 0;
    for (var i = 0; i < gameLevels.length; i++) {
      if (gameLevels[i].slug !== level.slug) {
        levelOptions.push(gameLevels[i]);
        // for (var j = 0; j < levelActions.length; j++) {
        //   if (levelActions[j].change_level !== null) {
        //     if (gameLevels[i].slug === levelActions[j].change_level.slug) {
        //       match += 1;
        //     }
        //   }
        // }
        // if (match === 0) {
        //   levelOptions.push(gameLevels[i]);
        // }
        // match = 0;
      }
    }
    this.setState({levelOptions});
  }

  componentDidMount() {
    const { levelAction } = this.props;
    this.setState({
      cutsceneSlug: levelAction.action_type === 'change-cutscene' ? levelAction.change_cutscene.slug : '',
      levelActionTypeSlug: levelAction.action_type.slug,
      requireComplete: levelAction.complete_required,
      levelChangeSlug: levelAction.change_level !== null ? levelAction.change_level.slug : '',
      defeatAllEnemies: levelAction.defeat_all_enemies,
      defeatBoss: levelAction.defeat_boss,
      id: levelAction.id,
    });
    if (levelAction.level_tile !== null) {
      this.setState({
        positionX: levelAction.level_tile.position_x,
        positionY: levelAction.level_tile.position_y,
        tileSlug: levelAction.level_tile.tile.slug,
        display: levelAction.level_tile.display,
      });
    }

    this.handleGetGameLevels();
    this.props.getGameActionTypes();
    this.props.getCutsceneActionTypes();
  }

  componentDidUpdate(lastProps) {
    if (this.props.editLevelActionObj !== null && lastProps.editLevelActionObj === null) {
      this.props.resetEditLevelAction();
      this.props.resetLevelActions();
      this.props.clearLevelTiles();
      this.props.getLevelActions(this.props.gameDetail.slug, this.props.level);
      this.props.getLevelTiles(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeEditLevelAction();
    }
    if (this.props.deleteLevelActionObj !== null && lastProps.deleteLevelActionObj === null) {
      this.props.resetDeleteLevelAction();
      this.props.resetLevelActions();
      this.props.clearLevelTiles();
      this.props.getLevelActions(this.props.gameDetail.slug, this.props.level);
      this.props.getLevelTiles(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeEditLevelAction();
    }
  }

  componentWillUnmount() {
    this.props.resetGameActionTypes();
  }

  render() {
    const { level, gameTiles, loadingGameTiles, levelAction, levelActionTypes, gameActionTypes, loadingGameActionTypes, gameCutscenes, loadingGameCutscenes } = this.props;
    const { positionX, positionY, tileSlug, uploadingData, display, levelOptions, levelActionTypeSlug, gameActionTypeSlug, cutsceneSlug, requireComplete, defeatAllEnemies, defeatBoss } = this.state;

    return (
      <div style={{margin: '20px auto', width: `${level.width}px`}}>
        <div style={{padding: 10}}>
          <span style={styles.nameText}>Edit Level Action:</span>
        </div>
        {levelAction.action_type.slug !== 'complete-level' ? 
          <div style={{paddingBottom: 25}}>
            <div style={{display: 'flex'}}>
              <div style={{flex: 1}}>
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1}}>
                    <span style={styles.defaultText}>Position X:</span>
                  </div>
                  <div style={{flex: 3}}>
                    <input 
                      name="positionX"
                      value={positionX}
                      placeholder="Position X"
                      style={styles.textInput}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1}}>
                    <span style={styles.defaultText}>Position Y:</span>
                  </div>
                  <div style={{flex: 3}}>
                    <input 
                      name="positionY"
                      value={positionY}
                      placeholder="Position Y"
                      style={styles.textInput}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                {!loadingGameTiles ? 
                  <div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Tile:</span>
                      </div>
                      {levelAction.level_tile !== null ? 
                        <div style={{flex: 3}}>
                          {gameTiles !== null ? 
                            <select 
                              name="gameTiles" 
                              id="gameTiles"
                              className="gameTilesSelect"
                              onChange={this.handleSelectChange.bind(this)}
                              style={styles.selectInput}
                            >
                              <option value="">Select a Tile</option>
                              {gameTiles.map((tile, index) => (
                                <option 
                                  key={index} 
                                  value={tile.slug}
                                  selected={tile.slug == tileSlug ? 'selected' : ''}
                                >
                                  {tile.name}
                                </option>
                              ))}
                            </select> :
                            <div>
                              <span style={styles.defaultText}>Loading Game Tiles...</span>
                            </div>
                          }
                        </div> :
                        <div style={{flex: 4}}>
                        {gameTiles !== null ? 
                          <select 
                            name="gameTiles" 
                            id="gameTiles"
                            className="gameTilesSelect"
                            onChange={this.handleSelectChange.bind(this)}
                            style={styles.selectInput}
                          >
                            <option value="">Select a Tile</option>
                            {gameTiles.map((tile, index) => (
                              <option 
                                key={index} 
                                value={tile.slug}
                              >
                                {tile.name}
                              </option>
                            ))}
                          </select> :
                          <div>
                            <span style={styles.defaultText}>Loading Game Tiles...</span>
                          </div>
                        }
                      </div>
                      }
                    </div>
                    <div style={{paddingBottom: 10}}>
                      <span style={styles.defaultText}>Leave Tile Blank for Defeat All Enemies / Defeat Boss</span>
                    </div>
                  </div> :
                  <div>
                    <span style={styles.defaultText}>Loading Game Tiles...</span>
                  </div>
                }
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                    <span style={styles.defaultText}>Display: </span>
                  </div>
                  <div style={{flex: 1}}>
                    <input 
                      type="checkbox" 
                      name="display" 
                      id="display" 
                      checked={display} 
                      onChange={this.handleCheckboxChange.bind(this)} 
                    />
                  </div>
                </div> 
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1, textAlign: 'right',  paddingRight: 10}}>
                    <span style={styles.defaultText}>Defeat All Enemies: </span>
                  </div>
                  <div style={{flex: 1}}>
                    <input 
                      type="checkbox" 
                      name="defeatAllEnemies" 
                      id="defeatAllEnemies" 
                      checked={defeatAllEnemies} 
                      onChange={this.handleCheckboxChange.bind(this)} 
                    />
                  </div>
                </div> 
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                    <span style={styles.defaultText}>Defeat Boss: </span>
                  </div>
                  <div style={{flex: 1}}>
                    <input 
                      type="checkbox" 
                      name="defeatBoss" 
                      id="defeatBoss" 
                      checked={defeatBoss} 
                      onChange={this.handleCheckboxChange.bind(this)} 
                    />
                  </div>
                </div> 
              </div>
              <div style={{flex: 1}}>
                {levelActionTypes !== null ? 
                  <div style={{paddingBottom: 10}}>
                    <div style={{paddingBottom: 10, display: 'flex'}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Action:</span>
                      </div>
                      <div style={{flex: 3}}>
                        <select 
                          name="levelActionTypes" 
                          id="levelActionTypes"
                          className="levelActionTypesSelect"
                          style={styles.selectInput}
                          onChange={this.handleSelectLevelActionType.bind(this)}
                        >
                          <option value="">...</option>
                          {levelActionTypes.map((actionType, index) => (
                            <option 
                              key={index} 
                              value={actionType.slug}
                              selected={actionType.slug == levelAction.action_type.slug ? 'selected' : ''}
                            >
                              {actionType.name}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                    {levelActionTypeSlug === "change-level" && 
                      <div style={{paddingBottom: 10}}>
                        <div style={{paddingBottom: 10, display: 'flex'}}>
                          <div style={{flex: 1}}>
                            <span style={styles.defaultText}>Select Level:</span>
                          </div>
                          <div style={{flex: 3}}>
                            <select 
                              name="gameLevels" 
                              id="gameLevels"
                              className="gameLevelSelect"
                              style={styles.selectInput}
                              onChange={this.handleSelectGameLevel.bind(this)}
                            >
                              <option value="">...</option>
                              {levelOptions.map((levelOption, index) => (
                                <option 
                                  key={index}
                                  value={levelOption.slug}
                                  selected={levelOption.slug == levelAction.change_level.slug ? 'selected' : ''}
                                >
                                  {levelOption.name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>
                    }
                    {levelActionTypeSlug === 'game-action' && 
                      <div style={{paddingBottom: 10}}>
                        <div style={{paddingBottom: 10, display: 'flex'}}>
                          <div style={{flex: 1}}>
                            <span style={styles.defaultText}>Game Action:</span>
                          </div>
                          <div style={{flex: 3}}>
                            {!loadingGameActionTypes ? 
                              <div style={{paddingBottom: 10}}>
                                {levelAction.game_action_type !== null ? 
                                  <select
                                    name="gameActionTypes" 
                                    id="gameActionTypes"
                                    className="gameActionTypeSelect"
                                    style={styles.selectInput}
                                    onChange={this.handleSelectGameActionType.bind(this)}
                                  >
                                    <option value="">...</option>
                                    {gameActionTypes.map((gameActionType, index) => (
                                      <option 
                                        key={index}
                                        value={gameActionType.slug}
                                        selected={gameActionType.slug == levelAction.game_action_type.slug ? 'selected' : ''}
                                      >
                                        {gameActionType.name}
                                      </option>
                                    ))}
                                  </select> : 
                                  <select
                                    name="gameActionTypes" 
                                    id="gameActionTypes"
                                    className="gameActionTypeSelect"
                                    style={styles.selectInput}
                                    onChange={this.handleSelectGameActionType.bind(this)}
                                  >
                                    <option value="">...</option>
                                    {gameActionTypes.map((gameActionType, index) => (
                                      <option 
                                        key={index}
                                        value={gameActionType.slug}
                                      >
                                        {gameActionType.name}
                                      </option>
                                    ))}
                                  </select>
                                }
                              </div> :
                              <div>
                                <span style={styles.defaultText}>Loading Game Action Types...</span>
                              </div>
                            }
                          </div>
                        </div>
                      </div>
                    }
                    {levelActionTypeSlug === 'change-cutscene' && 
                      <div style={{paddingBottom: 10}}>
                        <div style={{paddingBottom: 10, display: 'flex'}}>
                          <div style={{flex: 1}}>
                            <span style={styles.defaultText}>Change Cutscene:</span>
                          </div>
                          <div style={{flex: 3}}>
                            {!loadingGameCutscenes ? 
                              <div>
                                {gameCutscenes.length > 0 ? 
                                  <select
                                    name="gameCutscene" 
                                    id="gameCutscene"
                                    className="gameCutsceneSelect"
                                    onChange={this.handleSelectGameCutscene.bind(this)}
                                    style={styles.selectInput}
                                  >
                                    <option value="">...</option>
                                    {gameCutscenes.map((cutscene, index) => (
                                      <option 
                                        key={index}
                                        value={cutscene.slug}
                                        selected={levelAction.change_cutscene?.slug == cutscene.slug ? 'selected' : '' }
                                      >
                                        {cutscene.name}
                                      </option>
                                    ))}
                                  </select> :
                                  <div>
                                    <span style={styles.defaultText}>No Cutscenes</span>
                                  </div>
                                }
                              </div> :
                              <div>
                                <span style={styles.defaultText}>Loading Cutscenes...</span>
                              </div>
                            }
                          </div>
                        </div>
                      </div>
                    }
                  </div> :
                  <div>
                    <span style={styles.defaultText}>Loading Level Action Types...</span>
                  </div>
                }
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                    <span style={styles.defaultText}>Require Complete:</span>
                  </div>
                  <div style={{flex: 1}}>
                    <input 
                      type="checkbox" 
                      name="requireComplete" 
                      id="requireComplete" 
                      checked={requireComplete} 
                      onChange={this.handleCheckboxChange.bind(this)} 
                    />
                  </div>
                </div> 
              </div>
              
            </div>
          </div> :
          <div>
            <div style={{padding: 10}}>
              <span style={styles.nameText}>Complete Level:</span>
            </div>
            <div style={{display: 'flex', paddingBottom: 25}}>
              <div style={{flex: 1, textAlign: 'right',  paddingRight: 10}}>
                <span style={styles.defaultText}>Defeat All Enemies: </span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="defeatAllEnemies" 
                  id="defeatAllEnemies" 
                  checked={defeatAllEnemies} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Defeat Boss: </span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="defeatBoss" 
                  id="defeatBoss" 
                  checked={defeatBoss} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
        }
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex', paddingTop: 10, paddingBottom: 10}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditLevelAction}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeEditLevelAction}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              {levelAction.action_type.slug !== 'complete-level' && 
                <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
              }
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 
  gameLevels: state.games.gameLevels,
  gameCutscenes: state.games.gameCutscenes,
  loadingGameCutscenes: state.games.loadingGameCutscenes,

  gameTiles: state.games.gameTiles,
  loadingGameTiles: state.games.loadingGameTiles,

  editLevelActionObj: state.game_admin.editLevelActionObj,
  deleteLevelActionObj: state.game_admin.deleteLevelActionObj,

  levelActions: state.game_admin.levelActions,
  loadingLevelActions: state.game_admin.loadingLevelActions, 

  levelActionTypes: state.game_admin.levelActionTypes,

  gameActionTypes: state.game_admin.gameActionTypes,
  loadingGameActionTypes: state.game_admin.loadingGameActionTypes
})

const mapDispatchToProps = {
  editLevelAction, resetEditLevelAction, deleteLevelAction, resetDeleteLevelAction, getLevelActions, resetLevelActions, deleteLevelTile, resetDeleteLevelTile, getLevelTiles, clearLevelTiles, getGameActionTypes, resetGameActionTypes, getCutsceneActionTypes, resetCutsceneActionTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLevelAction)
