import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditBackground from './EditBackground';
import InlineBackground from './InlineBackground';

import NewBackground from '../new/NewBackground';

export class EditBackgrounds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showBackgrounds: true,
      showEditBackground: false,
      showNewBackground: false,
      editBackground: null
    }
  }

  handleCloseNewBackground = () => {
    this.setState({
      showBackgrounds: true,
      showEditBackground: false,
      showNewBackground: false,
      editBackground: null
    });
  }

  handleShowNewBackground = () => {
    this.setState({
      showBackgrounds: false,
      showEditBackground: false,
      showNewBackground: true,
      editBackground: null
    });
  }

  handleShowBackgrounds = () => {
    this.setState({
      showBackgrounds: true,
      showEditBackground: false,
      showNewBackground: false,
      editBackground: null
    });
  }

  handleShowEditBackground = (background) =>{
    this.setState({
      showBackgrounds: false,
      showEditBackground: true,
      showNewBackground: false,
      editBackground: background
    });
  }

  render() {
    const { gameBackgrounds, loadingGameBackgrounds } = this.props;
    const { showBackgrounds, showEditBackground, showNewBackground, editBackground } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 24}}>
          <button onClick={this.handleShowNewBackground}>New</button>
        </div>
        {showBackgrounds && 
          <div>
            {!loadingGameBackgrounds ? 
              <div>
                {gameBackgrounds.length > 0 ? 
                  <div>
                    {gameBackgrounds.map((background, index) => (
                      <button style={styles.button} key={index} onClick={this.handleShowEditBackground.bind(this, background)}>
                        <InlineBackground background={background} />
                      </button>
                    ))}
                  </div> :
                  <div>
                    <span>No Game Backgrounds!</span>
                  </div>
                }
              </div> :
              <div>
                <span>Loading...</span>
              </div>
            }
          </div>
        }
        {showEditBackground && 
          <EditBackground showBackgrounds={this.handleShowBackgrounds} background={editBackground} />
        }
        {showNewBackground && 
          <NewBackground closeNewBackground={this.handleCloseNewBackground} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameBackgrounds: state.games.gameBackgrounds,
  loadingGameBackgrounds: state.games.loadingGameBackgrounds,
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditBackgrounds)

const styles = {
  button: {
    fontSize: '100%',
    fontFamily: 'inherit',
    border: 0,
    padding: 0
  }
}
