import React, { Component } from 'react'

class InlineCutscene extends Component {
  render() {
    const { cutscene } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={cutscene.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{cutscene.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineCutscene;
