import React, { Component } from 'react'

class InlineTile extends Component {
  componentDidMount() {
    // console.log('Tile: ', this.props.tile);
  }

  render() {
    const { tile } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={tile.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <div style={{padding: 5}}><span style={{color: '#000'}}>{tile.name}</span></div>
          <div style={{padding: 5}}><span style={{color: '#000'}}>{tile.height} x {tile.width}</span></div>
          <div style={{padding: 5}}><span style={{color: '#000'}}>{tile.walkable ? 'Walkable' : 'Block'}</span></div>
        </div>
      </div>
    )
  }
}

export default InlineTile;
