import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditLevel from './EditLevel';
import InlineLevel from './InlineLevel';
import NewLevel from '../new/NewLevel';

import EditTile from './EditTile';
import InlineTile from './InlineTile';
import NewTile from '../new/NewTile';

import { styles } from '../../layout/Styles';

import { getGameTiles } from '../../../actions/games';

export class EditLevels extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLevelsSection: true,
      showLevels: true,
      showEditLevel: false,
      showNewLevel: false,
      editLevel: null,
      showTilesSection: false,
      showTiles: true,
      showEditTile: false,
      showNewTile: false,
      editTile: null,
    }
  }

  handleCloseNewLevel = () => {
    this.setState({
      showLevels: true,
      showEditLevel: false,
      showNewLevel: false,
      editLevel: null
    });
  }

  handleShowNewLevel = () => {
    this.setState({
      showLevels: false,
      showEditLevel: false,
      showNewLevel: true,
      editLevel: null
    });
  }

  handleShowLevels = () => {
    this.setState({
      showLevels: true,
      showEditLevel: false,
      showNewLevel: false,
      editLevel: null
    });
  }

  handleShowEditLevel = (level) => {
    this.setState({
      showLevels: false,
      showEditLevel: true,
      showNewLevel: false,
      editLevel: level
    });
  }

  handleCloseNewTile = () => {
    this.setState({
      showTiles: true,
      showEditTile: false,
      showNewTile: false,
      editTile: null
    });
  }

  handleShowNewTile = () => {
    this.setState({
      showTiles: false,
      showEditTile: false,
      showNewTile: true,
      editTile: null
    });
  }

  handleShowTiles = () => {
    this.setState({
      showTiles: true,
      showEditTile: false,
      showNewTile: false,
      editTile: null
    });
  }

  handleShowEditTile = (tile) => {
    this.setState({
      showTiles: false,
      showEditTile: true,
      showNewTile: false,
      editTile: tile
    });
  }

  handleShowLevelsSection = () => {
    this.setState({
      showLevelsSection: true,
      showTilesSection: false
    })
  }

  handleShowTilesSection = () => {
    this.setState({
      showLevelsSection: false,
      showTilesSection: true
    })
  }

  componentDidMount() {
    const { gameDetail } = this.props;
    this.props.getGameTiles(gameDetail.slug);
  }

  render() {
    const { gameLevels, loadingGameLevels, gameTiles, loadingGameTiles } = this.props;
    const { showLevels, showLevelsSection, showEditLevel, showNewLevel, editLevel } = this.state;
    const { showTiles, showTilesSection, showEditTile, showNewTile, editTile } = this.state;

    return (
      <div>
        <div style={{display: 'flex', paddingBottom:  24}}>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showLevelsSection ? 
              <div style={{padding: 10}}><span style={styles.nameText}>Levels</span></div> :
              <button style={styles.buttonAction} onClick={this.handleShowLevelsSection}>Levels</button>
            }
          </div>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showTilesSection ? 
              <div style={{padding: 10}}><span style={styles.nameText}>Tiles</span></div> :
              <button style={styles.buttonAction} onClick={this.handleShowTilesSection}>Tiles</button>
            }
          </div>
        </div>
        {showLevelsSection && 
          <div>
            {showLevels && 
              <div>
                <div style={{paddingBottom: 24}}>
                  <button style={styles.buttonDefault} onClick={this.handleShowNewLevel}>New</button>
                </div>
                {!loadingGameLevels ? 
                  <div>
                    {gameLevels.length > 0 ? 
                      <div>
                        {gameLevels.map((level, index) => (
                          <button key={index} style={styles.button} onClick={this.handleShowEditLevel.bind(this, level)}>
                            <InlineLevel level={level} />
                          </button>
                        ))}
                      </div> :
                      <div>
                        <span>No Levels</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span>Loading...</span>
                  </div>
                }
              </div>
            }
            {showEditLevel && 
              <EditLevel showLevels={this.handleShowLevels} level={editLevel} />
            }
            {showNewLevel && 
              <NewLevel closeNewLevel={this.handleCloseNewLevel} />
            }
          </div>
        }
        {showTilesSection && 
          <div>
            {showTiles && 
              <div>
                <div style={{paddingBottom: 24}}>
                  <button style={styles.buttonDefault} onClick={this.handleShowNewTile}>New</button>
                </div>
                {!loadingGameTiles ? 
                  <div>
                    {gameTiles.length > 0 ? 
                      <div>
                        {gameTiles.map((tile, index) => (
                          <button key={index} style={styles.button} onClick={this.handleShowEditTile.bind(this, tile)}>
                          <InlineTile tile={tile} />
                        </button>
                        ))}
                      </div> :
                      <div>
                        <span>No Tiles</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span>Loading...</span>
                  </div>
                }
              </div>
            }
            {showEditTile && 
              <EditTile showTiles={this.handleShowTiles} tile={editTile} />
            }
            {showNewTile && 
              <NewTile closeNewTile={this.handleCloseNewTile} />
            }
          </div>
        }  
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  gameLevels: state.games.gameLevels,
  loadingGameLevels: state.games.loadingGameLevels,

  gameTiles: state.games.gameTiles,
  loadingGameTiles: state.games.loadingGameTiles
})

const mapDispatchToProps = {
  getGameTiles
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLevels)