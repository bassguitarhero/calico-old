import React, { Component } from 'react'

class InlineDialogue extends Component {
  render() {
    const { dialogue } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        {dialogue.thumbnail !== null && 
          <div style={{flex: 1, paddingRight: 24}}>
            <img src={dialogue.thumbnail} style={{height: 100, width: 100}} />
          </div>
        }
        <div style={{flex: 9}}>
          <span>{dialogue.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineDialogue;
