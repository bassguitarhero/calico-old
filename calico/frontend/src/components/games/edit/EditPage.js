import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editPage, resetEditPage, deletePage, resetDeletePage } from '../../../actions/game_admin';
import { clearGamePages, getGamePages } from '../../../actions/games';
import { getPageActionTypes, resetPageActionTypes, getPageActions, resetPageActions } from '../../../actions/game_admin';

import EditPageActions from './EditPageActions';

export class EditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      slug: '',
      youtubeID: '',
      youtubeURL: '',
      replaceImage: false,
      removeImage: false
    }
  }

  handleShowReplaceImage = () => {
    this.setState({image: '', replaceImage: true, removeImage: true});
  }

  handleCloseEditPage = () => {
    this.props.closeEditPage();
  }

  handleDelete = () => {
    if (window.confirm("Delete Page?")) {
      this.props.deletePage(this.props.gameDetail.slug, this.props.page.slug);
    }
  }

  handleImageChange = e => this.setState({
    imageFile: e.target.files[0],
    removeImage: false
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditPage = () => {
    const { name, description, imageFile, slug, youtubeID, youtubeURL, removeImage } = this.state;
    const page = { name, description, imageFile, slug, youtubeID, youtubeURL, removeImage };
		if (name !== '') {
			if (window.confirm("Edit Page?")) {
				this.props.editPage(this.props.gameDetail.slug, page);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Pages must have a Name.");
		}
  }

  componentDidMount() {
    const { gameDetail, page } = this.props;
    this.setState({
      name: page.name,
      description: page.description,
      image: page.thumbnail,
      slug: page.slug,
      youtubeID: page.youtube_id,
      youtubeURL: page.youtube_url
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editPageObj !== null && lastProps.editPageObj === null) {
      this.props.resetEditPage();
      this.props.clearGamePages();
      this.props.getGamePages(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.closeEditPage();
    }
    if (this.props.deletePageObj !== null && lastProps.deletePageObj === null) {
      this.props.resetDeletePage();
      this.props.clearGamePages();
      this.props.getGamePages(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.closeEditPage();
    }
  }

  render() {
    const { page } = this.props;
    const { name, description, image, imageFile, uploadingData, youtubeID, youtubeURL, replaceImage } = this.state;
    const { pageActionTypes, pageActions, loadingPageActions } = this.props;

    return (
      <div style={{display: 'flex'}}>
        <div style={{flex: 1}}>
          <div style={styles.titleContainer}>
            <span style={styles.titleText}>Edit Page:</span>
          </div>
          {(!replaceImage && (image !== null && image !== '')) ? 
            <div style={{display: 'flex'}}>
              <div style={{flex: 2}}>
                {image && 
                  <img src={image} style={{height: 100, width: 100}} />
                }
                {(!replaceImage && (image !== null && image !== '')) && 
                  <button onClick={this.handleShowReplaceImage} style={styles.buttonAction}>
                    Remove / Replace
                  </button>
                }
              </div>
              <div style={{flex: 4, paddingLeft: 24}}>
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1}}>
                    <span style={styles.defaultText}>Name: </span>
                  </div>
                  <div style={{flex: 3}}>
                    <input 
                      name="name"
                      value={name}
                      placeholder="Name"
                      onChange={this.handleChange}
                      style={styles.textInput}
                    />
                  </div>
                </div>
                {(replaceImage || (image === '' || image === null)) && 
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}> 
                      <label style={styles.defaultText} for="image">Image:</label>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        style={styles.fileInput}
                        type="file" 
                        name="image" 
                        onChange={this.handleImageChange}
                      />
                    </div>
                  </div>
                }
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1}}>
                    <span style={styles.defaultText}>YouTube ID: </span>
                  </div>
                  <div style={{flex: 3}}>
                    <input 
                      name="youtubeID"
                      value={youtubeID}
                      placeholder="YouTube ID"
                      onChange={this.handleChange}
                      style={styles.textInput}
                    />
                  </div>
                </div>
              </div>
            </div> :
            <div style={{padding: 10}}>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Name: </span>
                </div>
                <div style={{flex: 3}}>
                  <input 
                    name="name"
                    value={name}
                    placeholder="Name"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              {(replaceImage || (image === '' || image === null)) && 
                <div style={{display: 'flex', paddingBottom: 10}}>
                  <div style={{flex: 1}}> 
                    <label style={styles.defaultText} for="image">Image:</label>
                  </div>
                  <div style={{flex: 3}}>
                    <input 
                      style={styles.fileInput}
                      type="file" 
                      name="image" 
                      onChange={this.handleImageChange}
                    />
                  </div>
                </div>
              }
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>YouTube ID: </span>
                </div>
                <div style={{flex: 3}}>
                  <input 
                    name="youtubeID"
                    value={youtubeID}
                    placeholder="YouTube ID"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
            </div>
          }
          <div style={{display: 'flex', paddingTop: 15, paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Story: </span>
            </div>
            <div style={{flex: 9}}>
              <textarea 
                className="updateFormTextArea" 
                id="description"
                name="description" 
                onChange={this.handleChange} 
                value={description} 
                placeholder="Description:" 
                style={styles.textareaPageInput}
                rows="10"
              />
            </div>
          </div>
          {uploadingData ? 
            <div style={{paddingTop: 15}}>
              <div style={styles.defaultText} id="uploadProgress" />
            </div> :
            <div style={{paddingTop: 15, display: 'flex'}}>
              <div style={{flex: 1, textAlign: 'center'}}>
                <button style={styles.buttonDefault} onClick={this.handleEditPage}>Save</button>
              </div>
              <div style={{flex: 1, textAlign: 'center'}}>
                <button style={styles.buttonDefault} onClick={this.handleCloseEditPage}>Cancel</button>
              </div>
              <div style={{flex: 1, textAlign: 'center'}}>
                <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
              </div>
            </div>
           }
        </div>
        <div style={{flex: 1}}>
           <EditPageActions page={page} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editPageObj: state.game_admin.editPageObj,
  deletePageObj: state.game_admin.deletePageObj,

  pageActionTypes: state.game_admin.pageActionTypes,

  pageActions: state.game_admin.pageActions,
  loadingPageActions: state.game_admin.loadingPageActions
})

const mapDispatchToProps = {
  editPage, 
  resetEditPage,
  deletePage,
  resetDeletePage,
  clearGamePages, 
  getGamePages,
  getPageActionTypes, 
  resetPageActionTypes, 
  getPageActions, 
  resetPageActions
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPage)
