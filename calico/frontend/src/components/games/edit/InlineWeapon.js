import React, { Component } from 'react'

class InlineWeapon extends Component {
  render() {
    const { weapon } = this.props;

    return (
      <div style={{display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={weapon.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{weapon.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineWeapon;
