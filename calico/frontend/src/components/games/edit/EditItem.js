import React, { Component } from 'react'
import { connect } from 'react-redux'

import { editItem, deleteItem } from '../../../actions/game_admin';
import { resetEditItem, resetDeleteItem } from '../../../actions/game_admin';
import { clearGameItems, getGameItems } from '../../../actions/games';

export class EditItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      slug: ''
    }
  }

  handleDelete = () => {
    if (window.confirm("Delete Item?")) {
      this.props.deleteItem(this.props.gameDetail.slug, this.props.item.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditItem = () => {
    const { name, description, imageFile, slug } = this.state;
    const item = { name, description, imageFile, slug };
		if (name !== '') {
			if (window.confirm("Edit Item?")) {
				this.props.editItem(this.props.gameDetail.slug, item);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Items must have a Name.");
		}
  }

  componentDidMount() {
    const { item } = this.props;
    this.setState({
      name: item.name,
      description: item.description,
      image: item.image,
      slug: item.slug
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editItemObj !== null && lastProps.editItemObj === null) {
      this.props.resetEditItem();
      this.props.clearGameItems();
      this.props.getGameItems(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showItems();
    }
    if (this.props.deleteItemObj !== null && lastProps.deleteItemObj === null) {
      this.props.resetDeleteItem();
      this.props.clearGameItems();
      this.props.getGameItems(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showItems();
    }
  }

  render() {
    const { item } = this.props;
    const { name, description, image, imageFile, uploadingData } = this.state;

    return (
      <div>
        <div style={{display: 'flex'}}>
          <div style={{flex: 1}}>
            <img src={item.thumbnail} style={{height: 100, width: 100}} />
          </div>
          <div style={{flex: 9, paddingLeft: 24}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Name: </span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Description: </span>
              </div>
              <div style={{flex: 4}}>
                <textarea 
                  style={styles.textareaInput}
                  type="textArea" 
                  name="description" 
                  onChange={this.handleChange} 
                  value={description} 
                  placeholder="Describe your Item"
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <label for="image">Image:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> : 
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleEditItem}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.props.showItems}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editItemObj: state.game_admin.editItemObj,
  deleteItemObj: state.game_admin.deleteItemObj
})

const mapDispatchToProps = {
  editItem,
  deleteItem,
  resetEditItem,
  resetDeleteItem,
  clearGameItems,
  getGameItems
}

export default connect(mapStateToProps, mapDispatchToProps)(EditItem)

const styles = {
  textInput: {
    size: 40
  },
  textareaInput: {

  },
  fileInput: {

  },
  checkboxInput: {

  }
}