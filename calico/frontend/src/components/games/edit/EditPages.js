import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import EditPage from './EditPage';
import InlinePage from './InlinePage';
import NewPage from '../new/NewPage';

export class EditPages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPages: true,
      showEditPage: false,
      showNewPage: false,
      editPage: null
    }
  }

  handleShowPages = () => {
    this.setState({
      showPages: true,
      showEditPage: false,
      showNewPage: false,
      editPage: null
    });
  }

  handleShowEditPage = (page) => {
    this.setState({
      showPages: false,
      showEditPage: true,
      showNewPage: false,
      editPage: page
    });
  }

  handleShowNewPage = () => {
    this.setState({
      showPages: false,
      showEditPage: false,
      showNewPage: true
    });
  }

  render() {
    const { showPages, showEditPage, showNewPage, editPage } = this.state;
    const { gamePages, loadingGamePages } = this.props;

    return (
      <div>
        {(!showEditPage && !showNewPage) && 
          <div style={{paddingBottom: 24}}>
            <button style={styles.buttonAction} onClick={this.handleShowNewPage}>New</button>
          </div>
        }
        {showPages && 
          <div>
            {!loadingGamePages ? 
              <div>
                {gamePages.length > 0 ? 
                  <div>
                    <div style={styles.titleContainer}>
                      <span style={styles.titleText}>Edit Pages:</span>
                    </div>
                    {gamePages.map((page, index) => (
                      <InlinePage editPage={this.handleShowEditPage} key={index} page={page} />
                    ))}
                  </div> :
                  <div style={styles.loadingTextContainer}>
                    <span style={styles.loadingTextContainer}>No Pages</span>
                  </div>
                }
              </div> :
              <div style={styles.loadingTextContainer}>
                <span style={styles.loadingText}>Loading Pages...</span>
              </div>
            }
          </div>
        }
        {showEditPage && 
          <EditPage page={editPage} closeEditPage={this.handleShowPages} />
        }
        {showNewPage && 
          <NewPage closeNewPage={this.handleShowPages} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  gamePages: state.games.gamePages,
  loadingGamePages: state.games.loadingGamePages
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPages)
