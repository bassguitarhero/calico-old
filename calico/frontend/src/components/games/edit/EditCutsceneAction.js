import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editCutsceneAction, resetEditCutsceneAction, deleteCutsceneAction, resetDeleteCutsceneAction } from '../../../actions/game_admin';

export class EditCutsceneAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      slug: '',
      gameActionTypeSlug: '',
      cutsceneActionTypeSlug: '',
      changeCutsceneSlug: '',
      levelSlug: '',
      requireComplete: false,
      uploadingData: false,
      autoSelect: false
    }
  }

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'actionName'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleDeleteCutsceneAction = () => {
    if (window.confirm("Delete Cutscene Action?")) {
      this.props.deleteCutsceneAction(this.props.gameDetail.slug, this.props.cutsceneAction.cutscene.slug, this.props.cutsceneAction.slug);
    }
  }

  handleEditCutsceneAction = () => {
    const { gameActionTypeSlug, cutsceneActionTypeSlug, changeCutsceneSlug, levelSlug, requireComplete, name, autoSelect } = this.state;
    const { gameDetail, cutsceneAction } = this.props;
    const cutsceneActionData = { cutsceneAction, gameActionTypeSlug, cutsceneActionTypeSlug, changeCutsceneSlug, levelSlug, requireComplete, name, autoSelect };
    if (name !== '') {
      if (window.confirm("Edit Cutscene Action?")) {
        this.props.editCutsceneAction(gameDetail.slug, cutsceneActionData);
        this.setState({uploadingData: true});
      }
    } else {
      alert('Cutscene Actions must have a name.');
    }
  }

  handleCheckboxChange = (e) => {
    if (e.target.name === 'autoSelect' && this.state.autoSelect === false) {
      if (window.confirm("Set as Auto Select?")) {
        this.setState({
          [e.target.name]: !this.state[e.target.name]
        });
      }
    } else {
      this.setState({
        [e.target.name]: !this.state[e.target.name]
      });
    }
  }

  handleSelectGameActionType = (e) => {
    this.setState({
      gameActionTypeSlug: e.target.value
    });
  }

  handleSelectGameLevel = (e) => {
    this.setState({
      levelSlug: e.target.value
    });
  }

  handleSelectCutsceneOption = (e) => {
    this.setState({
      changeCutsceneSlug: e.target.value
    });
  }

  handleSelectCutsceneActionType = (e) => {
    this.setState({
      cutsceneActionTypeSlug: e.target.value
    });
  }

  handleCloseEditCutsceneAction = () => {
    this.props.closeEditCutsceneAction();
  }

  componentDidMount() {
    const { cutsceneAction } = this.props;
    this.setState({
      name: cutsceneAction.name,
      slug: cutsceneAction.slug,
      gameActionTypeSlug: cutsceneAction.game_action_type !== null ? cutsceneAction.game_action_type.slug : '',
      cutsceneActionTypeSlug: cutsceneAction.action_type !== null ? cutsceneAction.action_type.slug : '',
      changeCutsceneSlug: cutsceneAction.change_cutscene !== null ? cutsceneAction.change_cutscene.slug : '',
      levelSlug: cutsceneAction.change_level !== null ? cutsceneAction.change_level.slug : '',
      requireComplete: cutsceneAction.complete_required,
      autoSelect: cutsceneAction.auto_select
    })
  }

  render() {
    const { cutscene, cutsceneAction, loadingCutsceneActionTypes, cutsceneActionTypes, gameLevels, cutsceneOptions, gameActionTypes } = this.props;
    const { name, gameActionTypeSlug, cutsceneActionTypeSlug, changeCutsceneSlug, levelSlug, requireComplete, uploadingData, autoSelect } = this.state;

    return (
      <div>
        <div><span style={styles.nameText}>Edit Cutscene Action:</span></div>
        <div><span>{cutsceneAction.name}</span></div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Name:</span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
            />
          </div>
        </div>
        {!loadingCutsceneActionTypes ? 
          <div>
            {cutsceneActionTypes.length > 0 ? 
              <select
                name="cutsceneActionTypes" 
                id="cutsceneActionTypes"
                className="cutsceneActionTypes"
                onChange={this.handleSelectCutsceneActionType.bind(this)}
                style={styles.selectInput}
              >
                <option value="">...</option>
                {cutsceneActionTypes.map((cutsceneActionType, index) => (
                  <option 
                    key={index}
                    value={cutsceneActionType.slug}
                    selected={cutsceneActionType.slug === cutsceneAction.action_type.slug ? 'selected' : ''}
                  >
                    {cutsceneActionType.name}
                  </option>
                ))}
              </select> :
              <div>
                <span>No Cutscene Action Types</span>
              </div>
            }
          </div> :
          <div>
            <span>Loading Cutscene Action Types...</span>
          </div>
        }
        {cutsceneActionTypeSlug === 'change-level' && 
          <div>
            <div><span>Change Level:</span></div>
            {gameLevels.length > 0 ? 
              <select 
                name="gameLevels" 
                id="gameLevels"
                className="gameLevelSelect"
                style={styles.selectInput}
                onChange={this.handleSelectGameLevel.bind(this)}
              >
                <option value="">...</option>
                {gameLevels.map((gameLevel, index) => (
                  <option 
                    key={index}
                    value={gameLevel.slug}
                    selected={gameLevel.slug === levelSlug ? 'selected': ''}
                  >
                    {gameLevel.name}
                  </option>
                ))}
              </select> :
              <div>
                <span>No Levels to Select</span>
              </div>
            }
          </div>
        }
        {cutsceneActionTypeSlug == 'change-cutscene' && 
          <div>
            <div><span>Change Cutscene:</span></div>
            {cutsceneOptions.length > 0 ? 
              <div>
                <select
                  name="cutsceneActionTypes" 
                  id="cutsceneActionTypes"
                  className="cutsceneActionTypes"
                  onChange={this.handleSelectCutsceneOption.bind(this)}
                  style={styles.selectInput}
                >
                  <option value="">...</option>
                  {cutsceneOptions.map((cutsceneOption, index) => (
                    <option 
                      key={index}
                      value={cutsceneOption.slug}
                      selected={cutsceneOption.slug === changeCutsceneSlug ? 'selected' : ''}
                    >
                      {cutsceneOption.name}
                    </option>
                  ))}
                </select>
              </div> :
              <div>
                <span>No Cutscenes to Select</span>
              </div>
            }
          </div>                
        }
        {cutsceneActionTypeSlug == 'game-action' && 
          <div>
            <div><span>Game Action:</span></div>
            {gameActionTypes.length > 0 ? 
              <div>
                <select
                  name="gameActionTypes" 
                  id="gameActionTypes"
                  className="gameActionTypeSelect"
                  style={styles.selectInput}
                  onChange={this.handleSelectGameActionType.bind(this)}
                >
                  <option value="">...</option>
                  {gameActionTypes.map((gameActionType, index) => (
                    <option 
                      key={index}
                      value={gameActionType.slug}
                      selected={gameActionType.slug === gameActionTypeSlug ? 'selected' : ''}
                    >
                      {gameActionType.name}
                    </option>
                  ))}
                </select>
              </div> :
              <div>
                <span>No Game Actions to Select</span>
              </div>
            }
          </div>
        }
        {cutscene.url_youtube !== '' && cutscene.url_youtube !== null && 
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
              <span style={styles.defaultText}>Require Complete:</span>
            </div>
            <div style={{flex: 1}}>
              <input 
                type="checkbox" 
                name="requireComplete" 
                id="requireComplete" 
                checked={requireComplete} 
                onChange={this.handleCheckboxChange.bind(this)} 
              />
            </div>
          </div> 
        }
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
            <span style={styles.defaultText}>Auto Select:</span>
          </div>
          <div style={{flex: 1}}>
            <input 
              type="checkbox" 
              name="autoSelect" 
              id="autoSelect" 
              checked={autoSelect} 
              onChange={this.handleCheckboxChange.bind(this)} 
            />
          </div>
        </div> 
        {uploadingData ? 
          <div id="uploadProgress" /> : 
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditCutsceneAction}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
            <button style={styles.buttonDefault} onClick={this.handleCloseEditCutsceneAction}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDeleteCutsceneAction}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,
  gameCutscenes: state.games.gameCutscenes,
  loadingGameCutscenes: state.games.loadingGameCutscenes,
  gameLevels: state.games.gameLevels,
  loadingGameLevels: state.games.loadingGameLevels,

  cutsceneActionTypes: state.game_admin.cutsceneActionTypes,
  loadingCutsceneActionTypes: state.game_admin.loadingCutsceneActionTypes,

  levelActionTypes: state.game_admin.levelActionTypes,

  gameActionTypes: state.game_admin.gameActionTypes,
  loadingGameActionTypes: state.game_admin.loadingGameActionTypes,
})

const mapDispatchToProps = {
  editCutsceneAction, resetEditCutsceneAction, deleteCutsceneAction, resetDeleteCutsceneAction
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCutsceneAction)
