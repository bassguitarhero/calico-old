import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../../layout/Styles';

import { editConversationDialogue, resetEditConversationDialogue } from '../../../actions/game_admin';
import { deleteConversationDialogue, resetDeleteConversationDialogue } from '../../../actions/game_admin';
import { getDialogueResponses, resetDialogueResponses, getGameConversationDialogues, resetGameConversationDialogues } from '../../../actions/games';

import NewDialogueResponse from '../new/NewDialogueResponse';
import EditDialogueResponse from './EditDialogueResponse';

export class EditDialogue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      openingDialogue: false,
      endingDialogue: false,
      embedFacebook: '',
      embedTwitter: '',
      embedYouTube: '',
      conversationSlug: '',
      uploadingData: false,
      slug: '',
      showNewDialogueResponse: false,
      showEditDialogueResponse: false,
      showDialogueResponses: true,
      dialogueResponseObj: null
    }
  }

  handleShowNewDialogueResponse = () => {
    this.setState({
      showNewDialogueResponse: true,
      showEditDialogueResponse: false,
      showDialogueResponses: false,
      dialogueResponseObj: null
    });
  }

  handleShowEditDialogueResponse = (response) => {
    this.setState({
      showNewDialogueResponse: false,
      showEditDialogueResponse: true,
      showDialogueResponses: false,
      dialogueResponseObj: response
    });
  }

  handleShowDialogueResponses = () => {
    this.setState({
      showNewDialogueResponse: false,
      showEditDialogueResponse: false,
      showDialogueResponses: true,
      dialogueResponseObj: null
    });
  }

  handleShowConversation = () => {
    this.props.showConversation();
  }

  handleDeleteDialogue = () => {
    if (window.confirm("Delete Dialogue?")) {
      const { dialogue } = this.props;
      this.setState({uploadingData: true});
      this.props.deleteConversationDialogue(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug);
    }
  }

  handleEditDialogue = () => {
    const { gameDetail, conversation } = this.props;
    const { name, description, image, imageFile, openingDialogue, endingDialogue, embedFacebook, embedTwitter, embedYouTube, slug } = this.state;
    const dialogueData = { name, description, image, imageFile, openingDialogue, endingDialogue, embedFacebook, embedTwitter, embedYouTube, slug };
    if (name !== '') {
			if (window.confirm("Edit Dialogue?")) {
				this.props.editConversationDialogue(gameDetail.slug, conversation.slug, dialogueData);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Dialogues must have a Name.");
		}
  }

  handleCheckboxChange = (e) => {
    this.setState({
      [e.target.name]: !this.state[e.target.name]
    });
  }
  
  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'actionName'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    const { dialogue } = this.props;
    // console.log('Dialogue: ', dialogue);
    this.setState({
      name: dialogue.name,
      description: dialogue.description,
      image: dialogue.image,
      openingDialogue: dialogue.opening_dialogue,
      endingDialogue: dialogue.closing_dialogue,
      embedFacebook: (dialogue.embed_facebook !== null && dialogue.embed_facebook !== '' && dialogue.embed_facebook !== 'null') ? dialogue.embed_facebook : '',
      embedTwitter: (dialogue.embed_twitter !== null && dialogue.embed_twitter !== '' && dialogue.embed_twitter !== 'null') ? dialogue.embed_twitter : '',
      embedYouTube: (dialogue.embed_youtube !== null && dialogue.embed_youtube !== '' && dialogue.embed_youtube !== 'null') ? dialogue.embed_youtube : '',
      conversationSlug: dialogue.conversation.slug,
      slug: dialogue.slug
    });
    this.props.getDialogueResponses(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug);
  }

  componentDidUpdate(lastProps) {
    if (this.props.editConversationDialogueObj !== null && lastProps.editConversationDialogueObj === null) {
      const { conversation } = this.props;
      this.props.resetEditConversationDialogue();
      this.props.resetGameConversationDialogues();
      this.props.getGameConversationDialogues(conversation.game.slug, conversation.slug);
      this.setState({uploadingData: false});
      this.props.showConversation();
    }
    if (this.props.deleteConversationDialogueObj !== null && lastProps.deleteConversationDialogueObj === null) {
      const { conversation } = this.props;
      this.props.resetDeleteConversationDialogue();
      this.props.resetGameConversationDialogues();
      this.props.getGameConversationDialogues(conversation.game.slug, conversation.slug);
      this.setState({uploadingData: false});
      this.props.showConversation();
    }
    // if (this.props.loadingDialogueResponses === false && lastProps.loadingDialogueResponses === true) {
    //   console.log('Dialogue Responses Component Update: ', this.props.dialogueResponses);
    // }
    // if (this.props.loadingDialogueResponses === true && lastProps.loadingDialogueResponses === false) {
    //   console.log('Dialogue Responses Component Null: ', this.props.dialogueResponses);
    // }
  }

  render() {
    const { name, description, image, imageFile, openingDialogue, closingDialogue, embedFacebook, embedTwitter, embedYouTube, uploadingData } = this.state;
    const { dialogue, dialogueResponses, loadingDialogueResponses } = this.props;
    const { showNewDialogueResponse, showEditDialogueResponse, showDialogueResponses, dialogueResponseObj } = this.state;

    return (
      <div>
        <div style={{display: 'flex', paddingBottom: 20}}>
          <div style={{flex: 1}}>
            <div style={{paddingBottom: 10}}>
              <span style={styles.nameText}>Edit Dialogue:</span>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Name:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  onChange={this.handleChange}
                  style={styles.textInput}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Description:</span>
              </div>
              <div style={{flex: 4}}>
                <textarea 
                  className="updateFormTextArea" 
                  id="description"
                  name="description" 
                  onChange={this.handleChange} 
                  value={description} 
                  placeholder="Description:"
                  style={styles.textareaInput}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}> 
                <label for="image" style={styles.defaultText}>Image:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Opening Dialogue:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="openingDialogue" 
                  id="openingDialogue" 
                  checked={openingDialogue} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Closing Dialogue:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="closingDialogue" 
                  id="closingDialogue" 
                  checked={closingDialogue} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>        
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Facebook:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="embedFacebook"
                  value={embedFacebook}
                  placeholder="Embed Facebook"
                  onChange={this.handleChange}
                  style={styles.textInput}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Twitter:</span>
              </div>
              <div style={{flex: 4}}>
                <textarea 
                  className="updateFormTextArea" 
                  id="embedTwitter"
                  name="embedTwitter" 
                  onChange={this.handleChange} 
                  value={embedTwitter} 
                  placeholder="Embed Twitter"
                  style={styles.textareaInput}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>YouTube:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="embedYouTube"
                  value={embedYouTube}
                  placeholder="Embed YouTube"
                  onChange={this.handleChange}
                  style={styles.textInput}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            {showDialogueResponses && 
              <div>
                {!loadingDialogueResponses ? 
                  <div>
                    <div style={{paddingBottom: 10}}>
                      <span style={styles.nameText}>Responses:</span>
                    </div>
                    {dialogueResponses.length > 0 ? 
                      <div>
                        {dialogueResponses.map((response, index) => (
                          <div key={index} style={{paddingBottom: 10, display: 'flex'}}>
                            <div style={{flex: 9}}>
                              <span>{response.name}</span>
                            </div>
                            <div style={{flex: 1}}>
                              <button onClick={this.handleShowEditDialogueResponse.bind(this, response)} style={styles.buttonDefault}>Edit</button>
                            </div>
                          </div>
                        ))}
                      </div> :
                      <div style={{paddingBottom: 10}}>
                        <span style={styles.defaultText}>No Dialogue Responses...</span>
                      </div>
                    }
                  </div> :
                  <div style={{paddingBottom: 10}}>
                    <span style={styles.loadingText}>Loading Dialogue Responses...</span>
                  </div>
                }
              </div>
            }
            {showEditDialogueResponse && 
              <EditDialogueResponse closeEditDialogueResponse={this.handleShowDialogueResponses} dialogueResponse={dialogueResponseObj} dialogue={dialogue} />
            }
            {showNewDialogueResponse && 
              <NewDialogueResponse closeNewDialogueResponse={this.handleShowDialogueResponses} dialogue={dialogue} />
            }
            {!showEditDialogueResponse && !showNewDialogueResponse && 
              <div>
                <button onClick={this.handleShowNewDialogueResponse} style={styles.buttonDefault}>New Response</button>
              </div>
            }
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditDialogue}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleShowConversation}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDeleteDialogue}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editConversationDialogueObj: state.game_admin.editConversationDialogueObj,
  deleteConversationDialogueObj: state.game_admin.deleteConversationDialogueObj,

  dialogueResponses: state.games.dialogueResponses,
	loadingDialogueResponses: state.games.loadingDialogueResponses
})

const mapDispatchToProps = {
  editConversationDialogue, resetEditConversationDialogue, deleteConversationDialogue, resetDeleteConversationDialogue, getDialogueResponses, resetDialogueResponses, getGameConversationDialogues, resetGameConversationDialogues
}

export default connect(mapStateToProps, mapDispatchToProps)(EditDialogue)
