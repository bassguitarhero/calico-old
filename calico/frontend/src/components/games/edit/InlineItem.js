import React, { Component } from 'react'

class InlineItem extends Component {
  render() {
    const { item } = this.props;

    return (
      <div style={{display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={item.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{item.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineItem;
