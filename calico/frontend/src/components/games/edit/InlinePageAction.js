import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

export class InlinePageAction extends Component {
  handleShowEditPageAction = (actionObj) => {
    this.props.showEditAction(actionObj);
  }

  render() {
    const { actionObj } = this.props;    
    return (
      <div style={{display: 'flex'}}>
        <div style={{flex: 4, paddingLeft: 5, paddingTop: 15}}>
          <span style={styles.defaultText}>{actionObj.action_type.name}: </span>
          {actionObj.go_to_page !== null && <span style={styles.defaultText}>{actionObj.go_to_page.name}: </span>}
          {actionObj.game_action_type !== null && <span style={styles.defaultText}>{actionObj.game_action_type.name}: </span>}
          <span style={styles.defaultText}>{actionObj.name}</span>
        </div>
        <div style={{flex: 1, textAlign: 'right', padding: 5}}>
          <button style={styles.buttonDefault} onClick={this.handleShowEditPageAction.bind(this, actionObj)}>
            Edit
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(InlinePageAction)
