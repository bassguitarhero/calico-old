import React, { Component } from 'react'

class InlineBackground extends Component {
  render() {
    const { background } = this.props;

    return (
      <div style={{display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={background.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{background.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineBackground;
