import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editGame, deleteGame, resetEditGame, resetDeleteGame, getGameTypes, resetGameTypes } from '../../../actions/game_admin';
import { getGameDetail, clearGameDetail, clearGames } from '../../../actions/games';

export class EditDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      slug: '',
      image: '',
      imageFile: null,
      description: '',
      publicAccess: '',
      published: false,
      uploadingData: false,
      gameTypesList: [],
      gameTypeSlug: '',
      replaceImage: false,
      removeImage: false,
      mapWidth: 800,
      mapHeight: 416,
      tileWidth: 32,
      tileHeight: 32,
      spriteWidth: 32,
      spriteHeight: 32
    }
  }

  handleShowReplaceImage = () => {
    this.setState({
      image: '',
      replaceImage: true,
      removeImage: true
    });
  }

  handleSelectChange = (e) => {
    this.setState({
      gameTypeSlug: e.target.value
    });
  }

  handleDelete = () => {
    if (window.confirm("Delete Game?")) {
      this.props.deleteGame(this.props.gameDetail.slug);
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		})
  }

  handleImageChange = e => this.setState({
    imageFile: e.target.files[0],
    removeImage: false
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditGame = () => {
    const { name, description, imageFile, publicAccess, published, gameTypeSlug, removeImage, mapWidth, mapHeight, tileWidth, tileHeight, spriteWidth, spriteHeight } = this.state;
    const game = { name, description, imageFile, publicAccess, published, gameTypeSlug, removeImage, mapWidth, mapHeight, tileWidth, tileHeight, spriteWidth, spriteHeight };
		if (name !== '') {
			if (window.confirm("Edit Game?")) {
				this.props.editGame(this.props.gameDetail.slug, game);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Games must have a Name.");
		}
  }

  componentDidMount() {
    const { gameDetail } = this.props;
    if (this.props.gameTypes === null) {
      this.props.getGameTypes();
    }
    this.setState({
      name: gameDetail.name,
      slug: gameDetail.slug,
      image: gameDetail.image,
      description: gameDetail.description,
      publicAccess: gameDetail.public,
      published: gameDetail.published,
      mapWidth: gameDetail.map_width,
      mapHeight: gameDetail.map_height,
      tileWidth: gameDetail.tile_width,
      tileHeight: gameDetail.tile_height,
      spriteWidth: gameDetail.sprite_width,
      spriteHeight: gameDetail.sprite_height
    });
    if (gameDetail.game_type !== null) {
      this.setState({
        gameTypeSlug: gameDetail.game_type.slug
      });
    }
  }

  componentDidUpdate(lastProps) {
    if (this.props.gameTypes !== null && lastProps.gameTypes === null) {
      var gameTypesList = [];
      for (var i = 0; i < this.props.gameTypes.length; i++) {
        gameTypesList.push(this.props.gameTypes[i]);
      }
      this.setState({gameTypesList});
    }
    if (this.props.editGameObj !== null && lastProps.editGameObj === null) {
      const { editGameObj } = this.props;
      if (editGameObj.image !== null) {
        this.setState({image: editGameObj.image});
      }
      this.props.resetEditGame();
      this.setState({uploadingData: false});
      this.props.clearGameDetail();
      this.props.getGameDetail(this.state.slug);
    }
    if (this.props.deleteGameObj !== null && lastProps.deleteGameObj === null) {
      this.props.resetDeleteGame();
      this.setState({uploadingData: false});
      this.props.clearGameDetail();
      this.props.clearGames();
      this.props.closeEditGame();
    }
  }

  componentWillUnmount() {
    this.props.resetGameTypes();
  }

  render() {
    function setTimestamp(s) {
      let fields = s.split('T');
      let timestamp = fields[1].split('.')[0];
      let date = fields[0].split('-');
      let time = timestamp.split(':');
      let period = '';
      if (time[0] > 12) {
        time[0] = time[0] - 12;
        period = "pm";
      } else if (time[0] == 12) {
        period = "pm";
      } else if (time[0] == 0) {
        period = "pm";
        time[0] = 12;
      } else {
        period = "am";
      }
      return `${time[0]}:${time[1]}${period}, ${date[1]}/${date[2]}/${date[0].substring(2,4)}`;
    }

    const { gameDetail, loadingGameDetail } = this.props;
    const { name, image, imageFile, description, publicAccess, published, uploadingData, gameTypesList } = this.state;
    const { gameTypes } = this.props;
    const { replaceImage, gameTypeSlug } = this.state;
    const { mapWidth, mapHeight, tileWidth, tileHeight, spriteWidth, spriteHeight } = this.state;

    return (
      <div>
        {!loadingGameDetail ? 
          <div>
            {(!replaceImage && (image !== null && image !== '')) ? 
              <div>
                <div style={{display: 'flex', paddingBottom: 20}}>
                  <div style={{flex: 1}}>
                    {image && 
                      <div><img src={gameDetail.thumbnail} style={{height: 100, width: 100}} /></div>
                    }
                    {(!replaceImage && (image !== null && image !== '')) && 
                      <div style={{paddingBottom: 10}}>
                        <button style={styles.buttonCheckbox} onClick={this.handleShowReplaceImage}>Remove / Replace</button>
                      </div>
                    }
                  </div>
                  <div style={{flex: 9, paddingLeft: 24}}>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Name:</span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="name"
                          value={name}
                          placeholder="Name"
                          onChange={this.handleChange} 
                          style={styles.textInput}
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Description:</span> 
                      </div>
                      <div style={{flex: 4}}>
                        <textarea 
                          id="description"
                          name="description" 
                          onChange={this.handleChange} 
                          value={description} 
                          placeholder="Description" 
                          style={styles.textareaInput}
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Type: </span>
                      </div>
                      <div style={{flex: 4}}>
                      {gameTypes !== null ? 
                        <select 
                          name="gameTypes" 
                          id="gameTypes"
                          className="gameTypesSelect"
                          onChange={this.handleSelectChange.bind(this)}
                          style={styles.selectInput}
                        >
                          <option value="">...</option>
                          {gameTypesList.map((gameType, index) => (
                            <option 
                              key={index} 
                              value={gameType.slug}
                              selected={gameType.slug == gameDetail.game_type.slug ? 'selected' : ''}
                            >
                              {gameType.name}
                            </option>
                          ))}
                        </select> :
                        <div>
                          Loading...
                        </div>
                      }
                      </div>
                    </div>
                    {(replaceImage || (image === '' || image === null)) && 
                      <div> 
                        <div style={{display: 'flex', paddingBottom: 10}}>
                          <div style={{flex: 1}}>
                            <label style={styles.defaultText} for="image">Image:</label>
                          </div>
                          <div style={{flex: 4}}>
                            <input 
                              style={styles.fileInput}
                              type="file" 
                              name="image" 
                              onChange={this.handleImageChange}
                            />
                          </div>
                        </div>
                      </div>
                    }
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Map Width: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="mapWidth" 
                          value={mapWidth} 
                          placeholder="Map Width" 
                          onChange={this.handleChange} 
                          style={styles.textInput} 
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Map Height: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="mapHeight" 
                          value={mapHeight} 
                          placeholder="Map Height" 
                          onChange={this.handleChange} 
                          style={styles.textInput} 
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Tile Width: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="tileWidth" 
                          value={tileWidth} 
                          placeholder="Tile Width" 
                          onChange={this.handleChange} 
                          style={styles.textInput} 
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Tile Height: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="tileHeight" 
                          value={tileHeight} 
                          placeholder="Tile Height" 
                          onChange={this.handleChange} 
                          style={styles.textInput} 
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Sprite Width: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="spriteWidth" 
                          value={spriteWidth} 
                          placeholder="Sprite Width" 
                          onChange={this.handleChange} 
                          style={styles.textInput} 
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1}}>
                        <span style={styles.defaultText}>Sprite Height: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          name="spriteHeight" 
                          value={spriteHeight} 
                          placeholder="Sprite Height" 
                          onChange={this.handleChange} 
                          style={styles.textInput} 
                        />
                      </div>
                    </div>
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                        <span style={styles.defaultText}>Public: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          type="checkbox" 
                          name="publicAccess" 
                          id="publicAccess" 
                          checked={publicAccess} 
                          onChange={this.handleCheckboxChange.bind(this)} 
                        />
                      </div>
                    </div> 
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                        <span style={styles.defaultText}>Published: </span>
                      </div>
                      <div style={{flex: 4}}>
                        <input 
                          type="checkbox" 
                          name="published" 
                          id="published" 
                          checked={published} 
                          onChange={this.handleCheckboxChange.bind(this)} 
                          style={styles.buttonCheckbox}
                        />
                      </div>
                    </div> 
                    <div style={{paddingBottom: 10}}>
                      <span style={styles.lastEditedText}>Last Edited: {setTimestamp(gameDetail.last_edited)}</span>
                    </div>
                  </div>
                </div>
              </div> :
              <div>
                <div style={{paddingLeft: 24}}>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Name:</span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="name"
                        value={name}
                        placeholder="Name"
                        onChange={this.handleChange} 
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Description:</span> 
                    </div>
                    <div style={{flex: 4}}>
                      <textarea 
                        id="description"
                        name="description" 
                        onChange={this.handleChange} 
                        value={description} 
                        placeholder="Description" 
                        style={styles.textareaInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Type: </span>
                    </div>
                    <div style={{flex: 4}}>
                    {gameTypes !== null ? 
                      <select 
                        name="gameTypes" 
                        id="gameTypes"
                        className="gameTypesSelect"
                        onChange={this.handleSelectChange.bind(this)}
                        style={styles.selectInput}
                      >
                        <option value="">...</option>
                        {gameTypesList.map((gameType, index) => (
                          <option 
                            key={index} 
                            value={gameType.slug}
                            selected={gameType.slug == gameDetail.game_type.slug ? 'selected' : ''}
                          >
                            {gameType.name}
                          </option>
                        ))}
                      </select> :
                      <div>
                        Loading...
                      </div>
                    }
                    </div>
                  </div>
                  {(replaceImage || (image === '' || image === null)) && 
                    <div> 
                      <div style={{display: 'flex', paddingBottom: 10}}>
                        <div style={{flex: 1}}>
                          <label style={styles.defaultText} for="image">Image:</label>
                        </div>
                        <div style={{flex: 4}}>
                          <input 
                            style={styles.fileInput}
                            type="file" 
                            name="image" 
                            onChange={this.handleImageChange}
                          />
                        </div>
                      </div>
                    </div>
                  }
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Map Width: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="mapWidth" 
                        value={mapWidth} 
                        placeholder="Map Width" 
                        onChange={this.handleChange} 
                        style={styles.textInput} 
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Map Height: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="mapHeight" 
                        value={mapHeight} 
                        placeholder="Map Height" 
                        onChange={this.handleChange} 
                        style={styles.textInput} 
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Tile Width: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="tileWidth" 
                        value={tileWidth} 
                        placeholder="Tile Width" 
                        onChange={this.handleChange} 
                        style={styles.textInput} 
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Tile Height: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="tileHeight" 
                        value={tileHeight} 
                        placeholder="Tile Height" 
                        onChange={this.handleChange} 
                        style={styles.textInput} 
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Sprite Width: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="spriteWidth" 
                        value={spriteWidth} 
                        placeholder="Sprite Width" 
                        onChange={this.handleChange} 
                        style={styles.textInput} 
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Sprite Height: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="spriteHeight" 
                        value={spriteHeight} 
                        placeholder="Sprite Height" 
                        onChange={this.handleChange} 
                        style={styles.textInput} 
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                      <span style={styles.defaultText}>Public: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        type="checkbox" 
                        name="publicAccess" 
                        id="publicAccess" 
                        checked={publicAccess} 
                        onChange={this.handleCheckboxChange.bind(this)} 
                      />
                    </div>
                  </div> 
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                      <span style={styles.defaultText}>Published: </span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        type="checkbox" 
                        name="published" 
                        id="published" 
                        checked={published} 
                        onChange={this.handleCheckboxChange.bind(this)} 
                        style={styles.buttonCheckbox}
                      />
                    </div>
                  </div> 
                  <div style={{paddingBottom: 10}}>
                    <span style={styles.lastEditedText}>Last Edited: {setTimestamp(gameDetail.last_edited)}</span>
                  </div>
                </div>
              </div>
            }
            
            {uploadingData ? 
              <div id="uploadProgress">
                &nbsp;
              </div> :
              <div style={{display: 'flex'}}>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.handleEditGame}>Save</button>
                </div>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.props.closeEditGame}>Cancel</button>
                </div>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
                </div>
              </div>
            }
          </div> :
          <div>
            <span>Loading...</span>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,
  loadingGameDetail: state.games.loadingGameDetail,

  editGameObj: state.game_admin.editGameObj,
  deleteGameObj: state.game_admin.deleteGameObj,

  gameTypes: state.game_admin.gameTypes
})

const mapDispatchToProps = {
  editGame,
  deleteGame,
  resetEditGame,
  resetDeleteGame,
  getGameDetail,
  clearGameDetail,
  clearGames,
  getGameTypes,
  resetGameTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(EditDetails)
