import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../../layout/Styles';

import { editConversation, deleteConversation } from '../../../actions/game_admin';
import { resetEditConversation, resetDeleteConversation } from '../../../actions/game_admin';
import { clearGameConversations, getGameConversations } from '../../../actions/games';
import { getGameConversationDialogues, resetGameConversationDialogues } from '../../../actions/games';

import NewDialogue from '../new/NewDialogue';
import EditDialogue from './EditDialogue';
import InlineDialogue from './InlineDialogue';
import EditDialogueResponse from './EditDialogueResponse';

export class EditConversation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      slug: '',
      showConversationDialogues: true,
      showNewConversationDialogue: false,
      showEditConversationDialogue: false,
      dialogueObj: null,
      showConversation: true,
      showConversationDialogueAndResponses: false
    }
  }

  handleShowConversation = () => {
    this.setState({
      showConversation: true,
      showEditConversationDialogue: false,
      dialogueObj: null
    });
  }

  handleShowEditConversationDialogue = (dialogue) => {
    this.setState({
      showConversation: false,
      showEditConversationDialogue: true,
      dialogueObj: dialogue
    });
  }

  handleShowConversationDialogues = () => {
    this.setState({
      showConversationDialogues: true,
      showNewConversationDialogue: false,
      showEditConversationDialogue: false,
      dialogueObj: null
    });
  }

  handleShowNewDialogue = () => {
    this.setState({
      showConversationDialogues: false,
      showNewConversationDialogue: true,
      showEditConversationDialogue: false
    })
  }

  handleDelete = () => {
    if (window.confirm("Delete Conversation?")) {
      this.props.deleteConversation(this.props.gameDetail.slug, this.props.conversation.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditConversation = () => {
    const { gameDetail } = this.props;
    const { name, description, imageFile, slug } = this.state;
    const conversation = { name, description, imageFile, slug };
		if (name !== '') {
			if (window.confirm("Edit Conversation?")) {
				this.props.editConversation(this.props.gameDetail.slug, conversation);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Conversations must have a Name.");
		}
  }

  componentDidMount() {
    const { gameDetail, conversation } = this.props;
    this.setState({
      name: conversation.name,
      description: conversation.description,
      image: conversation.image,
      slug: conversation.slug
    });
    this.props.getGameConversationDialogues(gameDetail.slug, conversation.slug);
  }

  componentDidUpdate(lastProps) {
    if (this.props.editConversationObj !== null && lastProps.editConversationObj === null) {
      this.props.resetEditConversation();
      this.props.clearGameConversations();
      this.props.getGameConversations(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showConversations();
    }
    if (this.props.deleteConversationObj !== null && lastProps.deleteConversationObj === null) {
      this.props.resetDeleteConversation();
      this.props.clearGameConversations();
      this.props.getGameConversations(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showConversations();
    }
  }

  componentWillUnmount() {
    this.props.resetGameConversationDialogues();
  }

  render() {
    const { conversation } = this.props;
    const { name, description, image, imageFile, uploadingData, dialogueObj } = this.state;
    const { gameConversationDialogues, loadingGameConversationDialogues } = this.props;
    const { showConversationDialogues, showNewConversationDialogue, showEditConversationDialogue } = this.state;
    const { showConversation, showConversationDialogueAndResponses } = this.state;

    return (
      <div>
        {showConversation && 
          <div>
            <div style={{display: 'flex', paddingBottom: 20}}>
              <div style={{flex: 1, display: 'flex'}}>
                <div style={{flex: 1}}>
                  <img src={conversation.thumbnail} style={{height: 100, width: 100}} />
                </div>
                <div style={{flex: 9, paddingLeft: 24}}>
                  <div style={{paddingBottom: 10}}>
                    <span style={styles.nameText}>Edit Conversation:</span>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Name:</span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="name"
                        value={name}
                        placeholder="Name"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Description:</span>
                    </div>
                    <div style={{flex: 4}}>
                      <textarea 
                        className="updateFormTextArea" 
                        id="description"
                        name="description" 
                        onChange={this.handleChange} 
                        value={description} 
                        placeholder="Description:"
                        style={styles.textareaInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}> 
                      <label for="image" style={styles.defaultText}>Image:</label>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        style={styles.fileInput}
                        type="file" 
                        name="image" 
                        onChange={this.handleImageChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div style={{flex: 1}}>
                {(!showNewConversationDialogue && !showEditConversationDialogue) && 
                  <div style={{padding: 10}}>
                    <button style={styles.buttonDefault} onClick={this.handleShowNewDialogue}>New Dialogue</button>
                  </div>
                }
                {showConversationDialogues && 
                  <div>
                    {!loadingGameConversationDialogues ? 
                      <div>
                        {gameConversationDialogues.length > 0 ? 
                          <div>
                            {gameConversationDialogues.map((dialogue, index) => (
                              <div style={{paddingBottom: 10}}>
                                <button style={styles.button} onClick={this.handleShowEditConversationDialogue.bind(this, dialogue)} key={index}>
                                  <InlineDialogue dialogue={dialogue} />
                                </button>
                              </div>
                            ))}
                          </div> :
                          <div style={{paddingBottom: 10}}>
                            <span style={styles.defaultText}>No Dialogue in Conversation.</span>
                          </div>
                        }
                      </div> :
                      <div style={{paddingBottom: 10}}>
                        <span style={styles.loadingText}>Loading Conversation Dialogues...</span>
                      </div>
                    }
                  </div>
                }
                {showNewConversationDialogue && 
                  <NewDialogue gameConversationDialogues={gameConversationDialogues} closeNewConversationDialogue={this.handleShowConversationDialogues} conversation={conversation} />
                }
              </div>
            </div>
            {uploadingData ? 
              <div id="uploadProgress" /> : 
              <div style={{display: 'flex', margin: '2rem 0'}}>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.handleEditConversation}>Save</button>
                </div>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.props.showConversations}>Cancel</button>
                </div>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
                </div>
              </div>
            }
          </div>
        }
        {showEditConversationDialogue && 
          <EditDialogue showConversation={this.handleShowConversation} dialogue={dialogueObj} conversation={conversation} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  gameConversationDialogues: state.games.gameConversationDialogues,
  loadingGameConversationDialogues: state.games.loadingGameConversationDialogues,

  editConversationObj: state.game_admin.editConversationObj,
  deleteConversationObj: state.game_admin.deleteConversationObj
})

const mapDispatchToProps = {
  editConversation,
  deleteConversation,
  resetEditConversation,
  resetDeleteConversation,
  clearGameConversations,
  getGameConversations,
  getGameConversationDialogues, 
  resetGameConversationDialogues
}

export default connect(mapStateToProps, mapDispatchToProps)(EditConversation)
