import React, { Component } from 'react'

class InlineCharacter extends Component {
  render() {
    const { character } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={character.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{character.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineCharacter;
