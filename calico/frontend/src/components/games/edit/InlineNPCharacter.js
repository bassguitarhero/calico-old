import React, { Component } from 'react'

class InlineNPCharacter extends Component {
  render() {
    const { npCharacter } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={npCharacter.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{npCharacter.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineNPCharacter;
