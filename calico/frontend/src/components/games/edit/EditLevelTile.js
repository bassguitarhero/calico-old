import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getLevelTiles, clearLevelTiles } from '../../../actions/games';
import { editLevelTile, resetEditLevelTile, deleteLevelTile, resetDeleteLevelTile } from '../../../actions/game_admin';

export class EditLevelTile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positionX: 0,
      positionY: 0,
      tileSlug: '',
      id: '',
      uploadingData: false,
      display: true
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		})
  }

  handleDelete = () => {
    if (window.confirm("Delete Level Tile?")) {
      this.props.deleteLevelTile(this.props.gameDetail.slug, this.props.level.slug, this.props.levelTile.tile.slug, this.props.levelTile.id);
    }
  }

  handleSelectTile = (tileSlug) => {
    this.setState({
      tileSlug: tileSlug
    });
  }

  handleEditLevelTile = () => {
    const { gameDetail, level } = this.props;
    const { positionX, positionY, tileSlug, id, display } = this.state;
    const positionData = { positionX, positionY }
    if (tileSlug !== '') {
			if (window.confirm("Edit Level Tile?")) {
        this.props.editLevelTile(gameDetail.slug, level.slug, tileSlug, positionData, id, display);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Level Tiles must have a Tile.");
		}
  }

  handleSelectChange = (e) => {
    this.setState({
      tileSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    const { levelTile } = this.props;
    this.setState({
      id: levelTile.id, 
      positionX: levelTile.position_x,
      positionY: levelTile.position_y,
      tileSlug: levelTile.tile.slug,
      display: levelTile.display
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editLevelTileObj !== null && lastProps.editLevelTileObj === null) {
      this.props.resetEditLevelTile();
      this.props.clearLevelTiles();
      this.props.getLevelTiles(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeEditLevelTile();
    }
    if (this.props.deleteLevelTileObj !== null && lastProps.deleteLevelTileObj === null) {
      this.props.resetDeleteLevelTile();
      this.props.clearLevelTiles();
      this.props.getLevelTiles(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeEditLevelTile();
    }
  }

  render() {
    const { level, gameTiles, loadingGameTiles, levelTile } = this.props;
    const { positionX, positionY, tileSlug, uploadingData, display } = this.state;

    return (
      <div style={{margin: '20px auto', width: `${level.width}px`}}>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>Edit Level Tile:</span>
        </div>
        <div style={{paddingBottom: 25, display: 'flex'}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position X:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionX"
                  value={positionX}
                  placeholder="Position X"
                  onChange={this.handleChange}
                  style={styles.textInput}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position Y:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionY"
                  value={positionY}
                  placeholder="Position Y"
                  onChange={this.handleChange}
                  style={styles.textInput}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Display:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
          <div style={{flex: 1}}>
            {!loadingGameTiles ? 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Tile:</span>
                </div>
                <div style={{flex: 3, paddingLeft: 24}}>
                  {gameTiles !== null ? 
                    <select 
                      name="gameTiles" 
                      id="gameTiles"
                      style={styles.selectInput}
                      onChange={this.handleSelectChange.bind(this)}
                      style={styles.selectInput}
                    >
                      <option value="">Select a Tile</option>
                      {gameTiles.map((tile, index) => (
                        <option 
                          key={index} 
                          value={tile.slug}
                          selected={tile.slug == levelTile.tile.slug ? 'selected' : ''}
                        >
                          {tile.name}
                        </option>
                      ))}
                    </select> :
                    <div>
                      <span style={styles.defaultText}>Loading...</span>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game Tiles...</span>
              </div>
            }
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex', paddingTop: 10, paddingBottom: 10}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditLevelTile}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeEditLevelTile}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 

  gameTiles: state.games.gameTiles,
  loadingGameTiles: state.games.loadingGameTiles,

  editLevelTileObj: state.game_admin.editLevelTileObj,
  deleteLevelTileObj: state.game_admin.deleteLevelTileObj
})

const mapDispatchToProps = {
  getLevelTiles, clearLevelTiles, editLevelTile, resetEditLevelTile, deleteLevelTile, resetDeleteLevelTile
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLevelTile)
