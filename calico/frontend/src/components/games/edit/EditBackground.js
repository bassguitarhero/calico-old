import React, { Component } from 'react'
import { connect } from 'react-redux'

import { editBackground, deleteBackground } from '../../../actions/game_admin';
import { resetEditBackground, resetDeleteBackground } from '../../../actions/game_admin';
import { clearGameBackgrounds, getGameBackgrounds } from '../../../actions/games';

export class EditBackground extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      slug: ''
    }
  }

  handleDelete = () => {
    if (window.confirm("Delete Background?")) {
      this.props.deleteBackground(this.props.gameDetail.slug, this.props.background.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditBackground = () => {
    const { gameDetail } = this.props;
    const { name, description, imageFile, slug } = this.state;
    const background = { name, description, imageFile, slug };
		if (name !== '') {
			if (window.confirm("Edit Background?")) {
				this.props.editBackground(this.props.gameDetail.slug, background);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Backgrounds must have a Name.");
		}
  }

  componentDidMount() {
    const { background } = this.props;
    this.setState({
      name: background.name,
      description: background.description,
      image: background.image,
      slug: background.slug
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editBackgroundObj !== null && lastProps.editBackgroundObj === null) {
      this.props.resetEditBackground();
      this.props.clearGameBackgrounds();
      this.props.getGameBackgrounds(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showBackgrounds();
    }
    if (this.props.deleteBackgroundObj !== null && lastProps.deleteBackgroundObj === null) {
      this.props.resetDeleteBackground();
      this.props.clearGameBackgrounds();
      this.props.getGameBackgrounds(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showBackgrounds();
    }
  }

  render() {
    const { background } = this.props;
    const { name, description, image, imageFile, uploadingData } = this.state;

    return (
      <div>
        <div style={{display: 'flex'}}>
          <div style={{flex: 1}}>
            <img src={background.thumbnail} style={{height: 100, width: 100}} />
          </div>
          <div style={{flex: 9, paddingLeft: 24}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Name: </span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Description: </span>
              </div>
              <div style={{flex: 4}}>
                <textarea 
                  className="updateFormTextArea" 
                  id="description"
                  name="description" 
                  onChange={this.handleChange} 
                  value={description} 
                  placeholder="Description:" 
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}> 
                <label for="image">Image:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> : 
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleEditBackground}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.props.showBackgrounds}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editBackgroundObj: state.game_admin.editBackgroundObj,
  deleteBackgroundObj: state.game_admin.deleteBackgroundObj
})

const mapDispatchToProps = {
  editBackground,
  deleteBackground,
  resetEditBackground,
  resetDeleteBackground,
  clearGameBackgrounds,
  getGameBackgrounds
}

export default connect(mapStateToProps, mapDispatchToProps)(EditBackground)

const styles = {
  textInput: {
    size: 40
  },
  textareaInput: {

  },
  fileInput: {

  },
  checkboxInput: {

  }
}