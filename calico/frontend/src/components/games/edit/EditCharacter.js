import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editCharacter, resetEditCharacter, deleteCharacter, resetDeleteCharacter } from '../../../actions/game_admin';
import { clearGameCharacters, getGameCharacters } from '../../../actions/games';

export class EditCharacter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: [],
      spriteSheet: '',
      spriteSheetFile: [],
      description: '',
      baseStrength: 5,
      baseSkill: 5,
      baseSpeed: 5,
      baseHealth: 5,
      baseWisdom: 5,
      baseLuck: 5,
      baseHP: 6,
      baseKeys: 0,
      baseBombs: 0,
      baseDamage: 10,
      slug: '',
      uploadingData: false,
      width: 0,
      height: 0
    }
  }

  handleDelete = () => {
    if (window.confirm("Delete Character?")) {
      this.props.deleteCharacter(this.props.gameDetail.slug, this.props.character.slug);
    }
  }
  
  handleSpriteSheetChange = e => this.setState({
		spriteSheetFile: e.target.files[0]
	});

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditCharacter = () => {
    const { name, description, imageFile, slug, spriteSheet, spriteSheetFile, width, height } = this.state;
    const { baseStrength, baseSpeed, baseSkill, baseWisdom, baseHealth, baseLuck, baseHP, baseBombs, baseKeys, baseDamage } = this.state;
    const character = { name, description, imageFile, slug, spriteSheet, spriteSheetFile, width, height, baseStrength, baseSpeed, baseSkill, baseWisdom, baseHealth, baseLuck, baseHP, baseBombs, baseKeys, baseDamage };
		if (name !== '') {
			if (window.confirm("Edit Character?")) {
				this.props.editCharacter(this.props.gameDetail.slug, character);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Characters must have a Name.");
		}
  }

  componentDidMount() {
    const { character } = this.props;
    this.setState({
      name: character.name,
      image: character.image,
      imageFile: [],
      spriteSheet: character.sprite_sheet,
      spriteSheetFile: [],
      description: character.description,
      baseStrength: character.base_strength,
      baseSkill: character.base_skill,
      baseSpeed: character.base_speed,
      baseHealth: character.base_health,
      baseWisdom: character.base_wisdom,
      baseLuck: character.base_luck,
      baseHP: character.base_hp,
      baseKeys: character.base_keys,
      baseBombs: character.base_bombs,
      baseDamage: character.base_damage,
      slug: character.slug,
      width: character.width,
      height: character.height
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editCharacterObj !== null && lastProps.editCharacterObj === null) {
      this.props.resetEditCharacter();
      this.props.clearGameCharacters();
      this.props.getGameCharacters(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showCharacters();
    }
    if (this.props.deleteCharacterObj !== null && lastProps.deleteCharacterObj === null) {
      this.props.resetDeleteCharacter();
      this.props.clearGameCharacters();
      this.props.getGameCharacters(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showCharacters();
    }
  }

  render() {
    const { character } = this.props;
    const { name, description, uploadingData } = this.state;
    const { baseStrength, baseSkill, baseSpeed, baseHealth, baseWisdom, baseLuck } = this.state;
    const { baseHP, baseKeys, baseBombs, baseDamage } = this.state;
    const { width, height } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>Edit Character:</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 20}}>
          <div style={{flex: 1}}>
            <div style={{paddingRight: 10}}>
              <img src={character.thumbnail} style={{height: 100, width: 100}} />
            </div>
          </div>
          <div style={{flex: 4, paddingLeft: 24, display: 'flex'}}>
            <div style={{flex: 1}}>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 2}}>
                  <span style={styles.defaultText}>Name:</span>
                </div>
                <div style={{flex: 3}}>
                  <input 
                    name="name"
                    value={name}
                    placeholder="Name"
                    style={styles.textInput}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 2}}>
                  <span style={styles.defaultText}>Description:</span>
                </div>
                <div style={{flex: 3}}>
                  <textarea 
                    style={styles.textareaInput}
                    className="updateFormTextArea" 
                    id="description"
                    name="description" 
                    onChange={this.handleChange} 
                    value={description} 
                    placeholder="Description" 
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 2}}>
                  <label for="image" style={styles.defaultText}>Image:</label>
                </div>
                <div style={{flex: 3}}>
                  <input 
                    style={styles.fileInput}
                    type="file" 
                    name="image" 
                    onChange={this.handleImageChange}
                  />
                </div>
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <div style={{paddingRight: 10}}>
              <img src={character.sprite_sheet_thumbnail} style={{height: 100, width: 100}} />
            </div>
          </div>
          <div style={{flex: 4}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <label for="spriteSheet" style={styles.defaultText}>Sprite Sheet:</label>
              </div>
              <div style={{flex: 3}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="spriteSheet" 
                  onChange={this.handleSpriteSheetChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Sprite Width:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="width"
                  value={width}
                  placeholder="Width"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Sprite Height:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="height"
                  value={height}
                  placeholder="Height"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div style={{display: 'flex', paddingTop: 20, paddingBottom: 25}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Strength:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseStrength"
                  value={baseStrength}
                  placeholder="Base Strength"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Skill:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseSkill"
                  value={baseSkill}
                  placeholder="Base Skill"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Speed:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseSpeed"
                  value={baseSpeed}
                  placeholder="Base Speed"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Health:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseHealth"
                  value={baseHealth}
                  placeholder="Base Health"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Wisdom:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseWisdom"
                  value={baseWisdom}
                  placeholder="Base Wisdom"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Luck:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseLuck"
                  value={baseLuck}
                  placeholder="Base Luck"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>HP:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseHP"
                  value={baseHP}
                  placeholder="Base HP"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Keys:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseKeys"
                  value={baseKeys}
                  placeholder="Base Keys"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Bombs:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseBombs"
                  value={baseBombs}
                  placeholder="Base Bombs"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Damage:</span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="baseDamage"
                  value={baseDamage}
                  placeholder="Base Damage"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditCharacter}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.showCharacters}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editCharacterObj: state.game_admin.editCharacterObj,
  deleteCharacterObj: state.game_admin.deleteCharacterObj
})

const mapDispatchToProps = {
  editCharacter,
  resetEditCharacter,
  deleteCharacter,
  resetDeleteCharacter,

  clearGameCharacters,
  getGameCharacters
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCharacter)
