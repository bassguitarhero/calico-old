import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getLevelActions, getLevelActionTypes, newLevelAction } from '../../../actions/game_admin';

export class EditLevelActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      levelChangeSlug: '',
      levelActionTypeSlug: '',
      levelOptions: [],
      uploadingData: false,
      requireComplete: false
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		})
  }

  handleSelectGameLevel = (e) => {
    this.setState({
      levelChangeSlug: e.target.value
    });
  }

  handleSelectLevelActionType = (e) => {
    this.setState({
      levelActionTypeSlug: e.target.value
    })
  }

  handleNewLevelAction = () => {
    const { levelChangeSlug, levelActionTypeSlug, requireComplete } = this.state;
    // slug, level, levelActionType
    const { gameDetail, level, gameLevels, levelActionTypes } = this.props;
    if (levelActionTypeSlug === 'change-level' && levelChangeSlug === '') {
      alert("You must select a Level");
    } else {
      this.props.newLevelAction(gameDetail.slug, level, levelActionTypeSlug, levelChangeSlug, requireComplete);
       this.setState({
        uploadingData: true
      });
    }
  }

  componentDidMount() {
    const { gameDetail, level } = this.props;
    this.props.getLevelActions(gameDetail.slug, level);
    this.props.getLevelActionTypes();
  }

  componentDidUpdate(lastProps) {
    if (1 < 0) {
      this.setState({
        uploadingData: false
      })
    }
    if (this.props.levelActions !== null && lastProps.levelActions === null) {
      const levelOptions = [];
      const { level, gameLevels, levelActions, } = this.props;
      var match = 0;
      for (var i = 0; i < gameLevels.length; i++) {
        if (gameLevels[i].slug !== level.slug) {
          for (var j = 0; j < levelActions.length; j++) {
            if (levelActions[j].change_level !== null) {
              if (gameLevels[i].slug === levelActions[j].change_level.slug) {
                match += 1;
              }
            }
          }
          if (match === 0) {
            levelOptions.push(gameLevels[i]);
          }
          match = 0;
        }
      }
      this.setState({levelOptions});
    }
  }

  render() {
    const { gameDetail, level, levelActions, levelActionTypes, gameLevels } = this.props;
    const { levelOptions, levelChangeSlug, levelActionTypeSlug, uploadingData, requireComplete } = this.state;

    return (
      <div>
        {levelActions !== null ? 
          <div>
            <div><span>Level Actions:</span></div>
            {levelActions.length > 0 ? 
              <ul>
                {levelActions.map((action, index) => (
                  <li key={index}>
                    <div><span>{action.action_type.name}</span></div>
                    {action.action_type.slug === 'change-level' && 
                      <div><span>To: {action.change_level.order}. {action.change_level.name}</span></div>
                    }
                    {action.complete_required ? 
                      <div>
                        <span>Complete Required</span>
                      </div> :
                      <div>
                        <span>Complete not Required</span>
                      </div>
                    }
                  </li>
                ))}
              </ul> :
              <div><span>No Level Actions</span></div>
            }
          </div> :
          <div>
            <span>Loading Level Actions...</span>
          </div>
        }
        {levelActionTypes !== null ? 
          <div>
            <div>
              <select 
                name="levelActionTypes" 
                id="levelActionTypes"
                className="levelActionTypesSelect"
                onChange={this.handleSelectLevelActionType.bind(this)}
              >
                <option value="">...</option>
                {levelActionTypes.map((actionType, index) => (
                  <option key={index} value={actionType.slug}>{actionType.name}</option>
                ))}
              </select>
            </div>
            {levelActionTypeSlug === "change-level" && 
              <div>
                <div><span>Select Level</span></div>
                <select 
                  name="gameLevels" 
                  id="gameLevels"
                  className="gameLevelSelect"
                  onChange={this.handleSelectGameLevel.bind(this)}
                >
                  <option value="">...</option>
                  {levelOptions.map((levelOption, index) => (
                    <option value={levelOption.slug}>{levelOption.name}</option>
                  ))}
                </select>
              </div>
            }
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}><span>Require Complete: </span></div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="requireComplete" 
                  id="requireComplete" 
                  checked={requireComplete} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div>
              {uploadingData ? 
                <div>
                  <div id="uploadProgress" />
                </div> :
                <div>
                  <button onClick={this.handleNewLevelAction}>New Level Action</button>
                </div>
              }
            </div>
          </div> :
          <div>
            <div><span>Loading Level Action Types...</span></div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,
  gameLevels: state.games.gameLevels,

  levelActions: state.game_admin.levelActions,
  loadingLevelActions: state.game_admin.loadingLevelActions, 

  levelActionTypes: state.game_admin.levelActionTypes
})

const mapDispatchToProps = {
  getLevelActions,
  getLevelActionTypes,
  newLevelAction
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLevelActions)
