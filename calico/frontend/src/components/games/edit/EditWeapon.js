import React, { Component } from 'react'
import { connect } from 'react-redux'

import { editWeapon, deleteWeapon } from '../../../actions/game_admin';
import { resetEditWeapon, resetDeleteWeapon } from '../../../actions/game_admin';
import { clearGameWeapons, getGameWeapons } from '../../../actions/games';

export class EditWeapon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      slug: ''
    }
  }

  handleDelete = () => {
    if (window.confirm("Delete Weapon?")) {
      this.props.deleteWeapon(this.props.gameDetail.slug, this.props.weapon.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditWeapon = () => {
    const { name, description, imageFile, slug } = this.state;
    const weapon = { name, description, imageFile, slug };
		if (name !== '') {
			if (window.confirm("Edit Weapon?")) {
				this.props.editWeapon(this.props.gameDetail.slug, weapon);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Weapons must have a Name.");
		}
  }

  componentDidMount() {
    const { weapon } = this.props;
    this.setState({
      name: weapon.name,
      description: weapon.description,
      image: weapon.image,
      slug: weapon.slug
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editWeaponObj !== null && lastProps.editWeaponObj === null) {
      this.props.resetEditWeapon();
      this.props.clearGameWeapons();
      this.props.getGameWeapons(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showWeapons();
    }
    if (this.props.deleteWeaponObj !== null && lastProps.deleteWeaponObj === null) {
      this.props.resetDeleteWeapon();
      this.props.clearGameWeapons();
      this.props.getGameWeapons(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showWeapons();
    }
  }

  render() {
    const { weapon } = this.props;
    const { name, description, image, imageFile, uploadingData } = this.state;

    return (
      <div>
        <div style={{display: 'flex'}}>
          <div style={{flex: 1}}>
            <img src={weapon.thumbnail} style={{height: 100, width: 100}} />
          </div>
          <div style={{flex: 9, paddingLeft: 24}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Name: </span>
              </div>
              <div style={{flex: 4}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span>Description: </span>
              </div>
              <div style={{flex: 4}}>
                <textarea 
                  className="updateFormTextArea" 
                  id="description"
                  name="description" 
                  onChange={this.handleChange} 
                  value={description} 
                  placeholder="Description:" 
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}> 
                <label for="image">Image:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleEditWeapon}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.props.showWeapons}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editWeaponObj: state.game_admin.editWeaponObj,
  deleteWeaponObj: state.game_admin.deleteWeaponObj
})

const mapDispatchToProps = {
  editWeapon,
  deleteWeapon,
  resetEditWeapon,
  resetDeleteWeapon,
  clearGameWeapons,
  getGameWeapons
}

export default connect(mapStateToProps, mapDispatchToProps)(EditWeapon)

const styles = {
  textInput: {
    size: 40
  },
  textareaInput: {

  },
  fileInput: {

  },
  checkboxInput: {

  }
}