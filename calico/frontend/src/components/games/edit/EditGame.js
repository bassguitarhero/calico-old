import React, { Component } from 'react'
import { connect } from 'react-redux'

import { editGame  } from '../../../actions/games';

import EditDetails from './EditDetails';
import EditCharacters from './EditCharacters';
import EditLevels from './EditLevels';
import EditCutscenes from './EditCutscenes';
import EditConversations from './EditConversations';
import EditBackgrounds from './EditBackgrounds';
import EditWeapons from './EditWeapons';
import EditItems from './EditItems';
import EditPages from './EditPages';

import { styles } from '../../layout/Styles';

export class EditGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDetails: true,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false,
      showChooseYourOwnAdventure: false,
      showDungeonCrawler: false,
      showPages: false
    }
  }

  handleShowDetails = () => {
    this.setState({
      showDetails: true,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false,
      showPages: false
    });
  }

  handleShowPages = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false,
      showPages: true
    });
  }

  handleShowCharacters = () => {
    this.setState({
      showDetails: false,
      showCharacters: true,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false
    });
  }

  handleShowLevels = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: true,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false
    });
  }

  handleShowCutscenes = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: false,
      showCutscenes: true,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false
    });
  }

  handleShowConversations = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: true,
      showBackgrounds: false,
      showWeapons: false,
      showItems: false
    });
  }

  handleShowBackgrounds = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: true,
      showWeapons: false,
      showItems: false
    });
  }

  handleShowWeapons = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: true,
      showItems: false
    });
  }

  handleShowItems = () => {
    this.setState({
      showDetails: false,
      showCharacters: false,
      showLevels: false,
      showCutscenes: false,
      showConversations: false,
      showBackgrounds: false,
      showWeapons: false,
      showItems: true
    });
  }

  componentDidMount() {
    const { gameDetail } = this.props;
    if (gameDetail.game_type.slug === 'choose-your-own-adventure') {
      this.setState({
        showChooseYourOwnAdventure: true
      });
    }
    if (gameDetail.game_type.slug === 'dungeon-crawler') {
      this.setState({
        showDungeonCrawler: true
      });
    }
  }

  render() {
    const { showDetails, showCharacters, showLevels, showCutscenes, showConversations, showBackgrounds, showWeapons, showItems, showPages } = this.state;
    const { showChooseYourOwnAdventure, showDungeonCrawler } = this.state;

    return (
      <div>
        <div style={{display: 'flex', paddingBottom: 24}}>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showDetails ? 
              <div style={{paddingTop: 10}}><span style={styles.defaultText}>Details</span></div> :
              <button style={styles.buttonNav} onClick={this.handleShowDetails}>Details</button>
            }
          </div>
          {showChooseYourOwnAdventure && 
            <div style={{flex: 1, textAlign: 'center'}}>
              {showPages ? 
                <div style={{paddingTop: 10}}><span style={styles.defaultText}>Pages</span></div> :
                <button style={styles.buttonNav} onClick={this.handleShowPages}>Pages</button>
              }
            </div>
          }
          {showDungeonCrawler && 
            <div style={{flex: 8, display: 'flex'}}>
              <div style={{flex: 1, textAlign: 'center'}}>
                {showCharacters ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Characters</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowCharacters}>Characters</button>
                }
              </div>
              <div style={{flex: 1, textAlign: 'center'}}>
                {showLevels ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Levels</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowLevels}>Levels</button>
                }
              </div>
              <div style={{flex: 1, textAlign: 'center'}}>
                {showConversations ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Conversations</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowConversations}>Conversations</button>
                }
              </div>
              <div style={{flex: 1, textAlign: 'center'}}>
                {showCutscenes ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Cutscenes</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowCutscenes}>Cutscenes</button>
                }
              </div>
              {/* <div style={{flex: 1, textAlign: 'center'}}>
                {showBackgrounds ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Backgrounds</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowBackgrounds}>Backgrounds</button>
                }
              </div> */}
              <div style={{flex: 1, textAlign: 'center'}}>
                {showWeapons ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Weapons</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowWeapons}>Weapons</button>
                }
              </div>
              <div style={{flex: 1, textAlign: 'center'}}>
                {showItems ? 
                  <div style={{paddingTop: 10}}><span style={styles.defaultText}>Items</span></div> :
                  <button style={styles.buttonNav} onClick={this.handleShowItems}>Items</button>
                }
              </div>
            </div>
          }
          <div style={{flex: 1, textAlign: 'right'}}>
            <button style={styles.buttonDefault} onClick={this.props.closeEditGame}>Exit</button>
          </div>
        </div>
        {showDetails && 
          <EditDetails closeEditGame={this.props.closeEditGame} />
        }
        {showPages && 
          <EditPages />
        }
        {showCharacters && 
          <EditCharacters />
        }
        {showLevels && 
          <EditLevels />
        }
        {showCutscenes && 
          <EditCutscenes />
        }
        {showConversations && 
          <EditConversations />
        }
        {showBackgrounds && 
          <EditBackgrounds />
        }
        {showWeapons && 
          <EditWeapons />
        }
        {showItems && 
          <EditItems />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail
})

const mapDispatchToProps = {
  editGame
}

export default connect(mapStateToProps, mapDispatchToProps)(EditGame)
