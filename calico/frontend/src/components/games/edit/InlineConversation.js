import React, { Component } from 'react'

class InlineConversation extends Component {
  render() {
    const { conversation } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={conversation.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <span>{conversation.name}</span>
        </div>
      </div>
    )
  }
}

export default InlineConversation;
