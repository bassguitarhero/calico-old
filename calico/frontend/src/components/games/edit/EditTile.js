import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getGameTiles, clearGameTiles } from '../../../actions/games';
import { editTile, resetEditTile, deleteTile, resetDeleteTile } from '../../../actions/game_admin';

export class EditTile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      image: '',
      imageFile: [],
      height: 0,
      width: 0,
      walkable: true,
      uploadingData: false,
      display: true
    }
  }

  handleDelete = () => {
    if (window.confirm("Delete Tile?")) {
      this.props.deleteTile(this.props.gameDetail.slug, this.props.tile.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditTile = () => {
    const { gameDetail } = this.props;
    const { name, imageFile, slug, display, height, width, walkable } = this.state;
    const tile = { name, imageFile, slug, height, width, display, walkable };
		if (name !== '') {
			if (window.confirm("Edit Tile?")) {
				this.props.editTile(gameDetail.slug, tile);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Tiles must have a Name.");
		}
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleWalkableChange = () => {
    this.setState({
      walkable: !this.state.walkable
    });
  }

  componentDidMount() {
    const { tile } = this.props;
    this.setState({
      name: tile.name,
      image: tile.image,
      slug: tile.slug,
      height: tile.height,
      width: tile.width,
      walkable: tile.walkable,
      display: tile.display
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editGameTileObj !== null && lastProps.editGameTileObj === null) {
      this.props.resetEditTile();
      this.props.clearGameTiles();
      this.props.getGameTiles(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showTiles();
    }
    if (this.props.deleteGameTileObj !== null && lastProps.deleteGameTileObj === null) {
      this.props.resetDeleteTile();
      this.props.clearGameTiles();
      this.props.getGameTiles(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showTiles();
    }
  }

  render() {
    const { tile } = this.props
    const { name, height, width, walkable, uploadingData, display } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>Edit Tile:</span>
        </div>
        <div style={{display: 'flex', paddingBottom: 25}}>
          <div style={{flex: 1, display: 'flex'}}>
            <div style={{flex: 1}}>
              <img src={tile.thumbnail} style={{height: 100, width: 100}} />
            </div>
            <div style={{flex: 9, paddingLeft: 24}}>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Name </span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="name"
                    value={name}
                    placeholder="Name"
                    style={styles.textInput}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <label for="image" style={styles.defaultText}>Image:</label>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    style={styles.fileInput}
                    type="file" 
                    name="image" 
                    onChange={this.handleImageChange}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Height:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="height"
                    value={height}
                    placeholder="Height"
                    style={styles.textInput}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Width:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="width"
                    value={width}
                    placeholder="Width"
                    style={styles.textInput}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            </div>
          </div> 
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <label for="display" style={styles.defaultText}>Display:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <label for="walkable" style={styles.defaultText}>Walkable:</label>
              </div>
              <div style={{flex: 4}}>
                <input 
                  type="checkbox" 
                  name="walkable" 
                  id="walkable" 
                  checked={walkable} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
        </div>       
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditTile}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.showTiles}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editGameTileObj: state.game_admin.editGameTileObj,
  deleteGameTileObj: state.game_admin.deleteGameTileObj
})

const mapDispatchToProps = {
  getGameTiles, clearGameTiles, editTile, resetEditTile, deleteTile, resetDeleteTile
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTile)
