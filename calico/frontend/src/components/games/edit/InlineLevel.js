import React, { Component } from 'react'

class InlineLevel extends Component {
  render() {
    const { level } = this.props;

    return (
      <div style={{margin: 10, display: 'flex'}}>
        <div style={{flex: 1}}>
          <img src={level.thumbnail} style={{height: 100, width: 100}} />
        </div>
        <div style={{flex: 9, paddingLeft: 24}}>
          <div>
            <span style={{color: '#000'}}>{level.name}</span>
          </div>
          <div>
            <span style={{color: '#000'}}>Order: {level.order}</span>
          </div>
        </div>
      </div>
    )
  }
}

export default InlineLevel;
