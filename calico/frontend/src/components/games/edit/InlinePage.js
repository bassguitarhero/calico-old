import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

export class InlinePage extends Component {
  handleGetPage = () => {
    this.props.editPage(this.props.page);
  }

  render() {
    function truncateText(s) {
    if (s.length > 150) {
      return s.substr(0, 150) + '...';
    } else {
      return s;
    }
  }
    const { page } = this.props;

    return (
      <div>
        {(page.image !== null && page.image !== '') ? 
          <div style={{padding: 5, display: 'flex'}}>
            <div style={{flex: 1, padding: 5}}>
              {page.image !== null && 
                <img src={page.thumbnail} style={{width: 150, height: 'auto'}} />
              }
            </div>
            <div style={{flex: 4, padding: 5}}>
              <div style={styles.defaultTextContainer}>
                <span style={styles.defaultText}>{page.page_number}: {page.name}</span>
              </div>
              {(page.youtube_id !== null && page.youtube_id !== 'null') && 
                <div style={styles.defaultTextContainer}>
                  <span style={styles.defaultText}>YouTube ID: {page.youtube_id}</span>
                </div>
              }
              <div style={styles.defaultTextContainer}>
                <span style={styles.defaultText}>{truncateText(page.description)}</span>
              </div>
              <div style={styles.defaultTextContainer}>
                <button style={styles.buttonDefault} onClick={this.handleGetPage}>Edit Page</button>
              </div>
            </div>
          </div> :
          <div>
            <div style={{padding: 5}}>
              <div style={styles.defaultTextContainer}>
                <span style={styles.defaultText}>{page.page_number}: {page.name}</span>
              </div>
              {(page.youtube_id !== null && page.youtube_id !== 'null') && 
                <div style={styles.defaultTextContainer}>
                  <span style={styles.defaultText}>YouTube ID: {page.youtube_id}</span>
                </div>
              }
              <div style={styles.defaultTextContainer}>
                <span style={styles.defaultText}>{truncateText(page.description)}</span>
              </div>
              <div style={styles.defaultTextContainer}>
                <button style={styles.buttonDefault} onClick={this.handleGetPage}>Edit Page</button>
              </div>
            </div>
          </div>
        }
      </div>
      
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(InlinePage)
