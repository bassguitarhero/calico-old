import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditConversation from './EditConversation';
import InlineConversation from './InlineConversation';

import NewConversation from '../new/NewConversation';

import { styles } from '../../layout/Styles';

export class EditConversations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showConversations: true,
      showEditConversation: false,
      showNewConversation: false,
      editConversation: null
    }
  }

  handleCloseNewConversation = () => {
    this.setState({
      showConversations: true,
      showEditConversation: false,
      showNewConversation: false,
      editConversation: null
    });
  }

  handleShowNewConversation = () => {
    this.setState({
      showConversations: false,
      showEditConversation: false,
      showNewConversation: true,
      editConversation: null
    });
  }

  handleShowConversations = () => {
    this.setState({
      showConversations: true,
      showEditConversation: false,
      showNewConversation: false,
      editConversation: null
    });
  }

  handleShowEditConversation = (conversation) =>{
    this.setState({
      showConversations: false,
      showEditConversation: true,
      showNewConversation: false,
      editConversation: conversation
    });
  }

  render() {
    const { gameConversations, loadingGameConversations } = this.props;
    const { showConversations, showEditConversation, showNewConversation, editConversation } = this.state;

    return (
      <div>
        {!showEditConversation && !showNewConversation && 
          <div style={{paddingBottom: 24}}>
            <button style={styles.buttonDefault} onClick={this.handleShowNewConversation}>New</button>
          </div>
        }
        {showConversations && 
          <div>
            {!loadingGameConversations ? 
              <div>
                {gameConversations.length > 0 ? 
                  <div>
                    {gameConversations.map((conversation, index) => (
                      <button style={styles.button} key={index} onClick={this.handleShowEditConversation.bind(this, conversation)}>
                        <InlineConversation conversation={conversation} />
                      </button>
                    ))}
                  </div> :
                  <div>
                    <span>No Game Conversations!</span>
                  </div>
                }
              </div> :
              <div>
                <span>Loading...</span>
              </div>
            }
          </div>
        }
        {showEditConversation && 
          <EditConversation showConversations={this.handleShowConversations} conversation={editConversation} />
        }
        {showNewConversation && 
          <NewConversation closeNewConversation={this.handleCloseNewConversation} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameConversations: state.games.gameConversations,
  loadingGameConversations: state.games.loadingGameConversations,
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditConversations)

