import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../../layout/Styles';

import { editCutscene, deleteCutscene } from '../../../actions/game_admin';
import { resetEditCutscene, resetDeleteCutscene, getCutsceneActionTypes, resetCutsceneActionTypes, getGameActionTypes, resetGameActionTypes, getLevelActionTypes } from '../../../actions/game_admin';
import { clearGameCutscenes, getGameCutscenes, getCutsceneActions, resetCutsceneActions } from '../../../actions/games';
import { newCutsceneAction, resetNewCutsceneAction, editCutsceneAction, resetEditCutsceneAction, deleteCutsceneAction, resetDeleteCutsceneAction } from '../../../actions/game_admin';

import EditCutsceneAction from './EditCutsceneAction';

export class EditCutscene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: null,
      uploadingData: false,
      slug: '',
      fieldYouTube: '',
      fieldVimeo: '',
      fieldArchive: '',
      width: 0,
      height: 0,
      autoplay: true,
      loop: false,
      gameActionTypeSlug: '',
      cutsceneActionTypeSlug: '',
      changeCutsceneSlug: '',
      cutsceneOptions: [],
      levelOptions: [],
      gameOptions: [],
      levelSlug: '',
      requireComplete: false,
      actionName: '',
      autoSelect: false,
      showNewCutsceneActions: true,
      showAllCutsceneActions: true,
      showEditCutsceneAction: false,
      cutsceneActionObj: null
    }
  }

  handleShowEditCutsceneAction = (cutsceneAction) => {
    this.setState({
      showNewCutsceneActions: false,
      showAllCutsceneActions: false,
      showEditCutsceneAction: true,
      cutsceneActionObj: cutsceneAction
    })
  }

  handleShowAllCutsceneActions = () => {
    this.setState({
      showNewCutsceneActions: true,
      showAllCutsceneActions: true,
      showEditCutsceneAction: false,
      cutsceneActionObj: null
    });
  }

  handleCreateNewCutsceneAction = () => {
    const { gameActionTypeSlug, cutsceneActionTypeSlug, changeCutsceneSlug, levelSlug, requireComplete, actionName } = this.state;
    const { gameDetail, cutscene } = this.props;
    const cutsceneActionData = { cutscene, gameActionTypeSlug, cutsceneActionTypeSlug, changeCutsceneSlug, levelSlug, requireComplete, actionName };
    if (actionName !== '') {
      if (window.confirm("Create New Cutscene Action?")) {
        this.props.newCutsceneAction(gameDetail.slug, cutsceneActionData);
        this.setState({uploadingData: true});
      }
    } else {
      alert('Cutscene Actions must have a name.');
    }
  }

  handleCheckboxChange = (e) => {
    if (e.target.name === 'autoSelect' && this.state.autoSelect === false) {
      if (window.confirm("Set as Auto Select?")) {
        this.setState({
          [e.target.name]: !this.state[e.target.name]
        });
      }
    } else {
      this.setState({
        [e.target.name]: !this.state[e.target.name]
      });
    }
  }

  handleSelectGameActionType = (e) => {
    this.setState({
      gameActionTypeSlug: e.target.value
    });
  }

  handleSelectGameLevel = (e) => {
    this.setState({
      levelSlug: e.target.value
    });
  }

  handleSelectCutsceneOption = (e) => {
    this.setState({
      changeCutsceneSlug: e.target.value
    });
  }

  handleSelectCutsceneActionType = (e) => {
    this.setState({
      cutsceneActionTypeSlug: e.target.value
    });
  }

  handleDelete = () => {
    if (window.confirm("Delete Cutscene?")) {
      this.props.deleteCutscene(this.props.gameDetail.slug, this.props.cutscene.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'actionName'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditCutscene = () => {
    const { gameDetail } = this.props;
    const { name, description, imageFile, slug, fieldYouTube, fieldVimeo, fieldArchive, width, height, autoplay, loop } = this.state;
    const cutsceneData = { name, description, imageFile, slug, fieldYouTube, fieldVimeo, fieldArchive, width, height, autoplay, loop };
		if (name !== '') {
			if (window.confirm("Edit Cutscene?")) {
				this.props.editCutscene(gameDetail.slug, cutsceneData);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Cutscenes must have a Name.");
		}
  }

  handleSetGameOptions = () => {
    var gameOptions = [];
    const { gameActionTypes } = this.props;
    for (var h = 0; h < gameActionTypes.length; h++) {
      gameOptions.push(gameActionTypes[h]);
    }
    this.setState({
      gameOptions
    });
  }

  handleSetLevelOptions = () => {
    var levelOptions = [];
    const { gameLevels } = this.props;
    for (var b = 0; b < gameLevels.length; b++) {
      levelOptions.push(gameLevels[b]);
    }
    this.setState({
      levelOptions
    });
  }

  handleSetCutsceneOptions = () => {
    const { gameCutscenes, cutscene } = this.props;
    var cutsceneOptions = [];
    for (var j = 0; j < gameCutscenes.length; j++) {
      if (cutscene.name !== gameCutscenes[j].name) {
        cutsceneOptions.push(gameCutscenes[j]);
      }
    }
    this.setState({
      cutsceneOptions
    });
  }

  componentDidMount() {
    const { cutscene, gameDetail } = this.props;
    this.setState({
      name: cutscene.name,
      description: cutscene.description,
      image: cutscene.image,
      slug: cutscene.slug,
      fieldYouTube: cutscene.url_youtube !== null ? cutscene.url_youtube : '',
      fieldVimeo: cutscene.url_vimeo !== null ? cutscene.url_vimeo : '',
      fieldArchive: cutscene.url_archive !== null ? cutscene.url_archive : '',
      width: cutscene.width,
      height: cutscene.height,
      autoplay: cutscene.autoplay,
      loop: cutscene.loop
    });
    this.props.getCutsceneActions(gameDetail.slug, cutscene.slug);
    this.props.getCutsceneActionTypes();
    this.props.getGameActionTypes();
    this.props.getLevelActionTypes();
    this.handleSetCutsceneOptions();
    // this.handleSetLevelOptions();
    // this.handleSetGameOptions();
  }

  componentDidUpdate(lastProps) {
    if (this.props.editCutsceneObj !== null && lastProps.editCutsceneObj === null) {
      this.props.resetEditCutscene();
      this.props.clearGameCutscenes();
      this.props.getGameCutscenes(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showCutscenes();
    }
    if (this.props.deleteCutsceneObj !== null && lastProps.deleteCutsceneObj === null) {
      this.props.resetDeleteCutscene();
      this.props.clearGameCutscenes();
      this.props.getGameCutscenes(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showCutscenes();
    }
    if (this.props.newCutsceneActionObj !== null && lastProps.newCutsceneActionObj === null) {
      const { cutscene, gameDetail } = this.props;
      this.props.resetCutsceneActions();
      this.props.getCutsceneActions(gameDetail.slug, cutscene.slug);
      this.props.resetNewCutsceneAction();
      this.setState({uploadingData: false});
      this.setState({
        gameActionTypeSlug: '',
        cutsceneActionTypeSlug: '',
        changeCutsceneSlug: '',
        levelSlug: '',
        requireComplete: false,
        actionName: ''
      });
    }
    if (this.props.editCutsceneActionObj !== null && lastProps.editCutsceneActionObj === null) {
      const { cutscene, gameDetail } = this.props;
      this.props.resetCutsceneActions();
      this.props.getCutsceneActions(gameDetail.slug, cutscene.slug);
      this.props.resetEditCutsceneAction();
      this.setState({uploadingData: false});
      this.setState({
        gameActionTypeSlug: '',
        cutsceneActionTypeSlug: '',
        changeCutsceneSlug: '',
        levelSlug: '',
        requireComplete: false,
        actionName: ''
      });
      this.handleShowAllCutsceneActions();
    }
    if (this.props.deleteCutsceneActionObj !== null && lastProps.deleteCutsceneActionObj === null) {
      const { cutscene, gameDetail } = this.props;
      this.props.resetCutsceneActions();
      this.props.getCutsceneActions(gameDetail.slug, cutscene.slug);
      this.props.resetDeleteCutsceneAction();
      this.setState({uploadingData: false});
      this.handleShowAllCutsceneActions();
    }
  }

  render() {
    const { cutscene, cutsceneActionTypes, loadingCutsceneActionTypes, cutsceneActions, loadingCutsceneActions, loadingGameActionTypes, gameActionTypes, levelActionTypes, gameLevels } = this.props;
    const { name, description, image, imageFile, uploadingData, fieldYouTube, fieldVimeo, fieldArchive, width, height, autoplay, loop, cutsceneActionTypeSlug, gameOptions, levelOptions, cutsceneOptions, gameActionTypeSlug, requireComplete, actionName, autoSelect } = this.state;
    const { showNewCutsceneActions, showAllCutsceneActions, showEditCutsceneAction, cutsceneActionObj } = this.state;

    return (
      <div>
        <div style={{display: 'flex'}}>
          <div style={{flex: 1, display: 'flex'}}>
            <div style={{flex: 1}}>
              <img src={cutscene.thumbnail} style={{height: 100, width: 100}} />
            </div>
            <div style={{flex: 9, paddingLeft: 24}}>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Name:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="name"
                    value={name}
                    placeholder="Name"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Description:</span>
                </div>
                <div style={{flex: 4}}>
                  <textarea 
                    className="updateFormTextArea" 
                    id="description"
                    name="description" 
                    onChange={this.handleChange} 
                    value={description} 
                    placeholder="Description:" 
                    style={styles.textareaInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}> 
                  <label for="image" style={styles.defaultText}>Image:</label>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    style={styles.fileInput}
                    type="file" 
                    name="image" 
                    onChange={this.handleImageChange}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>YouTube:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="fieldYouTube"
                    value={fieldYouTube}
                    placeholder="YouTube"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Vimeo:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="fieldVimeo"
                    value={fieldVimeo}
                    placeholder="Vimeo"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Internet Archive:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="fieldArchive"
                    value={fieldArchive}
                    placeholder="Internet Archive"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Width:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="width"
                    value={width}
                    placeholder="Width"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Height:</span>
                </div>
                <div style={{flex: 4}}>
                  <input 
                    name="height"
                    value={height}
                    placeholder="Height"
                    onChange={this.handleChange}
                    style={styles.textInput}
                  />
                </div>
              </div>
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                  <span style={styles.defaultText}>Auto Play:</span>
                </div>
                <div style={{flex: 1}}>
                  <input 
                    type="checkbox" 
                    name="autoplay" 
                    id="autoplay" 
                    checked={autoplay} 
                    onChange={this.handleCheckboxChange.bind(this)} 
                  />
                </div>
              </div> 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                  <span style={styles.defaultText}>Loop:</span>
                </div>
                <div style={{flex: 1}}>
                  <input 
                    type="checkbox" 
                    name="loop" 
                    id="loop" 
                    checked={loop} 
                    onChange={this.handleCheckboxChange.bind(this)} 
                  />
                </div>
              </div>  
            </div>
          </div>
          <div style={{flex: 1}}>
            <div>
              {showNewCutsceneActions && 
                <div>
                  <div style={{paddingBottom: 10}}><span style={styles.defaultText}>New Cutscene Action:</span></div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Name:</span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        name="actionName"
                        value={actionName}
                        placeholder="Name"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  {!loadingCutsceneActionTypes ? 
                    <div>
                      {cutsceneActionTypes.length > 0 ? 
                        <select
                          name="cutsceneActionTypes" 
                          id="cutsceneActionTypes"
                          className="cutsceneActionTypes"
                          onChange={this.handleSelectCutsceneActionType.bind(this)}
                          style={styles.selectInput}
                        >
                          <option value="">...</option>
                          {cutsceneActionTypes.map((cutsceneActionType, index) => (
                            <option 
                              key={index}
                              value={cutsceneActionType.slug}
                            >
                              {cutsceneActionType.name}
                            </option>
                          ))}
                        </select> :
                        <div>
                          <span>No Cutscene Action Types</span>
                        </div>
                      }
                    </div> :
                    <div>
                      <span>Loading Cutscene Action Types...</span>
                    </div>
                  }
                  {cutsceneActionTypeSlug === 'change-level' && 
                    <div>
                      <div><span>Change Level:</span></div>
                      {gameLevels.length > 0 ? 
                        <select 
                          name="gameLevels" 
                          id="gameLevels"
                          className="gameLevelSelect"
                          style={styles.selectInput}
                          onChange={this.handleSelectGameLevel.bind(this)}
                        >
                          <option value="">...</option>
                          {gameLevels.map((gameLevel, index) => (
                            <option 
                              key={index}
                              value={gameLevel.slug}
                            >
                              {gameLevel.name}
                            </option>
                          ))}
                        </select> :
                        <div>
                          <span>No Levels to Select</span>
                        </div>
                      }
                      
                    </div>
                  }
                  {cutsceneActionTypeSlug == 'change-cutscene' && 
                    <div>
                      <div><span>Change Cutscene:</span></div>
                      {cutsceneOptions.length > 0 ? 
                        <div>
                          <select
                            name="cutsceneActionTypes" 
                            id="cutsceneActionTypes"
                            className="cutsceneActionTypes"
                            onChange={this.handleSelectCutsceneOption.bind(this)}
                            style={styles.selectInput}
                          >
                            <option value="">...</option>
                            {cutsceneOptions.map((cutsceneOption, index) => (
                              <option 
                                key={index}
                                value={cutsceneOption.slug}
                              >
                                {cutsceneOption.name}
                              </option>
                            ))}
                          </select>
                        </div> :
                        <div>
                          <span>No Cutscenes to Select</span>
                        </div>
                      }
                    </div>                
                  }
                  {cutsceneActionTypeSlug == 'game-action' && 
                    <div>
                      <div><span>Game Action:</span></div>
                      {gameActionTypes.length > 0 ? 
                        <div>
                          <select
                            name="gameActionTypes" 
                            id="gameActionTypes"
                            className="gameActionTypeSelect"
                            style={styles.selectInput}
                            onChange={this.handleSelectGameActionType.bind(this)}
                          >
                            <option value="">...</option>
                            {gameActionTypes.map((gameActionType, index) => (
                              <option 
                                key={index}
                                value={gameActionType.slug}
                              >
                                {gameActionType.name}
                              </option>
                            ))}
                          </select>
                        </div> :
                        <div>
                          <span>No Game Actions to Select</span>
                        </div>
                      }
                    </div>
                  }
                  {url_youtube !== '' && url_youtube !== null && 
                    <div style={{display: 'flex', paddingBottom: 10}}>
                      <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                        <span style={styles.defaultText}>Require Complete:</span>
                      </div>
                      <div style={{flex: 1}}>
                        <input 
                          type="checkbox" 
                          name="requireComplete" 
                          id="requireComplete" 
                          checked={requireComplete} 
                          onChange={this.handleCheckboxChange.bind(this)} 
                        />
                      </div>
                    </div> 
                  }
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                      <span style={styles.defaultText}>Auto Select:</span>
                    </div>
                    <div style={{flex: 1}}>
                      <input 
                        type="checkbox" 
                        name="autoSelect" 
                        id="autoSelect" 
                        checked={autoSelect} 
                        onChange={this.handleCheckboxChange.bind(this)} 
                      />
                    </div>
                  </div> 
                  <div style={{paddingTop: 20, paddingBottom: 20, textAlign: 'center'}}>
                    <button onClick={this.handleCreateNewCutsceneAction} style={styles.buttonDefault}>Save</button>
                  </div>
                </div>
              }
              {showAllCutsceneActions && 
                <div>
                  <div style={{paddingBottom: 10}}>
                    <span style={styles.defaultText}>Edit Cutscene Actions:</span>
                  </div>
                  {!loadingCutsceneActions ? 
                    <div>
                      {cutsceneActions.length > 0 ? 
                        <div>
                          {cutsceneActions.map((cutsceneAction, index) => (
                            <div>
                              <button onClick={this.handleShowEditCutsceneAction.bind(this, cutsceneAction)}>{cutsceneAction.action_type.name}: {cutsceneAction.cutscene.name}</button>
                            </div>
                          ))}
                        </div> :
                        <div>
                          <span>No Cutscene Actions</span>
                        </div>
                      }
                    </div> :
                    <div>
                      <span>Loading Cutscene Actions...</span>
                    </div>
                  }
                </div>
              }
              {showEditCutsceneAction && 
                <EditCutsceneAction cutscene={cutscene} cutsceneOptions={cutsceneOptions} closeEditCutsceneAction={this.handleShowAllCutsceneActions} cutsceneAction={cutsceneActionObj} />
              }
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> : 
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditCutscene}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.showCutscenes}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,
  gameCutscenes: state.games.gameCutscenes,
  loadingGameCutscenes: state.games.loadingGameCutscenes,
  gameLevels: state.games.gameLevels,
  loadingGameLevels: state.games.loadingGameLevels,

  cutsceneActions: state.games.cutsceneActions,
  loadingCutsceneActions: state.games.loadingCutsceneActions,

  cutsceneActionTypes: state.game_admin.cutsceneActionTypes,
  loadingCutsceneActionTypes: state.game_admin.loadingCutsceneActionTypes,

  editCutsceneObj: state.game_admin.editCutsceneObj,
  deleteCutsceneObj: state.game_admin.deleteCutsceneObj,

  levelActionTypes: state.game_admin.levelActionTypes,

  gameActionTypes: state.game_admin.gameActionTypes,
  loadingGameActionTypes: state.game_admin.loadingGameActionTypes,

  newCutsceneActionObj: state.game_admin.newCutsceneActionObj,
  editCutsceneActionObj: state.game_admin.editCutsceneActionObj,
  deleteCutsceneActionObj: state.game_admin.deleteCutsceneActionObj
})

const mapDispatchToProps = {
  editCutscene,
  deleteCutscene,
  resetEditCutscene,
  resetDeleteCutscene,
  clearGameCutscenes,
  getGameCutscenes,
  getCutsceneActionTypes, 
  resetCutsceneActionTypes, 
  getCutsceneActions, 
  resetCutsceneActions,
  getGameActionTypes, 
  resetGameActionTypes, 
  getLevelActionTypes,
  newCutsceneAction, 
  resetNewCutsceneAction,
  editCutsceneAction, 
  resetEditCutsceneAction, 
  deleteCutsceneAction, 
  resetDeleteCutsceneAction
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCutscene)

