import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editPageAction, resetEditPageAction, deletePageAction, resetDeletePageAction } from '../../../actions/game_admin';
import { getPageActions, resetPageActions } from '../../../actions/game_admin';

export class EditPageAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      uploadingData: false,
      availablePages: [],
      actionTypeSlug: '',
      gameActionTypeSlug: '',
      goToPageSlug: '',
      pageSlug: '',
      actionSlug: ''
    }
  }

  handleDelete = () => { 
    if (window.confirm("Delete Action?")) {
      this.props.deletePageAction(this.props.gameDetail.slug, this.props.page.slug, this.state.actionSlug);
      this.setState({uploadingData: true});
    }
  } 

  handleEditPageAction = () => {
    if (window.confirm("Edit Action?")) {
      const { page } = this.props;
      const { name, description, actionTypeSlug, gameActionTypeSlug, goToPageSlug, actionSlug } = this.state;
      const thisEditPageActionObj = { name, description, actionTypeSlug, gameActionTypeSlug, goToPageSlug, page, actionSlug };
      if (name !== '') {
        this.props.editPageAction(this.props.gameDetail.slug, thisEditPageActionObj);
        this.setState({uploadingData: true});
      } else {
        alert('Page Actions must have a Name');
      }
    }
  }

  handleCloseEditPageAction = () => {
    this.props.closeEditPageAction();
  }

  handleSelectGoToPage = (e) => {
    this.setState({
      goToPageSlug: e.target.value,
      gameActionTypeSlug: ''
    });
  }

  handleSelectGameActionType = (e) => {
    this.setState({
      gameActionTypeSlug: e.target.value,
      goToPageSlug: ''
    });
  }

  handleSelectPageActionType = (e) => {
    this.setState({
      actionTypeSlug: e.target.value
    });
  }

  handleImageChange = e => this.setState({
    imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    const { editActionObj, gamePages, page } = this.props;
    var availablePages = [];
    for (var i = 0; i < gamePages.length; i++) {
      if (gamePages[i].slug !== page.slug) {
        availablePages.push(gamePages[i]);
      }
    }
    this.setState({
      name: editActionObj.name,
      description: editActionObj.description,
      uploadingData: false,
      availablePages,
      actionTypeSlug: editActionObj.action_type.slug,
      gameActionTypeSlug: editActionObj.game_action_type !== null ? editActionObj.game_action_type.slug : '',
      goToPageSlug: editActionObj.go_to_page !== null ? editActionObj.go_to_page.slug : '',
      pageSlug: page.slug,
      actionSlug: editActionObj.slug
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editPageActionObj !== null && lastProps.editPageActionObj === null) {
      this.props.resetEditPageAction();
      this.props.resetPageActions();
      this.props.getPageActions(this.props.gameDetail.slug, this.props.page.slug);
      this.setState({uploadingData: false});
      this.handleCloseEditPageAction();
    }
    if (this.props.deletePageActionObj !== null && lastProps.deletePageActionObj === null) {
      this.props.resetDeletePageAction();
      this.props.resetPageActions();
      this.props.getPageActions(this.props.gameDetail.slug, this.props.page.slug);
      this.setState({uploadingData: false});
      this.handleCloseEditPageAction();
    }
  }
  
  render() {
    const { name, description, uploadingData, availablePages, actionTypeSlug, gameActionTypeSlug, goToPageSlug } = this.state;
    const { page, pageActionTypes, gameActionTypes, loadingGameActionTypes } = this.props;

    return (
      <div>
        <div style={styles.titleContainer}>
          <span style={styles.titleText}>Edit Page Action:</span>
        </div>
        <select 
          name="pageActionTypes" 
          id="pageActionTypes"
          onChange={this.handleSelectPageActionType.bind(this)}
          style={styles.selectInput}
          value={actionTypeSlug}
        >
          <option value="">...</option>
          {pageActionTypes.map((actionType, index) => (
            <option key={index} value={actionType.slug}>{actionType.name}</option>
          ))}
        </select>
        {actionTypeSlug === "go-to-page" && 
          <div>
            <select 
              name="availablePages" 
              id="availablePages"
              onChange={this.handleSelectGoToPage.bind(this)}
              style={styles.selectInput}
              value={goToPageSlug}
            >
              <option value="">Select Page:</option>
              {availablePages.map((availablePage, index) => (
                <option value={availablePage.slug}>{availablePage.name}</option>
              ))}
            </select>
          </div>
        }
        {actionTypeSlug === 'game-action' && 
          <div>
            {!loadingGameActionTypes ? 
              <div>
                <div style={{paddingBottom: 15, paddingTop: 10}}>
                  <span style={styles.titleText}>Game Actions:</span>
                </div>
                {gameActionTypes.length > 0 ? 
                  <div>
                    <select 
                      name="gameActionTypes" 
                      id="gameActionTypes"
                      className="gameActionTypesSelect"
                      onChange={this.handleSelectGameActionType.bind(this)}
                      style={styles.selectInput}
                      value={gameActionTypeSlug}
                    >
                      <option value="">...</option>
                      {gameActionTypes.map((gameActionType, index) => (
                        <option key={index} value={gameActionType.slug}>{gameActionType.name}</option>
                      ))}
                    </select> 
                  </div> :
                  <div style={styles.defaultTextContainer}>
                    <span style={styles.defaultText}>No Game Action Types</span>
                  </div>
                }
              </div> :
              <div style={styles.defaultTextContainer}>
                <span style={styles.defaultText}>Loading Game Actions...</span>
              </div>
            }
          </div>
        }
        <div style={{display: 'flex', paddingTop: 15, paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Name: </span>
          </div>
          <div style={{flex: 4}}>
            <input 
              name="name"
              value={name}
              placeholder="Name"
              onChange={this.handleChange}
              style={styles.textInput}
            />
          </div>
        </div>
        <div style={{display: 'flex', paddingBottom: 10}}>
          <div style={{flex: 1}}>
            <span style={styles.defaultText}>Description:</span>
          </div>
          <div style={{flex: 4}}>
            <textarea 
              type="textArea" 
              name="description" 
              onChange={this.handleChange} 
              value={description} 
              placeholder="Describe your Page Action"
              style={styles.textareaInput}
            />
          </div>
        </div>
        {uploadingData ? 
          <div style={{paddingTop: 15}}>
            <div style={styles.defaultText} id="uploadProgress" />
          </div> :
          <div style={{paddingTop: 15, display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditPageAction}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleCloseEditPageAction}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
          }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 
  gamePages: state.games.gamePages,
  pageActionTypes: state.game_admin.pageActionTypes,
  gameActionTypes: state.game_admin.gameActionTypes,
  loadingGameActionTypes: state.game_admin.loadingGameActionTypes,

  editPageActionObj: state.game_admin.editPageActionObj,
  deletePageActionObj: state.game_admin.deletePageActionObj
})

const mapDispatchToProps = {
  editPageAction, 
  resetEditPageAction, 
  deletePageAction, 
  resetDeletePageAction ,
  getPageActions, 
  resetPageActions
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPageAction)
