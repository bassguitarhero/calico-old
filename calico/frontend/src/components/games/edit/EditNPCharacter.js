import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editNPCharacter, deleteNPCharacter } from '../../../actions/game_admin';
import { resetEditNPCharacter, resetDeleteNPCharacter } from '../../../actions/game_admin';
import { clearGameNPCharacters, getGameNPCharacters } from '../../../actions/games';

export class EditNPCharacter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      image: '',
      imageFile: [],
      description: '',
      baseStrength: 5,
      baseSkill: 5,
      baseSpeed: 5,
      baseHealth: 5,
      baseWisdom: 5,
      baseLuck: 5,
      baseHP: 6,
      baseKeys: 0,
      baseBombs: 0,
      baseDamage: 10,
      uploadingData: false,
      slug: '',
      spriteSheet: '',
      spriteSheetFile: null,
      width: 0,
      height: 0,
      health: 0,
      speed: 0,
      level: 1,
      hostile: 'true',
      contactDamage: 'true',
      contactDamageAmount: 1,
      canWalk: 'true',
      canFly: 'false',
      canAttack: '',
      attacks: [],
      hasMovement: 'false',
      movementType: 0
    }
  }

  handleDelete = () => {
    if (window.confirm("Delete Non-Playable Character?")) {
      this.props.deleteNPCharacter(this.props.gameDetail.slug, this.props.npCharacter.slug);
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleSpriteSheetChange = e => this.setState({
		spriteSheetFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditNPCharacter = () => {
    const { name, description, imageFile, slug, spriteSheetFile, width, height, health, speed, level, hostile, contactDamage, contactDamageAmount, canWalk, canFly, canAttack, attacks, hasMovement, movementType } = this.state;
    const npCharacter = { name, description, imageFile, slug, spriteSheetFile, width, height, health, speed, level, hostile, contactDamage, contactDamageAmount, canWalk, canFly, canAttack, attacks, hasMovement, movementType };
		if (name !== '') {
			if (window.confirm("Edit Non-Playable Character?")) {
				this.props.editNPCharacter(this.props.gameDetail.slug, npCharacter);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Non-Playable Characters must have a Name.");
		}
  }

  componentDidMount() {
    const { npCharacter } = this.props;
    console.log('npCharacter: ', npCharacter);
    this.setState({
      name: npCharacter.name,
      image: npCharacter.image,
      description: npCharacter.description,
      slug: npCharacter.slug,
      spriteSheet: npCharacter.sprite_sheet,
      width: npCharacter.width,
      height: npCharacter.height,
      health: npCharacter.health,
      speed: npCharacter.speed,
      level: npCharacter.level,
      hostile: npCharacter.hostile,
      contactDamage: npCharacter.contact_damage,
      contactDamageAmount: npCharacter.contact_damage_amount,
      canWalk: npCharacter.can_walk,
      canFly: npCharacter.can_fly,
      canAttack: npCharacter.can_attack,
      attacks: npCharacter.attacks,
      hasMovement: npCharacter.has_movement,
      movementType: npCharacter.movement_type
    })
  }

  componentDidUpdate(lastProps) {
    if (this.props.editNPCharacterObj !== null && lastProps.editNPCharacterObj === null) {
      this.props.resetEditNPCharacter();
      this.props.clearGameNPCharacters();
      this.props.getGameNPCharacters(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showNPCharacters();
    }
    if (this.props.deleteNPCharacterObj !== null && lastProps.deleteNPCharacterObj === null) {
      this.props.resetDeleteNPCharacter();
      this.props.clearGameNPCharacters();
      this.props.getGameNPCharacters(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showNPCharacters();
    }
  }

  render() {
    const { npCharacter } = this.props;
    const { name, description, uploadingData, width, height, health, speed, level, hostile, contactDamage, contactDamageAmount, canWalk, canFly, canAttack, attacks, hasMovement, movementType } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>Edit Non Playable Character:</span>
        </div>
        <div style={{paddingBottom: 20, display: 'flex'}}>
          <div style={{flex: 1}}>
            <img src={npCharacter.thumbnail} style={{height: 100, width: 100}} />
          </div>
          <div style={{flex: 4, paddingLeft: 24}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Name:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="name"
                  value={name}
                  placeholder="Name"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Description:</span>
              </div>
              <div style={{flex: 3}}>
                <textarea 
                  style={styles.textareaInput}
                  className="updateFormTextArea" 
                  id="description"
                  name="description" 
                  onChange={this.handleChange} 
                  value={description} 
                  placeholder="Description:" 
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <label for="image" style={styles.defaultText}>Image:</label>
              </div>
              <div style={{flex: 3}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="image" 
                  onChange={this.handleImageChange}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <img src={npCharacter.sprite_sheet_thumbnail} style={{height: 100, width: 100}} />
          </div>
          <div style={{flex: 4, paddingLeft: 24}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <label for="spriteSheet" style={styles.defaultText}>Sprite Sheet:</label>
              </div>
              <div style={{flex: 3}}>
                <input 
                  style={styles.fileInput}
                  type="file" 
                  name="spriteSheet" 
                  onChange={this.handleSpriteSheetChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Sprite Width:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="width"
                  value={width}
                  placeholder="Width"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Sprite Height:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="height"
                  value={height}
                  placeholder="Height"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div style={{paddingTop: 20, paddingBottom: 25, display: 'flex'}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Health:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="health"
                  value={health}
                  placeholder="Health"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Speed:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="speed"
                  value={speed}
                  placeholder="Speed"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Level:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="level"
                  value={level}
                  placeholder="Level"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Hostile:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  type="checkbox" 
                  name="hostile" 
                  id="hostile" 
                  checked={hostile} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Contact Damage:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  type="checkbox" 
                  name="contactDamage" 
                  id="contactDamage" 
                  checked={contactDamage} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2}}>
                <span style={styles.defaultText}>Contact Damage Amount:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="contactDamageAmount"
                  value={contactDamageAmount}
                  placeholder="Contact Damage Amount"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Can Walk:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  type="checkbox" 
                  name="canWalk" 
                  id="canWalk" 
                  checked={canWalk} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Can Fly:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  type="checkbox" 
                  name="canFly" 
                  id="canFly" 
                  checked={canFly} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Can Attack:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  type="checkbox" 
                  name="canAttack" 
                  id="canAttack" 
                  checked={canAttack} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 2, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Has Movement:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  type="checkbox" 
                  name="hasMovement" 
                  id="hasMovement" 
                  checked={hasMovement} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress">
            &nbsp;
          </div> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditNPCharacter}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.showNPCharacters}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editNPCharacterObj: state.game_admin.editNPCharacterObj,
  deleteNPCharacterObj: state.game_admin.deleteNPCharacterObj
})

const mapDispatchToProps = {
  editNPCharacter,
  deleteNPCharacter,
  resetEditNPCharacter,
  resetDeleteNPCharacter,
  clearGameNPCharacters,
  getGameNPCharacters
}

export default connect(mapStateToProps, mapDispatchToProps)(EditNPCharacter)

