import React, { Component } from 'react';
import { connect } from 'react-redux';

import { styles } from '../../layout/Styles';

import { editDialogueResponse, resetEditDialogueResponse } from '../../../actions/game_admin'
import { deleteDialogueResponse, resetDeleteDialogueResponse } from '../../../actions/game_admin';
import { getDialogueResponses, resetDialogueResponses } from '../../../actions/games';

export class EditDialogueResponse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: '',
      description: '',
      dialogueSlug: '',
      connectToSlug: '',
      endingResponse: false,
      uploadingData: false,
      slug: '',
      conversationDialogueOptions: [],
      order: 1
    }
  }

  handleSelectChange = (e) => {
    if (e.target.value !== '') {
      this.setState({
        connectToSlug: e.target.value,
        endingResponse: false
      });
    } else {
      this.setState({
        connectToSlug: e.target.value,
      });
    }
  }

  handleDeleteDialogueResponse = () => {
    const { dialogue, dialogueResponse } = this.props;
    if (window.confirm("Delete Dialogue Response?")) {
      this.props.deleteDialogueResponse(dialogue, dialogueResponse);
    }
  }

  handleEditDialogueResponse = () => {
    const { dialogue, dialogueResponse } = this.props;
    const { name, description, image, imageFile, dialogueSlug, connectToSlug, endingResponse, slug, order } = this.state;
    const dialogueResponseData = { name, description, image, imageFile,  dialogueSlug, connectToSlug, endingResponse, slug, order };
    if (name !== '') {
			if (window.confirm("Edit Dialogue Response?")) {
        this.props.editDialogueResponse(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug, dialogueResponseData);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Dialogue Responses must have a Name.");
		}
  }

  handleCheckboxChange = (e) => {
    this.setState({
      [e.target.name]: !this.state[e.target.name]
    });
  }
  
  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name' || key === 'actionName'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleSetUpConversationDialogueOptions = () => {
    const { dialogue, gameConversationDialogues } = this.props;
    var conversationDialogueOptions = [];
    for (var i = 0; i < gameConversationDialogues.length; i++) {
      if (gameConversationDialogues[i].slug !== dialogue.slug) {
        conversationDialogueOptions.push(gameConversationDialogues[i]);
      }
    }
    this.setState({
      conversationDialogueOptions
    });
  }  

  componentDidMount() {
    const { dialogueResponse } = this.props;
    this.setState({
      name: dialogueResponse.name,
      image: dialogueResponse.image,
      description: dialogueResponse.description,
      dialogueSlug: dialogueResponse.dialogue.slug,
      connectToSlug: dialogueResponse.connect_to !== null ? dialogueResponse.connect_to.slug : '',
      endingResponse: dialogueResponse.ending_response,
      slug: dialogueResponse.slug,
      order: dialogueResponse.order
    });
    this.handleSetUpConversationDialogueOptions();
  }

  componentDidUpdate(lastProps) {
    if (this.props.editDialogueResponseObj !== null && lastProps.editDialogueResponseObj === null) {
      const { dialogue } = this.props;
      this.props.resetEditDialogueResponse();
      this.props.resetDialogueResponses();
      this.props.getDialogueResponses(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug)
      this.setState({uploadingData: false});
      this.props.closeEditDialogueResponse();
    }
    if (this.props.deleteDialogueResponseObj !== null && lastProps.deleteDialogueResponseObj === null) {
      const { dialogue } = this.props;
      this.props.resetDeleteDialogueResponse();
      this.props.resetDialogueResponses();
      this.props.getDialogueResponses(dialogue.conversation.game.slug, dialogue.conversation.slug, dialogue.slug)
      this.setState({uploadingData: false});
      this.props.closeEditDialogueResponse();
    }
  }
  
  render() {
    const { name, description, image, imageFile, uploadingData, endingResponse, conversationDialogueOptions, connectToSlug, order } = this.state;
    const { dialogue, dialogueResponse } = this.props;

    return (
      <div>
        <div style={{paddingBottom: 20}}>
          <div style={{paddingBottom: 10}}><span style={styles.nameText}>Edit Dialogue Response:</span></div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Name:</span>
            </div>
            <div style={{flex: 4}}>
              <input 
                name="name"
                value={name}
                placeholder="Name"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Description:</span>
            </div>
            <div style={{flex: 4}}>
              <textarea 
                className="updateFormTextArea" 
                id="description"
                name="description" 
                onChange={this.handleChange} 
                value={description} 
                placeholder="Description:"
                style={styles.textareaInput}
              />
            </div>
          </div>
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}> 
              <label for="image" style={styles.defaultText}>Image:</label>
            </div>
            <div style={{flex: 4}}>
              <input 
                style={styles.fileInput}
                type="file" 
                name="image" 
                onChange={this.handleImageChange}
              />
            </div>
          </div>
          {image !== null && image !== '' && 
            <div style={{paddingBottom: 10}}>
              <span style={styles.defaultText}>Current: {image}</span>
            </div>
          }
          {dialogueResponse.connect_to !== null ? 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Connect to: </span>
              </div>
              <div style={{flex: 3}}>
                {conversationDialogueOptions.length > 0 ? 
                  <select 
                    name="conversationDialogueOptions" 
                    id="conversationDialogueOptions"
                    className="conversationDialogueOptions"
                    onChange={this.handleSelectChange.bind(this)}
                    style={styles.selectInput}
                  >
                    <option value="">Select Dialogue</option>
                    {conversationDialogueOptions.map((dialogue, index) => (
                      <option 
                        key={index} 
                        value={dialogue.slug}
                        selected={dialogueResponse.connect_to.slug === dialogue.slug ? 'selected' : ''}
                      >
                        {dialogue.name}
                      </option>
                    ))}
                  </select> :
                  <div>
                    <span style={styles.defaultText}>No Dialogue to Connect...</span>
                  </div>
                }
              </div>
            </div> :
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Connect to: </span>
              </div>
              <div style={{flex: 3}}>
                {conversationDialogueOptions.length > 0 ? 
                  <select 
                    name="conversationDialogueOptions" 
                    id="conversationDialogueOptions"
                    className="conversationDialogueOptions"
                    onChange={this.handleSelectChange.bind(this)}
                    style={styles.selectInput}
                  >
                    <option value="">Select Dialogue</option>
                    {conversationDialogueOptions.map((dialogue, index) => (
                      <option 
                        key={index} 
                        value={dialogue.slug}
                      >
                        {dialogue.name}
                      </option>
                    ))}
                  </select> :
                  <div>
                    <span style={styles.defaultText}>No Dialogue to Connect...</span>
                  </div>
                }
              </div>
            </div>
          }
          {connectToSlug === '' && 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Ending Response:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="endingResponse" 
                  id="endingResponse" 
                  checked={endingResponse} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          }
          <div style={{display: 'flex', paddingBottom: 10}}>
            <div style={{flex: 1}}>
              <span style={styles.defaultText}>Order:</span>
            </div>
            <div style={{flex: 4}}>
              <input 
                name="order"
                value={order}
                placeholder="Order"
                onChange={this.handleChange}
                style={styles.textInput}
              />
            </div>
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditDialogueResponse}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeEditDialogueResponse}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDeleteDialogueResponse}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editDialogueResponseObj: state.game_admin.editDialogueResponseObj,
  deleteDialogueResponseObj: state.game_admin.deleteDialogueResponseObj,

  gameConversationDialogues: state.games.gameConversationDialogues,
  loadingGameConversationDialogues: state.games.loadingGameConversationDialogues
})

const mapDispatchToProps = {
  editDialogueResponse, resetEditDialogueResponse, deleteDialogueResponse, resetDeleteDialogueResponse, getDialogueResponses, resetDialogueResponses
}

export default connect(mapStateToProps, mapDispatchToProps)(EditDialogueResponse)
