import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditCutscene from './EditCutscene';
import InlineCutscene from './InlineCutscene';

import NewCutscene from '../new/NewCutscene';

import { styles } from '../../layout/Styles';

export class EditCutscenes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCutscenes: true,
      showEditCutscene: false,
      showNewCutscene: false,
      editCutscene: null
    }
  }

  handleCloseNewCutscene = () => {
    this.setState({
      showCutscenes: true,
      showEditCutscene: false,
      showNewCutscene: false,
      editCutscene: null
    });
  }

  handleShowNewCutscene = () => {
    this.setState({
      showCutscenes: false,
      showEditCutscene: false,
      showNewCutscene: true,
      editCutscene: null
    });
  }

  handleShowCutscenes = () => {
    this.setState({
      showCutscenes: true,
      showEditCutscene: false,
      showNewCutscene: false,
      editCutscene: null
    });
  }

  handleShowEditCutscene = (cutscene) =>{
    this.setState({
      showCutscenes: false,
      showEditCutscene: true,
      showNewCutscene: false,
      editCutscene: cutscene
    });
  }

  render() {
    const { gameCutscenes, loadingGameCutscenes } = this.props;
    const { showCutscenes, showEditCutscene, showNewCutscene, editCutscene } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 24}}>
          <button style={styles.buttonDefault} onClick={this.handleShowNewCutscene}>New</button>
        </div>
        {showCutscenes && 
          <div>
            {!loadingGameCutscenes ? 
              <div>
                {gameCutscenes.length > 0 ? 
                  <div>
                    {gameCutscenes.map((cutscene, index) => (
                      <button style={styles.button} key={index} onClick={this.handleShowEditCutscene.bind(this, cutscene)}>
                        <InlineCutscene cutscene={cutscene} />
                      </button>
                    ))}
                  </div> :
                  <div>
                    <span>No Game Cutscenes!</span>
                  </div>
                }
              </div> :
              <div>
                <span>Loading...</span>
              </div>
            }
          </div>
        }
        {showEditCutscene && 
          <EditCutscene showCutscenes={this.handleShowCutscenes} cutscene={editCutscene} />
        }
        {showNewCutscene && 
          <NewCutscene closeNewCutscene={this.handleCloseNewCutscene} />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameCutscenes: state.games.gameCutscenes,
  loadingGameCutscenes: state.games.loadingGameCutscenes
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditCutscenes)
