import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { editLevel, deleteLevel, getLevelTypes } from '../../../actions/game_admin';
import { resetEditLevel, resetDeleteLevel } from '../../../actions/game_admin';
import { getLevelActions, getLevelActionTypes } from '../../../actions/game_admin';
import { getLevelNPCharacters } from '../../../actions/games';
import { clearGameLevels, getGameLevels, getLevelTiles } from '../../../actions/games';

import NewLevelTile from '../new/NewLevelTile';
import EditLevelTile from './EditLevelTile';
import NewLevelNPCharacter from '../new/NewLevelNPCharacter';
import EditLevelNPCharacter from './EditLevelNPCharacter';
import NewLevelAction from '../new/NewLevelAction';
import EditLevelAction from './EditLevelAction';

export class EditLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      image: '',
      imageFile: [],
      uploadingData: false,
      startingLevel: false,
      startingX: 0,
      startingY: 0,
      slug: '',
      order: 0,
      levelTypes: null,
      levelTypeSlug: '',
      showLevel: true,
      showNewLevelTile: false,
      showEditLevelTile: false,
      editLevelTileObj: null,
      width: 0,
      height: 0,
      tileWidth: 0,
      tileHeight: 0,
      tileColumns: [],
      tileRows: [],
      tileGrid: {},
      selectedColumn: 0,
      selectedRow: 0,
      showLevelDetails: true, 
      showLevelTiles: false,
      showLevelNPCharacters: false,
      showLevelActions: false,
      showNewLevelNPCharacter: false,
      showEditLevelNPCharacter: false,
      editLevelNPCharacterObj: null,
      showNewLevelAction: false,
      showEditLevelAction: false,
      editLevelActionObj: null
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleShowLevelDetails = () => {
    this.setState({
      showLevelDetails: true, 
      showLevelTiles: false,
      showLevelNPCharacters: false,
      showLevelActions: false
    });
  }

  handleShowLevelTiles = () => {
    this.setState({
      showLevel: true,
      showLevelDetails: false, 
      showLevelTiles: true,
      showLevelNPCharacters: false,
      showLevelActions: false
    });
  }

  handleShowLevelNPCharacters = () => {
    this.setState({
      showLevel: true,
      showLevelDetails: false, 
      showLevelTiles: false,
      showLevelNPCharacters: true,
      showLevelActions: false
    });
  }

  handleShowLevelActions = () => {
    this.setState({
      showLevel: true,
      showLevelDetails: false, 
      showLevelTiles: false,
      showLevelNPCharacters: false,
      showLevelActions: true
    });
  }

  handleBuildTileArrays = () => {
    const { level } = this.props;
    const tileColumns = [];
    const tileRows = [];
    for (var i = 0; i < (parseInt(level.height / level.tile_height)); i++) {
      tileRows.push(i);
      for (var j = 0; j < (parseInt(level.width / level.tile_width)); j++) {
        tileColumns.push(j);
      }
    }
    this.setState({
      tileColumns, tileRows
    });
  }

  handleBuildTileArray = () => {
    const { level } = this.props;
    var tileGrid = [];
    var row = [];
    for (var i = 0; i < (level.height / level.tile_height); i++) {
      for (var j = 0; j < (level.width / level.tile_width); j++) {
        row.push(0);
      }
      tileGrid.push({row});
      row = [];
    }
    this.setState({
      tileGrid
    });
  }

  handleBuildTileGrid = () => {
    const { level } = this.props;
    var positionX = 0;
    var positionY = 0;
    var numColumns = level.width / level.tile_width;
    var numRows = level.height / level.tile_height;
    var output = '';
    for (var i = 0; i < numRows; i++) {
      for (var j = 0; j < numColumns; j++) {
        output += `
          <div 
            style={{
              position: 'absolute',
              top: '${positionY}px',
              left: '${positionX}px',
              width: '${level.tile_width}px',
              height: '${level.tile_height}px',
              border: '2px dotted grey',
            }}
          >
            ${positionX},${positionY}
          </div>
        `;
        positionX += level.tile_width;  
      }
      positionY += level.tile_height;
    }
    return output;
  }

  handleSelectChange = (e) => {
    this.setState({
      levelTypeSlug: e.target.value
    });
  }

  handleDelete = () => {
    if (window.confirm("Delete Level?")) {
      this.props.deleteLevel(this.props.gameDetail.slug, this.props.level.slug);
    }
  }

  handleImageChange = e => this.setState({
		imageFile: e.target.files[0]
	});

  handleChange = (e) => {
    e.preventDefault()
		let key = e.target.name
		let value = e.target.value
		if (key === 'name'){
			if (value.length > 250){
				alert("Your title is too long")
			}
		}
  	this.setState({ [e.target.name]: e.target.value });
  }

  handleEditLevel = () => {
    const { gameDetail } = this.props;
    const { name, description, imageFile, slug, order, levelTypeSlug, width, height, tileWidth, tileHeight, startingLevel, startingX, startingY } = this.state;
    const levelData = { name, description, imageFile, slug, order, levelTypeSlug, width, height, tileWidth, tileHeight, startingLevel, startingX, startingY };
		if (name !== '') {
			if (window.confirm("Edit Level?")) {
				this.props.editLevel(gameDetail.slug, levelData);
				this.setState({uploadingData: true});
			}
		} else {
			alert("Levels must have a Name.");
		}
  }

  handleShowLevel = () => {
    this.setState({
      showLevel: true,
      showNewLevelTile: false,
      showEditLevelTile: false,
      editLevelTileObj: null,
      showNewLevelNPCharacter: false,
      showEditLevelNPCharacter: false,
      editLevelNPCharacterObj: null,
      showNewLevelAction: false,
      showEditLevelAction: false,
      editLevelActionObj: null
    });
  }

  handleShowNewLevelTile = (column, row) => {
    this.setState({
      showLevel: false,
      showNewLevelTile: true,
      showEditLevelTile: false,
      editLevelTileObj: null,
      selectedColumn: column,
      selectedRow: row
    });
  }

  handleShowEditLevelTile = (levelTile) => {
    this.setState({
      showLevel: false,
      showNewLevelTile: false,
      showEditLevelTile: true,
      editLevelTileObj: levelTile
    });
  }

  handleShowNewLevelNPCharacter = (column, row) => {
    this.setState({
      showLevel: false,
      showNewLevelNPCharacter: true,
      showEditLevelNPCharacter: false,
      editLevelNPCharacterObj: null,
      selectedColumn: column,
      selectedRow: row
    });
  }

  handleShowEditLevelNPCharacter = (levelNPCharacter) => {
    // console.log('levelNPCharacter in edit level: ', levelNPCharacter);
    this.setState({
      showLevel: false,
      showNewLevelNPCharacter: false,
      showEditLevelNPCharacter: true,
      editLevelNPCharacterObj: levelNPCharacter
    });
  }

  handleShowNewLevelAction = (column, row) => {
    this.setState({
      showLevel: false,
      showNewLevelAction: true,
      showEditLevelAction: false,
      editLevelActionObj: null,
      selectedColumn: column,
      selectedRow: row
    });
  }

  handleShowEditLevelAction = (levelAction) => {
    this.setState({
      showLevel: false,
      showNewLevelAction: false,
      showEditLevelAction: true,
      editLevelActionObj: levelAction
    });
  }

  componentDidMount() {
    const { gameDetail, level } = this.props;
    this.props.getLevelTypes();
    this.props.getLevelTiles(gameDetail.slug, level.slug);
    this.props.getLevelNPCharacters(gameDetail.slug, level.slug);
    this.props.getLevelActions(gameDetail.slug, level);
    this.props.getLevelActionTypes();
    this.setState({
      name: level.name,
      description: level.description,
      image: level.image,
      slug: level.slug,
      order: level.order,
      width: level.width,
      height: level.height,
      tileWidth: level.tile_width,
      tileHeight: level.tile_height,
      startingLevel: level.starting_level,
      startingX: level.starting_x,
      startingY: level.starting_y
    });
    if (level.level_type !== null) {
      this.setState({
        levelTypeSlug: level.level_type.slug
      });
    }
    // if (level.order == 1) {
    //   this.setState({
    //     startingLevel: true
    //   });
    // }
    this.handleBuildTileArrays();
  }

  componentDidUpdate(lastProps) {
    if (this.props.editLevelObj !== null && lastProps.editLevelObj === null) {
      this.props.resetEditLevel();
      this.props.clearGameLevels();
      this.props.getGameLevels(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showLevels();
    }
    if (this.props.deleteLevelObj !== null && lastProps.deleteLevelObj === null) {
      this.props.resetDeleteLevel();
      this.props.clearGameLevels();
      this.props.getGameLevels(this.props.gameDetail.slug);
      this.setState({uploadingData: false});
      this.props.showLevels();
    }
  }

  render() {
    const { level, levelTypes, levelTiles, loadingLevelTiles } = this.props;
    const { name, description, uploadingData, width, height, tileWidth, tileHeight, order } = this.state;
    const { showLevel, showNewLevelTile, showEditLevelTile, editLevelTileObj } = this.state;
    const { tileColumns, tileRows, selectedColumn, selectedRow } = this.state;
    const { showLevelDetails, showLevelTiles } = this.state;
    const { showLevelNPCharacters, showNewLevelNPCharacter, showEditLevelNPCharacter, editLevelNPCharacterObj } = this.state;
    const { levelNPCharacters, loadingLevelNPCharacters, loadingLevelActions, levelActions } = this.props;
    const { showLevelActions, showNewLevelAction, showEditLevelAction, editLevelActionObj } = this.state;
    const { startingLevel, startingX, startingY } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>Edit Level</span>
        </div>
        <div style={{display: 'flex', padding: 10}}>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showLevelDetails ? 
              <div style={{paddingTop: 10}}><span style={styles.defaultText}>Details</span></div> :
              <button style={styles.buttonDefault} onClick={this.handleShowLevelDetails}>Details</button>
            }
          </div>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showLevelTiles ? 
              <div style={{paddingTop: 10}}><span style={styles.defaultText}>Tiles</span></div> :
              <button style={styles.buttonDefault} onClick={this.handleShowLevelTiles}>Tiles</button>
            }
          </div>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showLevelNPCharacters ? 
              <div style={{paddingTop: 10}}><span style={styles.defaultText}>NPCharacters</span></div> :
              <button style={styles.buttonDefault} onClick={this.handleShowLevelNPCharacters}>NPCharacters</button>
            }
          </div>
          <div style={{flex: 1, textAlign: 'center'}}>
            {showLevelActions ? 
              <div style={{paddingTop: 10}}><span style={styles.defaultText}>Actions</span></div> :
              <button style={styles.buttonDefault} onClick={this.handleShowLevelActions}>Actions</button>
            }
          </div>
        </div>
        {showLevelDetails && 
          <div>
            <div>
              <div style={{display: 'flex'}}>
                <div style={{flex: 1}}>
                  <img src={level.thumbnail} style={{height: 100, width: 100}} />
                </div>
                <div style={{flex: 9, paddingLeft: 24}}>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Name:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="name"
                        value={name}
                        placeholder="Name"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Description:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <textarea 
                        style={styles.textareaInput}
                        type="textArea" 
                        name="description" 
                        onChange={this.handleChange} 
                        value={description} 
                        placeholder="Describe your Level"
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Type:</span>
                    </div>
                    <div style={{flex: 3}}>
                      {levelTypes !== null ? 
                        <select 
                          name="levelTypes" 
                          id="levelTypes"
                          style={styles.selectInput}
                          onChange={this.handleSelectChange.bind(this)}
                        >
                          <option value="">...</option>
                          {levelTypes.map((levelType, index) => (
                            <option 
                              key={index} 
                              value={levelType.slug}
                              selected={levelType.slug == level.level_type.slug ? 'selected' : ''}
                            >
                              {levelType.name}
                            </option>
                          ))}
                        </select> :
                        <div>
                          <span style={styles.loadingText}>Loading...</span>
                        </div>
                      }
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <label for="image" style={styles.defaultText}>Image:</label>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        style={styles.fileInput}
                        type="file" 
                        name="image" 
                        onChange={this.handleImageChange}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Map Width:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="width"
                        value={width}
                        placeholder="Width"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Map Height:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="height"
                        value={height}
                        placeholder="Height"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Tile Width:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="tileWidth"
                        value={tileWidth}
                        placeholder="Tile Width"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Tile Height:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="tileHeight"
                        value={tileHeight}
                        placeholder="Tile Height"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Starting X:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="startingX"
                        value={startingX}
                        placeholder="Starting X"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Starting Y:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="startingY"
                        value={startingY}
                        placeholder="Starting Y"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1}}>
                      <span style={styles.defaultText}>Order:</span>
                    </div>
                    <div style={{flex: 3}}>
                      <input 
                        name="order"
                        value={order}
                        placeholder="Order"
                        onChange={this.handleChange}
                        style={styles.textInput}
                      />
                    </div>
                  </div>
                  <div style={{display: 'flex', paddingBottom: 10}}>
                    <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                      <span style={styles.defaultText}>Starting Level:</span>
                    </div>
                    <div style={{flex: 4}}>
                      <input 
                        type="checkbox" 
                        name="startingLevel" 
                        id="startingLevel" 
                        checked={startingLevel} 
                        onChange={this.handleCheckboxChange.bind(this)} 
                      />
                    </div>
                  </div> 
                </div>
              </div> 
            </div>
            {uploadingData ? 
              <div id="uploadProgress" /> :
              <div style={{display: 'flex', padding: 20}}>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.handleEditLevel}>Save</button>
                </div>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.props.showLevels}>Cancel</button>
                </div>
                <div style={{flex: 1, textAlign: 'center'}}>
                  <button style={styles.buttonDefault} onClick={this.handleDelete}>Delete</button>
                </div>
              </div>
            }
          </div>
        }
        {showLevelTiles && 
          <div>
            <div style={{margin: '20px auto', width: `${level.width}px`}}>
              <span style={styles.nameText}>Level Tiles:</span>
            </div>
            {showLevel && 
              <div
                style={{
                  position: 'relative',
                  width: `${level.width}px`,
                  height: `${level.height}px`,
                  margin: '20px auto'
                }}
              >
                <div 
                  style={{
                    position: 'relative',
                    top: '0px',
                    left: '0px',
                    width: `${level.width}px`,
                    height: `${level.height}px`,
                    backgroundColor: 'black',
                    backgroundImage: `url(${level.image})`,
                    backgroundSize: 'auto'
                  }}
                />
                {level !== undefined && level !== null && 
                  <div>
                    {tileRows.map((row, index) => (
                      <div key={index}>
                        {tileColumns.map((column, index) => (
                          <a key={index}  onClick={this.handleShowNewLevelTile.bind(this, column, row)}>
                            <div 
                              style={{
                                position: 'absolute',
                                top: `${row * level.tile_width}px`,
                                left: `${column * level.tile_height}px`,
                                width: `${level.tile_width}px`,
                                height: `${level.tile_height}px`,
                                outline: '1px dotted black',
                                cursor: 'pointer',
                              }}
                            />
                          </a>
                        ))}
                      </div>
                    ))}
                  </div>
                }
                {!loadingLevelTiles ? 
                  <div>
                    {levelTiles.length > 0 ? 
                      <div>
                        {levelTiles.map((levelTile, index) => (
                          <a key={index} onClick={this.handleShowEditLevelTile.bind(this, levelTile)}>
                            <div 
                              style={{
                                position: 'absolute',
                                top: `${levelTile.position_y}px`,
                                left: `${levelTile.position_x}px`,
                                backgroundImage: `url(${levelTile.tile.image})`,
                                backgroundPosition: '0 0',
                                width: `${levelTile.tile.width}px`,
                                height: `${levelTile.tile.height}px`,
                                cursor: 'pointer'
                              }}
                            />
                          </a>
                        ))}
                      </div> :
                      <div>
                        <span style={styles.defaultText}>No Level Tiles...</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span style={styles.defaultText}>Loading Level Tiles...</span>
                  </div>
                }
              </div>
            }
            {showNewLevelTile && 
              <NewLevelTile selectedColumn={selectedColumn} selectedRow={selectedRow} level={level} closeNewLevelTile={this.handleShowLevel} />
            }
            {showEditLevelTile && 
              <EditLevelTile level={level} levelTile={editLevelTileObj} closeEditLevelTile={this.handleShowLevel} />
            }
          </div>
        }
        {showLevelNPCharacters && 
          <div>
            <div style={{margin: '20px auto', width: `${level.width}px`}}>
              <span style={styles.nameText}>Level NPCharacters:</span>
            </div>
            {showLevel && 
              <div
                style={{
                  position: 'relative',
                  width: `${level.width}px`,
                  height: `${level.height}px`,
                  margin: '20px auto'
                }}
              >
                <div 
                  style={{
                    position: 'relative',
                    top: '0px',
                    left: '0px',
                    width: `${level.width}px`,
                    height: `${level.height}px`,
                    backgroundColor: 'black',
                    backgroundImage: `url(${level.image})`,
                    backgroundSize: 'auto'
                  }}
                />
                {level !== undefined && level !== null && 
                  <div>
                    {tileRows.map((row, index) => (
                      <div key={index}>
                        {tileColumns.map((column, index) => (
                          <a key={index}  onClick={this.handleShowNewLevelNPCharacter.bind(this, column, row)}>
                            <div 
                              style={{
                                position: 'absolute',
                                top: `${row * level.tile_width}px`,
                                left: `${column * level.tile_height}px`,
                                width: `${level.tile_width}px`,
                                height: `${level.tile_height}px`,
                                outline: '1px dotted black',
                                cursor: 'pointer',
                              }}
                            />
                          </a>
                        ))}
                      </div>
                    ))}
                  </div>
                }
                {!loadingLevelNPCharacters ? 
                  <div>
                    {levelNPCharacters.length > 0 ? 
                      <div>
                        {levelNPCharacters.map((levelNPCharacter, index) => (
                          <a key={index} onClick={this.handleShowEditLevelNPCharacter.bind(this, levelNPCharacter)}>
                            <div 
                              style={{
                                position: 'absolute',
                                top: `${levelNPCharacter.position_y}px`,
                                left: `${levelNPCharacter.position_x}px`,
                                backgroundImage: `url(${levelNPCharacter.npcharacter.sprite_sheet})`,
                                backgroundPosition: '0 0',
                                width: `${levelNPCharacter.npcharacter.width}px`,
                                height: `${levelNPCharacter.npcharacter.height}px`,
                                cursor: 'pointer'
                              }}
                            />
                          </a>
                        ))}
                      </div> :
                      <div>
                        <span style={styles.defaultText}>No Level NPCharacters...</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span style={styles.defaultText}>Loading Level NPCharacters...</span>
                  </div>
                }
              </div>
            }
            {showNewLevelNPCharacter && 
              <NewLevelNPCharacter selectedColumn={selectedColumn} selectedRow={selectedRow} level={level} closeNewLevelNPCharacter={this.handleShowLevel} />
            }
            {showEditLevelNPCharacter && 
              <EditLevelNPCharacter level={level} levelNPCharacter={editLevelNPCharacterObj} closeEditLevelNPCharacter={this.handleShowLevel} />
            }
          </div>
        }
        {showLevelActions && 
          <div>
            <div style={{margin: '20px auto', width: `${level.width}px`, display: 'flex'}}>
              <div style={{flex: 9, paddingTop: 10}}>
                <span style={styles.nameText}>Level Actions:</span>
              </div>
              <div style={{flex: 1}}>
                {!showNewLevelAction && !showEditLevelAction && <button style={styles.buttonDefault} onClick={this.handleShowNewLevelAction.bind(this, 0, 0)}>New</button>}
              </div>
            </div>
            {showLevel && 
              <div
                style={{
                  position: 'relative',
                  width: `${level.width}px`,
                  height: `${level.height}px`,
                  margin: '20px auto'
                }}
              >
                <div 
                  style={{
                    position: 'relative',
                    top: '0px',
                    left: '0px',
                    width: `${level.width}px`,
                    height: `${level.height}px`,
                    backgroundColor: 'black',
                    backgroundImage: `url(${level.image})`,
                    backgroundSize: 'auto'
                  }}
                />
                {level !== undefined && level !== null && 
                  <div>
                    {tileRows.map((row, index) => (
                      <div key={index}>
                        {tileColumns.map((column, index) => (
                          <a key={index}  onClick={this.handleShowNewLevelAction.bind(this, column, row)}>
                            <div 
                              style={{
                                position: 'absolute',
                                top: `${row * level.tile_width}px`,
                                left: `${column * level.tile_height}px`,
                                width: `${level.tile_width}px`,
                                height: `${level.tile_height}px`,
                                outline: '1px dotted black',
                                cursor: 'pointer',
                              }}
                            />
                          </a>
                        ))}
                      </div>
                    ))}
                  </div>
                }
                {!loadingLevelActions ? 
                  <div>
                    {levelActions.length > 0 ? 
                      <div>
                        {levelActions.map((levelAction, index) => (
                          <div key={index}>
                            {levelAction.level_tile !== null ? 
                              <a onClick={this.handleShowEditLevelAction.bind(this, levelAction)}>
                                <div 
                                  style={{
                                    position: 'absolute',
                                    top: `${levelAction.level_tile.position_y}px`,
                                    left: `${levelAction.level_tile.position_x}px`,
                                    backgroundImage: `url(${levelAction.level_tile.tile.image})`,
                                    backgroundPosition: '0 0',
                                    width: `${levelAction.level_tile.tile.width}px`,
                                    height: `${levelAction.level_tile.tile.height}px`,
                                    cursor: 'pointer'
                                  }}
                                />
                              </a> : 
                              <div style={{padding: 10}}>
                                {levelAction.action_type.slug === 'game-action' ? 
                                  <div>
                                    <button style={styles.buttonDefault} onClick={this.handleShowEditLevelAction.bind(this, levelAction)}>
                                      {levelAction.action_type.name}: {levelAction.game_action_type.name}
                                    </button>
                                  </div> : 
                                  <div>
                                    <button style={styles.buttonDefault} onClick={this.handleShowEditLevelAction.bind(this, levelAction)}>
                                      {levelAction.action_type.name}
                                    </button>
                                  </div>
                                }
                              </div>
                            }
                          </div>
                        ))}
                      </div> :
                      <div>
                        <span style={styles.defaultText}>No Level Actions...</span>
                      </div>
                    }
                  </div> :
                  <div>
                    <span style={styles.defaultText}>Loading Level Actions...</span>
                  </div>
                }
              </div>
            }
            {showNewLevelAction && 
              <NewLevelAction selectedColumn={selectedColumn} selectedRow={selectedRow} level={level} closeNewLevelAction={this.handleShowLevel} />
            }
            {showEditLevelAction && 
              <EditLevelAction level={level} levelAction={editLevelActionObj} closeEditLevelAction={this.handleShowLevel} />
            }
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail,

  editLevelObj: state.game_admin.editLevelObj,
  deleteLevelObj: state.game_admin.deleteLevelObj,

  levelTypes: state.game_admin.levelTypes,

  gameTiles: state.games.gameTiles,
  loadingGameTiles: state.games.loadingGameTiles,

  levelTiles: state.games.levelTiles,
  loadingLevelTiles: state.games.loadingLevelTiles,

  levelNPCharacters: state.games.levelNPCharacters,
  loadingLevelNPCharacters: state.games.loadingLevelNPCharacters,

  levelActions: state.game_admin.levelActions,
  loadingLevelActions: state.game_admin.loadingLevelActions
})

const mapDispatchToProps = {
  editLevel,
  deleteLevel,
  resetEditLevel,
  resetDeleteLevel,
  clearGameLevels,
  getGameLevels,
  getLevelTypes,
  getLevelTiles,
  getLevelNPCharacters,
  getLevelActions, 
  getLevelActionTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLevel)