import React, { Component } from 'react'
import { connect } from 'react-redux'

import EditWeapon from './EditWeapon';
import InlineWeapon from './InlineWeapon';

import NewWeapon from '../new/NewWeapon';

export class EditWeapons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showWeapons: true,
      showEditWeapon: false,
      showNewWeapon: false,
      editWeapon: null
    }
  }

  handleCloseNewWeapon = () => {
    this.setState({
      showWeapons: true,
      showEditWeapon: false,
      showNewWeapon: false,
      editWeapon: null
    })
  }

  handleShowNewWeapon = () => {
    this.setState({
      showWeapons: false,
      showEditWeapon: false,
      showNewWeapon: true,
      editWeapon: null
    })
  }

  handleShowWeapons = () => {
    this.setState({
      showWeapons: true,
      showEditWeapon: false,
      showNewWeapon: false,
      editWeapon: null
    })
  }

  handleShowEditWeapon = (weapon) => {
    this.setState({
      showWeapons: false,
      showEditWeapon: true,
      showNewWeapon: false,
      editWeapon: weapon
    })
  }

  render() {
    const { gameWeapons, loadingGameWeapons } = this.props;
    const { showWeapons, showEditWeapon, showNewWeapon, editWeapon } = this.state;

    return (
      <div>
        <div style={{paddingBottom: 24}}>
          <button onClick={this.handleShowNewWeapon}>New</button>
        </div>
        {showWeapons && 
          <div>
            {!loadingGameWeapons ? 
              <div>
                {gameWeapons.length > 0 ? 
                  <div>
                    {gameWeapons.map((weapon, index) => (
                      <button key={index} style={styles.button} onClick={this.handleShowEditWeapon.bind(this, weapon)}>
                        <InlineWeapon weapon={weapon} />
                      </button>
                    ))}
                  </div> :
                  <div>
                    <span>No Game Weapons</span>
                  </div>
                }
              </div> :
              <div>
                <span>Loading...</span>
              </div>
            }
          </div>
        }
        {showEditWeapon && 
          <EditWeapon showWeapons={this.handleShowWeapons} weapon={editWeapon} />
        }
        {showNewWeapon && 
          <NewWeapon closeNewWeapon={this.handleCloseNewWeapon} />
        }
      </div>
      
    )
  }
}

const mapStateToProps = (state) => ({
  gameWeapons: state.games.gameWeapons,
  loadingGameWeapons: state.games.loadingGameWeapons,
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(EditWeapons)

const styles = {
  button: {
    fontSize: '100%',
    fontFamily: 'inherit',
    border: 0,
    padding: 0
  }
}