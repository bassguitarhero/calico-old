import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../../layout/Styles';

import { getLevelNPCharacters, clearLevelNPCharacters, getGameNPCharacters, clearGameNPCharacters } from '../../../actions/games';
import { editLevelNPCharacter, resetEditLevelNPCharacter, deleteLevelNPCharacter, resetDeleteLevelNPCharacter } from '../../../actions/game_admin';

export class EditLevelNPCharacter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positionX: 0,
      positionY: 0,
      npCharacterSlug: '',
      conversationSlug: '',
      uploadingData: false,
      health: 0,
      hostile: true,
      display: true,
      randomize: false,
      boss: false,
      id: ''
    }
  }

  handleDeleteLevelNPCharacter = () => {
    if (window.confirm("Delete Level NPCharacter?")) {
      this.props.deleteLevelNPCharacter(this.props.gameDetail.slug, this.props.level.slug, this.props.levelNPCharacter.npcharacter.slug, this.props.levelNPCharacter.id);
    }
  }

  handleCheckboxChange = (e) => {
    this.setState({
			[e.target.name]: !this.state[e.target.name]
		});
  }

  handleSelectNPCharacter = (e) => {
    const { gameNPCharacters } = this.props;
    for (var t = 0; t < gameNPCharacters.length; t++) {
      if (gameNPCharacters[t].id === e.target.value) {
        this.setState({
          npCharacterSlug: gameNPCharacters[t].slug,
          health: gameNPCharacters[t].health,
          hostile: gameNPCharacters[t].hostile
        })
      }
    }
  }

  handleEditLevelNPCharacter = () => {
    const { gameDetail, level } = this.props;
    const { positionX, positionY, npCharacterSlug, health, hostile, display, randomize, boss, conversationSlug, id } = this.state;
    const positionData = { positionX, positionY }
    if (npCharacterSlug !== '') {
      if (conversationSlug != '') {
        if (boss == false && hostile == false) {
          if (window.confirm("Edit Level NPCharacter?")) {
            this.props.editLevelNPCharacter(gameDetail.slug, level.slug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug);
            this.setState({uploadingData: true});
          }
        } else {
          alert('Conversation NPCharacters cannot be hostile or bosses.');
        }
      } else if (boss == true) {
        if (hostile == true) {
         if (window.confirm("Edit Level NPCharacter?")) {
            this.props.editLevelNPCharacter(gameDetail.slug, level.slug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug);
            this.setState({uploadingData: true});
          }
        } else {
          alert('Bosses must be hostile');
        }
      } else {
        if (window.confirm("Edit Level NPCharacter?")) {
          this.props.editLevelNPCharacter(gameDetail.slug, level.slug, npCharacterSlug, positionData, health, hostile, display, randomize, boss, conversationSlug);
          this.setState({uploadingData: true});
        }
      }
		} else {
			alert("Level NPCharacters must have an NPCharacter.");
		}
  }

  handleSelectConversation = (e) => {
    this.setState({
      conversationSlug: e.target.value
    });
  }

  handleSelectChange = (e) => {
    this.setState({
      tileSlug: e.target.value
    });
  }

  handleChange = (e) => {
    e.preventDefault()
  	this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    const { levelNPCharacter } = this.props;
    console.log('levelNPCharacter: ', levelNPCharacter);
    this.setState({
      id: levelNPCharacter.id, 
      positionX: levelNPCharacter.position_x,
      positionY: levelNPCharacter.position_y,
      npCharacterSlug: levelNPCharacter.npcharacter.slug,
      health: levelNPCharacter.health !== 0 ? levelNPCharacter.health : levelNPCharacter.npcharacter.health,
      display: levelNPCharacter.display,
      randomize: levelNPCharacter.randomize,
      hostile: levelNPCharacter.hostile !== null ? levelNPCharacter.hostile : levelNPCharacter.npcharacter.hostile,
      conversationSlug: levelNPCharacter.conversation !== null ? levelNPCharacter.conversation.slug : ''
    });
  }

  componentDidUpdate(lastProps) {
    if (this.props.editLevelNPCharacterObj !== null && lastProps.editLevelNPCharacterObj === null) {
      this.props.resetEditLevelNPCharacter();
      this.props.clearLevelNPCharacters();
      this.props.getLevelNPCharacters(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeEditLevelNPCharacter();
    }
    if (this.props.deleteLevelNPCharacterObj !== null && lastProps.deleteLevelNPCharacterObj === null) {
      this.props.resetDeleteLevelNPCharacter();
      this.props.getLevelNPCharacters();
      this.props.getLevelTiles(this.props.gameDetail.slug, this.props.level.slug);
      this.setState({uploadingData: false});
      this.props.closeEditLevelNPCharacter();
    }
  }

  render() {
    const { level, gameNPCharacters, loadingGameNPCharacters, levelNPCharacter, gameConversations, loadingGameConversations } = this.props;
    const { positionX, positionY, uploadingData, health, hostile, display, randomize, boss, conversationSlug } = this.state;

    return (
      <div style={{margin: '20px auto', width: `${level.width}px`}}>
        <div style={{paddingBottom: 10}}>
          <span style={styles.nameText}>Edit Level NPCharacter:</span>
        </div>
        <div style={{paddingBottom: 25, display: 'flex'}}>
          <div style={{flex: 1}}>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position X:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionX"
                  value={positionX}
                  placeholder="Position X"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Position Y:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="positionY"
                  value={positionY}
                  placeholder="Position Y"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Randomize X/Y:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="randomize" 
                  id="randomize" 
                  checked={randomize} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
          </div>
          <div style={{flex: 1}}>
            {!loadingGameNPCharacters ? 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>NPCharacter:</span>
                </div>
                <div style={{flex: 3}}>
                  {gameNPCharacters !== null ? 
                    <select 
                      name="gameNPCharacters" 
                      id="gameNPCharacters"
                      style={styles.selectInput}
                      onChange={this.handleSelectNPCharacter.bind(this)}
                    >
                      <option value="">Select an NPCharacter</option>
                      {gameNPCharacters.map((gameNPCharacter, index) => (
                        <option 
                          key={index} 
                          value={gameNPCharacter.id}
                          selected={gameNPCharacter.slug == levelNPCharacter.npcharacter.slug ? 'selected' : ''}
                        >
                          {gameNPCharacter.name}
                        </option>
                      ))}
                    </select> :
                    <div>
                      <span style={styles.defaultText}>Loading...</span>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game NPCharacters...</span>
              </div>
            }
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1}}>
                <span style={styles.defaultText}>Health:</span>
              </div>
              <div style={{flex: 3}}>
                <input 
                  name="health"
                  value={health}
                  placeholder="Health"
                  style={styles.textInput}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Hostile:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="hostile" 
                  id="hostile" 
                  checked={hostile} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Display:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="display" 
                  id="display" 
                  checked={display} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            <div style={{display: 'flex', paddingBottom: 10}}>
              <div style={{flex: 1, textAlign: 'right', paddingRight: 10}}>
                <span style={styles.defaultText}>Boss:</span>
              </div>
              <div style={{flex: 1}}>
                <input 
                  type="checkbox" 
                  name="boss" 
                  id="boss" 
                  checked={boss} 
                  onChange={this.handleCheckboxChange.bind(this)} 
                />
              </div>
            </div> 
            {!loadingGameConversations ? 
              <div style={{display: 'flex', paddingBottom: 10}}>
                <div style={{flex: 1}}>
                  <span style={styles.defaultText}>Conversation:</span>
                </div>
                <div style={{flex: 3}}>
                  {gameConversations !== null ? 
                    <select 
                      name="gameConversations" 
                      id="gameConversations"
                      style={styles.selectInput}
                      onChange={this.handleSelectConversation.bind(this)}
                    >
                      <option value="">Select a Conversation</option>
                      {gameConversations.map((gameConversation, index) => (
                        <option 
                          key={index} 
                          value={gameConversation.slug}
                          selected={(conversationSlug === gameConversation.slug) ? 'selected' : ''}
                        >
                          {gameConversation.name}
                        </option>
                      ))}
                    </select> :
                    <div>
                      <span style={styles.defaultText}>Loading...</span>
                    </div>
                  }
                </div>
              </div> :
              <div>
                <span style={styles.defaultText}>Loading Game NPCharacters...</span>
              </div>
            }
          </div>
        </div>
        {uploadingData ? 
          <div id="uploadProgress" /> :
          <div style={{display: 'flex'}}>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleEditLevelNPCharacter}>Save</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.props.closeEditLevelNPCharacter}>Cancel</button>
            </div>
            <div style={{flex: 1, textAlign: 'center'}}>
              <button style={styles.buttonDefault} onClick={this.handleDeleteLevelNPCharacter}>Delete</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  gameDetail: state.games.gameDetail, 
  gameConversations: state.games.gameConversations,
  loadingGameConversations: state.games.loadingGameConversations,

  gameNPCharacters: state.games.gameNPCharacters,
  loadingGameNPCharacters: state.games.loadingGameNPCharacters,

  editLevelNPCharacterObj: state.game_admin.editLevelNPCharacterObj,
  deleteLevelNPCharacterObj: state.game_admin.deleteLevelNPCharacterObj 
})

const mapDispatchToProps = {
  getGameNPCharacters, clearGameNPCharacters, getLevelNPCharacters, clearLevelNPCharacters, editLevelNPCharacter, resetEditLevelNPCharacter, deleteLevelNPCharacter, resetDeleteLevelNPCharacter
}

export default connect(mapStateToProps, mapDispatchToProps)(EditLevelNPCharacter)
