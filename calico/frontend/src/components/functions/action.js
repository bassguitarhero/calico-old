import store from '../../store';
import { CHANGE_PLAYER_ATTACK_POSITION } from '../../actions/types';

export default function handleAction(key, characterWidth, characterHeight, levelWidth, levelHeight, tileWidth, tileHeight, movementGrid) {
  const state = store.getState();
  // console.log('Key: ', key);
  function checkAttackPosition(key) {
    const playerPos = store.getState().player.playerPosition;
    switch(key) {
      case 'LEFT':
        if (playerPos[0]-characterWidth >= 0) {
          return movementGrid[playerPos[1] !== 0 ? playerPos[1]/tileHeight : 0].row[(playerPos[0]-characterWidth)/tileWidth]
        } else {
          return null;
        }
      case 'RIGHT':
        if (playerPos[0]+characterWidth <= levelWidth - 1) {
          return movementGrid[playerPos[1] !== 0 ? playerPos[1]/tileHeight : 0].row[(playerPos[0]+characterWidth)/tileWidth]
        } else {
          return null;
        }
      case 'UP':
        if (playerPos[1]-characterHeight >= 0) {
          return movementGrid[(playerPos[1]-characterHeight)/tileHeight].row[playerPos[0] !== 0 ? playerPos[0]/tileWidth : 0]
        } else {
          return null;
        }
      case 'DOWN':
        if (playerPos[1]+characterHeight <= levelHeight - 1) {
          return movementGrid[(playerPos[1]+characterHeight)/tileHeight].row[playerPos[0] !== 0 ? playerPos[0]/tileWidth : 0]
        } else {
          return null;
        }
    }
  }

  // function getActionPosition(key) {
  //   const playerPos = store.getState().player.playerPosition
  //   switch(key) {
  //     case 'RIGHT':
  //       return [ playerPos[0]-characterWidth, playerPos[1] ]
  //     case 'LEFT':
  //       return [ playerPos[0]+characterWidth, playerPos[1] ]
  //     case 'UP':
  //       return [ playerPos[0], playerPos[1]-characterHeight ]
  //     case 'DOWN':
  //       return [ playerPos[0], playerPos[1]+characterHeight ]
  //   }
  // }

  function getAttackPosition(key) {
    const playerPos = store.getState().player.playerPosition
    switch(key) {
      case 'RIGHT':
        return [ playerPos[0]+characterWidth, playerPos[1] ]
      case 'LEFT':
        return [ playerPos[0]-characterWidth, playerPos[1] ]
      case 'UP':
        return [ playerPos[0], playerPos[1]-characterHeight ]
      case 'DOWN':
        return [ playerPos[0], playerPos[1]+characterHeight ]
    }
  }

  function getSpriteAttackPosition(key, attackIndex) {
    switch(key) {
      // top, then left
      case 'DOWN':
        return [ `${characterHeight * 0}`, `${characterWidth * attackIndex}` ]
      case 'UP':
        return [ `${characterHeight * 1}`, `${characterWidth * attackIndex}` ]
      case 'LEFT':
        return [ `${characterHeight * 2}`, `${characterWidth * attackIndex}` ]
      case 'RIGHT':
        return [ `${characterHeight * 3}`, `${characterWidth * attackIndex}` ]
    }
  }

  function getAttackIndex() {
    // console.log('Getting Attack Index');
    const attackIndex = store.getState().player.attackIndex;
    return attackIndex >= 7 ? 0 : attackIndex + 1;
  }

  function dispatchAction(key) {
    switch(key) {
      case 'E':
        return console.log('E');
      case 'LEFT CTRL':
        return console.log('LEFT CTRL');
      case 'UP':
      case 'LEFT':
      case 'RIGHT':
      case 'DOWN':
        var newAttackPosition = checkAttackPosition(key);
        const attackIndex = getAttackIndex();
        // console.log('New Grid Position: ', newGridPosition);
        // console.log('Key: ', key);
        if (newAttackPosition !== null) {
          switch(newAttackPosition) {
            // WALKABLE
            case 0:
              return store.dispatch({
                type: CHANGE_PLAYER_ATTACK_POSITION,
                payload: {
                  playerAttackPosition: getAttackPosition(key),
                  attackKey: key,
                  attackIndex,
                  playerSpriteAttackPosition: getSpriteAttackPosition(key, attackIndex)
                }
              });
            // NOT WALKABLE
            case 2:
              return store.dispatch({
                type: CHANGE_PLAYER_ATTACK_POSITION,
                payload: {
                  playerAttackPosition: getAttackPosition(key),
                  attackKey: key,
                  attackIndex,
                  playerSpriteAttackPosition: getSpriteAttackPosition(key, attackIndex)
                }
              });
            // NP CHARACTER
            case 5:
              return store.dispatch({
                type: CHANGE_PLAYER_ATTACK_POSITION,
                payload: {
                  playerAttackPosition: getAttackPosition(key),
                  attackKey: key,
                  attackIndex,
                  playerSpriteAttackPosition: getSpriteAttackPosition(key, attackIndex)
                }
              });
            // ACTION
            case 9:
              return store.dispatch({
                type: CHANGE_PLAYER_ATTACK_POSITION,
                payload: {
                  playerAttackPosition: getAttackPosition(key),
                  attackKey: key,
                  attackIndex,
                  playerSpriteAttackPosition: getSpriteAttackPosition(key, attackIndex)
                }
              });
          } 
        } else {
          return store.dispatch({
            type: CHANGE_PLAYER_ATTACK_POSITION,
            payload: {
              playerAttackPosition: getAttackPosition(key),
              attackKey: key,
              attackIndex,
              playerSpriteAttackPosition: getSpriteAttackPosition(key, attackIndex)
            }
          });
        }
    }
  }

  dispatchAction(key);
}