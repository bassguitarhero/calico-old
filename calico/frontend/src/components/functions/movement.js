import store from '../../store';
import { CHANGE_PLAYER_POSITION, TAKE_ACTION, TAKE_DAMAGE, INITIATE_CONVERSATION } from '../../actions/types';

export default function handleMovement(direction, characterWidth, characterHeight, levelWidth, levelHeight, tileWidth, tileHeight, movementGrid, canWalk, canFly, canClimb, npcharacterGrid) {
  const state = store.getState();

  function checkNewPosition(direction) {
    const oldPos = store.getState().player.playerPosition
    switch(direction) {
      case 'WEST':
        if (oldPos[0]-characterWidth >= 0) {
          return movementGrid[oldPos[1] !== 0 ? oldPos[1]/tileHeight : 0].row[(oldPos[0]-characterWidth)/tileWidth]
        } else {
          return null;
        }
      case 'EAST':
        if (oldPos[0]+characterWidth <= levelWidth - 1) {
          return movementGrid[oldPos[1] !== 0 ? oldPos[1]/tileHeight : 0].row[(oldPos[0]+characterWidth)/tileWidth]
        } else {
          return null;
        }
      case 'NORTH':
        if (oldPos[1]-characterHeight >= 0) {
          return movementGrid[(oldPos[1]-characterHeight)/tileHeight].row[oldPos[0] !== 0 ? oldPos[0]/tileWidth : 0]
        } else {
          return null;
        }
      case 'SOUTH':
        if (oldPos[1]+characterHeight <= levelHeight - 1) {
          return movementGrid[(oldPos[1]+characterHeight)/tileHeight].row[oldPos[0] !== 0 ? oldPos[0]/tileWidth : 0]
        } else {
          return null;
        }
    }
  }

  function checkNPCharacterPosition(direction) {
    const oldPos = store.getState().player.playerPosition
    switch(direction) {
      case 'WEST':
        if (oldPos[0]-characterWidth >= 0) {
          return npcharacterGrid[oldPos[1] !== 0 ? oldPos[1]/tileHeight : 0].row[(oldPos[0]-characterWidth)/tileWidth]
        } else {
          return null;
        }
      case 'EAST':
        if (oldPos[0]+characterWidth <= levelWidth - 1) {
          return npcharacterGrid[oldPos[1] !== 0 ? oldPos[1]/tileHeight : 0].row[(oldPos[0]+characterWidth)/tileWidth]
        } else {
          return null;
        }
      case 'NORTH':
        if (oldPos[1]-characterHeight >= 0) {
          return npcharacterGrid[(oldPos[1]-characterHeight)/tileHeight].row[oldPos[0] !== 0 ? oldPos[0]/tileWidth : 0]
        } else {
          return null;
        }
      case 'SOUTH':
        if (oldPos[1]+characterHeight <= levelHeight - 1) {
          return npcharacterGrid[(oldPos[1]+characterHeight)/tileHeight].row[oldPos[0] !== 0 ? oldPos[0]/tileWidth : 0]
        } else {
          return null;
        }
    }
  }

  function getActionPosition(direction) {
    const oldPos = store.getState().player.playerPosition
    switch(direction) {
      case 'WEST':
        return [ oldPos[0]-characterWidth, oldPos[1] ]
      case 'EAST':
        return [ oldPos[0]+characterWidth, oldPos[1] ]
      case 'NORTH':
        return [ oldPos[0], oldPos[1]-characterHeight ]
      case 'SOUTH':
        return [ oldPos[0], oldPos[1]+characterHeight ]
    }
  }

  function getNewPosition(direction) {
    const oldPos = store.getState().player.playerPosition
    switch(direction) {
      case 'WEST':
        return [ oldPos[0]-characterWidth, oldPos[1] ]
      case 'EAST':
        return [ oldPos[0]+characterWidth, oldPos[1] ]
      case 'NORTH':
        return [ oldPos[0], oldPos[1]-characterHeight ]
      case 'SOUTH':
        return [ oldPos[0], oldPos[1]+characterHeight ]
    }
  }

  function getSpritePosition(direction, walkIndex) {
    switch(direction) {
      // top, then left
      case 'SOUTH':
        return [ `${characterHeight * 0}`, `${characterWidth * walkIndex}` ]
      case 'NORTH':
        return [ `${characterHeight * 1}`, `${characterWidth * walkIndex}` ]
      case 'EAST':
        return [ `${characterHeight * 2}`, `${characterWidth * walkIndex}` ]
      case 'WEST':
        return [ `${characterHeight * 3}`, `${characterWidth * walkIndex}` ]
    }
  }

  function getWalkIndex() {
    const walkIndex = store.getState().player.walkIndex;
    return walkIndex >= 7 ? 0 : walkIndex + 1;
  }

  function dispatchMove(direction) {
    const oldPos = store.getState().player.playerPosition;
    var newGridPosition = checkNewPosition(direction);
    var newNPCharacterPosition = checkNPCharacterPosition(direction);
    const walkIndex = getWalkIndex();
    if (newGridPosition !== null && (newNPCharacterPosition !== 5 && newNPCharacterPosition !== 9)) {
      switch(newGridPosition) {
        // WALKABLE
        case 0:
          return store.dispatch({
            type: CHANGE_PLAYER_POSITION,
            payload: {
              position: getNewPosition(direction),
              direction,
              walkIndex,
              playerSpritePosition: getSpritePosition(direction, walkIndex)
            }
          });
        // NOT WALKABLE
        case 2:
          return store.dispatch({
            type: CHANGE_PLAYER_POSITION,
            payload: {
              position: [ oldPos[0], oldPos[1] ],
              direction,
              walkIndex,
              playerSpritePosition: getSpritePosition(direction, walkIndex)
            }
          });
        // ACTION
        case 9:
          return store.dispatch({
            type: TAKE_ACTION,
            payload: {
              position: getActionPosition(direction),
              direction,
              walkIndex,
              playerSpritePosition: getSpritePosition(direction, walkIndex)
            }
          });
      } 
    } else if (newGridPosition !== null && newNPCharacterPosition === 5) {
      // NPCHARACTER IS NOT WALKABLE FOR NOW
      return store.dispatch({
        type: TAKE_DAMAGE,
        payload: {
          playerPosition: [ oldPos[0], oldPos[1] ],
          damagePosition: getNewPosition(direction),
          direction,
          walkIndex,
          playerSpritePosition: getSpritePosition(direction, walkIndex)
        }
      });
    } else if (newGridPosition !== null && newNPCharacterPosition == 9) {
      // NPCHARACTER HAS CONVERSATION
      return store.dispatch({
        type: INITIATE_CONVERSATION,
        payload: {
          playerPosition: [ oldPos[0], oldPos[1] ],
          conversationPosition: getNewPosition(direction),
          direction,
          walkIndex,
          playerSpritePosition: getSpritePosition(direction, walkIndex),
          initiateConversation: true
        }
      });
    } else {
      return store.dispatch({
        type: CHANGE_PLAYER_POSITION,
        payload: {
          position: [ oldPos[0], oldPos[1] ],
          direction,
          walkIndex,
          playerSpritePosition: getSpritePosition(direction, walkIndex)
        }
      });
    }
  }

  dispatchMove(direction);
}