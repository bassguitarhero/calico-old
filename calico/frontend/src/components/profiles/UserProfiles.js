import React, { Component } from 'react'
import { connect } from 'react-redux'

import UserProfile from './UserProfile';

export class UserProfiles extends Component {
  render() {
    return (
      <div>
        <UserProfile profileSlug={this.props.profileSlug} />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfiles)
