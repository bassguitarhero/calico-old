import React, { Component } from 'react'
import { connect } from 'react-redux'

import { styles } from '../layout/Styles';

import { getUserProfile, resetUserProfile } from '../../actions/profiles';

export class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  componentDidMount() {
    this.props.getUserProfile(this.props.profileSlug);
  }

  componentDidUpdate(lastProps) {

  }

  render() {
    const { profileDetail, loadingProfile } = this.props;

    return (
      <div>
        <div style={styles.titleContainer}>
          <span style={styles.titleText}>User Profile</span>
        </div>
        {!loadingProfile ? 
          <div style={styles.loadingTextContainer}>
            <span style={styles.loadingText}>Loaded...</span>
            <span>{profileDetail.user.username}</span>
          </div> :
          <div style={styles.loadingTextContainer}>
            <span style={styles.loadingText}>Loading...</span>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  profileDetail: state.profiles.profileDetail,
  loadingProfile: state.profiles.loadingProfile
})

const mapDispatchToProps = {
  getUserProfile, 
  resetUserProfile
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)
